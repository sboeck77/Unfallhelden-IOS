//
//  UserCurrentLocation.h
//  iOne
//
//  Created by adilanwer on 8/7/14.
//  Copyright (c) 2014 Tapcrowd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnfallheldenLC.h"

@interface UserCurrentLocation : NSObject<UnfallheldenLCDelegate>

@property (nonatomic,strong) UnfallheldenLC *locationController;
@property (nonatomic,strong) UnfallheldenUtilities *utilities;

+(UserCurrentLocation *)sharedSingletonInstance;

-(void)loadUserLocation;

@end
