#import "UnfallheldenLC.h"

@implementation UnfallheldenLC

@synthesize locationManager;
@synthesize delegate;

- (id) init {
    self = [super init];
    if (self != nil) {
        self.locationManager = [[CLLocationManager alloc] init];
        //		self.locationManager.delegate = self; // send loc updates to myself
    }
    return self;
}

-(void)clControllerStartUpdatingLocation
{
    
    if( [CLLocationManager locationServicesEnabled])
    {
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.delegate = self;
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
            [self.locationManager requestAlwaysAuthorization];
        }
        [self.locationManager startUpdatingLocation];
        
        self.locationManager.distanceFilter = 1000.0f;
    }
    
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"GPS abgeschaltet" message:@"Um Ihre aktuelle Position bestimmen zu können, müssen Sie die Ortungsdienste aktivieren" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    [self.delegate locationUpdate:newLocation];
}


- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    [self.delegate locationError:error];
}

- (void)dealloc {
    self.locationManager = nil;
}

@end
