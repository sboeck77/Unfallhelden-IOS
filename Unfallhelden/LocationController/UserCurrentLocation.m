//
//  UserCurrentLocation.m
//  iOne
//
//  Created by adilanwer on 8/7/14.
//  Copyright (c) 2014 Tapcrowd. All rights reserved.
//

#import "UserCurrentLocation.h"

@implementation UserCurrentLocation
@synthesize locationController,utilities;

+(UserCurrentLocation *)sharedSingletonInstance
{
    static UserCurrentLocation *sharedSingleton;
    @synchronized(self){
        if(!sharedSingleton){
            sharedSingleton = [[UserCurrentLocation alloc] init];
        }
    }
    return sharedSingleton;
}

-(void)loadUserLocation
{
    self.locationController = [[UnfallheldenLC alloc] init];
    self.locationController.delegate = self;
    [self.locationController clControllerStartUpdatingLocation];
    self.utilities = [UnfallheldenUtilities sharedSingletonInstance];
}

-(void)locationUpdate:(CLLocation *)location
{
    if(self.utilities.userlatitude != location.coordinate.latitude && self.utilities.userlongitude != location.coordinate.longitude){
        
        self.utilities.userlatitude = location.coordinate.latitude;
        self.utilities.userlongitude  = location.coordinate.longitude;
        
        NSString *latitude = [NSString stringWithFormat:@"%f",self.utilities.userlatitude];
        NSString *longitude = [NSString stringWithFormat:@"%f",self.utilities.userlongitude];
        
        [[PDKeychainBindings sharedKeychainBindings] setObject:latitude forKey:kKeychainUserLatitude];
        [[PDKeychainBindings sharedKeychainBindings] setObject:longitude forKey:kKeychainUserLongitude];
    }
    
    NSLog(@"Updated Location : lat--%f long--%f",self.utilities.userlatitude ,self.utilities.userlongitude);
}

-(void)locationError:(NSError *)error
{
    
}

@end
