//
//  CustomizeNavbar.h
//  TrafficAlarm
//
//  Created by adilanwer on 4/2/14.
//  Copyright (c) 2014 tapcrowd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CustomizeNavbar : NSObject

+(CustomizeNavbar*)sharedSingletonInstance;

-(void)setBackgroundImageForNavBar:(UINavigationBar*)navigationBar withImage:(UIImage *)imageNamed;
-(void)setBackButtonForNavBarWithoutTitleWithNavigationItem:(UINavigationItem*)navigationItem;
-(UIButton*)addLeftNavBarButtonForNavigationItem:(UINavigationItem*)navigationItem WithTitle:(NSString *)title andImage:(UIImage *)image;
-(UIButton*)addRightNavBarButtonForNavigationItem:(UINavigationItem*)navigationItem WithTitle:(NSString *)title andImage:(UIImage *)image;

@end
