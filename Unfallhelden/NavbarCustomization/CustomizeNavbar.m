//
//  CustomizeNavbar.m
//  TrafficAlarm
//
//  Created by adilanwer on 4/2/14.
//  Copyright (c) 2014 tapcrowd. All rights reserved.
//

#import "CustomizeNavbar.h"

@implementation CustomizeNavbar

+(CustomizeNavbar*)sharedSingletonInstance
{
    static CustomizeNavbar *sharedSingleton;
    
    @synchronized(self) {
		if(!sharedSingleton) {
			sharedSingleton = [[CustomizeNavbar alloc] init];
		}
	}
	
	return sharedSingleton;
}

-(void)setBackgroundImageForNavBar:(UINavigationBar*)navigationBar withImage:(UIImage *)imageNamed
{
    if ([navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
//        UIImage *image = [UIImage imageNamed:imageNamed];
        
//        [navigationBar setBackgroundImage:imageNamed forBarMetrics:UIBarMetricsDefault];
        
        [navigationBar setBackgroundImage:[imageNamed
                                           resizableImageWithCapInsets:UIEdgeInsetsMake(20, 0, 0, 0) resizingMode:UIImageResizingModeStretch] forBarMetrics:UIBarMetricsDefault];
//        navigationBar.clipsToBounds = NO;
        [navigationBar setContentMode:UIViewContentModeBottom];
    }
}

-(void)setBackButtonForNavBarWithoutTitleWithNavigationItem:(UINavigationItem *)navigationItem
{
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        navigationItem.backBarButtonItem=[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    }
}

-(UIButton*)addLeftNavBarButtonForNavigationItem:(UINavigationItem*)navigationItem WithTitle:(NSString *)title andImage:(UIImage *)image
{
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if(image){
        [aButton setBackgroundImage:image forState:UIControlStateNormal];
        aButton.frame = CGRectMake(0.0, 0.0, image.size.width,   image.size.height);
    }
    else{
         aButton.frame = CGRectMake(0.0, 0.0, 80, 29);
    }
    NSLog(@"Title: %@", title);
    [aButton setTitle:title forState:UIControlStateNormal];
    aButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    aButton.contentEdgeInsets = UIEdgeInsetsMake(0.0, -9.0f, 0.0, 0.0);
    aButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//    [[TrafficAlarmUtilities sharedSingletonInstance] setLatoFontForUILable:aButton.titleLabel withFontSize:16];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    [navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,barButton, nil] animated:NO];
//    [barButton setTitle:title];
//    [navigationItem setLeftBarButtonItem:barButton];
    return aButton;
}

-(UIButton*)addRightNavBarButtonForNavigationItem:(UINavigationItem*)navigationItem WithTitle:(NSString *)title andImage:(UIImage *)image
{
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if(image){
        [aButton setBackgroundImage:image forState:UIControlStateNormal];
        aButton.frame = CGRectMake(0.0, 0.0, image.size.width,   image.size.height);
    }
    else{
        aButton.frame = CGRectMake(0.0, 0.0, 80, 29);
    }
    
    aButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [aButton setTitle:title forState:UIControlStateNormal];
    [aButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [[TrafficAlarmUtilities sharedSingletonInstance] setLatoFontForUILable:aButton.titleLabel withFontSize:16];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
//    [barButton setTitle:title];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    [navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, barButton, nil] animated:NO];
//    [navigationItem setRightBarButtonItem:barButton];
    return aButton;
}

@end
