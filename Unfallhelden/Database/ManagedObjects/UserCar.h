//
//  UserCar.h
//  Unfallhelden
//
//  Created by Adil Anwer on 4/14/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UserCar : NSManagedObject

@property (nonatomic, retain) NSString * carId;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * insurerId;
@property (nonatomic, retain) NSString * leasingCar;
@property (nonatomic, retain) NSString * leasingCompany;
@property (nonatomic, retain) NSString * companyCar;
@property (nonatomic, retain) NSString * companyName;
@property (nonatomic, retain) NSString * regTicketPhoto;
@property (nonatomic, retain) NSString * issueDate;
@property (nonatomic, retain) NSString * vin;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * tradeName;
@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSString * displacement;
@property (nonatomic, retain) NSString * fuelType;
@property (nonatomic, retain) NSString * color;
@property (nonatomic, retain) NSString * extras;
@property (nonatomic, retain) NSString * powerKw;
@property (nonatomic, retain) NSString * carDeleted;
@property (nonatomic, retain) NSString * dateTime;
@property (nonatomic, retain) NSString * registrationNum;
@property (nonatomic, retain) NSString * brand;
@property (nonatomic, retain) NSString * firstReg;
@property (nonatomic, retain) NSString * regDate;
@property (nonatomic, retain) NSString * tireDimFront;
@property (nonatomic, retain) NSString * tireDimBack;

@end
