//
//  UserProfile.h
//  Unfallhelden
//
//  Created by Adil Anwer on 5/18/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UserProfile : NSManagedObject

@property (nonatomic, retain) NSString * birthdate;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * company;
@property (nonatomic, retain) NSString * dateTime;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * family;
@property (nonatomic, retain) NSString * fbId;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSString * lastLoginDateTime;
@property (nonatomic, retain) NSString * lastLoginIp;
@property (nonatomic, retain) NSString * mobile;
@property (nonatomic, retain) NSString * mobile2;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * phone2;
@property (nonatomic, retain) NSString * postCode;
@property (nonatomic, retain) NSString * streetName;
@property (nonatomic, retain) NSString * streetNum;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * country;

@end
