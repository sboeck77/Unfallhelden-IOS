//
//  UserProfile.m
//  Unfallhelden
//
//  Created by Adil Anwer on 5/18/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "UserProfile.h"


@implementation UserProfile

@dynamic birthdate;
@dynamic city;
@dynamic company;
@dynamic dateTime;
@dynamic email;
@dynamic family;
@dynamic fbId;
@dynamic gender;
@dynamic lastLoginDateTime;
@dynamic lastLoginIp;
@dynamic mobile;
@dynamic mobile2;
@dynamic name;
@dynamic phone;
@dynamic phone2;
@dynamic postCode;
@dynamic streetName;
@dynamic streetNum;
@dynamic userId;
@dynamic country;

@end
