//
//  UserCar.m
//  Unfallhelden
//
//  Created by Adil Anwer on 4/14/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "UserCar.h"


@implementation UserCar

@dynamic carId;
@dynamic userId;
@dynamic insurerId;
@dynamic leasingCar;
@dynamic leasingCompany;
@dynamic companyCar;
@dynamic companyName;
@dynamic regTicketPhoto;
@dynamic issueDate;
@dynamic vin;
@dynamic type;
@dynamic tradeName;
@dynamic body;
@dynamic displacement;
@dynamic fuelType;
@dynamic color;
@dynamic extras;
@dynamic powerKw;
@dynamic carDeleted;
@dynamic dateTime;
@dynamic registrationNum;
@dynamic brand;
@dynamic firstReg;
@dynamic regDate;
@dynamic tireDimFront;
@dynamic tireDimBack;

@end
