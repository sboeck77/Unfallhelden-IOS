//
//  UserProfile+DAO.m
//  Unfallhelden
//
//  Created by Adil Anwer on 4/15/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "UserProfile+DAO.h"

@implementation UserProfile (DAO)

#pragma mark - Fetching

+(NSArray*)getUserProfile {
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserProfile" inManagedObjectContext:[[CoreDataSingleTon sharedCoreDataSingleTon] managedObjectContext]];
    [fetchRequest setEntity:entity];
    return [[[CoreDataSingleTon sharedCoreDataSingleTon] managedObjectContext] executeFetchRequest:fetchRequest error:&error];
}

+(UserProfile *)saveUserProfileWithUserId:(NSString *)userId fbId:(NSString *)fbId gender:(NSString *)gender email:(NSString *)email name:(NSString *)name family:(NSString *)family company:(NSString *)company birthdate:(NSString *)birthdate streetName:(NSString *)streetName streetNum:(NSString *)streetNum postCode:(NSString *)postCode city:(NSString *)city phone:(NSString *)phone mobile:(NSString *)mobile dateTime:(NSString *)dateTime lastLoginDateTime:(NSString *)lastLoginDateTime lastLoginIp:(NSString *)lastLoginIp phone2:(NSString*)phone2 mobile2:(NSString*)mobile2 country:(NSString*)country;
{
    NSManagedObjectContext *context = [[CoreDataSingleTon sharedCoreDataSingleTon] managedObjectContext];
    UserProfile *profile = [NSEntityDescription insertNewObjectForEntityForName:@"UserProfile" inManagedObjectContext:context];
    
    profile.userId = userId;
    profile.fbId = fbId;
    profile.gender = gender;
    profile.email = email;
    profile.name = name;
    profile.family = family;
    profile.birthdate = birthdate;
    profile.streetName = streetName;
    profile.streetNum = streetNum;
    profile.postCode = postCode;
    profile.city = city;
    profile.phone = phone;
    profile.mobile = mobile;
    profile.dateTime = dateTime;
    profile.lastLoginDateTime = lastLoginDateTime;
    profile.lastLoginIp = lastLoginIp;
    profile.company = company;
    profile.mobile2 = mobile2;
    profile.phone2 = phone2;
    profile.country = country;
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    return profile;
}

+(UserProfile*)updateUserProfileWithProfile:(UserProfile*)profile userId:(NSString *)userId fbId:(NSString *)fbId gender:(NSString *)gender email:(NSString *)email name:(NSString *)name family:(NSString *)family company:(NSString *)company birthdate:(NSString *)birthdate streetName:(NSString *)streetName streetNum:(NSString *)streetNum postCode:(NSString *)postCode city:(NSString *)city phone:(NSString *)phone mobile:(NSString *)mobile dateTime:(NSString *)dateTime lastLoginDateTime:(NSString *)lastLoginDateTime lastLoginIp:(NSString *)lastLoginIp phone2:(NSString*)phone2 mobile2:(NSString*)mobile2 country:(NSString*)country;
{
    NSManagedObjectContext *context = [[CoreDataSingleTon sharedCoreDataSingleTon] managedObjectContext];
    
    profile.userId = userId;
    profile.fbId = fbId;
    profile.gender = gender;
    profile.email = email;
    profile.name = name;
    profile.family = family;
    profile.birthdate = birthdate;
    profile.streetName = streetName;
    profile.streetNum = streetNum;
    profile.postCode = postCode;
    profile.city = city;
    profile.phone = phone;
    profile.mobile = mobile;
    profile.dateTime = dateTime;
    profile.lastLoginDateTime = lastLoginDateTime;
    profile.lastLoginIp = lastLoginIp;
    profile.company = company;
    profile.mobile2 = mobile2;
    profile.phone2 = phone2;
    profile.country = country;
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    return profile;
}

#pragma mark - Deleting

+(BOOL)deleteUserProfile:(UserProfile *)userProfile
{
    NSManagedObjectContext *context = [[CoreDataSingleTon sharedCoreDataSingleTon] managedObjectContext];
    
    NSEntityDescription *favoriteEntity=[NSEntityDescription entityForName:@"UserProfile" inManagedObjectContext:context];
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:favoriteEntity];
    NSPredicate *p=[NSPredicate predicateWithFormat:@"self.userId == %@", userProfile.userId];
    [fetch setPredicate:p];
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        return FALSE;
    }
    
    return TRUE;
}

#pragma mark - Checking if entry is already exist

+(BOOL)checkIfProfileExistWithUserId:(NSString *)userId
{
    NSManagedObjectContext *context = [[CoreDataSingleTon sharedCoreDataSingleTon] managedObjectContext];
    NSEntityDescription *favoriteEntity=[NSEntityDescription entityForName:@"UserProfile" inManagedObjectContext:context];
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:favoriteEntity];
    NSPredicate *p=[NSPredicate predicateWithFormat:@"userId == %@", userId];
    [fetch setPredicate:p];
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    
    if([fetchedProducts count] > 0){
        return TRUE;
    }
    
    return FALSE;
}

@end
