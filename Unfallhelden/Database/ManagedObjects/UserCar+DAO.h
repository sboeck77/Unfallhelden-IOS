//
//  UserCar+DAO.h
//  Unfallhelden
//
//  Created by Adil Anwer on 4/14/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "UserCar.h"

@interface UserCar (DAO)

#pragma mark - Fetching

+(NSArray*)getUserCars;

#pragma mark - Adding

+(UserCar*)saveUserCarWithCarId:(NSString*)carId userId:(NSString*)userId insurerId:(NSString*)insurerId leasingCar:(NSString*)leasingCar leasingCompany:(NSString*)leasingCompany companyCar:(NSString*)companyCar companyName:(NSString*)companyName regTicketPhoto:(NSString*)regTicketPhoto issueDate:(NSString*)issueDate vin:(NSString*)vin type:(NSString*)type tradeName:(NSString*)tradeName body:(NSString*)body displacement:(NSString*)displacement fuelType:(NSString*)fuelType color:(NSString*)color extras:(NSString*)extras powerKw:(NSString*)powerKw carDeleted:(NSString*)carDeleted dateTime:(NSString*)dateTime registrationNum:(NSString*)registrationNum brand:(NSString*)brand firstReg:(NSString*)firstReg regDate:(NSString*)regDate tireDimFront:(NSString*)tireDimFront tireDimBack:(NSString*)tireDimBack;

+(BOOL)deleteUserCar:(UserCar*)userCar;

+(BOOL)checkIfCarExistWithCarId:(NSString *)carId;

@end
