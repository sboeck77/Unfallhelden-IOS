//
//  UserCar+DAO.m
//  Unfallhelden
//
//  Created by Adil Anwer on 4/14/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "UserCar+DAO.h"

@implementation UserCar (DAO)

#pragma mark - Fetching

+(NSArray*)getUserCars {
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserCar" inManagedObjectContext:[[CoreDataSingleTon sharedCoreDataSingleTon] managedObjectContext]];
    [fetchRequest setEntity:entity];
    return [[[CoreDataSingleTon sharedCoreDataSingleTon] managedObjectContext] executeFetchRequest:fetchRequest error:&error];
}

#pragma mark - Adding

+(UserCar *)saveUserCarWithCarId:(NSString *)carId userId:(NSString *)userId insurerId:(NSString *)insurerId leasingCar:(NSString *)leasingCar leasingCompany:(NSString *)leasingCompany companyCar:(NSString *)companyCar companyName:(NSString *)companyName regTicketPhoto:(NSString *)regTicketPhoto issueDate:(NSString *)issueDate vin:(NSString *)vin type:(NSString *)type tradeName:(NSString *)tradeName body:(NSString *)body displacement:(NSString *)displacement fuelType:(NSString *)fuelType color:(NSString *)color extras:(NSString *)extras powerKw:(NSString *)powerKw carDeleted:(NSString *)carDeleted dateTime:(NSString *)dateTime registrationNum:(NSString *)registrationNum brand:(NSString *)brand firstReg:(NSString *)firstReg regDate:(NSString *)regDate tireDimFront:(NSString *)tireDimFront tireDimBack:(NSString *)tireDimBack
{
    NSManagedObjectContext *context = [[CoreDataSingleTon sharedCoreDataSingleTon] managedObjectContext];
    UserCar *car = [NSEntityDescription insertNewObjectForEntityForName:@"UserCar" inManagedObjectContext:context];
    
    car.carId = carId;
    car.userId = userId;
    car.insurerId = insurerId;
    car.leasingCar = leasingCar;
    car.companyCar = companyCar;
    car.leasingCompany = leasingCompany;
    car.companyName = companyName;
    car.regTicketPhoto = regTicketPhoto;
    car.issueDate = issueDate;
    car.vin = vin;
    car.type = type;
    car.tradeName = tradeName;
    car.body = body;
    car.displacement = displacement;
    car.fuelType = fuelType;
    car.color = color;
    car.extras = extras;
    car.powerKw = powerKw;
    car.carDeleted = carDeleted;
    car.dateTime = dateTime;
    car.registrationNum = registrationNum;
    car.brand = brand;
    car.firstReg = firstReg;
    car.regDate = regDate;
    car.tireDimBack = tireDimBack;
    car.tireDimFront = tireDimFront;
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    return car;
}

#pragma mark - Deleting

+(BOOL)deleteUserCar:(UserCar*)userCar
{
    NSManagedObjectContext *context = [[CoreDataSingleTon sharedCoreDataSingleTon] managedObjectContext];
    
    NSEntityDescription *favoriteEntity=[NSEntityDescription entityForName:@"UserCar" inManagedObjectContext:context];
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:favoriteEntity];
    NSPredicate *p=[NSPredicate predicateWithFormat:@"self.carId == %@", userCar.carId];
    [fetch setPredicate:p];
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        return FALSE;
    }
    
    return TRUE;
}

#pragma mark - Checking if entry is already exist

+(BOOL)checkIfCarExistWithCarId:(NSString *)carId
{
    NSManagedObjectContext *context = [[CoreDataSingleTon sharedCoreDataSingleTon] managedObjectContext];
    NSEntityDescription *favoriteEntity=[NSEntityDescription entityForName:@"UserCar" inManagedObjectContext:context];
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:favoriteEntity];
    NSPredicate *p=[NSPredicate predicateWithFormat:@"carId == %@", carId];
    [fetch setPredicate:p];
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    
    if([fetchedProducts count] > 0){
        return TRUE;
    }
    
    return FALSE;
}

@end
