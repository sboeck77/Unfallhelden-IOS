//
//  UserProfile+DAO.h
//  Unfallhelden
//
//  Created by Adil Anwer on 4/15/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "UserProfile.h"

@interface UserProfile (DAO)

/*@property (nonatomic, retain) NSString * userId;
 @property (nonatomic, retain) NSString * fbId;
 @property (nonatomic, retain) NSString * gender;
 @property (nonatomic, retain) NSString * email;
 @property (nonatomic, retain) NSString * name;
 @property (nonatomic, retain) NSString * family;
 @property (nonatomic, retain) NSString * company;
 @property (nonatomic, retain) NSString * birthdate;
 @property (nonatomic, retain) NSString * streetName;
 @property (nonatomic, retain) NSString * streetNum;
 @property (nonatomic, retain) NSString * postCode;
 @property (nonatomic, retain) NSString * city;
 @property (nonatomic, retain) NSString * phone;
 @property (nonatomic, retain) NSString * mobile;
 @property (nonatomic, retain) NSString * dateTime;
 @property (nonatomic, retain) NSString * lastLoginDateTime;
 @property (nonatomic, retain) NSString * lastLoginIp;*/

#pragma mark - Fetching

+(NSArray*)getUserProfile;

#pragma mark - Adding

+(UserProfile *)saveUserProfileWithUserId:(NSString *)userId fbId:(NSString *)fbId gender:(NSString *)gender email:(NSString *)email name:(NSString *)name family:(NSString *)family company:(NSString *)company birthdate:(NSString *)birthdate streetName:(NSString *)streetName streetNum:(NSString *)streetNum postCode:(NSString *)postCode city:(NSString *)city phone:(NSString *)phone mobile:(NSString *)mobile dateTime:(NSString *)dateTime lastLoginDateTime:(NSString *)lastLoginDateTime lastLoginIp:(NSString *)lastLoginIp phone2:(NSString*)phone2 mobile2:(NSString*)mobile2 country:(NSString*)country;

+(UserProfile*)updateUserProfileWithProfile:(UserProfile*)profile userId:(NSString *)userId fbId:(NSString *)fbId gender:(NSString *)gender email:(NSString *)email name:(NSString *)name family:(NSString *)family company:(NSString *)company birthdate:(NSString *)birthdate streetName:(NSString *)streetName streetNum:(NSString *)streetNum postCode:(NSString *)postCode city:(NSString *)city phone:(NSString *)phone mobile:(NSString *)mobile dateTime:(NSString *)dateTime lastLoginDateTime:(NSString *)lastLoginDateTime lastLoginIp:(NSString *)lastLoginIp phone2:(NSString*)phone2 mobile2:(NSString*)mobile2 country:(NSString*)country;

+(BOOL)deleteUserProfile:(UserProfile*)userProfile;

+(BOOL)checkIfProfileExistWithUserId:(NSString *)userId;

@end
