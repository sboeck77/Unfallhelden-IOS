//
//  CoreDataSingleTon.h
//  TapcrowdV2
//
//  Created by Dwaynario on 25/10/13 (W43).
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CoreDataSingleTon : NSObject
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (CoreDataSingleTon *)sharedCoreDataSingleTon;
-(void)resetDB;
@end
