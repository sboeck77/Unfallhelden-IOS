//
//  AppDelegate.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/1/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "AppDelegate.h"
#import "CustomizeNavbar.h"
#import "FacebookHelper.h"
#import "HomeScreenVC.h"
#import "UserCurrentLocation.h"
#import "SideMenuViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "CarBrand.h"
#import "Insurer.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize navController,loginVC,menuVC,gasStationVC;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    NSMutableArray *views = [[NSMutableArray alloc] initWithObjects:@"Home",@"Register",@"Report Accident",@"Tank Navigator",@"Settings",@"About Unfallhelden", nil];
    SideMenuViewController *leftMenuViewController = [[SideMenuViewController alloc] initWithViews:views];
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:[self navigationController]
                                                    leftMenuViewController:leftMenuViewController
                                                    rightMenuViewController:nil];
    //    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.3]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.window.rootViewController = container;
    [[UserCurrentLocation sharedSingletonInstance] loadUserLocation];
    [Language setLanguage:@"en"];
    
    [GlobalScopeObjects sharedSingletonInstance].carBrandArray = [[NSMutableArray alloc] init];
    [GlobalScopeObjects sharedSingletonInstance].insurerArray = [[NSMutableArray alloc] init];
    
    [[UnfallheldenHttpClient sharedClient] getGeneralDataWithCompletionBlock:^(NSData *data) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *dataDictionary = [responseString JSONValue];
        NSLog(@"data dictionary : %@",dataDictionary);
        //        int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
        //        if(status == 1){
        NSMutableArray *brandsArray = [dataDictionary objectForKey:@"car_brands"];
        NSMutableArray *insurerArray = [dataDictionary objectForKey:@"insurers"];
        
        NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"name" ascending: YES];
        brandsArray = [[brandsArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]] mutableCopy];
        
        for (int brandsCount = 0; brandsCount < [brandsArray count]; brandsCount++) {
            CarBrand *brand = [[CarBrand alloc] init];
            brand.carBrandId = [[[brandsArray objectAtIndex:brandsCount] valueForKey:@"id"] description];
            brand.carBrandName = [[[brandsArray objectAtIndex:brandsCount] valueForKey:@"name"] description];
            [[GlobalScopeObjects sharedSingletonInstance].carBrandArray addObject:brand];
        }
        
        for (int insurerCount = 0; insurerCount < [insurerArray count]; insurerCount++) {
            Insurer *insurer = [[Insurer alloc] init];
            insurer.insurerId = [[[insurerArray objectAtIndex:insurerCount] valueForKey:@"id"] description];
            insurer.insurerName = [[[insurerArray objectAtIndex:insurerCount] valueForKey:@"name"] description];
            [[GlobalScopeObjects sharedSingletonInstance].insurerArray addObject:insurer];
        }
        
        /*"ambulance_number":"112",
         "hotline_number":""0800 72 41 794"*/
        
        [UnfallheldenUtilities sharedSingletonInstance].hotlinePhoneNum = [[dataDictionary valueForKey:@"hotline_number"] description];
        [UnfallheldenUtilities sharedSingletonInstance].ambulancePhoneNum = [[dataDictionary valueForKey:@"ambulance_number"] description];
        
        //        }
        //        else{
        //            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Abruf der Fahrzeugdaten fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Abruf ihrer Fahrzeugdaten. Bitte versuchen Sie es erneut"];
        //        }
    }];
    
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    //    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
    [GAI sharedInstance].dispatchInterval = 20;
    //    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-64640541-1"];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64685581-1"];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (HomeScreenVC *)gasStationsViewController {
    return [[HomeScreenVC alloc] initWithNibName:@"HomeScreenVC" bundle:nil];
}

- (UINavigationController *)navigationController {
    self.navController = [[UINavigationController alloc]
                          initWithRootViewController:[self gasStationsViewController]];
    self.navController.navigationBar.translucent = YES;
    return self.navController;
}

// For iOS 4.2+ support
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[[FacebookHelper sharedInstance] facebook] handleOpenURL:url];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // FBSample logic
    // if the app is going away, we close the session object
    [[FacebookHelper sharedInstance] logout];
}

- (void)applicationDidBecomeActive:(UIApplication *)application	{
    // FBSample logic
    // We need to properly handle activation of the application with regards to SSO
    //  (e.g., returning from iOS 6.0 authorization dialog or from fast app switching).
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

@end
