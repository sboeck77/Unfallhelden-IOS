//
//  AppDelegate.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/1/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MenuVC;
@class LoginVC;
@class FuelStationsMapVC;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic, strong) MenuVC *menuVC;
@property (nonatomic, strong) LoginVC *loginVC;
@property (nonatomic, strong) FuelStationsMapVC *gasStationVC;
@property (nonatomic, strong) UINavigationController *navController;

@end

