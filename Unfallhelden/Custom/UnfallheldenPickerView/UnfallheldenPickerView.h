//
//  UnfallheldenPickerView.h
//  Unfallhelden
//
//  Created by Adil Anwer on 3/19/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnfallheldenPickerView : UIView

@property (nonatomic, weak) IBOutlet UIPickerView *pickerView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *pickerDoneButton;

@end
