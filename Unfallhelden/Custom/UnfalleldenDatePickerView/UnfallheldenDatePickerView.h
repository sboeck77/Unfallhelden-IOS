//
//  UnfallheldenDatePickerView.h
//  Unfallhelden
//
//  Created by Adil Anwer on 4/7/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnfallheldenDatePickerView : UIView

@property (nonatomic, weak) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *pickerDoneButton;

@end
