//
//  UnfallheldenCustomPopup.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/23/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "UnfallheldenCustomPopup.h"

@interface UnfallheldenCustomPopup ()

@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;

@end

@implementation UnfallheldenCustomPopup
@synthesize unfallheldenPopup,unfallheldenPopupBackground,emailField,unfallheldenPopupDelegate, descriptionTextView, titleLabel;
@synthesize sendPswrdBtn, kaScrollView;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"UnfallheldenCustomPopup" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

-(id)initWithDialogPopupNib
{
    self = [super init];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"UnfallheldenDialogPopup" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

-(void)closePopup:(id)sender
{
    [self.emailField resignFirstResponder];
    // Animation
    [UIView animateWithDuration:0.5f
                     animations:^{
                         self.unfallheldenPopup.alpha = 0.0f;
                         self.unfallheldenPopupBackground.alpha = 0.0f;
                     }];
    [NSTimer scheduledTimerWithTimeInterval:0.6
                                     target:self
                                   selector:@selector(removePopupFromSuerView)
                                   userInfo:nil
                                    repeats:NO];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)removePopupFromSuerView
{
    [self removeFromSuperview];
}

-(void)displayPopup
{
    //    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
    // Support View
    //    self.unfallheldenPopup.alpha = 1.0f;
    self.unfallheldenPopupBackground.alpha = 0.5f;
    self.emailField.delegate = self;
    
    for(UIView *vw in self.unfallheldenPopup.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor blackColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor blackColor]];
            UIColor *color = [UIColor blackColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            if(IS_IPHONE_4 || IS_IPHONE_5){
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:14]];
            }
            else{
            [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
            }
        }  else if([vw isKindOfClass:[UIButton class]]) {
            //            ((UIButton*)vw).layer.cornerRadius = 3.0f;
            //            ((UIButton*)vw).layer.masksToBounds = YES;
            if(IS_IPHONE_4 || IS_IPHONE_5){
                [((UIButton*)vw).titleLabel setFont:[UIFont regularUnfallheldenFontWithSize:14]];
            }
            else{
                [((UIButton*)vw).titleLabel setFont:[UIFont regularUnfallheldenFontWithSize:15]];
            }
        }
    }
    
    
    // Animation
    [UIView animateWithDuration:0.5f
                     animations:^{
                         self.unfallheldenPopup.alpha = 1.0f;
                     }];
}

-(void)sendPassword:(id)sender
{
    if([self.emailField.text length]>0){
        if([[UnfallheldenUtilities sharedSingletonInstance] validateEmail:[self.emailField.text description]]){
            [[UnfallheldenHttpClient sharedClient] userForgotPasswordWithUserEmail:[self.emailField.text description] WithCompletionBlock:^(NSData *data) {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dataDictionary = [responseString JSONValue];
                NSLog(@"data dictionary : %@",dataDictionary);
                int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                if(status == 1){
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"E-Mail gesendet" andMessage:@"Prüfen Sie Ihr Postfach, wir haben das Passwort an Ihre E-Mail gesendet"];
                    [self.emailField resignFirstResponder];
                    // Animation
                    [UIView animateWithDuration:0.5f
                                     animations:^{
                                         self.unfallheldenPopup.alpha = 0.0f;
                                         self.unfallheldenPopupBackground.alpha = 0.0f;
                                     }];
                    [NSTimer scheduledTimerWithTimeInterval:0.6
                                                     target:self
                                                   selector:@selector(removePopupFromSuerView)
                                                   userInfo:nil
                                                    repeats:NO];
                    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
                    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
                }
                else{
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"E-Mail nicht gesendet" andMessage:@"Die E-Mail Adresse ist noch nicht registriert"];
                }
            }];
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Diese E-Mail Adresse liegt uns nicht vor."];
        }
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte geben Sie Ihre E-Mail ein"];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

-(void)didTapAnywhere:(id)sender {
    if(self.emailField.isFirstResponder) {
        [self.emailField resignFirstResponder];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
