//
//  UnfallheldenCustomPopup.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/23/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@protocol UnfallHeldenPopupDelegate <NSObject>

@required
-(void)emailSuccessfullySent;

@end

@interface UnfallheldenCustomPopup : UIView<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIView *unfallheldenPopupBackground;
@property (nonatomic, weak) IBOutlet UIView *unfallheldenPopup;
@property (nonatomic, weak) IBOutlet UITextField *emailField;
@property (nonatomic, weak) IBOutlet UITextView *descriptionTextView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *sendPswrdBtn;
@property (nonatomic, strong) id  <UnfallHeldenPopupDelegate> unfallheldenPopupDelegate;
@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *kaScrollView;

-(IBAction)closePopup:(id)sender;
-(IBAction)sendPassword:(id)sender;
-(void)displayPopup;
-(void)closePopup;
- (id)initWithDialogPopupNib;

@end
