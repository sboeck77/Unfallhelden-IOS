//
//  CarInfoCell.m
//  Unfallhelden
//
//  Created by Adil Anwer on 1/8/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "CarInfoCell.h"

@implementation CarInfoCell
@synthesize optionsBtn,carImage,numPlateLbl;

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CarInfoCell" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
