//
//  CarInfoCell.h
//  Unfallhelden
//
//  Created by Adil Anwer on 1/8/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarInfoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *carImage;
@property (nonatomic, weak) IBOutlet UILabel *numPlateLbl;
@property (nonatomic, weak) IBOutlet UIButton *optionsBtn;

@end
