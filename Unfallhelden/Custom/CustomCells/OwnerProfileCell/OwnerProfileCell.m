//
//  TableViewCell.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/25/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "OwnerProfileCell.h"

@implementation OwnerProfileCell
@synthesize profileTxtFieldBg;

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"OwnerProfileCell" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
