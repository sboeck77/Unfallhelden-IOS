//
//  TableViewCell.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/25/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OwnerProfileCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *profileTxtFieldBg;

@end
