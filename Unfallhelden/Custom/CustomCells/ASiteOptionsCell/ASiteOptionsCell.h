//
//  ASiteOptionsCell.h
//  Unfallhelden
//
//  Created by Adil Anwer on 1/27/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASiteOptionsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *optionsTitle;
@property (nonatomic, weak) IBOutlet UIImageView *optionsCellImage;

@end
