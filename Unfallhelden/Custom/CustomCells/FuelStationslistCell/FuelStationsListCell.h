//
//  FuelStationsListCell.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/4/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FuelStationsListCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton *expandCellBtn;

@end
