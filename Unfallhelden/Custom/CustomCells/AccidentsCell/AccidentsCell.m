//
//  AccidentsCellTableViewCell.m
//  Unfallhelden
//
//  Created by Adil Anwer on 1/25/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "AccidentsCell.h"

@implementation AccidentsCell
@synthesize carImage,dateLbl,optionsBtn;

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AccidentsCell" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
