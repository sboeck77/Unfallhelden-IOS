//
//  AccidentsCellTableViewCell.h
//  Unfallhelden
//
//  Created by Adil Anwer on 1/25/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccidentsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *carImage;
@property (nonatomic, weak) IBOutlet UILabel *dateLbl;
@property (nonatomic, weak) IBOutlet UIButton *optionsBtn;

@end
