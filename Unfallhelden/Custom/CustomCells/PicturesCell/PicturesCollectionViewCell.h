//
//  PicturesCollectionViewCell.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/2/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PicturesCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *carPictureImageView;
@property (nonatomic, weak) IBOutlet UIButton *deleteImgBtn;

@end
