//
//  PicturesCollectionViewCell.m
//  Unfallhelden
//
//  Created by Adil Anwer on 2/2/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "PicturesCollectionViewCell.h"

@implementation PicturesCollectionViewCell
@synthesize carPictureImageView, deleteImgBtn;


- (void)awakeFromNib {
    // Initialization code
}

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PicturesCollectionViewCell" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

@end
