//
//  FacebookHelper.m
//  FacebookTutorial
//
//  Created by Toni Sala Echaurren on 22/08/11.
//  Copyright 2011 indiedevstories.com. All rights reserved.
//

#import "FacebookHelper.h"
#import "Reachability.h"
#import "JSON.h"

//static NSString* kAppId = @"489413641102455";

static NSString* kAppId = @"293224610867324";

@implementation FacebookHelper

//@synthesize single;

@synthesize facebook = _facebook;
@synthesize delegate;
@synthesize navController;
@synthesize viewControllerForPush;

#pragma mark -
#pragma mark Singleton Variables
static FacebookHelper *singletonDelegate = nil;

#pragma mark -
#pragma mark Singleton Methods
- (id)init {
    if (!kAppId) {
        NSLog(@"missing app id!");
        exit(1);
        return nil;
    }
    
    if ((self = [super init])) {
        _permissions =  [[NSArray arrayWithObjects: @"read_stream", @"publish_stream", @"offline_access",@"email",@"user_birthday",@"user_location",@"user_about_me",nil] retain];
        //_permissions =  [[NSArray arrayWithObjects: @"read_stream", @"publish_stream", nil] retain];
    }
    
    return self;
}

+ (FacebookHelper *)sharedInstance {
	@synchronized(self) {
		if (singletonDelegate == nil) {
			[[self alloc] init]; // assignment not done here
		}
	}
	return singletonDelegate;
}

+ (id)allocWithZone:(NSZone *)zone {
	@synchronized(self) {
		if (singletonDelegate == nil) {
			singletonDelegate = [super allocWithZone:zone];
			// assignment and return on first allocation
			return singletonDelegate;
		}
	}
	// on subsequent allocation attempts return nil
	return nil;
}

- (id)copyWithZone:(NSZone *)zone {
	return self;
}

- (id)retain {
	return self;
}

- (unsigned)retainCount {
	return UINT_MAX;  // denotes an object that cannot be released
}

- (void)release {
	//do nothing
}

- (id)autorelease {
	return self;
}

#pragma mark - Private Methods

-(NSMutableDictionary*) buildPostParamsWithHighscore:(int)highscore {
    NSString *customMessage = [NSString stringWithFormat:kCustomMessage, highscore, kAppName];
    NSString *postName = kAppName;
    NSString *serverLink = [NSString stringWithFormat:kServerLink];
    NSString *imageSrc = kImageSrc;
    
    // Final params build.
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   //@"message", @"message",
                                   imageSrc, @"picture",
                                   serverLink, @"link",
                                   postName, @"name",
                                   @" ", @"caption",
                                   customMessage, @"description",
                                   nil];
    
    return params;
}

#pragma mark - Public Methods
-(void) login
{
    // Check if there is a valid session.
    _facebook = [[Facebook alloc] initWithAppId:kAppId andDelegate:self];
    _facebook.accessToken    = [[NSUserDefaults standardUserDefaults] stringForKey:@"FBAccessTokenKey"];
    _facebook.expirationDate = (NSDate *) [[NSUserDefaults standardUserDefaults] objectForKey:@"FBExpirationDateKey"];
    if (![_facebook isSessionValid]) {
        [_facebook authorize:_permissions];
    }
    else {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"AlreadyLogin" forKey:@"LoginStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
//        NSDictionary *userData = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] stringForKey:@"FBAccessTokenKey"],@"accessTocken", nil];
        
        [self.customLoginDelegate fbloggedInSuccessfull];
        
//        [self.navController pushViewController:self.viewControllerForPush animated:YES];
        
    }
}

-(void) logout {
    [_facebook logout:self];
}

-(void)getUserInfo
{
    [_facebook requestWithGraphPath:@"me" andDelegate:self];
}

-(void)commentOrLikePost:(NSString *)graphPath andParams:(NSMutableDictionary *)params
{
    [_facebook requestWithGraphPath:graphPath
                          andParams:params
                      andHttpMethod:@"POST"
                        andDelegate:self];
}

-(void)fbGetCommentsForPost:(NSString *)graphPath
{
    [_facebook requestWithGraphPath:graphPath andDelegate:self];
}

-(void)shareOnWallWithImage:(NSMutableDictionary *)params:(NSString *)userId
{
    NSString *graphPath = [NSString stringWithFormat:@"%@/feed",userId];
    
    [_facebook requestWithGraphPath:graphPath
                          andParams:params
                      andHttpMethod:@"POST"
                        andDelegate:self];
}

-(void)getAllFreinds{
    if(_facebook.isSessionValid)
    {
        [_facebook requestWithGraphPath:@"me/friends?fields=installed,name,picture" andDelegate:self];
    }
}

-(void) sendInvitation:(NSMutableDictionary *)params{
    
    [_facebook dialog:@"apprequests" andParams:params  andDelegate:self];
    
}

-(void)shareOnWall:(NSString *)attachmentStr:(NSString *)actionLinksStr{
    
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								   kAppId, @"api_key",
								   @"Share on Facebook",  @"user_message_prompt",
								   actionLinksStr, @"action_links",
								   attachmentStr, @"attachment",
								   nil];
	
	
	[_facebook dialog: @"stream.publish"
			andParams: params
		  andDelegate:self];
    
}

-(void) postToWallWithDialogNewHighscore:(int)highscore {
    NSMutableDictionary* params = [self buildPostParamsWithHighscore:highscore];
    
    // Post on Facebook.
    [_facebook dialog:@"feed" andParams:params andDelegate:self];
}

#pragma mark - FBDelegate Methods

- (void)fbDidLogin {
    NSLog(@"FB login OK");
    
    // Store session info.
    [[NSUserDefaults standardUserDefaults] setObject:_facebook.accessToken forKey:@"FBAccessTokenKey"];
    [[NSUserDefaults standardUserDefaults] setObject:_facebook.expirationDate forKey:@"FBExpirationDateKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //    [self getUserInfo];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"FirstLogin" forKey:@"LoginStatus"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    NSDictionary *userData = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] stringForKey:@"FBAccessTokenKey"],@"accessTocken", nil];
    
    [self.customLoginDelegate fbloggedInSuccessfull];
    
//    [self.navController pushViewController:self.viewControllerForPush animated:NO];
}

/**
 * Called when the user canceled the authorization dialog.
 */
-(void)fbDidNotLogin:(BOOL)cancelled {
    NSLog(@"FB did not login");
}

/**
 * Called when the request logout has succeeded.
 */
- (void)fbDidLogout {
    NSLog(@"FB logout OK");
    
    // Release stored session.
    //    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"FBAccessTokenKey"];
    //    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"FBExpirationDateKey"];
    //    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"]) {
        [defaults removeObjectForKey:@"FBAccessTokenKey"];
        [defaults removeObjectForKey:@"FBExpirationDateKey"];
        [defaults synchronize];
    }
    
}

/**
 * Called when a request returns and its response has been parsed into
 * an object.
 *
 * The resulting object may be a dictionary, an array, a string, or a number,
 * depending on thee format of the API response.
 */
- (void)request:(FBRequest *)request didLoad:(id)result {
    NSLog(@"FB request OK");
    [delegate myFBConnectRequest:request didLoad:result];
}

/**
 * Called when an error prevents the Facebook API request from completing
 * successfully.
 */
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"FB error: %@", [error localizedDescription]);
    [delegate fbConnectDidFailWithError];
}

/**
 * Called when a UIServer Dialog successfully return.
 */
- (void)dialogDidComplete:(FBDialog *)dialog {
    NSLog(@"published successfully on FB");
}

@end