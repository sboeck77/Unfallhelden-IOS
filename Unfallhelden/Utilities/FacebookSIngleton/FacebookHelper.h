//
//  FacebookHelper.h
//  FacebookTutorial
//
//  Created by Toni Sala Echaurren on 22/08/11.
//  Copyright 2011 indiedevstories.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBConnect.h"
//#import "Singleton.h"

#define kAppName        @"Your App's name"
#define kCustomMessage  @"I just got a score of %d in %@, an iPhone/iPod Touch game by me!"
#define kServerLink     @"http://indiedevstories.com"
#define kImageSrc       @"http://indiedevstories.files.wordpress.com/2011/08/newsokoban_icon.png"

@protocol fBConnectDelegate <NSObject>

@required
- (void)myFBConnectRequest:(FBRequest *)request didLoad:(id)result;
@optional
-(void)fbConnectDidFailWithError;

@end

@protocol FBCustomLoginDelegate <NSObject>

@required
- (void)fbloggedInSuccessfull;

@end

@interface FacebookHelper : NSObject <FBRequestDelegate, FBDialogDelegate, FBSessionDelegate> {
    Facebook* _facebook;
    NSArray* _permissions;
    id  <fBConnectDelegate>delegate;
    id  <FBCustomLoginDelegate> customLoginDelegate;
    UIViewController *viewControllerForPush;
    UINavigationController *navController;
//    Singleton *single;
    
    
}

@property(readonly) Facebook *facebook;
@property (retain, nonatomic) id <fBConnectDelegate> delegate;
@property (retain, nonatomic) id  <FBCustomLoginDelegate> customLoginDelegate;
@property (nonatomic, retain) UINavigationController *navController;
@property (nonatomic, retain) UIViewController *viewControllerForPush;
//@property (nonatomic, retain) Singleton *single


+ (FacebookHelper *) sharedInstance;

#pragma mark - Public Methods
// Public methods here.
-(void) login;
-(void) logout;
-(void) postToWallWithDialogNewHighscore:(int)highscore;
-(void)getAllFreinds;
-(void)getUserInfo;
-(void) sendInvitation:(NSMutableDictionary *)params;
-(void)shareOnWall:(NSString *)attachmentStr:(NSString *)actionLinksStr;
-(void)shareOnWallWithImage:(NSMutableDictionary *)params:(NSString *)userId;

-(void)commentOrLikePost:(NSString *)graphPath andParams:(NSMutableDictionary *)params;
-(void)fbGetCommentsForPost:(NSString *)graphPath;

@end