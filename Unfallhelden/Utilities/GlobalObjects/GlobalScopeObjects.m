//
//  GlobalScopeObjects.m
//  Unfallhelden
//
//  Created by Adil Anwer on 1/5/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "GlobalScopeObjects.h"

@implementation GlobalScopeObjects
@synthesize imageToUpload,carImage,documentImage,carInfoVC,myAccidentsVC,reportedAccidentData;
@synthesize carBrandArray, insurerArray, userCarsArray, manuallyEnteredCarData, audioFileName;

+(GlobalScopeObjects *)sharedSingletonInstance
{
    static GlobalScopeObjects *sharedSingleton;
    @synchronized(self){
        if(!sharedSingleton){
            sharedSingleton = [[GlobalScopeObjects alloc] init];
        }
    }
    return sharedSingleton;
}

@end
