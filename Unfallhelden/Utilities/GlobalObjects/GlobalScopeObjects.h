//
//  GlobalScopeObjects.h
//  Unfallhelden
//
//  Created by Adil Anwer on 1/5/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReportAccidentData.h"
#import "UserCarData.h"

@interface GlobalScopeObjects : NSObject

+(GlobalScopeObjects *)sharedSingletonInstance;

@property (nonatomic, strong) UIImage *imageToUpload;
@property (nonatomic, strong) UIImage *carImage;
@property (nonatomic, strong) UIImage *documentImage;
@property (nonatomic, strong) UIViewController *carInfoVC;
@property (nonatomic, strong) UIViewController *myAccidentsVC;
@property (nonatomic, strong) ReportAccidentData *reportedAccidentData;
@property (nonatomic, strong) NSMutableArray *carBrandArray;
@property (nonatomic, strong) NSMutableArray *insurerArray;
@property (nonatomic, strong) NSMutableArray *userCarsArray;
@property (nonatomic, strong) UserCarData *manuallyEnteredCarData;
@property (nonatomic, strong) NSString *audioFileName;

@end
