//
//  UnfallheldenParentVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 1/14/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface UnfallheldenParentVC : GAITrackedViewController

- (void)setupMenuBarButtonItems;
- (IBAction)leftSideMenuButtonPressed:(id)sender;

@end
