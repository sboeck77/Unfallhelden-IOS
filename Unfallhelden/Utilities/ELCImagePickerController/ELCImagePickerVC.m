//
//  ELCImagePickerDemoViewController.m
//  ELCImagePickerDemo
//
//  Created by ELC on 9/9/10.
//  Copyright 2010 ELC Technologies. All rights reserved.
//

#import "ELCImagePickerVC.h"
#import <MobileCoreServices/UTCoreTypes.h>

#define pictureDownloadBaseUrl @"http://unfallhelden.de/data/uploads/cars/"

@interface ELCImagePickerVC ()

@property (nonatomic, strong) ALAssetsLibrary *specialLibrary;

@end

@implementation ELCImagePickerVC
@synthesize imagesArray,imagePickerType,choseFromLibraryBtn,imageSelected, currentCarTag;
@synthesize imagePickerCallFor,preselectedImage, imgPickerCallFrom, documentPhotoName, alreadySelectedImg, noImgSelectdLbl;
@synthesize alreadySelectedImageHeight, alreadySelectedImageWidth;

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:@"Bild auswählen"];
    self.imagesArray = [[NSMutableArray alloc] init];
    if(imagePickerType == ELCImagePickerTypeMultipleImages){
        [self.choseFromLibraryBtn addTarget:self action:@selector(launchController) forControlEvents:UIControlEventTouchUpInside];
    }else{
        [self.choseFromLibraryBtn addTarget:self action:@selector(launchSpecialController) forControlEvents:UIControlEventTouchUpInside];
    }
    self.imageSelected = FALSE;
//    if(self.preselectedImage != nil && [self.imgPickerCallFrom isEqualToString:@"CarInfoVC"]){
//        [self setupScrollViewWithImage:self.preselectedImage];
//    }
    
    if([self.imgPickerCallFrom isEqualToString:@"CarView"]){
        
        self.alreadySelectedImg.hidden = NO;
        NSString *urlStr= [NSString stringWithFormat:@"%@%@",pictureDownloadBaseUrl,self.documentPhotoName];
        NSURL *imageUrl = [NSURL URLWithString:urlStr];
        [self.alreadySelectedImg sd_setImageWithURL:imageUrl];
        self.noImgSelectdLbl.hidden = YES;
        if(IS_IPHONE_4){
            self.alreadySelectedImageHeight.constant = 220;
            self.alreadySelectedImageWidth.constant = 220;
        }
//        [self adjustImagePosition:self.alreadySelectedImg];
    }
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
//    UIBarButtonItem *homeBtnItem = [UIBarButtonItem createBarButtonWithText:@"Home" withTarget:self andSelector:@selector(homeBtnPress:)];
    UIBarButtonItem *backBtnItem = [UIBarButtonItem createBackBarButtonItemWithtarget:self andSelector:@selector(goBack:)];
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,backBtnItem, nil] animated:NO];
    [[CustomizeNavbar sharedSingletonInstance] setBackgroundImageForNavBar:self.navigationController.navigationBar withImage:[UIImage imageNamed:@"navBarBg"]];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIFont boldUnfallheldenFontWithSize:15], NSFontAttributeName,
                                                                     [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                     nil]];
}

//Using generated synthesizers

- (IBAction)launchController
{
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
    
    elcPicker.maximumImagesCount = 100; //Set the maximum number of images to select to 100
    elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
    elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
    elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
    elcPicker.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie]; //Supports image and movie types
    
    elcPicker.imagePickerDelegate = self;
    
    [self presentViewController:elcPicker animated:YES completion:nil];
}

//Capture image
-(void)captureImage
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Alert" andMessage:@"Device has no camera"];
    }
    else{
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker setDelegate:self];
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image= [info objectForKey:UIImagePickerControllerEditedImage];
    
//    [self.imagesArray addObject:image];
//    [GlobalScopeObjects sharedSingletonInstance].imageToUpload = image;
    [self dismissViewControllerAnimated:YES completion:nil];
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [imagePicker setDelegate:self];
    
    [self setupScrollView:info];
}

//only for one image
- (IBAction)launchSpecialController
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    self.specialLibrary = library;
    NSMutableArray *groups = [NSMutableArray array];
    [_specialLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (group) {
            [groups addObject:group];
        } else {
            // this is the end
            [self displayPickerForGroup:[groups objectAtIndex:0]];
        }
    } failureBlock:^(NSError *error) {
        self.chosenImages = nil;
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Fehler" message:[NSString stringWithFormat:@"Album Error: %@ - %@", [error localizedDescription], [error localizedRecoverySuggestion]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        NSLog(@"A problem occured %@", [error description]);
        // an error here means that the asset groups were inaccessable.
        // Maybe the user or system preferences refused access.
    }];
}

- (void)displayPickerForGroup:(ALAssetsGroup *)group
{
    ELCAssetTablePicker *tablePicker = [[ELCAssetTablePicker alloc] initWithStyle:UITableViewStylePlain];
    tablePicker.singleSelection = YES;
    tablePicker.immediateReturn = YES;
    
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initWithRootViewController:tablePicker];
    elcPicker.maximumImagesCount = 1;
    elcPicker.imagePickerDelegate = self;
    elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
    elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
    elcPicker.onOrder = NO; //For single image selection, do not display and return order of selected images
    tablePicker.parent = elcPicker;
    
    // Move me
    tablePicker.assetGroup = group;
    [tablePicker.assetGroup setAssetsFilter:[ALAssetsFilter allAssets]];
    
    [self presentViewController:elcPicker animated:YES completion:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    } else {
        return toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown;
    }
}

#pragma mark ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    for (UIView *v in [_scrollView subviews]) {
        [v removeFromSuperview];
    }
    CGRect workingFrame = _scrollView.frame;
    workingFrame.origin.x = 0;
    
    //    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
    for (NSDictionary *dict in info) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                if(imagePickerType == ELCImagePickerTypeMultipleImages){
                    [self.imagesArray addObject:image];
                }
                else{
                    UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
                    [imageview setContentMode:UIViewContentModeScaleAspectFit];
                    imageview.frame = CGRectMake(0, 0, image.size.width, image.size.height);
                    
                    [_scrollView addSubview:imageview];
                    [GlobalScopeObjects sharedSingletonInstance].imageToUpload = image;
                    workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
                    [self adjustImagePosition:imageview];
                    self.imageSelected = TRUE;
                }
                
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        } else if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypeVideo){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                if(imagePickerType == ELCImagePickerTypeMultipleImages){
                    [self.imagesArray addObject:image];
                }
                else{
                    UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
                    [imageview setContentMode:UIViewContentModeScaleAspectFit];
                    imageview.frame = CGRectMake(0, 0, image.size.width, image.size.height);
                    [_scrollView addSubview:imageview];
                    workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
                    
                    [self adjustImagePosition:imageview];
                    [GlobalScopeObjects sharedSingletonInstance].imageToUpload = image;
                    self.imageSelected = TRUE;
                }
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        } else {
            NSLog(@"Uknown asset type");
        }
    }
    if(imagePickerType == ELCImagePickerTypeMultipleImages){
        for (UIImage *image in self.imagesArray) {
            UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
            [imageview setContentMode:UIViewContentModeScaleAspectFit];
            imageview.frame = workingFrame;
            
            [_scrollView addSubview:imageview];
            
            workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
            //            [self adjustImagePosition:imageview];
        }
        self.chosenImages = self.imagesArray;
    }
    
    [_scrollView setPagingEnabled:YES];
    [_scrollView setContentSize:CGSizeMake(workingFrame.origin.x, workingFrame.size.height)];
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

//Setup scrollveiw also with previously selected images
-(void)setupScrollView:(NSDictionary*)dict{
    for (UIView *v in [_scrollView subviews]) {
        [v removeFromSuperview];
    }
    
    CGRect workingFrame = _scrollView.frame;
    workingFrame.origin.x = 0;
    
//    if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
        if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
            UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
            
//            if(imagePickerType == ELCImagePickerTypeMultipleImages){
//                [self.imagesArray addObject:image];
//                
//                for (UIImage *image in self.imagesArray) {
//                    UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
//                    [imageview setContentMode:UIViewContentModeScaleAspectFit];
//                    imageview.frame = workingFrame;
//                    [_scrollView addSubview:imageview];
//                    
//                    workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
//                }
//            }
//            else{
                UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
                [imageview setContentMode:UIViewContentModeScaleAspectFit];
                //                imageview.frame = workingFrame;
                
                imageview.frame = CGRectMake(0, 0, image.size.width, image.size.height);
                
                [_scrollView addSubview:imageview];
                workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
                [GlobalScopeObjects sharedSingletonInstance].imageToUpload = image;
                [self adjustImagePosition:imageview];
                self.imageSelected = TRUE;
//            }
            
        } else {
            NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
        }
//    } else if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypeVideo){
//        if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
//            UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
//            
//            [self.imagesArray addObject:image];
//            
//            if(imagePickerType == ELCImagePickerTypeMultipleImages){
//                [self.imagesArray addObject:image];
//                
//                for (UIImage *image in self.imagesArray) {
//                    UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
//                    [imageview setContentMode:UIViewContentModeScaleAspectFit];
//                    imageview.frame = workingFrame;
//                    
//                    [_scrollView addSubview:imageview];
//                    
//                    workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
//                    //                    [self adjustImagePosition:imageview];
//                }
//            }
//            else{
//                UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
//                [imageview setContentMode:UIViewContentModeScaleAspectFit];
//                imageview.frame = workingFrame;
//                [_scrollView addSubview:imageview];
//                [GlobalScopeObjects sharedSingletonInstance].imageToUpload = image;
//                workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
//                [self adjustImagePosition:imageview];
//                self.imageSelected = TRUE;
//            }
//            
//        } else {
//            NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
//        }
//    } else {
//        NSLog(@"Uknown asset type");
//    }
    
    [_scrollView setPagingEnabled:YES];
    [_scrollView setContentSize:CGSizeMake(workingFrame.origin.x, workingFrame.size.height)];
}

-(void)setupScrollViewWithImage:(UIImage *)image
{
    for (UIView *v in [_scrollView subviews]) {
        [v removeFromSuperview];
    }
    CGRect workingFrame = _scrollView.frame;
    workingFrame.origin.x = 0;
    UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
    [imageview setContentMode:UIViewContentModeScaleAspectFit];
    imageview.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    [_scrollView addSubview:imageview];
    workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
    [GlobalScopeObjects sharedSingletonInstance].imageToUpload = image;
    [self adjustImagePosition:imageview];
    self.imageSelected = TRUE;
    [_scrollView setPagingEnabled:YES];
    [_scrollView setContentSize:CGSizeMake(workingFrame.origin.x, workingFrame.size.height)];
}

-(void)adjustImagePosition:(UIImageView*)imageView{
    // 2. calculate the size of the image view
    CGFloat scrollViewWidth = CGRectGetWidth(_scrollView.frame);
    CGFloat scrollViewHeight = CGRectGetHeight(_scrollView.frame);
    CGFloat imageViewWidth = CGRectGetWidth(imageView.frame);
    CGFloat imageViewHeight = CGRectGetHeight(imageView.frame);
    CGFloat widthRatio = scrollViewWidth / imageViewWidth;
    CGFloat heightRation = scrollViewHeight / imageViewHeight;
    CGFloat ratio = MIN(widthRatio, heightRation);
    CGRect newImageFrame = CGRectMake(0, 0, imageViewWidth * ratio, imageViewHeight * ratio);
    imageView.frame = newImageFrame;
    
    // 3. find the position of the imageView.
    CGFloat scrollViewCenterX = CGRectGetMidX(_scrollView.bounds);
    CGFloat scrollViewCenterY = CGRectGetMidY(_scrollView.bounds) + _scrollView.contentInset.top / 2 ;
    imageView.center = CGPointMake(scrollViewCenterX, scrollViewCenterY);
}

-(IBAction)goBack:(id)sender
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }

-(void)doneBtnPress:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if(self.imageSelected){
        if([self.imagePickerCallFor isEqualToString:@"CarImage"]){
            NSString *notificationName = @"CarImageSelected";
            [GlobalScopeObjects sharedSingletonInstance].carImage = [GlobalScopeObjects sharedSingletonInstance].imageToUpload;
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:nil];
        }
        else if([self.imagePickerCallFor isEqualToString:@"DocumentImage"]){
            NSString *notificationName = @"DocumentImageSelected";
            [GlobalScopeObjects sharedSingletonInstance].documentImage = [GlobalScopeObjects sharedSingletonInstance].imageToUpload;
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:nil];
        }
        else if([self.imgPickerCallFrom isEqualToString:@"CarView"]){
//            NSString *notificationName = @"UpdatedDocumentImageSelected";
//            NSString *currentNotificationName = [NSString stringWithFormat:@"%@-%d",kNotificationNameUpdatedDocumentImageSelected,self.currentCarTag];
            [GlobalScopeObjects sharedSingletonInstance].documentImage = [GlobalScopeObjects sharedSingletonInstance].imageToUpload;
            [self.carImageSelectedDelegate updatedDocumentImageSelected];
        }
    }
}

@end
