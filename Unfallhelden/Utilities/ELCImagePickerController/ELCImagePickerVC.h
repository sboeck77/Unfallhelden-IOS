//
//  ELCImagePickerDemoViewController.h
//  ELCImagePickerDemo
//
//  Created by ELC on 9/9/10.
//  Copyright 2010 ELC Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ELCImagePickerHeader.h"

typedef enum {
    ELCImagePickerTypeMultipleImages = 0,
    ELCImagePickerTypeSingleImage = 1
} ELCImagePickerType;

@protocol CarImageSelectedDelegate <NSObject>

@required
-(void)updatedDocumentImageSelected;

@end

@interface ELCImagePickerVC : UIViewController <ELCImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate, UIImagePickerControllerDelegate, CarImageSelectedDelegate>

@property (nonatomic, strong) id <CarImageSelectedDelegate> carImageSelectedDelegate;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, copy) NSArray *chosenImages;
@property (nonatomic, strong) NSMutableArray *imagesArray;
@property (nonatomic) ELCImagePickerType imagePickerType;
@property (nonatomic, weak) IBOutlet UIButton *choseFromLibraryBtn;
@property (nonatomic) BOOL imageSelected;
@property (nonatomic, strong) NSString *imagePickerCallFor;
@property (nonatomic, strong) NSString *imgPickerCallFrom;
@property (nonatomic, strong) UIImage *preselectedImage;
@property (nonatomic, strong) NSString *documentPhotoName;
@property (nonatomic, weak) IBOutlet UIImageView *alreadySelectedImg;
@property (nonatomic, weak) IBOutlet UILabel *noImgSelectdLbl;
@property (nonatomic) int currentCarTag;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alreadySelectedImageHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alreadySelectedImageWidth;

// the default picker controller
- (IBAction)launchController;

// a special picker controller that limits itself to a single album, and lets the user
// pick just one image from that album.
- (IBAction)launchSpecialController;

//Capture Image
- (IBAction)captureImage;

-(IBAction)doneBtnPress:(id)sender;

@end

