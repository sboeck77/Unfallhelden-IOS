//
//  UIFont+Unfallhelden.m
//  Unfallhelden
//
//  Created by Adil Anwer on 2/24/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "UIFont+Unfallhelden.h"

@implementation UIFont (Unfallhelden)

+(UIFont*)regularUnfallheldenFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"TitilliumWeb-Light" size:size];
}

+(UIFont*)boldUnfallheldenFontWithSize:(CGFloat)size {
    return [UIFont fontWithName:@"TitilliumWeb-Bold" size:size];
}


@end
