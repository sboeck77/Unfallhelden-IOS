//
//  UIBarButtonItem+Unfallhelden.h
//  Unfallhelden
//
//  Created by Adil Anwer on 1/22/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (Unfallhelden)

+(UIBarButtonItem*)buttonWithImageName:(NSString*)image target:(id)target andSelector:(SEL)selector;

+(UIBarButtonItem*) createBarButtonWithText:(NSString*)text withTarget:(id)target andSelector:(SEL)select;

+ (UIBarButtonItem*)getNavbarSpinner;

+ (UIBarButtonItem *)leftMenuBarButtonItemWithtarget:(id)target andSelector:(SEL)selector;

+ (UIBarButtonItem *)rightMenuBarButtonItemWithtarget:(id)target andSelector:(SEL)selector andImage:(UIImage*)image;

+ (UIBarButtonItem *)addAnotherRightMenuBarButtonItemWithtarget:(id)target andSelector:(SEL)selector andImage:(UIImage*)image;

+ (UIBarButtonItem *)createBackBarButtonItemWithtarget:(id)target andSelector:(SEL)selector;

+ (UIBarButtonItem*) createEmptyBarButton;

@end
