//
//  UIBarButtonItem+Unfallhelden.m
//  Unfallhelden
//
//  Created by Adil Anwer on 1/22/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "UIBarButtonItem+Unfallhelden.h"

@implementation UIBarButtonItem (Unfallhelden)

+(UIBarButtonItem*)buttonWithImageName:(NSString*)image target:(id)target andSelector:(SEL)selector {
    UIImage *buttonImage = [UIImage imageNamed:image];
    if(buttonImage) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0, 0, 44, 44);
        [button setContentEdgeInsets:UIEdgeInsetsMake(((44 - buttonImage.size.height) / 2), 44-buttonImage.size.width, ((44 - buttonImage.size.height) / 2), 0)];
        UIBarButtonItem *bbi = [[UIBarButtonItem alloc] initWithCustomView:button];
        return bbi;
    }else {
        return nil;
    }
}

#pragma mark - Custom BarButtonItem with text

+ (UIBarButtonItem*) createBarButtonWithText:(NSString*)text withTarget:(id)target andSelector:(SEL)select {
    CGSize buttonSize = [text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontAwesomeFontOfSize:18]}];
    UIButton *buttonView = [[UIButton alloc] initWithFrame:CGRectMake(0, 9, buttonSize.width, 44)];
    buttonView.backgroundColor = [UIColor clearColor];
    UILabel *buttonLabel = [[UILabel alloc] init];
    buttonLabel.text = text;
    buttonLabel.frame = CGRectMake(0, 0, buttonSize.width, 44);
    buttonLabel.font = [UIFont fontAwesomeFontOfSize:18];
    buttonLabel.textColor = [UIColor blueColor];
    buttonLabel.backgroundColor = [UIColor clearColor];
    buttonLabel.textAlignment = NSTextAlignmentCenter;
    [buttonView addSubview:buttonLabel];
    [buttonView addTarget:target action:select forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:buttonView];
}

+ (UIBarButtonItem *)leftMenuBarButtonItemWithtarget:(id)target andSelector:(SEL)selector {
    UIImage *image = [UIImage imageNamed:@"sidebarMenuImage"];
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [aButton setBackgroundImage:image forState:UIControlStateNormal];
    aButton.frame = CGRectMake(0.0, 0.0, image.size.width,   image.size.height);
    
    aButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [aButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    return barButton;
}

+ (UIBarButtonItem *)createBackBarButtonItemWithtarget:(id)target andSelector:(SEL)selector {
    UIImage *image = [UIImage imageNamed:@"backBtn"];
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [aButton setBackgroundImage:image forState:UIControlStateNormal];
    aButton.frame = CGRectMake(0.0, 0.0, image.size.width,   image.size.height);
    
    aButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [aButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    return barButton;
}

+ (UIBarButtonItem*) createEmptyBarButton{
    UIButton *buttonView = [[UIButton alloc] initWithFrame:CGRectMake(0, 9, 1, 44)];
    buttonView.backgroundColor = [UIColor clearColor];
    return [[UIBarButtonItem alloc] initWithCustomView:buttonView];
}

+ (UIBarButtonItem *)rightMenuBarButtonItemWithtarget:(id)target andSelector:(SEL)selector andImage:(UIImage*)image
{
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if(image){
        [aButton setBackgroundImage:image forState:UIControlStateNormal];
        aButton.frame = CGRectMake(0.0, 0.0, image.size.width,   image.size.height);
    }
    else{
        aButton.frame = CGRectMake(0.0, 0.0, 80, 29);
    }
    
    aButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [aButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //    [[TrafficAlarmUtilities sharedSingletonInstance] setLatoFontForUILable:aButton.titleLabel withFontSize:16];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    [aButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];

    return barButton;
}

+(UIBarButtonItem *)addAnotherRightMenuBarButtonItemWithtarget:(id)target andSelector:(SEL)selector andImage:(UIImage *)image
{
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if(image){
        [aButton setBackgroundImage:image forState:UIControlStateNormal];
        aButton.frame = CGRectMake(0.0, 0.0, image.size.width,   image.size.height);
    }
    else{
        aButton.frame = CGRectMake(0.0, 0.0, 80, 29);
    }
    
    aButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [aButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //    [[TrafficAlarmUtilities sharedSingletonInstance] setLatoFontForUILable:aButton.titleLabel withFontSize:16];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    [aButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return barButton;
}

#pragma mark - Spinner to show app is busy

+ (UIBarButtonItem*)getNavbarSpinner {
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.color = [UIColor blueColor];
    [spinner startAnimating];
    return [[UIBarButtonItem alloc] initWithCustomView:spinner];
}

@end
