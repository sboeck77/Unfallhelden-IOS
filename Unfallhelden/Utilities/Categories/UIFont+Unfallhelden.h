//
//  UIFont+Unfallhelden.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/24/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Unfallhelden)

+(UIFont*)regularUnfallheldenFontWithSize:(CGFloat)size;

+(UIFont*)boldUnfallheldenFontWithSize:(CGFloat)size;

@end
