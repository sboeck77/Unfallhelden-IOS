//
//  Created by adilanwer on 9/27/14.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface UnfallheldenUtilities : NSObject

@property (nonatomic, strong) UIAlertView *alert;
@property (nonatomic, strong) MBProgressHUD *HUD;
@property (nonatomic) double userlatitude;
@property (nonatomic) double userlongitude;
@property (nonatomic, strong) NSString *hotlinePhoneNum;
@property (nonatomic, strong) NSString *ambulancePhoneNum;

+(UnfallheldenUtilities *)sharedSingletonInstance;

-(void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message;
-(void) showProgressAlertTitle:(NSString *)title message:(NSString *)message;
-(void) hideProgressAlert;
-(bool)connectedToWiFi;
- (BOOL)validateEmail:(NSString *)email;

@end
