//
//  Created by adilanwer on 9/27/14.
//

#import "UnfallheldenUtilities.h"
#import "Reachability.h"

@implementation UnfallheldenUtilities
@synthesize alert,HUD,userlatitude,userlongitude,hotlinePhoneNum, ambulancePhoneNum;

+(UnfallheldenUtilities *)sharedSingletonInstance
{
    static UnfallheldenUtilities *sharedSingleton;
    @synchronized(self){
        if(!sharedSingleton){
            sharedSingleton = [[UnfallheldenUtilities alloc] init];
        }
    }
    
    return sharedSingleton;
}

//show alertview with title and message
-(void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message
{
        self.alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [self.alert show];
}

#pragma mark - ProgressHudMehtods
-(void) hideProgressAlert{
    [HUD hide:YES afterDelay:0];
    HUD = nil;
}

-(void) showProgressAlertTitle:(NSString *)title message:(NSString *)message{
    
    UIWindow *window=[[[UIApplication sharedApplication]delegate]window];
    
    HUD = [[MBProgressHUD alloc] initWithView:window];
    [window addSubview:HUD];
    
    HUD.labelText = title;
    if(message)
    {
        HUD.detailsLabelText=message;
    }
    [HUD show:YES];
}

#pragma mark - Others

- (BOOL)validateEmail:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark -
#pragma mark WifiConeectivity
-(bool)connectedToWiFi
{
    Reachability *r = [Reachability reachabilityWithHostName:@"www.apple.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    bool result = false;
    if (internetStatus == ReachableViaWiFi )
    {
        result = true;
    }
    else if(internetStatus == ReachableViaWWAN){
        result = true;
    }
    return result;
}

@end
