
//
//  Created by adilanwer on 9/27/14.

#import "UnfallheldenHttpClient.h"
#import "UnfallheldenUtilities.h"
#import "UnfallheldenApiConstants.h"
#import "AFHTTPRequestOperation.h"
#import "Reachability.h"

@implementation UnfallheldenHttpClient

+(UnfallheldenHttpClient *)sharedClient
{
    static UnfallheldenHttpClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:kWebserviceBaseUrl]];
    });
    return _sharedClient;
}

-(id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    [self registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [self setDefaultHeader:@"Accept" value:@"application/json"];
    self.parameterEncoding = AFJSONParameterEncoding;
    
    return self;
    
}

-(void)registerUserWithEmail:(NSString *)email andPassword:(NSString *)password andName:(NSString *)name andFamily:(NSString *)family andGender:(NSString*)gender  andPhone:(NSString *)phone andTerms:(NSString *)terms andFBUserId:(NSString*)fbUserId WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        
        NSDictionary *dictionary = @ {@"email" :email, @"password" :password, @"name":name, @"family":family, @"phone":phone, @"gender":gender ,@"terms":[NSNumber numberWithBool:YES], @"facebook_user_id":fbUserId};
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:NULL];
        
        NSMutableURLRequest *request = [self requestWithMethod:@"POST"
                                                          path:kWebServiceRegisterUser
                                                    parameters:nil];
        
        NSMutableData *body = [NSMutableData data];
        
        // Append of the body with your data
        [body appendData:jsonData];
        [request setHTTPBody:body];
        
        // Set request method POST
        [request setHTTPMethod:@"POST"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)registerUserWithAccidentHashWithEmail:(NSString *)email andPassword:(NSString *)password andName:(NSString *)name andFamily:(NSString *)family andGender:(NSString*)gender  andPhone:(NSString *)phone andTerms:(NSString *)terms andFBUserId:(NSString*)fbUserId WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSDictionary *dictionary = @ {@"email" :email, @"password" :password, @"name":name, @"family":family, @"phone":phone, @"gender":gender ,@"terms":[NSNumber numberWithBool:YES], @"facebook_user_id":fbUserId, @"accident_hash": [[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainAccidentHash] description]};
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:NULL];
        
        NSMutableURLRequest *request = [self requestWithMethod:@"POST"
                                                          path:kWebServiceRegisterUser
                                                    parameters:nil];
        
        NSMutableData *body = [NSMutableData data];
        
        // Append of the body with your data
        [body appendData:jsonData];
        [request setHTTPBody:body];
        
        // Set request method POST
        [request setHTTPMethod:@"POST"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)loginUserWithEmail:(NSString *)email andPassword:(NSString *)password WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        //        NSString *progressMessage=@"Fetching..";
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSDictionary *parameters = @ {@"email" :email, @"password" :password};
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:NULL];
        
        NSMutableURLRequest *request = [self requestWithMethod:@"POST"
                                                          path:kWebServiceUserLogin
                                                    parameters:nil];
        
        NSMutableData *body = [NSMutableData data];
        
        // Append of the body with your data
        [body appendData:jsonData];
        [request setHTTPBody:body];
        
        // Set request method POST
        [request setHTTPMethod:@"POST"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)checkIfEmailAlreadyExistWithEmail:(NSString *)email andCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        //        NSString *progressMessage=@"Fetching..";
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSURL *emailCheckUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.unfallhelden.de/Users/isExistEmail/?email=%@",email]];
        
        //Create the URL request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:emailCheckUrl];
        
        // Set request method POST
        [request setHTTPMethod:@"GET"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        
        //Start the request for the data
        [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            //If data were received
            if (data) {
                // Success
                block(data);
            }
            
            //No data received
            else {
                // Failure
                NSLog(@"Error: %@", error);
                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
                });
            }
        }];
        
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)getWeatherWithLatitude:(double)lati longitude:(double)longi WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        //        NSString *progressMessage=@"Fetching..";
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSString *latitude = [NSString stringWithFormat:@"%f",lati];
        NSString *longitude = [NSString stringWithFormat:@"%f",longi];
        
        NSURL *weatherApi = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?lat=%@&lon=%@",latitude,longitude]];
        
        //Create the URL request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:weatherApi];
        
        //Start the request for the data
        [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            //If data were received
            if (data) {
                // Success
                block(data);
            }
            
            //No data received
            else {
                // Failure
                NSLog(@"Error: %@", error);
                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
                });
            }
        }];
        
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)loginUserViaFacebookWithUserId:(NSString *)userId andAccessToken:(NSString *)accessToken WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        //        NSString *progressMessage=@"Fetching..";
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSDictionary *parameters = @ {@"user_id" :userId, @"access_token" :accessToken};
        NSLog(@"parameters : %@",parameters);
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:NULL];
        
        NSMutableURLRequest *request = [self requestWithMethod:@"POST"
                                                          path:kWebServiceFBLogin
                                                    parameters:nil];
        
        NSMutableData *body = [NSMutableData data];
        
        // Append of the body with your data
        [body appendData:jsonData];
        [request setHTTPBody:body];
        
        // Set request method POST
        [request setHTTPMethod:@"POST"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)reportAccidentWithCarId:(NSString *)carId carRegNum:(NSString *)carRegNum name:(NSString *)name family:(NSString *)lastName phone:(NSString *)phone email:(NSString *)email terms:(BOOL)terms address:(NSString *)address postCode:(NSString *)postCode city:(NSString *)city accidentLat:(NSString *)latitude accidentLongitude:(NSString *)accidentLongitude accidentDate:(NSString *)accidentDate accidentTime:(NSString *)accidentTime accidentDescription:(NSString *)accidentDescription opponentRegNum:(NSString *)opponentRegNum opponentCarBrand:(NSString *)opponentCarBrand opponentCarModel:(NSString *)opponentCarModel acFirstName:(NSString *)acFirstName acLastName:(NSString *)acLastName acCompanyCar:(BOOL)isCompanyCar acCompanyName:(NSString*)acCompanyName acCompanyLeasing:(BOOL)isLeasingCompany  acLeasingCompany:(NSString *)acLeasingCompany acPhone:(NSString *)acPhone acAddress:(NSString *)acAddress acPostCode:(NSString *)acpostCode acCity:(NSString *)acCity acStreet:(NSString *)acStreet acStreetNum:(NSString *)acStreetNum acInsurer:(NSString *)acInsurer audio:(NSMutableArray *)audioFileUrl witnesses:(NSMutableArray *)witnesses country:(NSString*)country weather:(NSString*)weather WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        //        NSString *progressMessage=@"Fetching..";
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSDictionary *parameters = @ {@"car_id" :carId, @"car_reg_number" :carRegNum, @"name" :name, @"family" :lastName, @"phone" :phone, @"email" :email, @"terms" :[NSNumber numberWithBool:terms], @"address" :address, @"postcode" :postCode, @"city" :city, @"accident_lat" :latitude, @"accident_lng" :accidentLongitude, @"accident_date" :accidentDate, @"accident_time" :accidentTime, @"description" :accidentDescription, @"opponent_reg_number" :opponentRegNum, @"opponent_car_brand" :opponentCarBrand, @"opponent_car_model" :opponentCarModel, @"opponent_first_name" :acFirstName, @"opponent_last_name" :acLastName, @"opponent_is_company" :[NSNumber numberWithBool:isCompanyCar], @"opponent_company" :acCompanyName, @"opponent_is_leasing" :[NSNumber numberWithBool:isLeasingCompany], @"opponent_leasing" :acLeasingCompany, @"opponent_phone" :acPhone, @"opponent_address" :acAddress, @"opponent_postcode" :acpostCode, @"opponent_city" :acCity, @"opponent_street" :acStreet, @"opponent_street_nr" :acStreetNum, @"opponent_insurer" :acInsurer , @"audio" :audioFileUrl, @"witnesses" :witnesses, @"country": country, @"weather":weather};
        
        NSLog(@"parameters : %@",parameters);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:NULL];
        NSMutableURLRequest *request = [self requestWithMethod:@"POST"
                                                          path:kWebServiceReportAccident
                                                    parameters:nil];
        
        NSMutableData *body = [NSMutableData data];
        
        // Append of the body with your data
        [body appendData:jsonData];
        [request setHTTPBody:body];
        
        // Set request method POST
        [request setHTTPMethod:@"POST"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        if([[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultsUserAlreadyLoggedIn]){
            if([[[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultsUserAlreadyLoggedIn] isEqualToString:@"YES"]){
                NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
                [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
            }
        }
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)updateUserCarWithInsurer:(NSString*)insurer carId:(NSString*)carId insurerId:(NSString*)insurerId regNum:(NSString*)regNum regDate:(NSString*)regDate issueDate:(NSString*)issueDate firstReg:(NSString*)firstReg vin:(NSString*)vin type:(NSString*)type tradeName:(NSString*)tradeName brand:(NSString*)brand fuel:(NSString*)fuel displacement:(NSString*)displacement ratedPower:(NSString*)ratedPower fTireDimen:(NSString*)fTireDimen bTireDimen:(NSString*)bTireDimen color:(NSString*)color leasingCar:(NSString*)leasingCar leasingCompany:(NSString*)leasingCompany companyCar:(NSString*)companyCar companyName:(NSString*)companyName extras:(NSString*)extras regTicketPhoto:(NSString*)regTicketPhoto WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        //        NSString *progressMessage=@"Fetching..";
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSDictionary *parameters = @ {@"insurer" :insurerId, @"registration_number" :regNum, @"registration_date" :regDate, @"issue_date" :issueDate, @"first_registration" :firstReg, @"vin" :vin, @"type" :type, @"trade_name" :tradeName, @"brand" :brand, @"fuel" :fuel, @"displacement" :displacement, @"rated_power" :ratedPower, @"tire_dimension_front" :fTireDimen, @"tire_dimension_back" :bTireDimen, @"color" :color, @"leasing_car" :leasingCar, @"leasing_company" :leasingCompany, @"company_car" :companyCar, @"company_name" :companyName, @"extras" :extras, @"reg_ticket_photo" :regTicketPhoto};
        
        NSLog(@"parameters : %@",parameters);
        
        NSString *path = [NSString stringWithFormat:@"Cars/%@",carId];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:NULL];
        NSMutableURLRequest *request = [self requestWithMethod:@"PUT"
                                                          path:path
                                                    parameters:nil];
        
        NSMutableData *body = [NSMutableData data];
        
        // Append of the body with your data
        [body appendData:jsonData];
        [request setHTTPBody:body];
        
        // Set request method POST
        [request setHTTPMethod:@"PUT"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        if([[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultsUserAlreadyLoggedIn]){
            NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
            [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        }
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)addCarWithinsurerId:(NSString *)insurerId regNum:(NSString *)regNum regDate:(NSString *)regDate issueDate:(NSString *)issueDate firstReg:(NSString *)firstReg vin:(NSString *)vin type:(NSString *)type tradeName:(NSString *)tradeName brand:(NSString *)brand fuel:(NSString *)fuel displacement:(NSString *)displacement ratedPower:(NSString *)ratedPower fTireDimen:(NSString *)fTireDimen bTireDimen:(NSString *)bTireDimen color:(NSString *)color leasingCar:(NSString *)leasingCar leasingCompany:(NSString *)leasingCompany companyCar:(NSString *)companyCar companyName:(NSString *)companyName extras:(NSString *)extras regTicketPhoto:(NSString *)regTicketPhoto WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        //        NSString *progressMessage=@"Fetching..";
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSDictionary *parameters = @ {@"insurer" :insurerId, @"registration_number" :regNum, @"registration_date" :regDate, @"issue_date" :issueDate, @"first_registration" :firstReg, @"vin" :vin, @"type" :type, @"trade_name" :tradeName, @"brand" :brand, @"fuel" :fuel, @"displacement" :displacement, @"rated_power" :ratedPower, @"tire_dimension_front" :fTireDimen, @"tire_dimension_back" :bTireDimen, @"color" :color, @"leasing_car" :leasingCar, @"leasing_company" :leasingCompany, @"company_car" :companyCar, @"company_name" :companyName, @"extras" :extras, @"reg_ticket_photo" :regTicketPhoto};
        
        NSLog(@"parameters : %@",parameters);
        
        //        NSString *path = [NSString stringWithFormat:@"Cars/%@",carId];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:NULL];
        NSMutableURLRequest *request = [self requestWithMethod:@"POST"
                                                          path:kWebServiceUserCars
                                                    parameters:nil];
        
        NSMutableData *body = [NSMutableData data];
        
        // Append of the body with your data
        [body appendData:jsonData];
        [request setHTTPBody:body];
        
        // Set request method POST
        [request setHTTPMethod:@"POST"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        if([[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultsUserAlreadyLoggedIn]){
            NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
            [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        }
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)uploadCarDocumentPhotoWithImage:(UIImage *)image andCarId:(NSString *)carId WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        NSData *imageData = UIImageJPEGRepresentation(image, 0.4);
        
        NSLog(@"document image data : %@ and carid : %@", imageData, carId);
        
        NSMutableURLRequest *request = [self multipartFormRequestWithMethod:@"POST" path:kWebServiceUserCarDocumentPhoto parameters:nil constructingBodyWithBlock:^(id <AFMultipartFormData>formData) {
            [formData appendPartWithFileData:imageData
                                        name:@"photo"
                                    fileName:@"carPhotoUpload.jpg" mimeType:@"image/jpeg"];
            
            [formData appendPartWithFormData:[carId dataUsingEncoding:NSUTF8StringEncoding]
                                        name:@"car_id"];
        }];
        
        // Set request Headers
        //        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        //        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        if([[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultsUserAlreadyLoggedIn]){
            NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
            [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        }
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)uploadCarDocumentPhotoWithImage:(UIImage *)image WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        NSData *imageData = UIImageJPEGRepresentation(image, 0.4);
        
        NSLog(@"document image data : %@", imageData);
        
        NSMutableURLRequest *request = [self multipartFormRequestWithMethod:@"POST" path:kWebServiceUserCarDocumentPhoto parameters:nil constructingBodyWithBlock:^(id <AFMultipartFormData>formData) {
            [formData appendPartWithFileData:imageData
                                        name:@"photo"
                                    fileName:@"carPhotoDocument.jpg" mimeType:@"image/jpeg"];
            
//            [formData appendPartWithFormData:[carId dataUsingEncoding:NSUTF8StringEncoding]
//                                        name:@"car_id"];
        }];
        
        // Set request Headers
        //        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        //        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        if([[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultsUserAlreadyLoggedIn]){
            NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
            [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        }
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)getUserAccidentsWithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSMutableURLRequest *request = [self requestWithMethod:@"GET"
                                                          path:kWebServiceGetUserAccidents
                                                    parameters:nil];
        NSMutableData *body = [NSMutableData data];
        [request setHTTPBody:body];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
        [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)userForgotPasswordWithUserEmail:(NSString *)userEmail WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        //        NSString *progressMessage=@"Fetching..";
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSDictionary *parameters = @ {@"email" :userEmail};
        NSLog(@"parameters : %@",parameters);
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:NULL];
        
        NSMutableURLRequest *request = [self requestWithMethod:@"POST"
                                                          path:kWebServiceForgotPassword
                                                    parameters:nil];
        
        NSMutableData *body = [NSMutableData data];
        
        // Append of the body with your data
        [body appendData:jsonData];
        [request setHTTPBody:body];
        
        // Set request method POST
        [request setHTTPMethod:@"POST"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)uploadUserProfileWithUserName:(NSString*)name andAnrede:(NSString*)anrede andFamily:(NSString*)family andBirthdate:(NSString*)birthdate andStreet:(NSString*)street andStreetNum:(NSString*)streetNum andZip:(NSString*)zip andResidence:(NSString*)residence andMobile:(NSString*)mobile andCompany:(NSString*)company andPhone:(NSString*)phone andPhoneSecond:(NSString*)secondPhone andMobileSecond:(NSString*)mobileSecond country:(NSString*)country city:(NSString*)city WithCompletionBlock:(void(^)(NSData *data))block
{
    if ([self connectedToWiFi]){
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        NSDictionary *parameters;
        //        if([company length] > 0 && [phone length]>0){
        parameters = @ {@"name" :[name description], @"anrede" :[anrede description], @"family" :[family description], @"zip" :[zip description], @"birthdate" : [birthdate description], @"street": [street description], @"street_number": [streetNum description], @"residence": [residence description], @"mobile": [mobile description],@"company": [company description], @"phone": [phone description], @"phone2": [secondPhone description], @"mobile2": [mobileSecond description], @"country": [country description], @"residence":[city description]};
        //        }
        //        else{
        //            parameters = @ {@"name" :[name description], @"anrede" :[anrede description], @"family" :[family description], @"zip" :[zip description], @"birthdate" : [birthdate description], @"street": [street description], @"residence": [residence description], @"mobile": [mobile description]};
        //        }
        
        NSLog(@"parameters : %@",parameters);
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:NULL];
        
        NSMutableURLRequest *request = [self requestWithMethod:@"PUT"
                                                          path:kWebServiceUserProfile
                                                    parameters:nil];
        NSMutableData *body = [NSMutableData data];
        [body appendData:jsonData];
        [request setHTTPBody:body];
        //        [request setHTTPMethod:@"PUT"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
        [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)uploadRecordingWithAudioFilePath:(NSURL *)audioFilePathUrl WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
//        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
//                                                                        message:nil];
        NSLog(@"%@",audioFilePathUrl);
        NSData *audioFileData = [NSData dataWithContentsOfURL:audioFilePathUrl];
        NSLog(@"%@",audioFileData);
        
        NSMutableURLRequest *request = [self multipartFormRequestWithMethod:@"POST" path:kWebServiceUploadAudioRecording parameters:nil constructingBodyWithBlock:^(id <AFMultipartFormData>formData) {
            [formData appendPartWithFileData:audioFileData
                                        name:@"file"
                                    fileName:@"audioRecordingFile.mp3" mimeType:@"audio/mpeg"];
            
        }];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Success
            block(responseObject);
//            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)downloadAudioWithURL:(NSString*)Url WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        //Set Params
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:60];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/json" forHTTPHeaderField: @"Content-Type"];
        [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        [request setURL:[NSURL URLWithString:Url]];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   NSLog(@"responce : %@",response);
                                   //                                   NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   //                                   NSLog(@"responce %@",responseString);
                                   NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                                   
                                   if ([httpResponse statusCode] == 200) {
                                       NSLog(@"success");
                                       block(data);
                                   }
                                   [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
                               }];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)uploadCarPhotoWithImage:(UIImage *)image andAccidentId:(NSString*)accidentId WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        NSData *imageData = UIImageJPEGRepresentation(image, 0.4);
        
        //        NSString *key1 = @"123";
        //        NSString *key2 = @"asdf";
        
        //        NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
        //        [self setDefaultHeader:@"Authorization" value:authHeader];
        
        NSMutableURLRequest *request = [self multipartFormRequestWithMethod:@"POST" path:kWebServiceUploadCarPhoto parameters:nil constructingBodyWithBlock:^(id <AFMultipartFormData>formData) {
            [formData appendPartWithFileData:imageData
                                        name:@"photo"
                                    fileName:@"carPhoto.jpg" mimeType:@"image/jpeg"];
            
            
            [formData appendPartWithFormData:[accidentId dataUsingEncoding:NSUTF8StringEncoding]
                                        name:@"accident_id"];
            /*
             [formData appendPartWithFormData:[key2 dataUsingEncoding:NSUTF8StringEncoding]
             name:@"key2"];
             */
            
            // etc.
            //            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)uploadCarOtherInformationWithRegistrationNumber:(NSString *)registrationNumber andRegdate:(NSString *)regDate andIssueDate:(NSString *)issueDate andFirstReg:(NSString *)firstReg andVin:(NSString *)vin andType:(NSString *)type andTradeName:(NSString *)tradeName andFuel:(NSString *)fuel andDisplacement:(NSString *)displacement andRatedPower:(NSString *)ratedPower andTireDimFront:(NSString *)tireDimFront andtireDimBack:(NSString *)tireDimBack andColor:(NSString *)color andExtras:(NSString *)extras andCarType:(NSString *)carType andComercialVichle:(NSString *)commercialVichle andInsurer:(NSString *)insurer andBrand:(NSString *)brand andFile:(UIImage *)file WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        //        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
        //                                                                        message:nil];
        NSData *imageData;
        if(file != nil){
            imageData = UIImagePNGRepresentation(file);
        }
        
        NSLog(@"insurer name : %@ and registration num : %@",insurer, registrationNumber);
        
        NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
        //        [self setDefaultHeader:@"Authorization" value:authHeader];
        //
        //        NSDictionary *parameters = @ {@"insurer" :[insurer description], @"registration_number" :[registrationNumber description]};
        //
        //        NSLog(@"parameters : %@",parameters);
        //        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:NULL];
        
        //        NSMutableURLRequest *request = [self requestWithMethod:@"POST"
        //                                                          path:kWebServiceUploadCarInfo
        //                                                    parameters:nil];
        //        NSMutableData *body = [NSMutableData data];
        //        [body appendData:jsonData];
        //        [request setHTTPBody:body];
        //        [request setHTTPMethod:@"POST"];
        //
        //        // Set request Headers
        //        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        //        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        
        //create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        //Set Params
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:60];
        [request setHTTPMethod:@"POST"];
        
        //Create boundary, it can be anything
        NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        //Populate a dictionary with all the regular values you would like to send.
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        [parameters setValue:insurer forKey:@"insurer"];
        [parameters setValue:registrationNumber forKey:@"registration_number"];
        
        // add params (all params are strings)
        for (NSString *param in parameters) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        NSString *FileParamConstant = @"imageParamName";
        
        //         NSData *imageData = UIImageJPEGRepresentation(imageObject, 1);
        
        //Assuming data is not nil we add this to the multipart form
        if (imageData)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.png\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type:image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        //Close off the request with the boundary
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the request
        [request setHTTPBody:body];
        
        // set URL
        [request setURL:[NSURL URLWithString:@"http://api.unfallhelden.de/Cars"]];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   NSLog(@"responce : %@",response);
                                   NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   NSLog(@"responce %@",responseString);
                                   NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                                   
                                   if ([httpResponse statusCode] == 200) {
                                       NSLog(@"success");
                                   }
                               }];
        /*
         NSMutableURLRequest *request = [self multipartFormRequestWithMethod:@"POST" path:kWebServiceUploadCarInfo parameters:nil constructingBodyWithBlock:^(id <AFMultipartFormData>formData) {
         if(file != nil){
         [formData appendPartWithFileData:imageData
         name:@"file"
         fileName:@"carPhoto.png" mimeType:@"image/png"];
         }
         
         [formData appendPartWithFormData:[@"115" dataUsingEncoding:NSUTF8StringEncoding]
         name:@"insurer"];
         [formData appendPartWithFormData:[registrationNumber dataUsingEncoding:NSUTF8StringEncoding]
         name:@"registration_number"];
         }];*/
        
        NSLog(@"request : %@",request.description);
        
        /*
         AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
         [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
         // Success
         block(responseObject);
         [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
         
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         // Failure
         NSLog(@"Error: %@", error);
         [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
         [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
         }];
         [operation start];*/
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)getGeneralDataWithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSMutableURLRequest *request = [self requestWithMethod:@"GET"
                                                          path:kWebServiceGetGeneralData
                                                    parameters:nil];
        NSMutableData *body = [NSMutableData data];
        [request setHTTPBody:body];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)getNotificationsWithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        //        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
        //                                                                        message:nil];
        
        NSMutableURLRequest *request = [self requestWithMethod:@"GET"
                                                          path:kWebServiceGetNotifications
                                                    parameters:nil];
        NSMutableData *body = [NSMutableData data];
        [request setHTTPBody:body];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
        [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Success
            block(responseObject);
            //            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)deleteCarWithCarId:(NSString *)carid andCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        //        NSString *progressMessage=@"Fetching..";
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSURL *deleteCarApi = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.unfallhelden.de/Cars/%@",carid]];
        
        //Create the URL request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:deleteCarApi];
        // Set request method POST
        [request setHTTPMethod:@"DELETE"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
        [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        //Start the request for the data
        [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            //If data were received
            if (data) {
                // Success
                block(data);
            }
            
            //No data received
            else {
                // Failure
                NSLog(@"Error: %@", error);
                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
                });
            }
        }];
        
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)getUserCarsWithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSMutableURLRequest *request = [self requestWithMethod:@"GET"
                                                          path:kWebServiceUserCars
                                                    parameters:nil];
        NSMutableData *body = [NSMutableData data];
        [request setHTTPBody:body];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
        [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)getCarBrandsWithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        
        NSMutableURLRequest *request = [self requestWithMethod:@"GET"
                                                          path:kWebServiceUserCarBrands
                                                    parameters:nil];
        NSMutableData *body = [NSMutableData data];
        [request setHTTPBody:body];
        //        [request setHTTPMethod:@"GET"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)getUserProfileWithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        //        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
        //                                                                        message:nil];
        
        NSMutableURLRequest *request = [self requestWithMethod:@"GET"
                                                          path:kWebServiceUserProfile
                                                    parameters:nil];
        NSMutableData *body = [NSMutableData data];
        [request setHTTPBody:body];
        //        [request setHTTPMethod:@"GET"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
        [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Success
            block(responseObject);
            //            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)changePasswordWithOldPassword:(NSString *)oldPassword newPswrd:(NSString *)newPassword confirmPassword:(NSString *)confirmPassword WithCompletionBlock:(void (^)(NSData *))block
{
    if ([self connectedToWiFi]){
        [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                        message:nil];
        NSDictionary *parameters = @ {@"current_password" :oldPassword, @"password" :newPassword, @"cpassword" :confirmPassword};
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:NULL];
        
        NSMutableURLRequest *request = [self requestWithMethod:@"PUT"
                                                          path:kWebServiceChangePassword
                                                    parameters:nil];
        NSMutableData *body = [NSMutableData data];
        // Append of the body with your data
        [body appendData:jsonData];
        [request setHTTPBody:body];
        //        [request setHTTPMethod:@"GET"];
        
        // Set request Headers
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        NSString *authHeader = [NSString stringWithFormat:@"access_token=\"%@\"",[[[PDKeychainBindings sharedKeychainBindings] objectForKey:kKeychainUserAccessToken] description]];
        [request setValue:authHeader forHTTPHeaderField:@"Authorization"];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Success
            block(responseObject);
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Failure
            NSLog(@"Error: %@", error);
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Keine Verbindung zum Server. Bitte versuchen Sie es später erneut."];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        }];
        [operation start];
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

#pragma mark -
#pragma mark WifiConeectivity
-(bool)connectedToWiFi
{
    Reachability *r = [Reachability reachabilityWithHostName:@"www.apple.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    bool result = false;
    if (internetStatus == ReachableViaWiFi )
    {
        result = true;
    }
    else if(internetStatus == ReachableViaWWAN){
        result = true;
    }
    return result;
}

@end
