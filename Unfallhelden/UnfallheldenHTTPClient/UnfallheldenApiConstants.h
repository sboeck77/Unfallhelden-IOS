//
//  Created by adilanwer on 9/27/14.
//

//#define kWebserviceBaseUrl @"http://dev.e7clients.com/webmedia7/unfallhelden_v3_api/"
#define kWebserviceBaseUrl @"http://api.unfallhelden.de/"
#define kWebServiceRegisterUser @"Auth/Register"
#define kWebServiceUserLogin @"Auth/Login"
#define kWebServiceFBLogin @"Auth/FacebookLogin"
#define kWebServiceForgotPassword @"Auth/Forgot"
#define kWebServiceUserProfile @"Users"
#define kWebServiceUploadCarPhoto @"Accidents/Photo"
#define kWebServiceGetGeneralData @"Data"
#define kWebServiceGetNotifications @"Notifications"
#define kWebServiceGetUserAccidents @"Accidents"
#define kWebServiceUploadCarInfo @"Cars"
#define kWebServiceUploadAudioRecording @"Accidents/File"
#define kWebServiceUserCars @"Cars"
#define kWebServiceUserCarDocumentPhoto @"Cars/Photo"
#define kWebServiceUpdateUserCar @"Cars/Id"
#define kWebServiceUserCarBrands @"Brands"
#define kWebServiceReportAccident @"Accidents"
#define kWebServiceChangePassword @"Users/Password"
#define kWebServiceCheckEmailExist @"Users/isExistEmail/"