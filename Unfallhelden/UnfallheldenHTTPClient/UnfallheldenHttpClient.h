//
//  Created by adilanwer on 9/27/14.
//

#import <Foundation/Foundation.h>
#import "AFHTTPClient.h"

@interface UnfallheldenHttpClient : AFHTTPClient

+(UnfallheldenHttpClient *)sharedClient;

-(void)registerUserWithEmail:(NSString *)email andPassword:(NSString *)password andName:(NSString *)name andFamily:(NSString *)family andGender:(NSString*)gender  andPhone:(NSString *)phone andTerms:(NSString *)terms andFBUserId:(NSString*)fbUserId WithCompletionBlock:(void (^)(NSData *))block;

-(void)loginUserWithEmail:(NSString*)email andPassword:(NSString*)password WithCompletionBlock:(void(^)(NSData *data))block;

-(void)loginUserViaFacebookWithUserId:(NSString*)userId andAccessToken:(NSString *)accessToken  WithCompletionBlock:(void(^)(NSData *data))block;

-(void)userForgotPasswordWithUserEmail:(NSString*)userEmail WithCompletionBlock:(void(^)(NSData *data))block;

-(void)uploadUserProfileWithUserName:(NSString*)name andAnrede:(NSString*)anrede andFamily:(NSString*)family andBirthdate:(NSString*)birthdate andStreet:(NSString*)street andStreetNum:(NSString*)streetNum andZip:(NSString*)zip andResidence:(NSString*)residence andMobile:(NSString*)mobile andCompany:(NSString*)company andPhone:(NSString*)phone andPhoneSecond:(NSString*)secondPhone andMobileSecond:(NSString*)mobileSecond country:(NSString*)country city:(NSString*)city WithCompletionBlock:(void(^)(NSData *data))block;

-(void)uploadCarPhotoWithImage:(UIImage *)image andAccidentId:(NSString*)accidentId WithCompletionBlock:(void (^)(NSData *))block;

-(void)uploadCarOtherInformationWithRegistrationNumber:(NSString*)registrationNumber andRegdate:(NSString*)regDate andIssueDate:(NSString*)issueDate andFirstReg:(NSString*)firstReg andVin:(NSString*)vin andType:(NSString*)type andTradeName:(NSString*)tradeName andFuel:(NSString*)fuel andDisplacement:(NSString*)displacement andRatedPower:(NSString*)ratedPower andTireDimFront:(NSString*)tireDimFront andtireDimBack:(NSString*)tireDimBack andColor:(NSString*)color andExtras:(NSString*)extras andCarType:(NSString*)carType andComercialVichle:(NSString*)commercialVichle andInsurer:(NSString*)insurer andBrand:(NSString*)brand andFile:(UIImage *)file WithCompletionBlock:(void(^)(NSData *data))block;

-(void)reportAccidentWithCarId:(NSString *)carId carRegNum:(NSString *)carRegNum name:(NSString *)name family:(NSString *)lastName phone:(NSString *)phone email:(NSString *)email terms:(BOOL)terms address:(NSString *)address postCode:(NSString *)postCode city:(NSString *)city accidentLat:(NSString *)latitude accidentLongitude:(NSString *)accidentLongitude accidentDate:(NSString *)accidentDate accidentTime:(NSString *)accidentTime accidentDescription:(NSString *)accidentDescription opponentRegNum:(NSString *)opponentRegNum opponentCarBrand:(NSString *)opponentCarBrand opponentCarModel:(NSString *)opponentCarModel acFirstName:(NSString *)acFirstName acLastName:(NSString *)acLastName acCompanyCar:(BOOL)isCompanyCar acCompanyName:(NSString*)acCompanyName acCompanyLeasing:(BOOL)isLeasingCompany  acLeasingCompany:(NSString *)acLeasingCompany acPhone:(NSString *)acPhone acAddress:(NSString *)acAddress acPostCode:(NSString *)acpostCode acCity:(NSString *)acCity acStreet:(NSString *)acStreet acStreetNum:(NSString *)acStreetNum acInsurer:(NSString *)acInsurer audio:(NSMutableArray *)audioFileUrl witnesses:(NSMutableArray *)witnesses country:(NSString*)country weather:(NSString*)weather WithCompletionBlock:(void (^)(NSData *))block;

-(void)updateUserCarWithInsurer:(NSString*)insurer carId:(NSString*)carId insurerId:(NSString*)insurerId regNum:(NSString*)regNum regDate:(NSString*)regDate issueDate:(NSString*)issueDate firstReg:(NSString*)firstReg vin:(NSString*)vin type:(NSString*)type tradeName:(NSString*)tradeName brand:(NSString*)brand fuel:(NSString*)fuel displacement:(NSString*)displacement ratedPower:(NSString*)ratedPower fTireDimen:(NSString*)fTireDimen bTireDimen:(NSString*)bTireDimen color:(NSString*)color leasingCar:(NSString*)leasingCar leasingCompany:(NSString*)leasingCompany companyCar:(NSString*)companyCar companyName:(NSString*)companyName extras:(NSString*)extras regTicketPhoto:(NSString*)regTicketPhoto WithCompletionBlock:(void (^)(NSData *))block;


-(void)addCarWithinsurerId:(NSString*)insurerId regNum:(NSString*)regNum regDate:(NSString*)regDate issueDate:(NSString*)issueDate firstReg:(NSString*)firstReg vin:(NSString*)vin type:(NSString*)type tradeName:(NSString*)tradeName brand:(NSString*)brand fuel:(NSString*)fuel displacement:(NSString*)displacement ratedPower:(NSString*)ratedPower fTireDimen:(NSString*)fTireDimen bTireDimen:(NSString*)bTireDimen color:(NSString*)color leasingCar:(NSString*)leasingCar leasingCompany:(NSString*)leasingCompany companyCar:(NSString*)companyCar companyName:(NSString*)companyName extras:(NSString*)extras regTicketPhoto:(NSString*)regTicketPhoto WithCompletionBlock:(void (^)(NSData *))block;

-(void)registerUserWithAccidentHashWithEmail:(NSString *)email andPassword:(NSString *)password andName:(NSString *)name andFamily:(NSString *)family andGender:(NSString*)gender  andPhone:(NSString *)phone andTerms:(NSString *)terms andFBUserId:(NSString*)fbUserId WithCompletionBlock:(void (^)(NSData *))block;

-(void)uploadRecordingWithAudioFilePath:(NSURL *)audioFilePathUrl WithCompletionBlock:(void (^)(NSData *))block;

-(void)uploadCarDocumentPhotoWithImage:(UIImage *)image andCarId:(NSString *)carId WithCompletionBlock:(void (^)(NSData *))block;

-(void)downloadAudioWithURL:(NSString*)Url WithCompletionBlock:(void (^)(NSData *))block;

-(void)getGeneralDataWithCompletionBlock:(void(^)(NSData *data))block;

-(void)getWeatherWithLatitude:(double)latitude longitude:(double)longitude WithCompletionBlock:(void(^)(NSData *data))block;

-(void)getUserProfileWithCompletionBlock:(void(^)(NSData *data))block;

-(void)deleteCarWithCarId:(NSString*)carid andCompletionBlock:(void(^)(NSData *data))block;

-(void)getUserCarsWithCompletionBlock:(void(^)(NSData *data))block;

-(void)getCarBrandsWithCompletionBlock:(void(^)(NSData *data))block;

-(void)checkIfEmailAlreadyExistWithEmail:(NSString*)email andCompletionBlock:(void(^)(NSData *data))block;

-(void)getUserAccidentsWithCompletionBlock:(void(^)(NSData *data))block;

-(void)getNotificationsWithCompletionBlock:(void(^)(NSData *data))block;

-(void)changePasswordWithOldPassword:(NSString*)oldPassword newPswrd:(NSString*)newPassword confirmPassword:(NSString*)confirmPassword WithCompletionBlock:(void(^)(NSData *data))block;

-(void)uploadCarDocumentPhotoWithImage:(UIImage *)image WithCompletionBlock:(void (^)(NSData *))block;

-(bool)connectedToWiFi;

@end
