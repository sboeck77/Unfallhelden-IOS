//
//  HIPlacesConstants.m
//  HIPlaces
//
//  Created by Hozefa Indorewala on 22/01/15.
//  Copyright (c) 2015 Hozefa Indorewala. All rights reserved.
//

#import "HIPlacesConstants.h"

NSString * const HIInvalidKeyException                      = @"HIInvalidKeyException";
NSString * const HIInvalidInputException                    = @"HIInvalidInputException";
NSString * const HIInvalidLocationException                 = @"HIInvalidLocationException";
NSString * const HIInvalidPlaceTypeException                = @"HIInvalidPlaceTypeException";

NSString * const HIPlacesErrorDomain                        = @"HIPlacesErrorDomain";
NSUInteger const HIPlacesErrorZeroResults                   = 101;
NSUInteger const HIPlacesErrorOverQueryLimit                = 102;
NSUInteger const HIPlacesErrorRequestDenied                 = 103;
NSUInteger const HIPlacesErrorInvalidRequest                = 104;
NSUInteger const HIPlacesErrorNotFound                      = 105;
NSUInteger const HIPlacesErrorUnkownError                   = 106;
NSUInteger const HIPlacesErrorInvalidJSON                   = 107;
NSUInteger const HIPlacesErrorConnectionFailed              = 108;