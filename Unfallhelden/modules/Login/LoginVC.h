//
//  LoginVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/16/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacebookHelper.h"
#import "UnfallheldenCustomPopup.h"
@class TPKeyboardAvoidingScrollView;

@interface LoginVC : UnfallheldenParentVC<UITextFieldDelegate, FBCustomLoginDelegate, fBConnectDelegate, UnfallHeldenPopupDelegate>

@property (nonatomic, weak) IBOutlet UITextField *emailField;
@property (nonatomic, weak) IBOutlet UITextField *passwordField;
@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, strong) UnfallheldenCustomPopup *customPopup;
@property (nonatomic, weak) IBOutlet UIButton *fbLoginBtn;
@property (nonatomic, weak) IBOutlet UIButton *forgotPasswordBtn;
@property (nonatomic, weak) IBOutlet UIButton *registerBtn;
@property (nonatomic) int viewMoveCount;
@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
//@property (weak, nonatomic) IBOutlet FBLoginView *loginButton;

-(IBAction)registerBtnPress:(id)sender;
-(IBAction)userLogin:(id)sender;
-(IBAction)fbLogin:(id)sender;
-(IBAction)forgotPassword:(id)sender;

@end
