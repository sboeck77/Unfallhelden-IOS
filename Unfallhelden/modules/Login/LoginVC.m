//
//  LoginVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/16/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "LoginVC.h"
#import "RegistrationVC.h"
#import "HomeScreenVC.h"
#import "FacebookHelper.h"
#import "MFSideMenu.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface LoginVC ()
@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;

@end

@implementation LoginVC
@synthesize passwordField,emailField,containerView,customPopup,fbLoginBtn;
@synthesize forgotPasswordBtn,registerBtn,viewMoveCount,scrollView;

- (void)viewDidLoad {
    [super viewDidLoad];

    //GoogleAnalytics
//    self.screenName = @"LoginScreen";
    
    // Do any additional setup after loading the view from its nib.
    //    self.navigationController.navigationBar.translucent = NO;
    [self setTitle:@"Einloggen"];
    [super setupMenuBarButtonItems];
    [self setupView];
    [self.scrollView contentSizeToFit];
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"LoginScreen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)setupView
{
    for(UIView *vw in self.containerView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
        }
        else if([vw isKindOfClass:[UIButton class]]) {
            ((UIButton*)vw).layer.cornerRadius = 3.0f;
            ((UIButton*)vw).layer.masksToBounds = YES;
            ((UIButton*)vw).titleLabel.adjustsFontSizeToFitWidth = YES;
            ((UIButton*)vw).titleLabel.minimumScaleFactor = .5f;
            [((UIButton*)vw).titleLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultLow
                                                                        forAxis:UILayoutConstraintAxisHorizontal];
            [((UIButton*)vw).titleLabel setFont:[UIFont regularUnfallheldenFontWithSize:19]];
        }
    }
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:self.forgotPasswordBtn.titleLabel.text];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    self.forgotPasswordBtn.titleLabel.attributedText = attributeString;
    self.forgotPasswordBtn.titleLabel.textColor = [UIColor lightGrayColor];
    [self.forgotPasswordBtn.titleLabel setFont:[UIFont regularUnfallheldenFontWithSize:12]];
    
    NSMutableAttributedString *attributeString2 = [[NSMutableAttributedString alloc] initWithString:self.registerBtn.titleLabel.text];
    [attributeString2 addAttribute:NSUnderlineStyleAttributeName
                             value:[NSNumber numberWithInt:1]
                             range:(NSRange){0,[attributeString2 length]}];
    self.registerBtn.titleLabel.attributedText = attributeString2;
    self.registerBtn.titleLabel.textColor = [UIColor lightGrayColor];
    [self.registerBtn.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:12]];
    
    //    [tmp_testBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 20, 0)];
    //    [tmp_testBtn setImage:[UIImage imageNamed:@"France"] forState:UIControlStateNormal];
    //    [self.fbLoginBtn setTitleEdgeInsets:UIEdgeInsetsMake(6, 10, 0, 0)];
    //    [self.fbLoginBtn setTitle:@"testbutton" forState:UIControlStateNormal];
}

-(void)fbLogin:(id)sender
{
    //    MenuVC *menuView = [[MenuVC alloc]init];
    //    [FacebookHelper sharedInstance].navController = self.navigationController;
    //    [FacebookHelper sharedInstance].viewControllerForPush = menuView;
    [FacebookHelper sharedInstance].customLoginDelegate = self;
    [[FacebookHelper sharedInstance] login];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)registerBtnPress:(id)sender
{
    RegistrationVC *registerationVC = [[RegistrationVC alloc] init];
    registerationVC.calledFrom = @"login";
    [self.navigationController pushViewController:registerationVC animated:YES];
}

#pragma mark - FBCustomLogin Delegate method implementation-
-(void)fbloggedInSuccessfull
{
    [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                    message:nil];
    if ([[UnfallheldenUtilities sharedSingletonInstance] connectedToWiFi])
    {
        [FacebookHelper sharedInstance].delegate = self;
        [[FacebookHelper sharedInstance] getUserInfo];
    }
    
    else
    {
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Application requires internet\n connection. Please connect to a data network\n and try again."];
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }
}

-(void)fbConnectDidFailWithError
{
    [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Error occur while getting the data from facebook. Please try again"];
}

- (void)myFBConnectRequest:(FBRequest *)request didLoad:(id)result;
{
    [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    NSString *fbUserId = [result valueForKey:@"id"];
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"FBAccessTokenKey"];
    
    [[UnfallheldenHttpClient sharedClient] loginUserViaFacebookWithUserId:fbUserId andAccessToken:accessToken WithCompletionBlock:^(NSData *data) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *dataDictionary = [responseString JSONValue];
        NSLog(@"data dictionary : %@",dataDictionary);
        int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
        if(status == 1){
            
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Erfolreich eingeloggt" andMessage:@"Sie sind nun in der Unfallhelden App eingeloggt"];
            HomeScreenVC *homeScreenVC = [[HomeScreenVC alloc] init];
            [self.navigationController pushViewController:homeScreenVC animated:YES];
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Login fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Login. Bitte versuchen Sie es erneut."];
        }
    }];
}

-(void)userLogin:(id)sender
{
    if([self.emailField.text length] > 0 && [self.passwordField.text length] > 0){
        if([[UnfallheldenUtilities sharedSingletonInstance] validateEmail:[self.emailField.text description]]){
            [[UnfallheldenHttpClient sharedClient] loginUserWithEmail:self.emailField.text andPassword:self.passwordField.text WithCompletionBlock:^(NSData *data) {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dataDictionary = [responseString JSONValue];
                //            NSDictionary *userData = [[dataDictionary valueForKey:@"data"] description];
                int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                if(status == 1){
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Erfolgreich eingeloggt" andMessage:@"Sie sind nun in der Unfallhelden App eingeloggt."];
                    HomeScreenVC *homeScreenVC = [[HomeScreenVC alloc] init];
                    homeScreenVC.calledFrom = @"login";
                    [self.navigationController pushViewController:homeScreenVC animated:YES];
                    NSLog(@"%@",[dataDictionary valueForKey:@"data"]);
                    NSLog(@"%@",[[[dataDictionary valueForKey:@"data"] valueForKey:@"token"] description]);
                    //token
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[[dataDictionary valueForKey:@"data"] valueForKey:@"token"] description] forKey:kKeychainUserAccessToken];
                    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:kUserDefaultsUserAlreadyLoggedIn];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNameUserLogin object:nil userInfo:nil];
                }
                else{
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Login fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Login. Bitte versuchen Sie es erneut."];
                }
            }];
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Diese E-Mail Adresse liegt uns nicht vor."];
        }
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte geben Sie E-Mail und Password ein"];
    }
}

-(void)forgotPassword:(id)sender
{
    self.customPopup = [[UnfallheldenCustomPopup alloc] init];
    
    self.customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    self.customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [self.view addSubview:self.customPopup];
    self.customPopup.frame = self.view.frame;
    
    // Center Support view
    //    self.customPopup.center = self.view.center;
    
    // Alpha
//    self.customPopup.unfallheldenPopup.alpha = 0.0f;
    //    self.customPopup.alpha = 0.0f;
//    self.customPopup.unfallheldenPopupBackground.alpha = 0.0f;
    
    [self.customPopup displayPopup];
    //    customPopup.emailField.delegate = self;
    customPopup.unfallheldenPopupDelegate = self;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

#pragma mark - Unfallhelden Custom Popup Delegate Methods
-(void)emailSuccessfullySent
{
    [self.customPopup closePopup];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
