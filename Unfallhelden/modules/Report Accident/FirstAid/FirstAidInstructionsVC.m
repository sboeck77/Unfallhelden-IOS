//
//  FirstAidInstructionsVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/8/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "FirstAidInstructionsVC.h"
#import "TutorialView.h"

@interface FirstAidInstructionsVC ()

@end

@implementation FirstAidInstructionsVC
@synthesize tutorialScrollView,tutorialPageControl,tutorialTitleLbl,tutorialView,tutorialText;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [super setupMenuBarButtonItems];
    [self setTitle:@"Erste Hilfe Anleitung"];
    pageControlBeingUsed = NO;
    self.tutorialText.font = [UIFont regularUnfallheldenFontWithSize:14];
    self.tutorialTitleLbl.text = @"Direkt nach dem Unfall";
    //GoogleAnalytics
    self.screenName = @"FirstAidScreen";
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.view layoutIfNeeded];
    [self.view layoutSubviews];
    
    NSArray *colors = [NSArray arrayWithObjects:[UIColor redColor], [UIColor greenColor], [UIColor blueColor], nil];
    CGFloat screenWidth = 0.0f;
    if(IS_IPHONE_4){
        screenWidth = 320;
    }
    else if (IS_IPHONE_5){
        screenWidth = 320;
    }
    else if(IS_IPHONE_6){
        screenWidth = 375;
    }
    else if (IS_IPHONE_6_Plus){
        screenWidth = 414;
    }
    
//    Erste Hilfe Anleitung
    
    for (int i = 0; i < colors.count; i++) {
        CGRect frame;
        frame.origin.x = self.view.frame.size.width * i;
        frame.origin.y = 0;
        frame.size  = CGSizeMake(screenWidth, self.tutorialScrollView.frame.size.height);
        
        if(i == 0){
            TutorialView *tView = [[TutorialView alloc] init];
            tView.frame = frame;
            [tView setupView];
            tView.tutorialImage.image = [UIImage imageNamed:@"tutorialImageFirst"];
//            tView.tutorialText.text = [NSString stringWithFormat:@"Bewahren Sie Ruhe\r\nZiehen Sie Ihre Warnweste an\r\nAchten Sie auf vorbeifahrende Autos\r\nStellen Sie ein Warndreieck am Fahrbahnrand auf\r\n(Entfernung zur Unfallstelle: 100m auf der Landstraße, 200m auf der Autobahn)"];
            tView.tutorialText.text = [NSString stringWithFormat:@"1. Bewahren Sie Ruhe\r\n2. Ziehen Sie Ihre Warnweste an\r\n3. Achten Sie auf vorbeifahrende Autos\r\n4. Stellen Sie ein Warndreieck am Fahrbahnrand auf"];
//            self.tutorialTitleLbl.text = @"Direkt nach dem Unfall";
            [self.tutorialScrollView addSubview:tView];
        }
        else if(i == 1){
            TutorialView *tView = [[TutorialView alloc] init];
            tView.frame = frame;
            [tView setupView];
            tView.tutorialImage.image = [UIImage imageNamed:@"tutorialImageSecond"];
            tView.tutorialText.text = [NSString stringWithFormat:@"Bei Personenschäden:\r\n1. Setzen Sie einen Notruf ab: 112\r\n2. Wo ist der Unfall?\r\n3. Was ist geschehen?\r\n4. Wie viele Verletzte/Betroffene sind zu versorgen?\r\n5. Welche Verletzungen oder Krankheitszeichen haben die Betroffenen?\r\n6. Warten Sie immer auf Rückfragen des Notrufes"];
//            self.tutorialTitleLbl.text = @"Gibt es Verletzte?";
            [self.tutorialScrollView addSubview:tView];
        }
        else if (i == 2){
            TutorialView *tView = [[TutorialView alloc] init];
            tView.frame = frame;
            [tView setupView];
            tView.tutorialImage.image = [UIImage imageNamed:@"tutorialImageThird"];
            tView.tutorialText.text = [NSString stringWithFormat:@"1. Helfen Sie der Person aus dem Fahrzeug\r\n2. Beruhigen Sie die Person\r\n3. Beginnen Sie eine Herz-Lungen-Massage\r\n4. Stillen Sie blutende Wunden\r\n5. Bringen Sie die Person in eine stabile Seitenlage"];
//            self.tutorialTitleLbl.text = @"Erste Hilfe Maßnahmen";
            [self.tutorialScrollView addSubview:tView];
        }
    }
    
    self.tutorialScrollView.contentSize = CGSizeMake(self.tutorialScrollView.frame.size.width * colors.count, self.tutorialScrollView.frame.size.height);
    
    self.tutorialPageControl.currentPage = 0;
    self.tutorialPageControl.numberOfPages = colors.count;
    self.tutorialView.hidden = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (!pageControlBeingUsed) {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = self.tutorialScrollView.frame.size.width;
        int page = floor((self.tutorialScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.tutorialPageControl.currentPage = page;
        if(page == 0){
            self.tutorialTitleLbl.text = @"Direkt nach dem Unfall";
        }
        else if(page == 1){
            self.tutorialTitleLbl.text = @"Gibt es Verletzte?";
        }
        else if (page == 2){
            self.tutorialTitleLbl.text = @"Erste Hilfe Maßnahmen";
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changePage {
    // Update the scroll view to the appropriate page
    CGRect frame;
    frame.origin.x = self.tutorialScrollView.frame.size.width * self.tutorialPageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.tutorialScrollView.frame.size;
    [self.tutorialScrollView scrollRectToVisible:frame animated:YES];
    
    // Keep track of when scrolls happen in response to the page control
    // value changing. If we don't do this, a noticeable "flashing" occurs
    // as the the scroll delegate will temporarily switch back the page
    // number.
    pageControlBeingUsed = YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
