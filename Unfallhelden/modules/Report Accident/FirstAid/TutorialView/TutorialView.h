//
//  TutorialView.h
//  Unfallhelden
//
//  Created by Adil Anwer on 3/17/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialView : UIView

@property (nonatomic, weak) IBOutlet UIImageView *tutorialImage;
@property (nonatomic, weak) IBOutlet UITextView *tutorialText;

-(void)setupView;

@end
