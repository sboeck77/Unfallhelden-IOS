//
//  TutorialView.m
//  Unfallhelden
//
//  Created by Adil Anwer on 3/17/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "TutorialView.h"

@implementation TutorialView
@synthesize tutorialImage,tutorialText;

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TutorialView" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

-(void)setupView
{
//    if(IS_IPHONE_4 || IS_IPHONE_5){
//        self.tutorialText.font = [UIFont regularUnfallheldenFontWithSize:13];
//    }
//    else{
        self.tutorialText.font = [UIFont regularUnfallheldenFontWithSize:14];
//    }
}

@end
