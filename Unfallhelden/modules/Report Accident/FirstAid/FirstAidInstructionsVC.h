//
//  FirstAidInstructionsVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/8/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstAidInstructionsVC : UnfallheldenParentVC
{
    BOOL pageControlBeingUsed;
}

@property (nonatomic, retain) IBOutlet UIScrollView *tutorialScrollView;
@property (nonatomic, weak) IBOutlet UILabel *tutorialTitleLbl;
@property (nonatomic,weak) IBOutlet UIPageControl *tutorialPageControl;
@property (nonatomic, weak) IBOutlet UIView *tutorialView;
@property (nonatomic, weak) IBOutlet UITextView *tutorialText;

@end
