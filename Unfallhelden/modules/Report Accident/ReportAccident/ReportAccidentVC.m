//
//  ReportAccidentVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/7/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "ReportAccidentVC.h"
#import "AtAccidentSiteVC.h"

@interface ReportAccidentVC ()

@end

@implementation ReportAccidentVC
@synthesize calledFrom;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"Report Accident"];
    //    if([self.calledFrom isEqualToString:@"sideMenu"]){
    [super setupMenuBarButtonItems];
    //    }
    for(UIView *vw in self.view.subviews) {
        if([vw isKindOfClass:[UIButton class]]) {
            ((UIButton*)vw).layer.cornerRadius = 3.0f;
            ((UIButton*)vw).layer.masksToBounds = YES;
            ((UIButton*)vw).titleLabel.adjustsFontSizeToFitWidth = YES;
            ((UIButton*)vw).titleLabel.minimumScaleFactor = .5f;
            [((UIButton*)vw).titleLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultLow
                                                                        forAxis:UILayoutConstraintAxisHorizontal];
            [((UIButton*)vw).titleLabel setFont:[UIFont regularUnfallheldenFontWithSize:19]];
        }
    }
}

-(void)atAccidentSiteBtnPress:(id)sender
{
    AtAccidentSiteVC *aSiteVC = [[AtAccidentSiteVC alloc] init];
    [self.navigationController pushViewController:aSiteVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
