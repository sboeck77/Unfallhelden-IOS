//
//  ReportAccidentVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/7/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportAccidentVC : UnfallheldenParentVC

@property (nonatomic, strong) NSString *calledFrom;

-(IBAction)atAccidentSiteBtnPress:(id)sender;

@end
