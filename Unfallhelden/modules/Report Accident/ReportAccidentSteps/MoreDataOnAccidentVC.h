//
//  MoreDataOnAccidentVCViewController.h
//  Unfallhelden
//
//  Created by Adil Anwer on 1/28/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

typedef enum {
    CarBrandPicker = 0,
    InsuranceCompanyPicker = 1,
    CountryPicker = 2,
    CarTypePicker = 3
} MoreDataPickerType;

@interface MoreDataOnAccidentVC : UnfallheldenParentVC<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, weak) IBOutlet UILabel *moreAccidentLbl;
@property (nonatomic, weak) IBOutlet UIView *nameContainerView;
@property (nonatomic, weak) IBOutlet UIView *continueBtnBg;
@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet UIImageView *backgroundImg;
@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *rAccidentScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottomSpaceToSuperview;
@property (nonatomic, strong) NSMutableArray *countryArray;
@property (nonatomic, strong) NSMutableArray *carBrandArray;
@property (nonatomic, strong) NSMutableArray *insuranceCompanyArray;
@property (nonatomic, strong) NSMutableArray *carTypeArray;
@property (nonatomic, strong) UnfallheldenPickerView *unfallheldenPickerView;
@property (nonatomic) MoreDataPickerType typeOfPicker;
@property (nonatomic, strong) NSString *selectedCarBrandId;
@property (nonatomic, strong) NSString *selectedInsurerId;
@property (nonatomic, weak) IBOutlet UIButton *plateNumCheckBox;
@property (nonatomic, weak) IBOutlet UIButton *companyCarCheckBox;
@property (nonatomic, weak) IBOutlet UIButton *leaseCarCheckBox;

-(IBAction)companyCheckBtnPress:(id)sender;
-(IBAction)leaseCheckBtnPress:(id)sender;
-(IBAction)plateNumCheckBtnPress:(id)sender;
-(IBAction)showCountryPicker:(id)sender;
-(IBAction)showBrandsPicker:(id)sender;
-(IBAction)showInsuranceComapniesPicker:(id)sender;
-(IBAction)showCarTypePicker:(id)sender;
-(IBAction)continueBtnPress:(id)sender;

@end
