//
//  PicturesVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/2/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PicturesVC : UnfallheldenParentVC<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UIImage *carImage;
@property (nonatomic, weak) IBOutlet UICollectionView *picturesCollectionView;
@property (nonatomic, weak) IBOutlet UILabel *noImageSelectedLbl;
@property (nonatomic, strong) NSMutableArray *carImagesArray;
@property (nonatomic, strong) NSMutableArray *carImagesArrayAfterDeletion;
@property (nonatomic) BOOL imgDeleteBtnHidden;
@property (nonatomic, strong) NSString *accidentId;

-(IBAction)selectCarPicture:(id)sender;
-(IBAction)continueBtnPress:(id)sender;

@end
