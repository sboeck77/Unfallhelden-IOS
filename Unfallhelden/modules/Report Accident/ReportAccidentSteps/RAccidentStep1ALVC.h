//
//  RAccidentStep1ALVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 3/25/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "ReportAccidentData.h"

@interface RAccidentStep1ALVC : UnfallheldenParentVC<UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic, strong) NSMutableArray *registeredCarsArray;

@property (nonatomic, weak) IBOutlet UITextField *plateNumberField;
@property (nonatomic, weak) IBOutlet UITextField *registeredCarField;

@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *rAccidentScrollView;
@property (nonatomic, strong) ReportAccidentData *reportAccidentData;
@property (nonatomic, strong) NSString *selectedCarId;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpacingContinueToNxtBtn;

@property (nonatomic, weak) IBOutlet UIButton *companyCarCheckBox;
@property (nonatomic, weak) IBOutlet UIButton *leaseCarCheckBox;
@property (nonatomic, weak) IBOutlet UILabel *personalInfoLbl;
@property (nonatomic, weak) IBOutlet UITextField *companyName;
@property (nonatomic, weak) IBOutlet UITextField *leasingCompanyName;
@property (nonatomic) BOOL isCompanyCar;
@property (nonatomic) BOOL isLeaseCar;

-(IBAction)companyCheckBtnPress:(id)sender;
-(IBAction)leaseCheckBtnPress:(id)sender;

-(IBAction)continueToNextStep:(id)sender;
-(IBAction)openPickerForRegisterCars:(id)sender;

@end
