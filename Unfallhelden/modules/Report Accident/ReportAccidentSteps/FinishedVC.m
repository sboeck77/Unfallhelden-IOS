//
//  FinishedVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 2/3/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "FinishedVC.h"
#import "PersonalDataVC.h"
#import "HomeScreenVC.h"

@interface FinishedVC ()

@end

@implementation FinishedVC
@synthesize closeAndRegisterLaterBtn,continueBtn,closeBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //    [self setTitle:@"Finished!"];
    [self setTitle:@"Fertig!"];
    [super setupMenuBarButtonItems];
    
    //GoogleAnalytics
//    self.screenName = @"ReportingAccidentFinishedScreen";
    
    self.navigationItem.backBarButtonItem = nil;
    UIBarButtonItem *sideMenuBtn = [UIBarButtonItem leftMenuBarButtonItemWithtarget:self andSelector:@selector(leftSideMenuBtnPress:)];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,sideMenuBtn, nil] animated:NO];
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:self];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    
    if([[[[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultsUserAlreadyLoggedIn] description] isEqualToString:@"YES"]){
        self.closeAndRegisterLaterBtn.hidden = YES;
        self.continueBtn.hidden = YES;
        self.closeBtn.hidden = NO;
    }
    else{
        self.closeAndRegisterLaterBtn.hidden = NO;
        self.continueBtn.hidden = NO;
        self.closeBtn.hidden = YES;
    }
    [self.view setNeedsUpdateConstraints];
    if([[[[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsUserAlreadyLoggedIn] description] isEqualToString:@"NO"]){
        UIBarButtonItem *barButtonItem = [UIBarButtonItem rightMenuBarButtonItemWithtarget:self andSelector:@selector(helpBtnPress:) andImage:[UIImage imageNamed:@"helpBtn"]];
        UIBarButtonItem *negativeSpacer2 = [[UIBarButtonItem alloc]
                                            initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                            target:nil action:nil];
        negativeSpacer.width = -8;
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer2,barButtonItem, nil] animated:NO];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ReportingAccidentFinishedScreen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(IBAction)leftSideMenuBtnPress:(id)sender
{
    [super leftSideMenuButtonPressed:sender];
}

-(IBAction)helpBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = Step6Text;
    customPopup.titleLabel.text = @"Hilfe";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}


-(void)continueBtnPress:(id)sender
{
    PersonalDataVC *personalDataVC = [[PersonalDataVC alloc] init];
    [self.navigationController pushViewController:personalDataVC animated:YES];
}

-(void)closeBtnPress:(id)sender
{
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:[[HomeScreenVC alloc] init]];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
