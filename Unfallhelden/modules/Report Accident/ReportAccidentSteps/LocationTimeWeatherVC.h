//
//  LocationTimeWeatherVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/2/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "HIPlaces.h"
#import "UnfallheldenDatePickerView.h"

typedef enum {
    TimePicker = 0,
    DatePicker = 1
} LTWPickerType;

@interface LocationTimeWeatherVC : UnfallheldenParentVC<MKMapViewDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate,HIPlacesManagerDelegate, UIAlertViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *ltwScrollView;
@property (nonatomic, weak) IBOutlet UITableView *placeSgstnTbl;
@property (nonatomic, weak) IBOutlet UIButton *locationOnOffBtn;
@property (nonatomic, strong) NSArray *placeSuggestionsArray;
@property (nonatomic, strong) UIAlertView *alertView;
@property (nonatomic) BOOL isUserLocationOn;
@property (nonatomic, strong) UnfallheldenDatePickerView *dateTimePicker;
@property (nonatomic) LTWPickerType typeOfPicker;
@property (nonatomic, strong) NSMutableArray *weatherArray;

-(IBAction)continueBtnPress:(id)sender;
-(IBAction)userLocationOnOff:(id)sender;
-(void)refreshMap;

@end
