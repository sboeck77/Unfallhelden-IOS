//
//  FinishedVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/3/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinishedVC : UnfallheldenParentVC

@property (nonatomic, weak) IBOutlet UIButton *closeAndRegisterLaterBtn;
@property (nonatomic, weak) IBOutlet UIButton *continueBtn;
@property (nonatomic, weak) IBOutlet UIButton *closeBtn;

-(IBAction)continueBtnPress:(id)sender;
-(IBAction)closeBtnPress:(id)sender;

@end
