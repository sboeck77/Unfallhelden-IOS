//
//  RAccidentStep1ALVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 3/25/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "RAccidentStep1ALVC.h"
#import "MoreDataOnAccidentVC.h"
//#import "UserCar+DAO.h"
#import "UserCarData.h"

@interface RAccidentStep1ALVC ()

@end

@implementation RAccidentStep1ALVC
@synthesize rAccidentScrollView,registeredCarsArray,plateNumberField,registeredCarField,verticalSpacingContinueToNxtBtn,reportAccidentData;
@synthesize selectedCarId,personalInfoLbl;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [super setupMenuBarButtonItems];
    //    [self setTitle:@"Step 1 of 5"];
    [self setTitle:@"Schritt 1 von 5"];
    
    //GoogleAnalytics
//    self.screenName = @"ReportAccidentStep1";
    
    for(UIView *vw in self.rAccidentScrollView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            if(IS_IPHONE_5 || IS_IPHONE_4){
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:13]];
            }
            else{
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
            }
        }
        else if([vw isKindOfClass:[UILabel class]]) {
            
        }
    }
    
    if(IS_IPHONE_5 || IS_IPHONE_4){
        [self.personalInfoLbl setFont:[UIFont regularUnfallheldenFontWithSize:13]];
    }else{
        [self.personalInfoLbl setFont:[UIFont regularUnfallheldenFontWithSize:14]];
    }
    
    self.registeredCarsArray = [[NSMutableArray alloc] init];
    [self.view layoutIfNeeded];
    [self.view layoutSubviews];
    if(IS_IPHONE_5){
        self.verticalSpacingContinueToNxtBtn.constant = 170;
    }
    else if (IS_IPHONE_4){
        self.verticalSpacingContinueToNxtBtn.constant = 100;
    }
    [self.view setNeedsUpdateConstraints];
    self.reportAccidentData = [ReportAccidentData sharedSingletonInstance];
    self.selectedCarId = @"";
    [[UnfallheldenHttpClient sharedClient] getUserCarsWithCompletionBlock:^(NSData *data) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *dataDictionary = [responseString JSONValue];
        NSLog(@"data dictionary : %@",dataDictionary);
        int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
        if(status == 1){
            NSMutableArray *carsArray = [dataDictionary objectForKey:@"data"];
            for (int count = 0; count < [carsArray count]; count++) {
                UserCarData *car = [UserCarData saveUserCarWithCarId:[[[carsArray objectAtIndex:count] valueForKey:@"id"] description] userId:[[[carsArray objectAtIndex:count] valueForKey:@"user_id"] description] insurerId:[[[carsArray objectAtIndex:count] valueForKey:@"insurer_id"] description] leasingCar:[[[carsArray objectAtIndex:count] valueForKey:@"leasing_car"] description] leasingCompany:[[[carsArray objectAtIndex:count] valueForKey:@"leasing_company"] description] companyCar:[[[carsArray objectAtIndex:count] valueForKey:@"company_car"] description] companyName:[[[carsArray objectAtIndex:count] valueForKey:@"company_name"] description] regTicketPhoto:[[[carsArray objectAtIndex:count] valueForKey:@"reg_ticket_photo"] description] issueDate:[[[carsArray objectAtIndex:count] valueForKey:@"issue_date"] description] vin:[[[carsArray objectAtIndex:count] valueForKey:@"vin"] description]  type:[[[carsArray objectAtIndex:count] valueForKey:@"type"] description] tradeName:[[[carsArray objectAtIndex:count] valueForKey:@"trade_name"] description]  body:[[[carsArray objectAtIndex:count] valueForKey:@"body"] description] displacement:[[[carsArray objectAtIndex:count] valueForKey:@"displacement"] description] fuelType:[[[carsArray objectAtIndex:count] valueForKey:@"fuel_type"] description] color:[[carsArray objectAtIndex:count] valueForKey:@"color"] extras:[[carsArray objectAtIndex:count] valueForKey:@"extras"] powerKw:[[[carsArray objectAtIndex:count] valueForKey:@"power_kw"] description]  carDeleted:[[[carsArray objectAtIndex:count] valueForKey:@"deleted"] description] dateTime:[[[carsArray objectAtIndex:count] valueForKey:@"datetime"] description] registrationNum:[[[carsArray objectAtIndex:count] valueForKey:@"registration_number"] description] brand:[[[carsArray objectAtIndex:count] valueForKey:@"brand"] description] firstReg:[[[carsArray objectAtIndex:count] valueForKey:@"first_registration"] description]  regDate:[[[carsArray objectAtIndex:count] valueForKey:@"registration_date"] description] tireDimFront:[[[carsArray objectAtIndex:count] valueForKey:@"tire_dimension_front"] description] tireDimBack:[[[carsArray objectAtIndex:count] valueForKey:@"tire_dimension_back"] description]];
                [self.registeredCarsArray addObject:car];
            }
            
            if(self.registeredCarsArray.count == 1){
                UserCarData *car = [self.registeredCarsArray objectAtIndex:0];
                self.registeredCarField.text = car.registrationNum;
                self.selectedCarId = car.carId;
            }
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Abruf der Fahrzeugdaten fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Abruf ihrer Fahrzeugdaten. Bitte versuchen Sie es erneut"];
        }
    }];
    self.isCompanyCar = NO;
    self.isLeaseCar = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ReportAccidentStep1"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [self.view layoutIfNeeded];
    [self.view layoutSubviews];
    CGFloat heightOfPicker = 0.0f;
    if(IS_IPHONE_5){
        self.rAccidentScrollView.frame = CGRectMake(self.view.frame.origin.x, self.rAccidentScrollView.frame.origin.y, self.view.frame.size.width, self.rAccidentScrollView.frame.size.height);
        self.rAccidentScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.rAccidentScrollView.contentSize.height + 80);
        heightOfPicker = 206;
    }
    else if (IS_IPHONE_4){
        self.rAccidentScrollView.frame = CGRectMake(self.view.frame.origin.x, self.rAccidentScrollView.frame.origin.y, self.view.frame.size.width, self.rAccidentScrollView.frame.size.height);
        self.rAccidentScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.rAccidentScrollView.contentSize.height + 80);
        heightOfPicker = 206;
    }
    else{
        self.rAccidentScrollView.frame = CGRectMake(self.view.frame.origin.x, self.rAccidentScrollView.frame.origin.y, self.view.frame.size.width, self.rAccidentScrollView.frame.size.height);
        self.rAccidentScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.rAccidentScrollView.contentSize.height);
    }
    [self.rAccidentScrollView contentSizeToFit];
    
    if (IS_IPHONE_6 || IS_IPHONE_6_Plus){
        heightOfPicker = 260;
    }
    
    UnfallheldenPickerView *pickerView = [[UnfallheldenPickerView alloc] init];
    CGRect pickerFrame = pickerView.frame;
    pickerFrame.size.height = heightOfPicker;
    pickerView.frame = pickerFrame;
    [pickerView.pickerDoneButton setAction:@selector(pickerDoneBtnPress:)];
    
    pickerView.pickerView.dataSource = self;
    pickerView.pickerView.delegate = self;
    
    [self.registeredCarField setTintColor:[UIColor clearColor]];
    self.registeredCarField.inputView = pickerView;
}

-(void)companyCheckBtnPress:(id)sender
{
    self.isCompanyCar = !self.isCompanyCar;
    if(self.isCompanyCar) {
        [self.companyCarCheckBox setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
    else {
        [self.companyCarCheckBox setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
    }
}

-(void)leaseCheckBtnPress:(id)sender
{
    self.isLeaseCar = !self.isLeaseCar;
    if(self.isLeaseCar) {
        [self.leaseCarCheckBox setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
    else {
        [self.leaseCarCheckBox setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
    }
}

-(void)continueToNextStep:(id)sender
{
    if([self.plateNumberField.text length]>0 || [self.registeredCarField.text length] > 0 ){
        //        [self.reportAccidentData prepareStepOneDataWithFirstName:@"abc" lastName:@"abc" phoneNumber:@"123" email:@"a@c.com"  plateNumber:@"a1" termsAccepted:YES acCompanyCar:NO acLeaseCar:NO acCompanyName:@"" acLeasingCompany:@""];
        
        if([self.plateNumberField.text length]>0){
            
            BOOL containsLetter = NSNotFound != [self.plateNumberField.text rangeOfCharacterFromSet:NSCharacterSet.letterCharacterSet].location;
            BOOL containsNumber = NSNotFound != [self.plateNumberField.text rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location;
            if(containsLetter != TRUE || containsNumber != TRUE){
                [self.plateNumberField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                self.plateNumberField.layer.borderColor=[[UIColor redColor]CGColor];
                self.plateNumberField.layer.borderWidth= 1.3f;
                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Das KFZ Kennzeichen muss Zahlen und Buchstaben enthalten."];
            }
            else{
                if((self.isCompanyCar == TRUE && [self.companyName.text length] > 0) || self.isCompanyCar == FALSE){
                    if((self.isLeaseCar == TRUE && [self.leasingCompanyName.text length] > 0) || self.isLeaseCar == FALSE){
                        [self.reportAccidentData prepareStepOneDataLoggedInUserWithCarId:@"" andRegistrationNumber:self.plateNumberField.text acCompanyCar:self.isCompanyCar acLeaseCar:self.isLeaseCar acCompanyName:self.companyName.text acLeasingCompany:self.leasingCompanyName.text];
                        MoreDataOnAccidentVC *moreDataOnAccident = [[MoreDataOnAccidentVC alloc] init];
                        [self.navigationController pushViewController:moreDataOnAccident animated:YES];
                    }
                    else{
                        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte geben Sie die Leasingfirma an."];
                    }
                }
                else{
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte geben Sie das Unternehmen an."];
                }
            }
        }
        else{
            
            if((self.isCompanyCar == TRUE && [self.companyName.text length] > 0) || self.isCompanyCar == FALSE){
                if((self.isLeaseCar == TRUE && [self.leasingCompanyName.text length] > 0) || self.isLeaseCar == FALSE){
                    [self.reportAccidentData prepareStepOneDataLoggedInUserWithCarId:self.selectedCarId andRegistrationNumber:@"" acCompanyCar:self.isCompanyCar acLeaseCar:self.isLeaseCar acCompanyName:self.companyName.text acLeasingCompany:self.leasingCompanyName.text];
                    MoreDataOnAccidentVC *moreDataOnAccident = [[MoreDataOnAccidentVC alloc] init];
                    [self.navigationController pushViewController:moreDataOnAccident animated:YES];
                }
                else{
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte geben Sie die Leasingfirma an."];
                }
            }
            else{
                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte geben Sie das Unternehmen an."];
            }
        }
    }
    else{
        for(UIView *vw in self.rAccidentScrollView.subviews) {
            if([vw isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField*)vw;
                if([textField.text length] == 0 && textField.background == nil && textField != self.leasingCompanyName && textField != self.companyName){
                    textField.layer.borderColor=[[UIColor redColor]CGColor];
                    textField.layer.borderWidth= 1.3f;
                    [textField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                    [textField setTextColor:[UIColor whiteColor]];
                    UIColor *color = [UIColor whiteColor];
                    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ wird benötigt",textField.placeholder] attributes:@{NSForegroundColorAttributeName: color}];
                }
            }
        }
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte füllen Sie alle Felder aus"];
    }
}

#pragma mark - UITextFieldDelegateMethods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if([textField isEqual:self.registeredCarField] && [self.registeredCarsArray count] == 0){
        [textField resignFirstResponder];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if([textField isEqual:self.plateNumberField]){
        BOOL containsLetter = NSNotFound != [self.plateNumberField.text rangeOfCharacterFromSet:NSCharacterSet.letterCharacterSet].location;
        BOOL containsNumber = NSNotFound != [self.plateNumberField.text rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location;
        
        if(containsLetter != TRUE || containsNumber != TRUE){
            [self.plateNumberField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
            self.plateNumberField.layer.borderColor=[[UIColor redColor]CGColor];
            self.plateNumberField.layer.borderWidth= 1.3f;
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Das KFZ Kennzeichen muss Zahlen und Buchstaben enthalten."];
        }
        else{
            self.plateNumberField.background = nil;
            self.plateNumberField.layer.borderColor=[[UIColor whiteColor]CGColor];
            self.plateNumberField.layer.borderWidth= 1.3f;
            self.registeredCarField.background = nil;
            self.registeredCarField.layer.borderColor=[[UIColor whiteColor]CGColor];
            self.registeredCarField.layer.borderWidth= 1.3f;
        }
    }
    else if([textField.text length] > 0){
        self.plateNumberField.background = nil;
        plateNumberField.layer.borderColor=[[UIColor whiteColor]CGColor];
        plateNumberField.layer.borderWidth= 1.3f;
        self.registeredCarField.background = nil;
        registeredCarField.layer.borderColor=[[UIColor whiteColor]CGColor];
        registeredCarField.layer.borderWidth= 1.3f;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.plateNumberField]){
        BOOL containsLetter = NSNotFound != [self.plateNumberField.text rangeOfCharacterFromSet:NSCharacterSet.letterCharacterSet].location;
        BOOL containsNumber = NSNotFound != [self.plateNumberField.text rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location;
        
        if(containsLetter != TRUE || containsNumber != TRUE){
            [self.plateNumberField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
            self.plateNumberField.layer.borderColor=[[UIColor redColor]CGColor];
            self.plateNumberField.layer.borderWidth= 1.3f;
        }
        else{
            self.plateNumberField.background = nil;
            self.plateNumberField.layer.borderColor=[[UIColor whiteColor]CGColor];
            self.plateNumberField.layer.borderWidth= 1.3f;
            self.registeredCarField.background = nil;
            self.registeredCarField.layer.borderColor=[[UIColor whiteColor]CGColor];
            self.registeredCarField.layer.borderWidth= 1.3f;
        }
    }
    else if([textField.text length] > 0){
        self.plateNumberField.background = nil;
        self.plateNumberField.layer.borderColor=[[UIColor whiteColor]CGColor];
        self.plateNumberField.layer.borderWidth= 1.3f;
        self.registeredCarField.background = nil;
        self.registeredCarField.layer.borderColor=[[UIColor whiteColor]CGColor];
        self.registeredCarField.layer.borderWidth= 1.3f;
    }
    [textField resignFirstResponder];
    return TRUE;
}

#pragma mark - Open PickerView

-(void)openPickerForRegisterCars:(id)sender
{
    for(UIView *vw in self.rAccidentScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    if([self.registeredCarsArray count] == 0){
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Kein registriertes Fahrzeug" andMessage:@"Sie haben noch kein registriertes Fahrzeug"];
    }
    else{
        [self.registeredCarField becomeFirstResponder];
    }
}

#pragma mark - PickerView Done Button Press
-(IBAction)pickerDoneBtnPress:(id)sender
{
    [self.registeredCarField resignFirstResponder];
}

#pragma mark - UIPickerView DataSource

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [self.registeredCarsArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    UserCarData *car = [self.registeredCarsArray objectAtIndex:row];
    return car.registrationNum;
}

#pragma mark - UIPickerView Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    UserCarData *car = [self.registeredCarsArray objectAtIndex:row];
    self.selectedCarId = car.carId;
    self.registeredCarField.text = car.registrationNum;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
