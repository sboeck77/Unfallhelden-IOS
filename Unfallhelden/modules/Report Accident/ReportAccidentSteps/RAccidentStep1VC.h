//
//  AccidentSiteVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/8/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "ReportAccidentData.h"

@interface RAccidentStep1VC : UnfallheldenParentVC<UITextFieldDelegate, UIAlertViewDelegate>

@property (nonatomic, weak) IBOutlet UILabel *personalDataLbl;
@property (nonatomic, weak) IBOutlet UITextField *firstNameField;
@property (nonatomic, weak) IBOutlet UITextField *lastNameField;
@property (nonatomic, weak) IBOutlet UITextField *phoneNumField;
@property (nonatomic, weak) IBOutlet UITextField *emailField;
@property (nonatomic, weak) IBOutlet UITextField *plateNumField;
@property (nonatomic, weak) IBOutlet UIButton *continueNxtStpBtn;
@property (nonatomic, strong) NSMutableArray *carBrandArray;
@property (nonatomic, strong) ReportAccidentData *reportAccidentData;
@property (nonatomic) BOOL termsAndConditions;

@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *rAccidentScrollView;
@property (nonatomic, weak) IBOutlet UIButton *checkBoxBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpacingContinueToNxtBtn;
@property (nonatomic, weak) IBOutlet UIButton *companyCarCheckBox;
@property (nonatomic, weak) IBOutlet UIButton *leaseCarCheckBox;
@property (nonatomic, weak) IBOutlet UITextField *companyName;
@property (nonatomic, weak) IBOutlet UITextField *leasingCompanyName;
@property (nonatomic) BOOL isCompanyCar;
@property (nonatomic) BOOL isLeaseCar;

-(IBAction)companyCheckBtnPress:(id)sender;
-(IBAction)leaseCheckBtnPress:(id)sender;
-(IBAction)checkBoxBtnPress:(id)sender;
-(IBAction)continueToNextStep:(id)sender;
-(IBAction)termsAndCndtnsBtnPress:(id)sender;
-(IBAction)dataPrvcyBtnPress:(id)sender;

@end
