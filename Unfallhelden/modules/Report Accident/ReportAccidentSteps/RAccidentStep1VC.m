//
//  AccidentSiteVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/8/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "RAccidentStep1VC.h"
#import "HelpAndInstructionsVC.h"
#import "MoreDataOnAccidentVC.h"
#import "UnfallheldenPickerView.h"
#import "LoginVC.h"

@interface RAccidentStep1VC ()

@property (nonatomic, weak) IBOutlet UILabel *termsAndCndtnsLbl;
@property (nonatomic, weak) IBOutlet UILabel *dataPrivacyLbl;

@end

@implementation RAccidentStep1VC
@synthesize firstNameField,lastNameField,emailField,phoneNumField,termsAndConditions,plateNumField,termsAndCndtnsLbl,personalDataLbl;
@synthesize rAccidentScrollView,continueNxtStpBtn,carBrandArray,reportAccidentData,checkBoxBtn,verticalSpacingContinueToNxtBtn;
@synthesize isLeaseCar, isCompanyCar, companyCarCheckBox, leaseCarCheckBox, companyName, leasingCompanyName,dataPrivacyLbl;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setTranslatesAutoresizingMaskIntoConstraints:YES];
    //    [self setTitle:@"Step 1 of 5"];
//    [self setTitle:@"Schritt 1 von 5"];
    
    UIBarButtonItem *barButtonItem = [UIBarButtonItem rightMenuBarButtonItemWithtarget:self andSelector:@selector(helpBtnPress:) andImage:[UIImage imageNamed:@"helpBtn"]];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,barButtonItem, nil] animated:NO];
    
    for(UIView *vw in self.rAccidentScrollView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            if(IS_IPHONE_5 || IS_IPHONE_4){
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:13]];
            }
            else{
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
            }
        } else if([vw isKindOfClass:[UIImageView class]]) {
            ((UIImageView*)vw).layer.cornerRadius = 3.0f;
            ((UIImageView*)vw).layer.masksToBounds = YES;
            //            [((UILabel*)vw) setFont:[UIFont regularLobstrFontWithSize:14]];
        } else if([vw isKindOfClass:[UIButton class]]) {
            ((UIButton*)vw).layer.cornerRadius = 3.0f;
            ((UIButton*)vw).layer.masksToBounds = YES;
            //            [((UIButton*)vw).titleLabel setFont:[UIFont regularLobstrFontWithSize:18]];
        }
    }
    [super setupMenuBarButtonItems];
    //GoogleAnalytics
//    self.screenName = @"ReportAccidentStep1";
    [self.view layoutIfNeeded];
    [self.view layoutSubviews];
    if(IS_IPHONE_5){
        self.verticalSpacingContinueToNxtBtn.constant = 110;
    }
    else if (IS_IPHONE_4){
        self.verticalSpacingContinueToNxtBtn.constant = 60;
    }
    else if (IS_IPHONE_6_Plus){
        self.verticalSpacingContinueToNxtBtn.constant = 188;
    }
    self.personalDataLbl.font = [UIFont regularUnfallheldenFontWithSize:13];
    [self.view setNeedsUpdateConstraints];
    [self.rAccidentScrollView contentSizeToFit];
    self.carBrandArray = [[NSMutableArray alloc] initWithObjects:@"BMW",@"Mercedes-Benz",@"Honda",@"Mazda",@"Nissan",@"Audi",@"Porsche",@"Toyota", nil];
    self.reportAccidentData = [ReportAccidentData sharedSingletonInstance];
    self.termsAndConditions = NO;
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:self.termsAndCndtnsLbl.text];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    self.termsAndCndtnsLbl.attributedText = attributeString;
    self.termsAndCndtnsLbl.textColor = [UIColor whiteColor];
    [self.termsAndCndtnsLbl setFont:[UIFont regularUnfallheldenFontWithSize:11]];
    NSMutableAttributedString *attributeString2 = [[NSMutableAttributedString alloc] initWithString:self.dataPrivacyLbl.text];
    [attributeString2 addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString2 length]}];
    self.dataPrivacyLbl.attributedText = attributeString2;
    self.dataPrivacyLbl.textColor = [UIColor whiteColor];
    [self.dataPrivacyLbl setFont:[UIFont regularUnfallheldenFontWithSize:11]];
    self.isCompanyCar = NO;
    self.isLeaseCar = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ReportAccidentStep1"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    if(IS_IPHONE_5){
        self.rAccidentScrollView.frame = CGRectMake(self.view.frame.origin.x, self.rAccidentScrollView.frame.origin.y, self.view.frame.size.width, self.rAccidentScrollView.frame.size.height);
        self.rAccidentScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.rAccidentScrollView.contentSize.height + 80);
    }
    else if (IS_IPHONE_4){
        self.rAccidentScrollView.frame = CGRectMake(self.view.frame.origin.x, self.rAccidentScrollView.frame.origin.y, self.view.frame.size.width, self.rAccidentScrollView.frame.size.height);
        self.rAccidentScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.rAccidentScrollView.contentSize.height + 80);
    }
    else{
        self.rAccidentScrollView.frame = CGRectMake(self.view.frame.origin.x, self.rAccidentScrollView.frame.origin.y, self.view.frame.size.width, self.rAccidentScrollView.frame.size.height);
        self.rAccidentScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.rAccidentScrollView.contentSize.height);
    }
    [self.rAccidentScrollView contentSizeToFit];
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];
    UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[self class] toolbarHeight])];
    [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
    self.phoneNumField.inputAccessoryView = toolbar;
}

- (void)doneButtonDidPressed:(id)sender {
    [self.phoneNumField resignFirstResponder];
}

+ (CGFloat)toolbarHeight {
    // This method will handle the case that the height of toolbar may change in future iOS.
    return 44.f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)companyCheckBtnPress:(id)sender
{
    self.isCompanyCar = !self.isCompanyCar;
    if(self.isCompanyCar) {
        [self.companyCarCheckBox setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
    else {
        [self.companyCarCheckBox setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
    }
}

-(void)leaseCheckBtnPress:(id)sender
{
    self.isLeaseCar = !self.isLeaseCar;
    if(self.isLeaseCar) {
        [self.leaseCarCheckBox setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
    else {
        [self.leaseCarCheckBox setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
    }
}

-(void)continueToNextStep:(id)sender
{
    if([self.firstNameField.text length]>0 && [self.lastNameField.text length]>0 && [self.phoneNumField.text length]>0 && [self.emailField.text length]>0 && [self.plateNumField.text length]>0){
        if([[UnfallheldenUtilities sharedSingletonInstance] validateEmail:self.emailField.text]){
            if(self.termsAndConditions == TRUE){
                [[UnfallheldenHttpClient sharedClient] checkIfEmailAlreadyExistWithEmail:self.emailField.text andCompletionBlock:^(NSData *data) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
                        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                        NSDictionary *dataDictionary = [responseString JSONValue];
                        //            NSDictionary *userData = [[dataDictionary valueForKey:@"data"] description];
                        int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                        if(status == 1){
                            int emailAlreadyExist = [[[dataDictionary objectForKey:@"data"] valueForKey:@"exist"] intValue];
                            if(emailAlreadyExist == 1){
                                
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email already exist" message:@"Diese E-Mail liegt uns bereits vor" delegate:self cancelButtonTitle:@"Login" otherButtonTitles:@"Abbrechen", nil];
                                [alert show];
                            }
                            else{
                                [self.reportAccidentData prepareStepOneDataWithFirstName:self.firstNameField.text lastName:self.lastNameField.text phoneNumber:self.phoneNumField.text email:self.emailField.text plateNumber:self.plateNumField.text termsAccepted:YES acCompanyCar:self.isCompanyCar acLeaseCar:self.isLeaseCar acCompanyName:self.companyName.text acLeasingCompany:self.leasingCompanyName.text];
                                
                                //                                [self.reportAccidentData prepareStepOneDataLoggedInUserWithCarId:@"123" andRegistrationNumber:@"a1"];
                                
                                BOOL containsLetter = NSNotFound != [self.plateNumField.text rangeOfCharacterFromSet:NSCharacterSet.letterCharacterSet].location;
                                BOOL containsNumber = NSNotFound != [self.plateNumField.text rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location;
                                if(containsLetter != TRUE || containsNumber != TRUE){
                                    [self.plateNumField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                                    self.plateNumField.layer.borderColor=[[UIColor redColor]CGColor];
                                    self.plateNumField.layer.borderWidth= 1.3f;
                                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Das KFZ Kennzeichen muss Zahlen und Buchstaben enthalten."];
                                }
                                else{
                                    
                                    if((self.isCompanyCar == TRUE && [self.companyName.text length] > 0) || self.isCompanyCar == FALSE){
                                        if((self.isLeaseCar == TRUE && [self.leasingCompanyName.text length] > 0) || self.isLeaseCar == FALSE){
                                            
                                            MoreDataOnAccidentVC *moreDataOnAccident = [[MoreDataOnAccidentVC alloc] init];
                                            [self.navigationController pushViewController:moreDataOnAccident animated:YES];
                                        }
                                        else{
                                            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte geben Sie die Leasingfirma an."];
                                        }
                                    }
                                    else{
                                        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte geben Sie das Unternehmen an."];
                                    }
                                    
                                }
                            }
                        }
                        else{
                            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehler" andMessage:@"Es gibt ein Problem mit dem E-Mail Server. Bitte versuchen Sie es erneut"];
                        }
                    });
                }];
            }
            else{
                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte stimmen Sie den AGB und den Datenschutzbestimmungen zu."];
            }
            
        }else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Diese E-Mail Adresse liegt uns nicht vor."];
        }
    }
    else{
        for(UIView *vw in self.rAccidentScrollView.subviews) {
            if([vw isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField*)vw;
                if([textField.text length] == 0 && textField.background == nil && ![textField isEqual:self.companyName] && ![textField isEqual:self.leasingCompanyName]){
                    textField.layer.borderColor=[[UIColor redColor]CGColor];
                    textField.layer.borderWidth= 1.3f;
                    [textField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                    [textField setTextColor:[UIColor whiteColor]];
                    UIColor *color = [UIColor whiteColor];
                    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ wird benötigt",textField.placeholder] attributes:@{NSForegroundColorAttributeName: color}];
                }
            }
        }
        
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte füllen Sie zuerst alle Felder aus."];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Login"]) {
        LoginVC *loginVC = [[LoginVC alloc] init];
        [self.navigationController pushViewController:loginVC animated:YES];
    }
}

-(void)termsAndCndtnsBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = UnfallheldenTermsAndConditions;
    customPopup.titleLabel.text = @"AGB";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}

-(void)dataPrvcyBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = UnfallheldenPrivacyPolicy;
    customPopup.titleLabel.text = @"Datenschutz";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}

-(void)checkBoxBtnPress:(id)sender
{
    self.termsAndConditions = !self.termsAndConditions;
    if(self.termsAndConditions) {
        [self.checkBoxBtn setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
    else {
        [self.checkBoxBtn setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
    }
}

#pragma mark - UITextFieldDelegateMethods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if([textField isEqual:self.plateNumField]){
        BOOL containsLetter = NSNotFound != [self.plateNumField.text rangeOfCharacterFromSet:NSCharacterSet.letterCharacterSet].location;
        BOOL containsNumber = NSNotFound != [self.plateNumField.text rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location;
        
        if(containsLetter != TRUE || containsNumber != TRUE){
            [self.plateNumField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
            self.plateNumField.layer.borderColor=[[UIColor redColor]CGColor];
            self.plateNumField.layer.borderWidth= 1.3f;
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Das KFZ Kennzeichen muss Zahlen und Buchstaben enthalten."];
        }
        else{
            self.plateNumField.background = nil;
            self.plateNumField.layer.borderColor=[[UIColor whiteColor]CGColor];
            self.plateNumField.layer.borderWidth= 1.3f;
        }
    }
    else if([textField.text length] > 0){
        textField.background = nil;
        textField.layer.borderColor=[[UIColor whiteColor]CGColor];
        textField.layer.borderWidth= 1.3f;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.plateNumField]){
        BOOL containsLetter = NSNotFound != [self.plateNumField.text rangeOfCharacterFromSet:NSCharacterSet.letterCharacterSet].location;
        BOOL containsNumber = NSNotFound != [self.plateNumField.text rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location;
        
        if(containsLetter != TRUE || containsNumber != TRUE){
            [self.plateNumField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
            self.plateNumField.layer.borderColor=[[UIColor redColor]CGColor];
            self.plateNumField.layer.borderWidth= 1.3f;
            //            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Das KFZ Kennzeichen muss Zahlen und Buchstaben enthalten."];
        }
        else{
            self.plateNumField.background = nil;
            self.plateNumField.layer.borderColor=[[UIColor whiteColor]CGColor];
            self.plateNumField.layer.borderWidth= 1.3f;
        }
    }
    else if([textField.text length] > 0){
        textField.background = nil;
        textField.layer.borderColor=[[UIColor whiteColor]CGColor];
        textField.layer.borderWidth= 1.3f;
    }
    [textField resignFirstResponder];
    return TRUE;
}

-(IBAction)helpBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = Step1Text;
    customPopup.titleLabel.text = @"Hilfe";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
