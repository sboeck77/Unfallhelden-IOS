//
//  LocationTimeWeatherVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 2/2/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "LocationTimeWeatherVC.h"
#import "MapAnnotation.h"
#import "RecordVoiceVC.h"
#import "ReportAccidentData.h"

#define kGoogleMapApiKey @"AIzaSyDEedrcOr6YPhe6qZfZWLO0jhCttQHLm4w"

@interface LocationTimeWeatherVC ()
{
    HIPlacesManager *_placesManager;
}
@property (nonatomic, weak) IBOutlet UITextField *locationNameTxtField;
@property (nonatomic, weak) IBOutlet UITextField *timetxtField;
@property (nonatomic, weak) IBOutlet UITextField *dateTxtField;
@property (nonatomic, weak) IBOutlet UITextField *weatherTxtField;
@property (nonatomic, strong) NSString *accidentDateToUpload;

@property (nonatomic) double userLatitude;
@property (nonatomic) double userLongitude;

@end

@implementation LocationTimeWeatherVC
@synthesize ltwScrollView, locationNameTxtField, placeSgstnTbl, placeSuggestionsArray,userLatitude,userLongitude,locationOnOffBtn,isUserLocationOn;
@synthesize dateTimePicker, weatherArray, accidentDateToUpload;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //    [self setTitle:@"Step 3 of 5"];
    [self setTitle:@"Schritt 3 von 5"];
    
    //GoogleAnalytics
//    self.screenName = @"ReportAccidentStep3";
    
    [super setupMenuBarButtonItems];
    [self refreshMap];
    [self.ltwScrollView contentSizeToFit];
    self.placeSgstnTbl.hidden = YES;
    for(UIView *vw in self.ltwScrollView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            if([textField isEqual:self.locationNameTxtField]){
                textField.rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
                textField.rightViewMode = UITextFieldViewModeAlways;
            }
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            if(IS_IPHONE_5 || IS_IPHONE_4){
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:11.5]];
            }
            else{
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:14]];
            }
        }
    }
    _placesManager = [[HIPlacesManager alloc] init];
    _placesManager.delegate = self;
    
    self.placeSgstnTbl.dataSource = self;
    [self.placeSgstnTbl registerClass:[UITableViewCell class] forCellReuseIdentifier:@"PlaceAutocompleteResultCell"];
    
    self.placeSuggestionsArray = nil;
    [self.locationNameTxtField addTarget:self
                                  action:@selector(textFieldDidChange:)
                        forControlEvents:UIControlEventEditingChanged];
    self.isUserLocationOn = FALSE;
    
    [[UnfallheldenHttpClient sharedClient] getWeatherWithLatitude:[UnfallheldenUtilities sharedSingletonInstance].userlatitude longitude:[UnfallheldenUtilities sharedSingletonInstance].userlongitude WithCompletionBlock:^(NSData *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSDictionary *dataDictionary = [responseString JSONValue];
            NSLog(@"%@",[[[dataDictionary objectForKey:@"main"] valueForKey:@"temp"] description]);
            double temp = [[[[dataDictionary objectForKey:@"main"] valueForKey:@"temp"] description] doubleValue] - 273.15;
            //            self.weatherTxtField.text = [NSString stringWithFormat:@"%@, %.1f °C",[[[[dataDictionary objectForKey:@"weather"] objectAtIndex:0] valueForKey:@"main"] description],temp];
            NSString *locationName = [NSString stringWithFormat:@"%@, %@",[[dataDictionary valueForKey:@"name"] description], [[[dataDictionary objectForKey:@"sys"] valueForKey:@"country"] description]];
            self.locationNameTxtField.text = locationName;
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        });
    }];
    
    //weather temp - 273.15
    
    UIBarButtonItem *barButtonItem = [UIBarButtonItem rightMenuBarButtonItemWithtarget:self andSelector:@selector(helpBtnPress:) andImage:[UIImage imageNamed:@"helpBtn"]];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,barButtonItem, nil] animated:NO];
    
    /* Sunny: Sonnig Blinding Sun: Sonne blendet*/
    
    self.weatherArray = [[NSMutableArray alloc] initWithObjects:@"Regen",@"Schneefall",@"Stark bewölkt",@"Leicht Bewölkt",@"Windig",@"Klar",@"Trüb",@"Dunst",@"Nebel",@"Glatteis",@"Hagel",@"Schneeregen",@"Starker Regen",@"Sturm",@"Gewitter",@"Sonnig",@"Sonne hat geblendet", nil];
    
    NSSortDescriptor *sortDscrptr = [[NSSortDescriptor alloc] initWithKey:@"self" ascending:YES];
    self.weatherArray = [[self.weatherArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:sortDscrptr, nil]] mutableCopy];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    NSString *time = [formatter stringFromDate:[NSDate date]];
    NSLog(@"Time : %@", time);
    self.timetxtField.text = time;
    
    [formatter setDateFormat:@"dd.MM.yyy"];
    NSString *date = [formatter stringFromDate:[NSDate date]];
    NSLog(@"Date : %@", date);
    self.dateTxtField.text = date;
    
    NSDateFormatter *formatterSecond = [[NSDateFormatter alloc] init];
    [formatterSecond setDateFormat:@"yyyy-MM-dd"];
    self.accidentDateToUpload = [formatterSecond stringFromDate:[NSDate date]];
}

-(IBAction)helpBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = Step3Text;
    customPopup.titleLabel.text = @"Hilfe";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}

-(void)refreshMap
{
    /*
     [self.map removeAnnotations:self.map.annotations];
     MapAnnotation *annotation = [[MapAnnotation alloc] init];
     annotation.coordinate = CLLocationCoordinate2DMake(25.132703, 55.380464);
     annotation.title = @"First Gas Station";
     annotation.gsIdentifier = [NSNumber numberWithInt:123];
     [self.map addAnnotation:annotation];*/
    self.map.showsUserLocation = NO;
}

#pragma mark - MKMapView Delegate
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    static NSString *RestoAnnotationIdentifier = @"GasStationAnnotationIdentifier";
    MKAnnotationView *annView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:RestoAnnotationIdentifier];
    annView.canShowCallout = YES;
    if (annotation == self.map.userLocation && self.isUserLocationOn == TRUE) {
        annView.image = [UIImage imageNamed:@"markerUserLocation"];
        annView.calloutOffset = CGPointMake(0.0,0.0);
    } else {
        annView.image = [UIImage imageNamed:@"markerStation"];
        //        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        //        MapAnnotation *restoAnnotation = (MapAnnotation*)annotation;
        //        rightButton.tag = [restoAnnotation.gsIdentifier integerValue];
        //        annView.rightCalloutAccessoryView = rightButton;
        annView.calloutOffset = CGPointMake(1.0,3.0);
    }
    return annView;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)annotationViews {
    
    for (MKAnnotationView *annView in annotationViews){
        
        CGRect endFrame = annView.frame;
        annView.frame = CGRectOffset(endFrame, 0, -500);
        [UIView animateWithDuration:0.5 animations:^{
            annView.frame = endFrame;
        }];
    }
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    //    Resto *resto = [Resto getRestoWithId:[NSNumber numberWithInteger:control.tag]];
    //    DetailViewController *vc = [[DetailViewController alloc] initWithResto:resto];
    //    [self.parentViewController.navigationController pushViewController:vc animated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ReportAccidentStep3"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    if(IS_IPHONE_5){
        self.ltwScrollView.frame = CGRectMake(self.view.frame.origin.x, self.ltwScrollView.frame.origin.y, self.view.frame.size.width, self.ltwScrollView.frame.size.height);
        self.ltwScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.ltwScrollView.contentSize.height);
    }
    else if (IS_IPHONE_4){
        self.ltwScrollView.frame = CGRectMake(self.view.frame.origin.x, self.ltwScrollView.frame.origin.y, self.view.frame.size.width, self.ltwScrollView.frame.size.height);
        self.ltwScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.ltwScrollView.contentSize.height);
    }
    else{
        self.ltwScrollView.frame = CGRectMake(self.view.frame.origin.x, self.ltwScrollView.frame.origin.y, self.view.frame.size.width, self.ltwScrollView.frame.size.height);
        self.ltwScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.ltwScrollView.contentSize.height);
    }
    [self.ltwScrollView contentSizeToFit];
    CGFloat heightOfPicker = 0.0f;
    
    if(IS_IPHONE_5){
        heightOfPicker = 206;
    }
    else if (IS_IPHONE_4){
        heightOfPicker = 206;
    }
    else{
    }
    if (IS_IPHONE_6 || IS_IPHONE_6_Plus){
        heightOfPicker = 260;
    }
    self.dateTimePicker = [[UnfallheldenDatePickerView alloc] init];
    CGRect pickerFrame = self.dateTimePicker.frame;
    pickerFrame.size.height = heightOfPicker;
    self.dateTimePicker.frame = pickerFrame;
    [self.dateTimePicker.pickerDoneButton setAction:@selector(pickerDoneBtnPress:)];
    [self.dateTimePicker.datePicker addTarget:self action:@selector(pickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.timetxtField setTintColor:[UIColor clearColor]];
    self.timetxtField.inputView = self.dateTimePicker;
    
    [self.dateTxtField setTintColor:[UIColor clearColor]];
    self.dateTxtField.inputView = self.dateTimePicker;
    
    
    UnfallheldenPickerView *unfallheldenPickerView = [[UnfallheldenPickerView alloc] init];
    //    CGRect pickerFrame = unfallheldenPickerView.frame;
    pickerFrame.size.height = heightOfPicker;
    unfallheldenPickerView.frame = pickerFrame;
    [unfallheldenPickerView.pickerDoneButton setAction:@selector(weatherPickerDoneBtnPress:)];
    
    unfallheldenPickerView.pickerView.dataSource = self;
    unfallheldenPickerView.pickerView.delegate = self;
    
    [self.weatherTxtField setTintColor:[UIColor clearColor]];
    self.weatherTxtField.inputView = unfallheldenPickerView;
    
}

-(void)pickerValueChanged:(UIDatePicker *)timePicker
{
    if(self.typeOfPicker == TimePicker){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm"];
        NSString *time = [formatter stringFromDate:timePicker.date];
        NSLog(@"Time : %@", time);
        self.timetxtField.text = time;
    }
    else if(self.typeOfPicker == DatePicker){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd.MM.yyy"];
        NSString *date = [formatter stringFromDate:timePicker.date];
        NSLog(@"Date : %@", date);
        self.dateTxtField.text = date;
        NSDateFormatter *formatterSecond = [[NSDateFormatter alloc] init];
        [formatterSecond setDateFormat:@"yyyy-MM-dd"];
        self.accidentDateToUpload = [formatterSecond stringFromDate:timePicker.date];
    }
}

#pragma mark - PickerView Done Button Press
-(IBAction)pickerDoneBtnPress:(id)sender
{
    [self.timetxtField resignFirstResponder];
    [self.dateTxtField resignFirstResponder];
}

-(IBAction)textFieldDidChange:(id)sender
{
    if ([self.locationNameTxtField.text isEqualToString:@""]) {
        self.placeSuggestionsArray = nil;
        self.placeSuggestionsArray = [[NSArray alloc] init];
        [self.placeSgstnTbl reloadData];
        self.placeSgstnTbl.hidden = YES;
        return;
    }
    
    HIPlaceAutocompleteRequest *placeAutocompleteRequest = [[HIPlaceAutocompleteRequest alloc] init];
    placeAutocompleteRequest.key = kGoogleMapApiKey;
    placeAutocompleteRequest.input = self.locationNameTxtField.text;
    [_placesManager searchForPlaceAutocompleteResultsWithRequest:placeAutocompleteRequest];
}

#pragma mark - UITextFieldDelegateMethods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if([textField isEqual:self.timetxtField]){
        self.typeOfPicker = TimePicker;
        [self.dateTimePicker.datePicker setDatePickerMode:UIDatePickerModeTime];
    }
    else if ([textField isEqual:self.dateTxtField]){
        self.typeOfPicker = DatePicker;
        [self.dateTimePicker.datePicker setDatePickerMode:UIDatePickerModeDate];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.locationNameTxtField]){
        //        [self searchMapLocation];
        if ([self.locationNameTxtField.text isEqualToString:@""]) {
            self.placeSuggestionsArray = nil;
            self.placeSuggestionsArray = [[NSArray alloc] init];
            [self.placeSgstnTbl reloadData];
            self.placeSgstnTbl.hidden = YES;
        }
    }
    [textField resignFirstResponder];
    return TRUE;
}

-(void)continueBtnPress:(id)sender
{
    [[ReportAccidentData sharedSingletonInstance] prepareLTWDataWithLocationName:self.locationNameTxtField.text latitude:self.userLatitude longitude:self.userLongitude time:self.timetxtField.text date:self.accidentDateToUpload weather:self.weatherTxtField.text];
    RecordVoiceVC *recordVoice = [[RecordVoiceVC alloc] init];
    [self.navigationController pushViewController:recordVoice animated:YES];
}

#pragma mark - PickerView Done Button Press
-(IBAction)weatherPickerDoneBtnPress:(id)sender
{
    [self.weatherTxtField resignFirstResponder];
}
#pragma mark - UIPickerView DataSource

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.weatherArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [self.weatherArray objectAtIndex:row];
}
#pragma mark - UIPickerView Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.weatherTxtField.text = [self.weatherArray objectAtIndex:row];
}

- (void) searchMapLocation
{
    [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Suche..." message:nil];
    //Perform the JSON query.
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:self.locationNameTxtField.text
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     BOOL locationFound = NO;
                     for (CLPlacemark* aPlacemark in placemarks)
                     {
                         // Process the placemark.
                         NSLog(@"latitude %f and longitude %f",aPlacemark.location.coordinate.latitude, aPlacemark.location.coordinate.longitude);
                         [self zoomMapAndCenterAtLatitude:aPlacemark.location.coordinate.latitude andLongitude:aPlacemark.location.coordinate.longitude andTitle:aPlacemark.name];
                         locationFound = YES;
                     }
                     if(locationFound == NO){
                         [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Ort nicht gefunden" andMessage:@"Die angegebene Adresse konnte nicht gefunden werden"];
                     }
                     [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
                     /*lat = "33.7155649";
                      lng = "73.0759055";*/
                 }];
}

#pragma mark - UITableViewDataSourceMethods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.placeSuggestionsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlaceAutocompleteResultCell" forIndexPath:indexPath];
    
    HIPlaceAutocompleteResult *placeAutocompleteResult = [self.placeSuggestionsArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = placeAutocompleteResult.placeDescription;
    cell.textLabel.font = [UIFont systemFontOfSize:12];
    
    return cell;
}

#pragma mark - UITableViewDelegateMethods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                    message:nil];
    HIPlaceAutocompleteResult *placeAutocompleteResult = [self.placeSuggestionsArray objectAtIndex:indexPath.row];
    self.locationNameTxtField.text = placeAutocompleteResult.placeDescription;
    HIPlaceDetailsRequest *placeDetailRequest = [[HIPlaceDetailsRequest alloc] init];
    placeDetailRequest.key = kGoogleMapApiKey;
    placeDetailRequest.placeId = placeAutocompleteResult.placeId;
    [_placesManager searchForPlaceDetailsResultWithRequest:placeDetailRequest];
    self.placeSuggestionsArray = nil;
    self.placeSuggestionsArray = [[NSArray alloc] init];
    [self.placeSgstnTbl reloadData];
    self.placeSgstnTbl.hidden = YES;
    [self.locationNameTxtField resignFirstResponder];
}

#pragma mark - HIPlacesManagerDelegate protocol methods

- (void)placesManager:(HIPlacesManager *)placesManager didSearchForPlaceAutocompleteResults:(NSArray *)placeAutocompleteResults
{
    NSLog(@"places %@",placeAutocompleteResults);
    self.placeSuggestionsArray = placeAutocompleteResults;
    [self.placeSgstnTbl reloadData];
    self.placeSgstnTbl.hidden = NO;
}

- (void)placesManager:(HIPlacesManager *)placesManager searchForPlaceAutocompleteResultsDidFailWithError:(NSError *)error
{
    //    NSString *errorCode = [NSString stringWithFormat:@"Error Code: %ld", (long)error.code];
    /*
     if(!self.alertView){
     self.alertView = [[UIAlertView alloc]
     initWithTitle:@"Ort nicht gefunden"
     message:nil
     delegate:self
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [self.alertView show];
     }
     */
}

- (void)placesManager:(HIPlacesManager *)placesManager didSearchForPlaceDetailsResult:(HIPlaceDetailsResult *)placeDetailsResult
{
    NSLog(@"latitude %f, longitude %f and name %@",placeDetailsResult.location.latitude, placeDetailsResult.location.longitude, placeDetailsResult.name);
    [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    [self zoomMapAndCenterAtLatitude:placeDetailsResult.location.latitude andLongitude:placeDetailsResult.location.longitude andTitle:placeDetailsResult.name];
}

- (void)placesManager:(HIPlacesManager *)placesManager searchForPlaceDetailsResultDidFailWithError:(NSError *)error
{
    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Ort nicht gefunden" andMessage:@"Die angegebene Adresse konnte nicht gefunden werden"];
    [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    self.alertView = nil;
}

-(void)userLocationOnOff:(id)sender
{
    self.isUserLocationOn = !self.isUserLocationOn;
    //    [[PDKeychainBindings sharedKeychainBindings] valueForKey:kKeychainUserLatitude];
    
    if(self.isUserLocationOn) {
        [self.locationOnOffBtn setImage:[UIImage imageNamed:@"gpsOn"] forState:UIControlStateNormal];
        self.userLatitude = [UnfallheldenUtilities sharedSingletonInstance].userlatitude;
        self.userLongitude = [UnfallheldenUtilities sharedSingletonInstance].userlongitude;
        self.map.showsUserLocation = YES;
        MKCoordinateRegion region;
        region.center.latitude  = self.userLatitude;
        region.center.longitude = self.userLongitude;
        //Set Zoom level using Span
        MKCoordinateSpan span;
        span.latitudeDelta  = .1;
        span.longitudeDelta = .1;
        region.span = span;
        [self.map setRegion:region animated:YES];
        [[UnfallheldenHttpClient sharedClient] getWeatherWithLatitude:[UnfallheldenUtilities sharedSingletonInstance].userlatitude longitude:[UnfallheldenUtilities sharedSingletonInstance].userlongitude WithCompletionBlock:^(NSData *data) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dataDictionary = [responseString JSONValue];
                NSLog(@"%@",[[[dataDictionary objectForKey:@"main"] valueForKey:@"temp"] description]);
                double temp = [[[[dataDictionary objectForKey:@"main"] valueForKey:@"temp"] description] doubleValue] - 273.15;
                //                self.weatherTxtField.text = [NSString stringWithFormat:@"%@, %.1f °C",[[[[dataDictionary objectForKey:@"weather"] objectAtIndex:0] valueForKey:@"main"] description],temp];
                NSString *locationName = [NSString stringWithFormat:@"%@, %@",[[dataDictionary valueForKey:@"name"] description], [[[dataDictionary objectForKey:@"sys"] valueForKey:@"country"] description]];
                self.locationNameTxtField.text = locationName;
                [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            });
        }];
        
        /*
         CLGeocoder *geocoder = [[CLGeocoder alloc] init];
         
         CLLocation *currentLocation = [[CLLocation alloc] initWithLatitude:[UnfallheldenUtilities sharedSingletonInstance].userlatitude  longitude:[UnfallheldenUtilities sharedSingletonInstance].userlongitude];
         
         [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
         {
         NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
         if (error == nil && [placemarks count] > 0)
         {
         CLPlacemark *placemark = [placemarks lastObject];
         
         // strAdd -> take bydefault value nil
         NSString *strAdd = nil;
         
         if ([placemark.subThoroughfare length] != 0)
         strAdd = placemark.subThoroughfare;
         
         if ([placemark.thoroughfare length] != 0)
         {
         // strAdd -> store value of current location
         if ([strAdd length] != 0)
         strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
         else
         {
         // strAdd -> store only this value,which is not null
         strAdd = placemark.thoroughfare;
         }
         }
         
         if ([placemark.postalCode length] != 0)
         {
         if ([strAdd length] != 0)
         strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark postalCode]];
         else
         strAdd = placemark.postalCode;
         }
         
         if ([placemark.locality length] != 0)
         {
         if ([strAdd length] != 0)
         strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
         else
         strAdd = placemark.locality;
         }
         
         if ([placemark.administrativeArea length] != 0)
         {
         if ([strAdd length] != 0)
         strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
         else
         strAdd = placemark.administrativeArea;
         }
         
         if ([placemark.country length] != 0)
         {
         if ([strAdd length] != 0)
         strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark country]];
         else
         strAdd = placemark.country;
         }
         NSLog(@"placemark.ISOcountryCode =%@",placemark.ISOcountryCode);
         NSLog(@"placemark.country =%@",placemark.country);
         NSLog(@"placemark.postalCode =%@",placemark.postalCode);
         NSLog(@"placemark.administrativeArea =%@",placemark.administrativeArea);
         NSLog(@"placemark.locality =%@",placemark.locality);
         NSLog(@"placemark.subLocality =%@",placemark.subLocality);
         NSLog(@"placemark.subThoroughfare =%@",placemark.subThoroughfare);
         }
         
         }];
         */
    }
    else {
        [self.locationOnOffBtn setImage:[UIImage imageNamed:@"gpsOff"] forState:UIControlStateNormal];
        self.userLatitude = 0.0;
        self.userLongitude = 0.0;
        self.map.showsUserLocation = NO;
        self.locationNameTxtField.text = @"";
    }
}

- (void) zoomMapAndCenterAtLatitude:(double)latitude andLongitude:(double)longitude andTitle:(NSString*)locationName
{
    NSLog(@"latitude %f , longitude %f", latitude,longitude);
    MKCoordinateRegion region;
    region.center.latitude  = latitude;
    region.center.longitude = longitude;
    
    self.userLatitude = latitude;
    self.userLongitude = longitude;
    
    [self.map removeAnnotations:self.map.annotations];
    MapAnnotation *annotation = [[MapAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    annotation.title = locationName;
    annotation.gsIdentifier = [NSNumber numberWithInt:123];
    [self.map addAnnotation:annotation];
    
    //Set Zoom level using Span
    MKCoordinateSpan span;
    span.latitudeDelta  = .1;
    span.longitudeDelta = .1;
    region.span = span;
    
    //Move the map and zoom
    [self.map setRegion:region animated:YES];
    
    [[UnfallheldenHttpClient sharedClient] getWeatherWithLatitude:self.userLatitude longitude:self.userLongitude WithCompletionBlock:^(NSData *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSDictionary *dataDictionary = [responseString JSONValue];
            NSLog(@"%@",[[[dataDictionary objectForKey:@"main"] valueForKey:@"temp"] description]);
            double temp = [[[[dataDictionary objectForKey:@"main"] valueForKey:@"temp"] description] doubleValue] - 273.15;
            //            self.weatherTxtField.text = [NSString stringWithFormat:@"%@, %.1f °C",[[[[dataDictionary objectForKey:@"weather"] objectAtIndex:0] valueForKey:@"main"] description],temp];
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        });
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
