//
//  PicturesVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 2/2/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "PicturesVC.h"
#import "PicturesCollectionViewCell.h"
#import "ELCImagePickerVC.h"
#import "FinishedVC.h"
#import "UIImage+ResizeMagick.h"

@interface PicturesVC ()
{
    PicturesCollectionViewCell *sizingCell;
}


@end

@implementation PicturesVC
@synthesize picturesCollectionView,carImage,carImagesArray, noImageSelectedLbl, imgDeleteBtnHidden, carImagesArrayAfterDeletion;
@synthesize accidentId;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [self setTitle:@"Step 5 of 5"];
    [self setTitle:@"Schritt 5 von 5"];
    [super setupMenuBarButtonItems];
    
    //GoogleAnalytics
//    self.screenName = @"ReportAccidentPictureUploadScreen";
    
    NSString *identifier = @"PicturesCollectionViewCell";
    UINib *nib = [UINib nibWithNibName:@"PicturesCollectionViewCell" bundle: nil];
    [self.picturesCollectionView registerNib:nib forCellWithReuseIdentifier:identifier];
    self.carImage = [UIImage imageNamed:@"accidentImg"];
    NSString *carImageSelectedNotification = @"CarImageSelected";
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(carImageIsSelected)
     name:carImageSelectedNotification
     object:nil];
    self.carImagesArray = [[NSMutableArray alloc] init];
    self.navigationItem.rightBarButtonItems = nil;
    
    UIBarButtonItem *barButtonItem = [UIBarButtonItem addAnotherRightMenuBarButtonItemWithtarget:self andSelector:@selector(deleteImages:) andImage:[UIImage imageNamed:@"deleteBtn"]];
    
    UIBarButtonItem *barButtonItemHelp = [UIBarButtonItem rightMenuBarButtonItemWithtarget:self andSelector:@selector(helpBtnPress:) andImage:[UIImage imageNamed:@"helpBtn"]];
    
    UIBarButtonItem *barButtonItemEmpty = [UIBarButtonItem createEmptyBarButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,barButtonItemHelp,barButtonItemEmpty,barButtonItem, nil] animated:NO];
    self.imgDeleteBtnHidden = YES;
    self.carImagesArrayAfterDeletion = [[NSMutableArray alloc] init];
    
    self.navigationItem.backBarButtonItem = nil;
    UIBarButtonItem *sideMenuBtn = [UIBarButtonItem leftMenuBarButtonItemWithtarget:self andSelector:@selector(leftSideMenuBtnPress:)];
    UIBarButtonItem *negativeSpacer2 = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                        target:nil action:nil];
    negativeSpacer2.width = -8;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects: negativeSpacer2,sideMenuBtn, nil] animated:NO];
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:self];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    
    /*
     UINib *cellNib = [UINib nibWithNibName:@"PicturesCollectionViewCell" bundle:nil];
     [self.picturesCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"PicturesCollectionViewCell"];
     
     // get a cell as template for sizing
     sizingCell = [[cellNib instantiateWithOwner:nil options:nil] objectAtIndex:0];
     */
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ReportAccidentStep5"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.carImagesArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PicturesCollectionViewCell *cell=[self.picturesCollectionView dequeueReusableCellWithReuseIdentifier:@"PicturesCollectionViewCell" forIndexPath:indexPath];
    UIImage *imageOfCar = [self.carImagesArray objectAtIndex:indexPath.item];
    UIImage* resizedImage;
    if(IS_IPHONE_4 || IS_IPHONE_5){
        resizedImage = [imageOfCar resizedImageByMagick: @"145x110#"];
    }
    else if(IS_IPHONE_6) {
        resizedImage = [imageOfCar resizedImageByMagick: @"170x140#"];
    }
    else{
        resizedImage = [imageOfCar resizedImageByMagick: @"189x165#"];
    }
    
//    UIImage* resizedImage = [imageOfCar resizedImageWithMinimumSize:CGSizeMake(145, 110)];
//    UIImage* resizedImage = [self resizeGivenImage:imageOfCar];
    
    cell.carPictureImageView.image = resizedImage;
    
    if(self.imgDeleteBtnHidden == YES){
        cell.deleteImgBtn.hidden = YES;
    }
    else{
        cell.deleteImgBtn.hidden = NO;
    }
    cell.deleteImgBtn.tag = indexPath.item;
    [cell.deleteImgBtn addTarget:self action:@selector(deleteCellAndImage:) forControlEvents:UIControlEventTouchUpInside];
    //    cell.backgroundColor=[UIColor greenColor];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPHONE_4 || IS_IPHONE_5){
        return CGSizeMake(145, 110);
    }
    else if(IS_IPHONE_6) {
        return CGSizeMake(170, 140);
    }
    else{
        return CGSizeMake(189, 165);
    }
    /*
     [self _configureCell:sizingCell forIndexPath:indexPath];
     return [sizingCell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
     */
}

/*
 - (void)_configureCell:(PicturesCollectionViewCell *)cell forIndexPath:(NSIndexPath *)indexPath
 {
 sizingCell.carPictureImageView.image = [self.carImagesArray objectAtIndex:indexPath.row];
 }
 */

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    long ind = indexPath.item;
    long section=indexPath.section;
    NSLog(@"%ld,%ld",section,ind);
}

-(UIImage*)resizeGivenImage:(UIImage*)currentImage
{
    CGRect rect = CGRectMake(0,0,145,110);
    if(IS_IPHONE_4 || IS_IPHONE_5){
        rect.size = CGSizeMake(145, 110);
    }
    else if(IS_IPHONE_6) {
        rect.size = CGSizeMake(170, 140);
    }
    else{
        rect.size = CGSizeMake(189, 165);
    }
    UIGraphicsBeginImageContext( rect.size );
    [currentImage drawInRect:rect];
    UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imageData = UIImagePNGRepresentation(picture1);
    UIImage *img=[UIImage imageWithData:imageData];
    return img;
}

-(void)selectCarPicture:(id)sender
{
    ELCImagePickerVC *imagePicker = [[ELCImagePickerVC alloc] init];
    imagePicker.imagePickerType = ELCImagePickerTypeSingleImage;
    imagePicker.imagePickerCallFor = @"CarImage";
    //    if([GlobalScopeObjects sharedSingletonInstance].carImage != nil){
    //        imagePicker.preselectedImage = [GlobalScopeObjects sharedSingletonInstance].carImage;
    //    }
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:imagePicker];
    navController.navigationBar.translucent = YES;
    [self presentViewController:navController animated:YES completion:nil];
    self.imgDeleteBtnHidden = YES;
    [self.picturesCollectionView reloadData];
}

-(void)carImageIsSelected
{
    [self.carImagesArray addObject:[GlobalScopeObjects sharedSingletonInstance].carImage];
    self.noImageSelectedLbl.hidden = YES;
    [self.picturesCollectionView reloadData];
    
    [[UnfallheldenHttpClient sharedClient] uploadCarPhotoWithImage:[GlobalScopeObjects sharedSingletonInstance].carImage andAccidentId:self.accidentId WithCompletionBlock:^(NSData *data) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *dataDictionary = [responseString JSONValue];
        //            NSDictionary *userData = [[dataDictionary valueForKey:@"data"] description];
        NSLog(@"data dictionary : %@",dataDictionary);
        int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
        if(status == 1){
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Photo wurde erfolgreich hochgeladen" andMessage:@"Ihre Photos wurden erfolgreich in unserer Datenbank erfasst."];
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Bildupload fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Hochladen des Photos. Bitte versuchen Sie es erneut"];
        }
    }];
}

-(void)continueBtnPress:(id)sender
{
    FinishedVC *finishedVC = [[FinishedVC alloc] init];
    [self.navigationController pushViewController:finishedVC animated:YES];
    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Unfall erfolgreich gemeldet" andMessage:@"Wir haben Ihren Unfall erfasst und werden uns so schnell wie möglich mit Ihnen in Verbindung setzen."];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)deleteCellAndImage:(id)sender{
    if([self.carImagesArray count] == 1){
        //        NSIndexPath *indxPath = [NSIndexPath indexPathForItem:0 inSection:0];
        // Delete the items from the data source.
        //        [self deleteItemsFromDataSourceAtIndexPaths:[NSArray arrayWithObjects:indxPath,nil]];
        [self.carImagesArray removeObjectAtIndex:0];
        //        [self.picturesCollectionView deleteItemsAtIndexPaths:[NSArray arrayWithObjects:indxPath,nil]];
        [self.picturesCollectionView reloadData];
    }
    else{
        NSIndexPath *indxPath = [NSIndexPath indexPathForItem:[sender tag] inSection:0];
        // Delete the items from the data source.
        [self.carImagesArray removeObjectAtIndex:indxPath.item];
        //        [self.picturesCollectionView deleteItemsAtIndexPaths:[NSArray arrayWithObjects:indxPath,nil]];
        [self.picturesCollectionView reloadData];
    }
}

// This method is for deleting the selected images from the data source array
-(void)deleteItemsFromDataSourceAtIndexPaths:(NSArray  *)itemPaths {
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    for (NSIndexPath *itemPath  in itemPaths) {
        [indexSet addIndex:itemPath.row];
    }
    [self.carImagesArray removeObjectsAtIndexes:indexSet]; // self.images is my data source
}

-(IBAction)deleteImages:(id)sender
{
    self.imgDeleteBtnHidden = !self.imgDeleteBtnHidden;
    [self.picturesCollectionView reloadData];
}

-(IBAction)helpBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = Step5Text;
    customPopup.titleLabel.text = @"Hilfe";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}

-(IBAction)leftSideMenuBtnPress:(id)sender
{
    [super leftSideMenuButtonPressed:sender];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
