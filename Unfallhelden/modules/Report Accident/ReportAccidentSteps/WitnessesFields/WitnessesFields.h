//
//  WitnessesFields.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/19/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WitnessesFields : UIView

@property (weak, nonatomic) IBOutlet UITextField *witnessNameField;
@property (weak, nonatomic) IBOutlet UITextField *witnessPhoneField;

@end
