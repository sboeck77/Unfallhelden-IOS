//
//  WitnessesFields.m
//  Unfallhelden
//
//  Created by Adil Anwer on 2/19/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "WitnessesFields.h"

@implementation WitnessesFields

@synthesize witnessNameField,witnessPhoneField;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"WitnessesFields" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
