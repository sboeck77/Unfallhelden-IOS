//
//  MoreDataOnAccidentVCViewController.m
//  Unfallhelden
//
//  Created by Adil Anwer on 1/28/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "MoreDataOnAccidentVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "LocationTimeWeatherVC.h"
#import "ReportAccidentData.h"
#import "Insurer.h"
#import "CarBrand.h"

@interface MoreDataOnAccidentVC ()
@property (nonatomic, weak) IBOutlet UITextField *firtsNameTxtField;
@property (nonatomic, weak) IBOutlet UITextField *lastnameTxtField;
@property (nonatomic, weak) IBOutlet UITextField *plateNumTxtField;
@property (nonatomic, weak) IBOutlet UITextField *phoneTxtField;
@property (nonatomic, weak) IBOutlet UITextField *streetNameTxtField;
@property (nonatomic, weak) IBOutlet UITextField *streetNumTxtField;
@property (nonatomic, weak) IBOutlet UITextField *cityNameTxtField;
@property (nonatomic, weak) IBOutlet UITextField *zipCodeTxtField;
@property (nonatomic, weak) IBOutlet UITextField *countryTxtField;
@property (nonatomic, weak) IBOutlet UITextField *additionalAddresstxtField;
@property (nonatomic, weak) IBOutlet UITextField *carBrandTxtField;
@property (nonatomic, weak) IBOutlet UITextField *carTypeField;
@property (nonatomic, weak) IBOutlet UITextField *insuranceCompanyTxtField;
@property (nonatomic) BOOL isCompanyCar;
@property (nonatomic) BOOL isLeaseCar;
@property (nonatomic) BOOL plateNumNotSure;
@property (nonatomic, weak) IBOutlet UITextField *companyName;
@property (nonatomic, weak) IBOutlet UITextField *leasingCompanyName;
@end

@implementation MoreDataOnAccidentVC
@synthesize moreAccidentLbl,rAccidentScrollView = _rAccidentScrollView;
@synthesize nameContainerView,continueBtnBg,contentView,backgroundImg,contentViewHeightContraint,contentViewBottomSpaceToSuperview,countryTxtField,countryArray,carBrandTxtField,insuranceCompanyTxtField;
@synthesize carBrandArray,insuranceCompanyArray,typeOfPicker,carTypeField,carTypeArray,unfallheldenPickerView,selectedCarBrandId,selectedInsurerId;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setTranslatesAutoresizingMaskIntoConstraints:YES];
    [super setupMenuBarButtonItems];
    //    [self setTitle:@"Step 2 of 5"];
    [self setTitle:@"Schritt 2 von 5"];
    
    //GoogleAnalytics
//    self.screenName = @"ReportAccidentStep2";
    
    self.isCompanyCar = NO;
    self.isLeaseCar = NO;
    self.plateNumNotSure = NO;
    
    self.selectedInsurerId = @"";
    self.selectedCarBrandId = @"";
    
    for(UIView *vw in self.nameContainerView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
        }
    }
    
    for(UIView *vw in self.contentView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            if(IS_IPHONE_5 || IS_IPHONE_4){
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:13]];
            }
            else{
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
            }
        } else if([vw isKindOfClass:[UIImageView class]]) {
            ((UIImageView*)vw).layer.cornerRadius = 3.0f;
            ((UIImageView*)vw).layer.masksToBounds = YES;
            //            [((UILabel*)vw) setFont:[UIFont regularLobstrFontWithSize:14]];
        } else if([vw isKindOfClass:[UIButton class]]) {
            ((UIButton*)vw).layer.cornerRadius = 3.0f;
            ((UIButton*)vw).layer.masksToBounds = YES;
            //            [((UIButton*)vw).titleLabel setFont:[UIFont regularLobstrFontWithSize:18]];
        }
    }
    if(IS_IPHONE_5 || IS_IPHONE_4){
        self.moreAccidentLbl.font = [UIFont regularUnfallheldenFontWithSize:11.8];
    }
    else{
        self.moreAccidentLbl.font = [UIFont regularUnfallheldenFontWithSize:14];
    }
    [self.rAccidentScrollView contentSizeToFit];
    
    self.carBrandArray = [[NSMutableArray alloc] init];
    self.insuranceCompanyArray = [[NSMutableArray alloc] init];
    
    [[UnfallheldenHttpClient sharedClient] getGeneralDataWithCompletionBlock:^(NSData *data) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *dataDictionary = [responseString JSONValue];
        NSLog(@"data dictionary : %@",dataDictionary);
        //        int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
        //        if(status == 1){
        NSMutableArray *brandsArray = [dataDictionary objectForKey:@"car_brands"];
        NSMutableArray *insurerArray = [dataDictionary objectForKey:@"insurers"];
        
        NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"name" ascending: YES];
        brandsArray = [[brandsArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]] mutableCopy];
        
        for (int brandsCount = 0; brandsCount < [brandsArray count]; brandsCount++) {
            CarBrand *brand = [[CarBrand alloc] init];
            brand.carBrandId = [[[brandsArray objectAtIndex:brandsCount] valueForKey:@"id"] description];
            brand.carBrandName = [[[brandsArray objectAtIndex:brandsCount] valueForKey:@"name"] description];
            [self.carBrandArray addObject:brand];
        }
        
        for (int insurerCount = 0; insurerCount < [insurerArray count]; insurerCount++) {
            Insurer *insurer = [[Insurer alloc] init];
            insurer.insurerId = [[[insurerArray objectAtIndex:insurerCount] valueForKey:@"id"] description];
            insurer.insurerName = [[[insurerArray objectAtIndex:insurerCount] valueForKey:@"name"] description];
            [self.insuranceCompanyArray addObject:insurer];
        }
        //        }
        //        else{
        //            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Abruf der Fahrzeugdaten fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Abruf ihrer Fahrzeugdaten. Bitte versuchen Sie es erneut"];
        //        }
    }];
    
    UIBarButtonItem *barButtonItem = [UIBarButtonItem rightMenuBarButtonItemWithtarget:self andSelector:@selector(helpBtnPress:) andImage:[UIImage imageNamed:@"helpBtn"]];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,barButtonItem, nil] animated:NO];
    
    self.countryArray = [[NSMutableArray alloc] init];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CountryList" ofType:@"txt"];
    NSData *jsonData = [NSData dataWithContentsOfFile:filePath];
    NSString *responseString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *dataDictionary = [responseString JSONValue];
    self.countryArray = [[dataDictionary objectForKey:@"string-array"] objectForKey:@"item"];
}

-(IBAction)helpBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = Step2Text;
    customPopup.titleLabel.text = @"Hilfe";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ReportAccidentStep2"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [self.view layoutIfNeeded];
    [self.view layoutSubviews];
    CGFloat heightOfPicker = 0.0f;
    
    if(IS_IPHONE_5){
        self.rAccidentScrollView.frame = CGRectMake(self.view.frame.origin.x, self.rAccidentScrollView.frame.origin.y, self.view.frame.size.width, self.rAccidentScrollView.frame.size.height);
        self.contentViewHeightContraint.constant = 820;
        self.contentViewBottomSpaceToSuperview.constant = -90;
        [self.view setNeedsUpdateConstraints];
        heightOfPicker = 206;
    }
    else if (IS_IPHONE_4){
        self.rAccidentScrollView.frame = CGRectMake(self.view.frame.origin.x, self.rAccidentScrollView.frame.origin.y, self.view.frame.size.width, self.rAccidentScrollView.frame.size.height);
        self.contentViewHeightContraint.constant = 800;
        self.contentViewBottomSpaceToSuperview.constant = -130;
        [self.view setNeedsUpdateConstraints];
        heightOfPicker = 206;
    }
    else{
        self.rAccidentScrollView.frame = CGRectMake(self.view.frame.origin.x, self.rAccidentScrollView.frame.origin.y, self.view.frame.size.width, self.rAccidentScrollView.frame.size.height);
    }
    [self.rAccidentScrollView contentSizeToFit];
    
    if (IS_IPHONE_6 || IS_IPHONE_6_Plus){
        heightOfPicker = 260;
    }
    
    self.unfallheldenPickerView = [[UnfallheldenPickerView alloc] init];
    CGRect pickerFrame = self.unfallheldenPickerView.frame;
    pickerFrame.size.height = heightOfPicker;
    self.unfallheldenPickerView.frame = pickerFrame;
    [self.unfallheldenPickerView.pickerDoneButton setAction:@selector(pickerDoneBtnPress:)];
    
    self.unfallheldenPickerView.pickerView.dataSource = self;
    self.unfallheldenPickerView.pickerView.delegate = self;
    
    [self.countryTxtField setTintColor:[UIColor clearColor]];
    self.countryTxtField.inputView = self.unfallheldenPickerView;
    
    [self.carBrandTxtField setTintColor:[UIColor clearColor]];
    self.carBrandTxtField.inputView = self.unfallheldenPickerView;
    
    [self.insuranceCompanyTxtField setTintColor:[UIColor clearColor]];
    self.insuranceCompanyTxtField.inputView = self.unfallheldenPickerView;
    
    //    [self.carTypeField setTintColor:[UIColor clearColor]];
    //    self.carTypeField.inputView = self.unfallheldenPickerView;
    
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];
    UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[self class] toolbarHeight])];
    [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
    self.phoneTxtField.inputAccessoryView = toolbar;
    self.streetNumTxtField.inputAccessoryView = toolbar;
    self.zipCodeTxtField.inputAccessoryView = toolbar;
}

- (void)doneButtonDidPressed:(id)sender {
    [self.phoneTxtField resignFirstResponder];
    [self.streetNumTxtField resignFirstResponder];
    [self.zipCodeTxtField resignFirstResponder];
}

+ (CGFloat)toolbarHeight {
    // This method will handle the case that the height of toolbar may change in future iOS.
    return 44.f;
}

#pragma mark - Others
-(void)continueBtnPress:(id)sender
{
    if(([self.firtsNameTxtField.text length] > 0 && [self.lastnameTxtField.text length] > 0) || [self.plateNumTxtField.text length] > 0 || self.plateNumNotSure == TRUE){
        
        if([self.plateNumTxtField.text length] > 0){
            BOOL containsLetter = NSNotFound != [self.plateNumTxtField.text rangeOfCharacterFromSet:NSCharacterSet.letterCharacterSet].location;
            BOOL containsNumber = NSNotFound != [self.plateNumTxtField.text rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location;
            
            if(containsLetter != TRUE || containsNumber != TRUE){
                [self.plateNumTxtField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                self.plateNumTxtField.layer.borderColor=[[UIColor redColor]CGColor];
                self.plateNumTxtField.layer.borderWidth= 1.3f;
                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Das KFZ Kennzeichen muss Zahlen und Buchstaben enthalten."];
            }
            else{
                [[ReportAccidentData sharedSingletonInstance] prepareMoreDataWithACFirstName:self.firtsNameTxtField.text acLastName:self.lastnameTxtField.text acPlateNumber:self.plateNumTxtField.text acPlateNumSure:self.plateNumNotSure acPhone:self.phoneTxtField.text acStreetName:self.streetNameTxtField.text acStreetNum:self.streetNumTxtField.text acCity:self.cityNameTxtField.text acZipCode:self.zipCodeTxtField.text acCountry:self.countryTxtField.text acAdditionalAddress:self.additionalAddresstxtField.text acCarBrand:self.selectedCarBrandId acCarType:self.carTypeField.text acInsuranceCompany:self.selectedInsurerId];
                
                LocationTimeWeatherVC *ltwVC = [[LocationTimeWeatherVC alloc] init];
                [self.navigationController pushViewController:ltwVC animated:YES];
            }
        }
        else{
            //            [[ReportAccidentData sharedSingletonInstance] prepareMoreDataWithACFirstName:self.firtsNameTxtField.text acLastName:self.lastnameTxtField.text acPlateNumber:self.plateNumTxtField.text acPlateNumSure:self.plateNumNotSure acPhone:self.phoneTxtField.text acStreetName:self.streetNameTxtField.text acStreetNum:self.streetNumTxtField.text acCity:self.cityNameTxtField.text acZipCode:self.zipCodeTxtField.text acCountry:self.countryTxtField.text acAdditionalAddress:self.additionalAddresstxtField.text acCarBrand:self.selectedCarBrandId acCarType:self.carTypeField.text acInsuranceCompany:self.selectedInsurerId acCompanyCar:self.isCompanyCar acLeaseCar:self.isLeaseCar acCompanyName:self.companyName.text acLeasingCompany:self.leasingCompanyName.text];
            
            [[ReportAccidentData sharedSingletonInstance] prepareMoreDataWithACFirstName:self.firtsNameTxtField.text acLastName:self.lastnameTxtField.text acPlateNumber:self.plateNumTxtField.text acPlateNumSure:self.plateNumNotSure acPhone:self.phoneTxtField.text acStreetName:self.streetNameTxtField.text acStreetNum:self.streetNumTxtField.text acCity:self.cityNameTxtField.text acZipCode:self.zipCodeTxtField.text acCountry:self.countryTxtField.text acAdditionalAddress:self.additionalAddresstxtField.text acCarBrand:self.selectedCarBrandId acCarType:self.carTypeField.text acInsuranceCompany:self.selectedInsurerId];
            
            LocationTimeWeatherVC *ltwVC = [[LocationTimeWeatherVC alloc] init];
            [self.navigationController pushViewController:ltwVC animated:YES];
        }
    }
    else{
        if(self.plateNumTxtField.background == nil && [self.plateNumTxtField.text length] == 0){
            self.plateNumTxtField.layer.borderColor=[[UIColor redColor]CGColor];
            self.plateNumTxtField.layer.borderWidth= 1.3f;
            [self.plateNumTxtField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
            [self.plateNumTxtField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            self.plateNumTxtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ wird benötigt",self.plateNumTxtField.placeholder] attributes:@{NSForegroundColorAttributeName: color}];
        }
        if(self.firtsNameTxtField.background == nil && [self.firtsNameTxtField.text length] == 0){
            self.firtsNameTxtField.layer.borderColor=[[UIColor redColor]CGColor];
            self.firtsNameTxtField.layer.borderWidth= 1.3f;
            [self.firtsNameTxtField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
            [self.firtsNameTxtField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            self.firtsNameTxtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ wird benötigt",self.firtsNameTxtField.placeholder] attributes:@{NSForegroundColorAttributeName: color}];
        }
        if(self.lastnameTxtField.background == nil && [self.lastnameTxtField.text length] == 0){
            self.lastnameTxtField.layer.borderColor=[[UIColor redColor]CGColor];
            self.lastnameTxtField.layer.borderWidth= 1.3f;
            [self.lastnameTxtField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
            [self.lastnameTxtField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            self.lastnameTxtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ wird benötigt",self.lastnameTxtField.placeholder] attributes:@{NSForegroundColorAttributeName: color}];
        }
        
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Vorname, Nachname oder KFZ Kennzeichen werden benötigt. Sie können auch \"Ich weiß leider weder den Namen noch das Kennzeichen\" auswählen."];
    }
}

#pragma mark - UITextFieldDelegateMethods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if([textField isEqual:self.countryTxtField]){
        self.typeOfPicker = CountryPicker;
    }
    else if ([textField isEqual:self.carBrandTxtField]){
        self.typeOfPicker = CarBrandPicker;
    }
    else if ([textField isEqual:self.insuranceCompanyTxtField]){
        self.typeOfPicker = InsuranceCompanyPicker;
    }
    //    else if ([textField isEqual:self.carTypeField]){
    //        //        self.typeOfPicker = CarTypePicker;
    //    }
    [self.unfallheldenPickerView.pickerView reloadAllComponents];
}

#pragma mark - UITextFieldDelegateMethods
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if([textField isEqual:self.plateNumTxtField]){
        BOOL containsLetter = NSNotFound != [self.plateNumTxtField.text rangeOfCharacterFromSet:NSCharacterSet.letterCharacterSet].location;
        BOOL containsNumber = NSNotFound != [self.plateNumTxtField.text rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location;
        
        if(containsLetter != TRUE || containsNumber != TRUE){
            [self.plateNumTxtField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
            self.plateNumTxtField.layer.borderColor=[[UIColor redColor]CGColor];
            self.plateNumTxtField.layer.borderWidth= 1.3f;
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Das KFZ Kennzeichen muss Zahlen und Buchstaben enthalten."];
        }
        else{
            self.plateNumTxtField.background = nil;
            self.plateNumTxtField.layer.borderColor=[[UIColor whiteColor]CGColor];
            self.plateNumTxtField.layer.borderWidth= 1.3f;
        }
    }
    if([textField isEqual:self.firtsNameTxtField] && [textField.text length] > 0){
        textField.background = nil;
        textField.layer.borderColor=[[UIColor whiteColor]CGColor];
        textField.layer.borderWidth= 1.3f;
    }
    else if([textField isEqual:self.lastnameTxtField] && [textField.text length] > 0){
        textField.background = nil;
        textField.layer.borderColor=[[UIColor whiteColor]CGColor];
        textField.layer.borderWidth= 1.3f;
    }
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.plateNumTxtField]){
        BOOL containsLetter = NSNotFound != [self.plateNumTxtField.text rangeOfCharacterFromSet:NSCharacterSet.letterCharacterSet].location;
        BOOL containsNumber = NSNotFound != [self.plateNumTxtField.text rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location;
        
        if(containsLetter != TRUE || containsNumber != TRUE){
            [self.plateNumTxtField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
            self.plateNumTxtField.layer.borderColor=[[UIColor redColor]CGColor];
            self.plateNumTxtField.layer.borderWidth= 1.3f;
        }
        else{
            self.plateNumTxtField.background = nil;
            self.plateNumTxtField.layer.borderColor=[[UIColor whiteColor]CGColor];
            self.plateNumTxtField.layer.borderWidth= 1.3f;
        }
    }
    if([textField isEqual:self.firtsNameTxtField] && [textField.text length] > 0){
        textField.background = nil;
        textField.layer.borderColor=[[UIColor whiteColor]CGColor];
        textField.layer.borderWidth= 1.3f;
    }
    else if([textField isEqual:self.lastnameTxtField] && [textField.text length] > 0){
        textField.background = nil;
        textField.layer.borderColor=[[UIColor whiteColor]CGColor];
        textField.layer.borderWidth= 1.3f;
    }
    [textField resignFirstResponder];
    return TRUE;
}

#pragma mark - Open PickerView

-(void)showCountryPicker:(id)sender
{
    for(UIView *vw in self.rAccidentScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    self.typeOfPicker = CountryPicker;
    [self.unfallheldenPickerView.pickerView reloadAllComponents];
    [self.countryTxtField becomeFirstResponder];
}

-(void)showBrandsPicker:(id)sender
{
    for(UIView *vw in self.rAccidentScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    self.typeOfPicker = CarBrandPicker;
    [self.unfallheldenPickerView.pickerView reloadAllComponents];
    [self.carBrandTxtField becomeFirstResponder];
}

-(void)showInsuranceComapniesPicker:(id)sender
{
    for(UIView *vw in self.rAccidentScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    self.typeOfPicker = InsuranceCompanyPicker;
    [self.unfallheldenPickerView.pickerView reloadAllComponents];
    [self.insuranceCompanyTxtField becomeFirstResponder];
}

-(void)showCarTypePicker:(id)sender
{
    for(UIView *vw in self.rAccidentScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    self.typeOfPicker = CarTypePicker;
    [self.unfallheldenPickerView.pickerView reloadAllComponents];
    [self.carTypeField becomeFirstResponder];
}

#pragma mark - PickerView Done Button Press
-(IBAction)pickerDoneBtnPress:(id)sender
{
    [self.countryTxtField resignFirstResponder];
    [self.insuranceCompanyTxtField resignFirstResponder];
    [self.carBrandTxtField resignFirstResponder];
    [self.carTypeField resignFirstResponder];
}

#pragma mark - UIPickerView DataSource

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(self.typeOfPicker == CountryPicker){
        return [self.countryArray count];
    }
    else if (self.typeOfPicker == CarBrandPicker){
        return [self.carBrandArray count];
    }
    //    else if (self.typeOfPicker == CarTypePicker){
    //        return [self.carTypeArray count];
    //    }
    else {
        return [self.insuranceCompanyArray count];
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if(self.typeOfPicker == CountryPicker){
        return [self.countryArray objectAtIndex:row];
    }
    else if (self.typeOfPicker == CarBrandPicker){
        CarBrand *carBrand =  [self.carBrandArray objectAtIndex:row];
        return carBrand.carBrandName;
    }
    //    else if (self.typeOfPicker == CarTypePicker){
    //        return [self.carTypeArray objectAtIndex:row];
    //    }
    else {
        Insurer *insurerCompany = [self.insuranceCompanyArray objectAtIndex:row];
        return insurerCompany.insurerName;
    }
}

#pragma mark - UIPickerView Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if(self.typeOfPicker == CountryPicker){
        if(row == 0){
            self.countryTxtField.text = @"";
        }
        else{
            self.countryTxtField.text = [self.countryArray objectAtIndex:row];
        }
    }
    else if (self.typeOfPicker == CarBrandPicker){
        CarBrand *carBrand =  [self.carBrandArray objectAtIndex:row];
        self.carBrandTxtField.text = carBrand.carBrandName;
        self.selectedCarBrandId = carBrand.carBrandId;
    }
    //    else if (self.typeOfPicker == CarTypePicker){
    //        self.carTypeField.text = [self.carTypeArray objectAtIndex:row];
    //    }
    else {
        Insurer *insurerCompany = [self.insuranceCompanyArray objectAtIndex:row];
        self.insuranceCompanyTxtField.text = insurerCompany.insurerName;
        self.selectedInsurerId = insurerCompany.insurerId;
    }
}

#pragma mark - check box pressed
-(void)plateNumCheckBtnPress:(id)sender
{
    self.plateNumNotSure = !self.plateNumNotSure;
    if(self.plateNumNotSure) {
        [self.plateNumCheckBox setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
    else {
        [self.plateNumCheckBox setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
    }
}

-(void)companyCheckBtnPress:(id)sender
{
    self.isCompanyCar = !self.isCompanyCar;
    if(self.isCompanyCar) {
        [self.companyCarCheckBox setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
    else {
        [self.companyCarCheckBox setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
    }
}

-(void)leaseCheckBtnPress:(id)sender
{
    self.isLeaseCar = !self.isLeaseCar;
    if(self.isLeaseCar) {
        [self.leaseCarCheckBox setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
    else {
        [self.leaseCarCheckBox setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
