//
//  RecordVoiceVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 2/2/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "RecordVoiceVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "PicturesVC.h"
#import "WitnessesFields.h"
#import "ReportAccidentData.h"

@interface RecordVoiceVC ()

@property (nonatomic, strong) AudioRecorder *recorder;
@property (nonatomic) int secondsLeft;
@property (nonatomic, strong) NSTimer *audioRecordingCounter;
@property (nonatomic, strong) NSTimer *audioRecordingTime;
@property (nonatomic, strong) UILabel *txtViewPleaceholderLbl;
@property (nonatomic, strong) NSMutableArray *arrayWithWitnesses;
@property (nonatomic) int witnsNameFieldTag;
@property (nonatomic) int witnsPhoneFieldTag;
@property (nonatomic) int witnsesCount;

@property (nonatomic, strong) UITextField *wNameField2;
@property (nonatomic, strong) UITextField *wPhoneField2;
@property (nonatomic, strong) UITextField *wNameField3;
@property (nonatomic, strong) UITextField *wPhoneField3;
@property (nonatomic, strong) UITextField *wNameField4;
@property (nonatomic, strong) UITextField *wPhoneField4;

@end

@implementation RecordVoiceVC
@synthesize kaScrollView,witnessesFieldsView,bottomBtnsContainer,witnessesArray,deleteRecordingBtn,step4Lbl;
@synthesize containerView,nameField,phoneField,recordButton,descriptionTxtView,audioFileURL,countDownLabel;
@synthesize contentViewHeightContraint,bottomBtnsContainerViewTopConstraint,addFieldBtn,secondContinueBtn,continueBtn,witnessFieldsContainerHeight;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //    [self setTitle:@"Step 4 of 5"];
    [self setTitle:@"Schritt 4 von 5"];
    [super setupMenuBarButtonItems];
    
    //GoogleAnalytics
//    self.screenName = @"ReportAccidentStep4";
    
    self.kaScrollView.frame = self.view.frame;
    self.recorder = [AudioRecorder sharedSingletonInstance];
    [self.recorder setupAudioRecorder];
    [self.recorder setAudioPlayerDelegate:self];
    [self setupTextFields];
    self.deleteRecordingBtn.hidden = YES;
    self.secondsLeft = 20;
    self.countDownLabel.hidden = YES;
    
    self.txtViewPleaceholderLbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0,self.descriptionTxtView.frame.size.width - 10.0, 34.0)];
    
    //    [self.txtViewPleaceholderLbl setText:@"Your description of the accident"];
    [self.txtViewPleaceholderLbl setText:@"Beschreibung des Unfallhergangs"];
    [self.txtViewPleaceholderLbl setBackgroundColor:[UIColor clearColor]];
    [self.txtViewPleaceholderLbl setTextColor:[UIColor whiteColor]];
    if(IS_IPHONE_5 || IS_IPHONE_4){
        [self.txtViewPleaceholderLbl setFont:[UIFont regularUnfallheldenFontWithSize:13]];
    }
    else{
        [self.txtViewPleaceholderLbl setFont:[UIFont regularUnfallheldenFontWithSize:15]];
    }
    
    [self.descriptionTxtView addSubview:self.txtViewPleaceholderLbl];
    
    self.witnessesArray = [[NSMutableArray alloc] init];
    for (int count = 0; count < 4; count++) {
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"",@"", nil] forKeys:[NSArray arrayWithObjects:@"name",@"phone", nil]];
        [self.witnessesArray addObject:dictionary];
    }
    
    self.witnsesCount = 1;
    
    [self.view layoutIfNeeded];
    [self.view layoutSubviews];
    if(IS_IPHONE_5){
        self.contentViewHeightContraint.constant = 560;
    }
    else if (IS_IPHONE_4){
        self.contentViewHeightContraint.constant = 540;
    }
    else if (IS_IPHONE_6_Plus){
        self.contentViewHeightContraint.constant = 666;
    }
    [self.view setNeedsUpdateConstraints];
    
    self.witnsNameFieldTag = 1001;
    self.witnsPhoneFieldTag = 2001;
    
    /*
     self.nameField.frame = CGRectMake(self.nameField.frame.origin.x, self.nameField.frame.origin.y, self.containerView.center.x - 20, self.nameField.frame.size.height);
     self.phoneField.frame = CGRectMake(CGRectGetMaxX(self.nameField.frame)+10, self.phoneField.frame.origin.y, self.containerView.center.x - 20, self.phoneField.frame.size.height);*/
    
    UIBarButtonItem *barButtonItem = [UIBarButtonItem rightMenuBarButtonItemWithtarget:self andSelector:@selector(helpBtnPress:) andImage:[UIImage imageNamed:@"helpBtn"]];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,barButtonItem, nil] animated:NO];
    [GlobalScopeObjects sharedSingletonInstance].audioFileName = @"";
    
    if(IS_IPHONE_4 || IS_IPHONE_5){
        self.step4Lbl.font = [UIFont regularUnfallheldenFontWithSize:11];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ReportAccidentStep4"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    /*
     self.nameField.frame = CGRectMake(self.nameField.frame.origin.x, self.nameField.frame.origin.y, self.containerView.center.x - 20, self.nameField.frame.size.height);
     self.phoneField.frame = CGRectMake(CGRectGetMaxX(self.nameField.frame)+10, self.phoneField.frame.origin.y, self.containerView.center.x - 20, self.phoneField.frame.size.height);*/
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];
    UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[self class] toolbarHeight])];
    [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
    self.descriptionTxtView.inputAccessoryView = toolbar;
}

- (void)doneButtonDidPressed:(id)sender {
    [self.descriptionTxtView resignFirstResponder];
}

+ (CGFloat)toolbarHeight {
    // This method will handle the case that the height of toolbar may change in future iOS.
    return 44.f;
}

-(void)continueBtnPress:(id)sender
{
    NSString *message = @"";
    if([[[[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsUserAlreadyLoggedIn] description] isEqualToString:@"YES"]){
    message = [NSString stringWithFormat:@"Ihr Unfall wird nun gemeldet und Sie können die Daten später nicht mehr ändern."];
    }
    else{
        message = [NSString stringWithFormat:@"Ihr Unfall wird nun gemeldet. Sie können die Daten danach nicht mehr ändern, sofern Sie den Unfall ohne Registrierung melden möchten."];
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bitte bestätigen Sie" message:message delegate:self cancelButtonTitle:@"Abbrechen" otherButtonTitles:@"Unfall melden!", nil];
    alert.tag = 125;
    [alert show];
}

-(void)setupTextFields
{
    for(UIView *vw in self.witnessesFieldsView.subviews) {
        if([vw isKindOfClass:[UIView class]]){
            UIView *vw1 = (UIView*)vw;
            for(UIView *vw2 in vw1.subviews) {
                if([vw2 isKindOfClass:[UITextField class]]) {
                    UITextField *textField = (UITextField*)vw2;
                    textField.layer.cornerRadius = 2.0f;
                    textField.layer.masksToBounds = YES;
                    textField.layer.borderColor=[[UIColor whiteColor]CGColor];
                    textField.layer.borderWidth= 1.3f;
                    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
                    textField.leftView = paddingView;
                    textField.leftViewMode = UITextFieldViewModeAlways;
                    [textField setTextColor:[UIColor whiteColor]];
                    UIColor *color = [UIColor whiteColor];
                    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
                    textField.delegate = self;
                    
                    if (IS_IPHONE_4 || IS_IPHONE_5) {
                        [textField setFont:[UIFont regularUnfallheldenFontWithSize:13]];
                    }
                    else{
                        [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
                    }
                }
            }
        }
    }
    self.descriptionTxtView.layer.cornerRadius = 2.0f;
    self.descriptionTxtView.layer.masksToBounds = YES;
    self.descriptionTxtView.layer.borderColor=[[UIColor whiteColor]CGColor];
    self.descriptionTxtView.layer.borderWidth= 1.3f;
    [self.descriptionTxtView setTextColor:[UIColor whiteColor]];
    self.descriptionTxtView.delegate = self;
    if (IS_IPHONE_4 || IS_IPHONE_5) {
        [self.descriptionTxtView setFont:[UIFont regularUnfallheldenFontWithSize:13]];
    }
    else{
        [self.descriptionTxtView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
    }
    
    [self.nameField addTarget:self
                       action:@selector(textFieldValueChanged:)
             forControlEvents:UIControlEventEditingChanged];
    [self.phoneField addTarget:self
                        action:@selector(textFieldValueChanged:)
              forControlEvents:UIControlEventEditingChanged];
}

-(void)recordAudio:(id)sender
{
    if([[self.recordButton currentTitle] isEqualToString:@"Sprachnachricht aufzeichnen"]){
        [self.recordButton setTitle:@"Aufnahme stoppen" forState:UIControlStateNormal];
        [self.recorder recordAudio];
        self.audioRecordingTime = [NSTimer scheduledTimerWithTimeInterval:60
                                                                   target:self
                                                                 selector:@selector(stopAudio)
                                                                 userInfo:nil
                                                                  repeats:NO];
        self.secondsLeft = 60;
        self.countDownLabel.text = @"00:60";
        self.countDownLabel.hidden = NO;
        self.deleteRecordingBtn.hidden = YES;
        self.audioRecordingCounter = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateRecordingCountdown) userInfo:nil repeats: YES];
    }
    else if([[self.recordButton currentTitle] isEqualToString:@"Aufnahme stoppen"]){
        [self.recordButton setTitle:@"Aufnahme abspielen" forState:UIControlStateNormal];
        [self stopAudio];
    }
    else if([[self.recordButton currentTitle] isEqualToString:@"Aufnahme abspielen"]){
        [self.recordButton setTitle:@"Pause/Stopp" forState:UIControlStateNormal];
        self.deleteRecordingBtn.enabled = NO;
        [self.recorder playAudio];
    }
    else if([[self.recordButton currentTitle] isEqualToString:@"Pause/Stopp"]){
        [self.recordButton setTitle:@"Aufnahme abspielen" forState:UIControlStateNormal];
        self.deleteRecordingBtn.enabled = YES;
        [self.recorder stopPlaying];
    }
    /*
     if([[self.recordButton currentTitle] isEqualToString:@"Record voice message"]){
     [self.recordButton setTitle:@"Aufnahme stoppen" forState:UIControlStateNormal];
     [self.recorder recordAudio];
     self.audioRecordingTime = [NSTimer scheduledTimerWithTimeInterval:20
     target:self
     selector:@selector(stopAudio)
     userInfo:nil
     repeats:NO];
     self.secondsLeft = 20;
     self.countDownLabel.text = @"00:20";
     self.countDownLabel.hidden = NO;
     self.deleteRecordingBtn.hidden = YES;
     self.audioRecordingCounter = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateRecordingCountdown) userInfo:nil repeats: YES];
     }
     else if([[self.recordButton currentTitle] isEqualToString:@"Aufnahme stoppen"]){
     [self.recordButton setTitle:@"Aufnahme abspielen" forState:UIControlStateNormal];
     [self stopAudio];
     }
     else if([[self.recordButton currentTitle] isEqualToString:@"Aufnahme abspielen"]){
     [self.recordButton setTitle:@"Pause/Stopp" forState:UIControlStateNormal];
     self.deleteRecordingBtn.enabled = NO;
     [self.recorder playAudio];
     }
     else if([[self.recordButton currentTitle] isEqualToString:@"Pause/Stopp"]){
     [self.recordButton setTitle:@"Aufnahme abspielen" forState:UIControlStateNormal];
     self.deleteRecordingBtn.enabled = YES;
     [self.recorder stopPlaying];
     }
     */
}

-(void)viewWillDisappear:(BOOL)animated
{
    if([[self.recordButton currentTitle] isEqualToString:@"Pause/Stopp"]){
        [self.recorder stopPlaying];
    }
}

-(void)stopAudio
{
    [self.recorder stopRecording];
    [self.recordButton setTitle:@"Aufnahme abspielen" forState:UIControlStateNormal];
    self.countDownLabel.hidden = YES;
    self.deleteRecordingBtn.hidden = NO;
    [self.audioRecordingCounter invalidate];
    self.audioRecordingCounter = nil;
    [self.audioRecordingTime invalidate];
    self.audioRecordingTime = nil;
}

-(IBAction)helpBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = Step4Text;
    customPopup.titleLabel.text = @"Hilfe";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}

-(void)deleteRecording:(id)sender
{
    //    [self.recordButton setTitle:@"Record voice message" forState:UIControlStateNormal];
    [self.recordButton setTitle:@"Sprachnachricht aufzeichnen" forState:UIControlStateNormal];
    self.deleteRecordingBtn.hidden = YES;
    [self.audioRecordingTime invalidate];
    self.audioRecordingTime = nil;
    [self.recorder removeAudio];
}

-(void) updateRecordingCountdown {
    //    int hours, minutes;
    int seconds;
    
    self.secondsLeft--;
    //    hours = self.secondsLeft / 3600;
    //    minutes = (self.secondsLeft % 3600) / 60;
    seconds = (self.secondsLeft %3600) % 60;
    self.countDownLabel.text = [NSString stringWithFormat:@"00:%02d", seconds];
}

#pragma mark - AudioPlayerDelegate
-(void)audioPlayerFinishPlayingAudio
{
    [self.recordButton setTitle:@"Aufnahme abspielen" forState:UIControlStateNormal];
    self.deleteRecordingBtn.enabled = YES;
}

#pragma mark - AlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1 && alertView.tag != 125){
        [self.recordButton setTitle:@"Aufnahme stoppen" forState:UIControlStateNormal];
        [self.audioRecordingTime invalidate];
        self.audioRecordingTime = nil;
        [self.recorder removeAudioWithNoAlert];
        [self.recorder recordAudio];
        self.audioRecordingTime = [NSTimer scheduledTimerWithTimeInterval:60
                                                                   target:self
                                                                 selector:@selector(stopAudio)
                                                                 userInfo:nil
                                                                  repeats:NO];
        self.secondsLeft = 60;
        self.countDownLabel.text = @"00:60";
        self.countDownLabel.hidden = NO;
        self.deleteRecordingBtn.hidden = YES;
        [self.audioRecordingCounter invalidate];
        self.audioRecordingCounter = nil;
        self.audioRecordingCounter = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateRecordingCountdown) userInfo:nil repeats: YES];
    }
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Unfall melden!"]) {
        NSLog(@"witnesses array : %@", self.witnessesArray);
        NSMutableArray *wArray = [[NSMutableArray alloc] init];
        
        for(int witnessesCount = 0; witnessesCount < self.witnsesCount ; witnessesCount++){
            if([[[[self.witnessesArray objectAtIndex:witnessesCount] valueForKey:@"name"] description] length] > 0){
                [wArray addObject:[self.witnessesArray objectAtIndex:witnessesCount]];
            }
        }
        
        [[ReportAccidentData sharedSingletonInstance] prepareWitnessesDataWithDescription:self.descriptionTxtView.text audioFileLink:self.audioFileURL witnessesArray:self.witnessesArray];
        
        NSMutableArray *audioFileArray = [[NSMutableArray alloc] init];
        if ([GlobalScopeObjects sharedSingletonInstance].audioFileName.length > 0) {
            [audioFileArray addObject:[GlobalScopeObjects sharedSingletonInstance].audioFileName];
        }
        
        ReportAccidentData *accidentData = [ReportAccidentData sharedSingletonInstance];
        
        NSString *userLatitude = [NSString stringWithFormat:@"%f",accidentData.userLatitude];
        NSString *userLongitude = [NSString stringWithFormat:@"%f",accidentData.userLongitude];
        if([[[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultsUserAlreadyLoggedIn] isEqualToString:@"YES"]){
            [[UnfallheldenHttpClient sharedClient] reportAccidentWithCarId:accidentData.carId carRegNum:accidentData.registrationNumber name:@"" family:@"" phone:@"" email:@"" terms:@"" address:accidentData.placeName postCode:@"" city:@"" accidentLat:userLatitude accidentLongitude:userLongitude accidentDate:accidentData.date accidentTime:accidentData.time accidentDescription:accidentData.accidentDescription opponentRegNum:accidentData.acPlateNumber opponentCarBrand:accidentData.acCarBrand opponentCarModel:accidentData.acCarType acFirstName:accidentData.acFirstName acLastName:accidentData.acLastName acCompanyCar:accidentData.acCompanyCar acCompanyName:accidentData.acCompanyName acCompanyLeasing:accidentData.acLeaseCar acLeasingCompany:accidentData.acLeasingCompany acPhone:accidentData.acPhone acAddress:accidentData.acAdditionalAddress acPostCode:accidentData.acZipCode acCity:accidentData.acCity acStreet:accidentData.acStreetName acStreetNum:accidentData.acStreetNumber acInsurer:accidentData.acInsuranceCompany audio:audioFileArray witnesses:wArray country:accidentData.acCountry weather:accidentData.weather WithCompletionBlock:^(NSData *data) {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dataDictionary = [responseString JSONValue];
                //            NSDictionary *userData = [[dataDictionary valueForKey:@"data"] description];
                NSLog(@"data dictionary : %@",dataDictionary);
                int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                if(status == 1){
                    PicturesVC *picturesVC = [[PicturesVC alloc] init];
                    picturesVC.accidentId = [[[dataDictionary objectForKey:@"data"] valueForKey:@"id"] description];
                    [self.navigationController pushViewController:picturesVC animated:YES];
                    //                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Accident is reported successfully" andMessage:@"Your accident is reported successfully now you can add accident pictures if you want."];
                }
                else{
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Unfall melden fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Melden des Unfalls. Bitte versuchen Sie es erneut"];
                }
            }];
        }
        else{
            [[UnfallheldenHttpClient sharedClient] reportAccidentWithCarId:@"" carRegNum:accidentData.plateNumber name:accidentData.firstName family:accidentData.lastName phone:accidentData.phoneNumber email:accidentData.email terms:accidentData.isTermsAccepted address:accidentData.placeName postCode:@"" city:@"" accidentLat:userLatitude accidentLongitude:userLongitude accidentDate:accidentData.date accidentTime:accidentData.time accidentDescription:accidentData.accidentDescription opponentRegNum:accidentData.acPlateNumber opponentCarBrand:accidentData.acCarBrand opponentCarModel:accidentData.acCarType acFirstName:accidentData.acFirstName acLastName:accidentData.acLastName acCompanyCar:accidentData.acCompanyCar acCompanyName:accidentData.acCompanyName acCompanyLeasing:accidentData.acLeaseCar acLeasingCompany:accidentData.acLeasingCompany acPhone:accidentData.acPhone acAddress:accidentData.acAdditionalAddress acPostCode:accidentData.acZipCode acCity:accidentData.acCity acStreet:accidentData.acStreetName acStreetNum:accidentData.acStreetNumber acInsurer:accidentData.acInsuranceCompany audio:audioFileArray witnesses:wArray country:accidentData.acCountry weather:accidentData.weather WithCompletionBlock:^(NSData *data) {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dataDictionary = [responseString JSONValue];
                //            NSDictionary *userData = [[dataDictionary valueForKey:@"data"] description];
                NSLog(@"data dictionary : %@",dataDictionary);
                int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                if(status == 1){
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[[dataDictionary valueForKey:@"data"] valueForKey:@"accident_hash"] description] forKey:kKeychainAccidentHash];
                    PicturesVC *picturesVC = [[PicturesVC alloc] init];
                    picturesVC.accidentId = [[[dataDictionary objectForKey:@"data"] valueForKey:@"id"] description];
                    [self.navigationController pushViewController:picturesVC animated:YES];
                    //                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Accident is reported successfully" andMessage:@"Your accident is reported successfully now you can add accident pictures if you want."];
                }
                else{
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Unfall melden fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Melden des Unfalls. Bitte versuchen Sie es erneut"];
                }
            }];
        }
    }
}

- (void)addBtnPress:(id)sender {
    if([self.witnessesFieldsView.subviews count] < 5){
        WitnessesFields *witnessesFields = [[WitnessesFields alloc] init];
        NSMutableArray *viewsArray = [[NSMutableArray alloc] initWithArray:self.witnessesFieldsView.subviews];
        UIView *lastSubview = (UIView*)[viewsArray objectAtIndex:[viewsArray count]-1];
        witnessesFields.frame = CGRectMake(lastSubview.frame.origin.x, lastSubview.frame.origin.y + self.nameField.frame.size.height + 10, self.witnessesFieldsView.frame.size.width, self.witnessesFieldsView.frame.size.height);
        
        self.containerView.autoresizesSubviews = NO;
        witnessesFields.autoresizesSubviews = NO;
        self.witnessesFieldsView.autoresizesSubviews = NO;
        
        for(UIView *vw in witnessesFields.subviews) {
            if([vw isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField*)vw;
                textField.layer.cornerRadius = 2.0f;
                textField.layer.masksToBounds = YES;
                textField.layer.borderColor=[[UIColor whiteColor]CGColor];
                textField.layer.borderWidth= 1.3f;
                UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
                textField.leftView = paddingView;
                textField.leftViewMode = UITextFieldViewModeAlways;
                [textField setTextColor:[UIColor whiteColor]];
                UIColor *color = [UIColor whiteColor];
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
                textField.delegate = self;
                if(IS_IPHONE_4 || IS_IPHONE_5){
                    [textField setFont:[UIFont regularUnfallheldenFontWithSize:13]];
                }
                else{
                    [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
                }
            }
        }
        
        witnessesFields.witnessNameField.frame = CGRectMake(self.nameField.frame.origin.x, witnessesFields.witnessNameField.frame.origin.y, self.nameField.frame.size.width, self.nameField.frame.size.height);
        witnessesFields.witnessPhoneField.frame = CGRectMake(self.phoneField.frame.origin.x, witnessesFields.witnessPhoneField.frame.origin.y, self.phoneField.frame.size.width, self.phoneField.frame.size.height);
        
        [UIView animateWithDuration:0.3 animations:^{
            //            CGRect frame = self.witnessesFieldsView.frame;
            //            frame.size.height += witnessesFields.frame.size.height;
            //            self.witnessesFieldsView.frame = frame;
            //            CGRect bottomBtnsContainerFrame = self.bottomBtnsContainer.frame;
            //            bottomBtnsContainerFrame.origin.y = CGRectGetMaxY(self.witnessesFieldsView.frame);
            //            self.bottomBtnsContainer.frame = bottomBtnsContainerFrame;
            [self.witnessesFieldsView addSubview:witnessesFields];
            //            CGRect containerViewFrame = self.containerView.frame;
            //            containerViewFrame.size.height = CGRectGetMaxY(self.bottomBtnsContainer.frame);
            //            self.containerView.frame = containerViewFrame;
            //            self.kaScrollView.contentSize = CGSizeMake(self.containerView.frame.size.width, self.containerView.frame.size.height+100);
            self.contentViewHeightContraint.constant += self.nameField.frame.size.height + 10;
            //            self.bottomBtnsContainerViewTopConstraint.constant += self.nameField.frame.size.height + 12;
            self.witnessFieldsContainerHeight.constant += self.nameField.frame.size.height + 10;
            ;
        }];
        [self.kaScrollView contentSizeToFit];
        if([self.witnessesFieldsView.subviews count] == 5){
            self.continueBtn.hidden = YES;
            self.addFieldBtn.hidden = YES;
            self.secondContinueBtn.hidden = NO;
            //            self.contentViewHeightContraint.constant -= witnessesFields.frame.size.height;
        }
        
        witnessesFields.witnessNameField.tag = self.witnsNameFieldTag;
        witnessesFields.witnessPhoneField.tag = self.witnsPhoneFieldTag;
        
        [witnessesFields.witnessNameField addTarget:self
                                             action:@selector(textFieldValueChanged:)
                                   forControlEvents:UIControlEventEditingChanged];
        [witnessesFields.witnessPhoneField addTarget:self
                                              action:@selector(textFieldValueChanged:)
                                    forControlEvents:UIControlEventEditingChanged];
        
        if(self.witnsNameFieldTag == 1001){
            self.wNameField2 = witnessesFields.witnessNameField;
            self.wPhoneField2 = witnessesFields.witnessPhoneField;
        }
        else if(self.witnsNameFieldTag == 1002){
            self.wNameField3 = witnessesFields.witnessNameField;
            self.wPhoneField3 = witnessesFields.witnessPhoneField;
        }
        else if(self.witnsNameFieldTag == 1003){
            self.wNameField4 = witnessesFields.witnessNameField;
            self.wPhoneField4 = witnessesFields.witnessPhoneField;
        }
        
        self.witnsesCount++;
        self.witnsNameFieldTag++;
        self.witnsPhoneFieldTag++;
    }
}

- (void)textViewDidEndEditing:(UITextView *)theTextView
{
    if (![self.descriptionTxtView hasText]) {
        self.txtViewPleaceholderLbl.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView
{
    if(![self.descriptionTxtView  hasText]) {
        self.txtViewPleaceholderLbl.hidden = NO;
    }
    else{
        self.txtViewPleaceholderLbl.hidden = YES;
    }
}

#pragma mark - UITextFieldDelegateMethods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

-(IBAction)textFieldValueChanged:(UITextField *)textField
{
    if(([textField isEqual:self.nameField] || [textField isEqual:self.phoneField]) && [textField.text length] > 0){
        NSDictionary *witnesses = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:self.nameField.text,self.phoneField.text, nil] forKeys:[NSArray arrayWithObjects:@"name",@"phone", nil]];
        [self.witnessesArray replaceObjectAtIndex:0 withObject:witnesses];
    }
    
    if(([textField isEqual:self.wNameField2] || [textField isEqual:self.wPhoneField2]) && [textField.text length] > 0){
        NSDictionary *witnesses = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:self.wNameField2.text,self.wPhoneField2.text, nil] forKeys:[NSArray arrayWithObjects:@"name",@"phone", nil]];
        [self.witnessesArray replaceObjectAtIndex:1 withObject:witnesses];
    }
    
    if(([textField isEqual:self.wNameField3] || [textField isEqual:self.wPhoneField3]) && [textField.text length] > 0){
        NSDictionary *witnesses = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:self.wNameField3.text,self.wPhoneField3.text, nil] forKeys:[NSArray arrayWithObjects:@"name",@"phone", nil]];
        [self.witnessesArray replaceObjectAtIndex:2 withObject:witnesses];
    }
    
    if(([textField isEqual:self.wNameField4] || [textField isEqual:self.wPhoneField4]) && [textField.text length] > 0){
        NSDictionary *witnesses = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:self.wNameField4.text,self.wPhoneField4.text, nil] forKeys:[NSArray arrayWithObjects:@"name",@"phone", nil]];
        [self.witnessesArray replaceObjectAtIndex:3 withObject:witnesses];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
