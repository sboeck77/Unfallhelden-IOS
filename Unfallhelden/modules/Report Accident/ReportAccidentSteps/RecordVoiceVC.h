//
//  RecordVoiceVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/2/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "AudioRecorder.h"

@interface RecordVoiceVC : UnfallheldenParentVC<UITextFieldDelegate,AudioPlayerDelegate,UIAlertViewDelegate,UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *witnessesFieldsView;
@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *kaScrollView;
@property (weak, nonatomic) IBOutlet UIView *bottomBtnsContainer;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (nonatomic, strong) IBOutlet UITextView *descriptionTxtView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteRecordingBtn;
@property (nonatomic, weak) IBOutlet UILabel *countDownLabel;
@property (nonatomic, strong) NSMutableArray *witnessesArray;
@property (nonatomic, strong) NSString *audioFileURL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomBtnsContainerViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *witnessFieldsContainerHeight;
@property (nonatomic, weak) IBOutlet UIButton *addFieldBtn;
@property (nonatomic, weak) IBOutlet UIButton *continueBtn;
@property (nonatomic, weak) IBOutlet UIButton *secondContinueBtn;
@property (nonatomic, weak) IBOutlet UILabel *step4Lbl;

-(IBAction)continueBtnPress:(id)sender;
- (IBAction)addBtnPress:(id)sender;
- (IBAction)recordAudio:(id)sender;
-(IBAction)deleteRecording:(id)sender;

@end
