//
//  ViewController.m
//  Unfallhelden
//
//  Created by Adil Anwer on 1/27/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "AtAccidentSiteVC.h"
#import "ASiteOptionsCell.h"
#import "RAccidentStep1VC.h"
#import "FirstAidInstructionsVC.h"
#import "RAccidentStep1ALVC.h"
#import "RecordVoiceVC.h"

@interface AtAccidentSiteVC ()

@end
//id = 107;
//data = "c735c2ca85b2d32d4fa4faf8f7fba0c5.jpg";
/*audio =             (
{
    "accident_id" = 107;
    file = "2dfe1ed80fa3233f24bada75a0a0a3a0.mp3";
    id = 5;
}
);
 */
/*photos =             (
 {
 "accident_id" = 107;
 id = 17;
 photo = "4a7000ebf26ed7950227e9bf280379df.jpg";
 },
 {
 "accident_id" = 107;
 id = 18;
 photo = "c735c2ca85b2d32d4fa4faf8f7fba0c5.jpg";
 }
 );*/

@implementation AtAccidentSiteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"Unfall melden"];
    
    //GoogleAnalytics
//    self.screenName = @"ReportAccidentScreen";
    
    [super setupMenuBarButtonItems];
    
    UIBarButtonItem *barButtonItem = [UIBarButtonItem addAnotherRightMenuBarButtonItemWithtarget:self andSelector:@selector(helpBtnPress:) andImage:[UIImage imageNamed:@"helpBtn"]];
    
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,barButtonItem, nil] animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ReportAccidentScreen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)reportAccidentBtnPress:(id)sender
{
    /*
     RecordVoiceVC *picVC = [[RecordVoiceVC alloc] init];
     [self.navigationController pushViewController:picVC animated:YES];
     */
    
    if([[[[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultsUserAlreadyLoggedIn] description] isEqualToString:@"YES"]){
        RAccidentStep1ALVC *raStep1VC = [[RAccidentStep1ALVC alloc] init];
        [self.navigationController pushViewController:raStep1VC animated:YES];
    }
    else{
        RAccidentStep1VC *raStep1VC = [[RAccidentStep1VC alloc] init];
        [self.navigationController pushViewController:raStep1VC animated:YES];
    }
}

-(void)firstAidTutorialBtnPress:(id)sender
{
    FirstAidInstructionsVC *firstAidInstructions = [[FirstAidInstructionsVC alloc] init];
    [self.navigationController pushViewController:firstAidInstructions animated:YES];
}

-(IBAction)callBtnPress:(id)sender
{
    NSString *phoneNum = @"";
    if([[UnfallheldenUtilities sharedSingletonInstance].ambulancePhoneNum length] > 0){
        phoneNum = [UnfallheldenUtilities sharedSingletonInstance].ambulancePhoneNum;
    }
    else{
        phoneNum = @"112";
    }
    NSString *message = [NSString stringWithFormat:@"Möchten Sie einen Notruf absetzen? : %@",phoneNum];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"Abbrechen" otherButtonTitles:@"Anrufen", nil];
    alert.tag = 123;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 123){
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        /*
         "ambulance_number":"112",
         "hotline_number":""0800 72 41 794"
         */
        NSString *phoneNum = @"";
        if([[UnfallheldenUtilities sharedSingletonInstance].ambulancePhoneNum length] > 0){
            phoneNum = [UnfallheldenUtilities sharedSingletonInstance].ambulancePhoneNum;
        }
        else{
            phoneNum = @"112";
        }
        
        if([title isEqualToString:@"Anrufen"]) {
            NSString *phNumber = [NSString stringWithFormat:@"tel:%@",[phoneNum stringByReplacingOccurrencesOfString:@" " withString:@""]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phNumber]];
        }
    }
    else{
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        /*
         "ambulance_number":"112",
         "hotline_number":""0800 72 41 794"
         */
        NSString *phoneNum = @"";
        if([[UnfallheldenUtilities sharedSingletonInstance].hotlinePhoneNum length] > 0){
            phoneNum = [UnfallheldenUtilities sharedSingletonInstance].hotlinePhoneNum;
        }
        else{
            phoneNum = @"0800 72 41 794";
        }
        
        if([title isEqualToString:@"Anrufen"]) {
            NSString *phNumber = [NSString stringWithFormat:@"tel:%@",[phoneNum stringByReplacingOccurrencesOfString:@" " withString:@""]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phNumber]];
        }
    }
}

-(IBAction)accidentViaHotlineBtnPress:(id)sender
{
    NSString *phoneNum = @"";
    if([[UnfallheldenUtilities sharedSingletonInstance].hotlinePhoneNum length] > 0){
        phoneNum = [UnfallheldenUtilities sharedSingletonInstance].hotlinePhoneNum;
    }
    else{
        phoneNum = @"0800 72 41 794";
    }
    NSString *message = [NSString stringWithFormat:@"Möchten sie die Unfallhelden Hotline anrufen? : %@",phoneNum];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"Abbrechen" otherButtonTitles:@"Anrufen", nil];
    alert.tag = 124;
    [alert show];
}


-(IBAction)helpBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = AtAccidentSiteHelpText;
    customPopup.titleLabel.text = @"Hilfe";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
