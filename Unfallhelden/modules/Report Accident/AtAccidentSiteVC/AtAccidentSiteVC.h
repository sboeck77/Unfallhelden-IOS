//
//  ViewController.h
//  Unfallhelden
//
//  Created by Adil Anwer on 1/27/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AtAccidentSiteVC : UnfallheldenParentVC

-(IBAction)reportAccidentBtnPress:(id)sender;
-(IBAction)firstAidTutorialBtnPress:(id)sender;

@end
