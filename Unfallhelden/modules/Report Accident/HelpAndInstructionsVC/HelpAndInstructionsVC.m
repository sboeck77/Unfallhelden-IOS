//
//  HelpAndInstructionsVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 1/22/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "HelpAndInstructionsVC.h"

@interface HelpAndInstructionsVC ()

@end

@implementation HelpAndInstructionsVC
@synthesize helpAndInstructionsTbl, helpAndInstructionsArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.helpAndInstructionsArray = [[NSMutableArray alloc] initWithObjects:@"First aid tutorial", @"Guide to secure accident location", @"Record a voice message",@"Data of accident causer",@"Save location/time/weather",@"Take pictures",@"Call ambulance",@"Call police", nil];
    [self.helpAndInstructionsTbl reloadData];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    UIBarButtonItem *homeBtnItem = [UIBarButtonItem createBarButtonWithText:@"Home" withTarget:self andSelector:@selector(homeBtnPress:)];
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,homeBtnItem, nil] animated:NO];
    [[CustomizeNavbar sharedSingletonInstance] setBackgroundImageForNavBar:self.navigationController.navigationBar withImage:[UIImage imageNamed:@"navBarBg"]];
    //GoogleAnalytics
    self.screenName = @"HelpAndInstructionsScreen";
}

#pragma mark - UITableViewDataSourceMethods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [helpAndInstructionsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"CarInfoCell";
    UITableViewCell *categoryCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    UILabel *racLbl;
    if (categoryCell == nil) {
        categoryCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        categoryCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        racLbl = [[UILabel alloc] initWithFrame:CGRectMake(categoryCell.contentView.frame.origin.x + 10, categoryCell.contentView.frame.origin.y + 6, categoryCell.contentView.frame.size.width - 20, categoryCell.contentView.frame.size.height - 20)];
        [categoryCell.contentView addSubview:racLbl];
    }
    racLbl.text = [[helpAndInstructionsArray objectAtIndex:indexPath.row] description];
    //    categoryCell.textLabel.text = [self.helpAndInstructionsArray objectAtIndex:indexPath.row];
    return categoryCell;
}

#pragma mark - UITableViewDelegateMethods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.helpAndInstructionsTbl deselectRowAtIndexPath:indexPath animated:YES];
    
    /*NSString *phoneNumber = [@"tel://" stringByAppendingString:mymobileNO.titleLabel.text];
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)homeBtnPress:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
