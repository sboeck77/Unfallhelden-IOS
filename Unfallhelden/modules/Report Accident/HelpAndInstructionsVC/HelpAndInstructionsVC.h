//
//  HelpAndInstructionsVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 1/22/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpAndInstructionsVC : GAITrackedViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *helpAndInstructionsArray;
@property (nonatomic, weak) IBOutlet UITableView *helpAndInstructionsTbl;

@end
