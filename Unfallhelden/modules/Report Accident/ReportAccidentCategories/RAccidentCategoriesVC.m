//
//  RAccidentCategoriesVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 1/19/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "RAccidentCategoriesVC.h"
#import "ReportAccidentVC.h"

@interface RAccidentCategoriesVC ()

@end

@implementation RAccidentCategoriesVC
@synthesize raCategoriesTbl,categoriesArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.categoriesArray = [[NSMutableArray alloc] initWithObjects:@"Call Unfallhelden Hotline", @"Call me back", @"Report accident through app", nil];
    [self.raCategoriesTbl reloadData];
    
    //GoogleAnalytics
    self.screenName = @"HelpAndInstructionsScreen";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSourceMethods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [categoriesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"CarInfoCell";
    UITableViewCell *categoryCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    UILabel *racLbl;
    if (categoryCell == nil) {
        categoryCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        categoryCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        racLbl = [[UILabel alloc] initWithFrame:CGRectMake(categoryCell.contentView.frame.origin.x + 10, categoryCell.contentView.frame.origin.y + 6, categoryCell.contentView.frame.size.width - 20, categoryCell.contentView.frame.size.height - 20)];
        [categoryCell.contentView addSubview:racLbl];
    }
    racLbl.text = [[categoriesArray objectAtIndex:indexPath.row] description];
    //    categoryCell.textLabel.text = [self.categoriesArray objectAtIndex:indexPath.row];
    return categoryCell;
}

#pragma mark - UITableViewDelegateMethods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.raCategoriesTbl deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row == 0){
        NSString *phoneNumber = [@"tel://" stringByAppendingString:@"+971557819348"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
    else if (indexPath.row == 1){
        
    }
    else if (indexPath.row == 2){
        ReportAccidentVC *rAccidentVC = [[ReportAccidentVC alloc] init];
        [self.navigationController pushViewController:rAccidentVC animated:YES];
    }
    
    /*NSString *phoneNumber = [@"tel://" stringByAppendingString:mymobileNO.titleLabel.text];
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];*/
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
