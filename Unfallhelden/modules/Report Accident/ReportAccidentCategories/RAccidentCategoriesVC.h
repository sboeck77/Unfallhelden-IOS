//
//  RAccidentCategoriesVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 1/19/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RAccidentCategoriesVC : GAITrackedViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *raCategoriesTbl;
@property (nonatomic, strong) NSMutableArray *categoriesArray;

@end
