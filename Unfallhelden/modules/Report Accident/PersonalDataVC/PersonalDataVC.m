//
//  PersonalDataVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 2/3/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "PersonalDataVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "CarDataVC.h"
#import "ReportAccidentData.h"
#import "UnfallheldenDatePickerView.h"
#import "HomeScreenVC.h"

@interface PersonalDataVC ()

@property (nonatomic) BOOL termsAndConditions;
@property (nonatomic, weak) IBOutlet UILabel *termsAndCndtnsLbl;

@end

@implementation PersonalDataVC
@synthesize personalDataScroll,pswrdField,cnfrmPswrdField,streetNameField,cityNameField,streetNumField,zipCodeField,birthdayField;
@synthesize checkBoxBtn,terms,verticalSpacingContinueToNxtBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"Registrierung"];
    [super setupMenuBarButtonItems];
    
    //GoogleAnalytics
    //    self.screenName = @"RegistraionAfterReportingAccident";
    
    self.termsAndConditions = NO;
    for(UIView *vw in self.personalDataScroll.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
        }
    }
    [self.view layoutIfNeeded];
    [self.view layoutSubviews];
    if(IS_IPHONE_5){
        self.verticalSpacingContinueToNxtBtn.constant = 140;
    }
    else if (IS_IPHONE_4){
        self.verticalSpacingContinueToNxtBtn.constant = 100;
    }
    [self.view setNeedsUpdateConstraints];
    
    //    UIBarButtonItem *barButtonItem = [UIBarButtonItem rightMenuBarButtonItemWithtarget:self andSelector:@selector(helpBtnPress:) andImage:[UIImage imageNamed:@"helpBtn"]];
    //    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
    //                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
    //                                       target:nil action:nil];
    //    negativeSpacer.width = -8;
    //    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,barButtonItem, nil] animated:NO];
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:self.termsAndCndtnsLbl.text];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    self.termsAndCndtnsLbl.attributedText = attributeString;
    self.termsAndCndtnsLbl.textColor = [UIColor whiteColor];
    [self.termsAndCndtnsLbl setFont:[UIFont regularUnfallheldenFontWithSize:11]];
}

-(IBAction)helpBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = Step6Text;
    customPopup.titleLabel.text = @"Hilfe";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"RegistraionAfterReportingAccident"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    self.personalDataScroll.frame = CGRectMake(self.view.frame.origin.x, self.personalDataScroll.frame.origin.y, self.view.frame.size.width, self.personalDataScroll.frame.size.height);
    self.personalDataScroll.contentSize = CGSizeMake(self.view.frame.size.width, self.personalDataScroll.contentSize.height);
    [self.personalDataScroll contentSizeToFit];
    CGFloat heightOfPicker = 0.0f;
    
    if(IS_IPHONE_5){
        heightOfPicker = 206;
    }
    else if (IS_IPHONE_4){
        heightOfPicker = 206;
    }
    else{
    }
    if (IS_IPHONE_6 || IS_IPHONE_6_Plus){
        heightOfPicker = 260;
    }
    UnfallheldenDatePickerView *dateTimePicker = [[UnfallheldenDatePickerView alloc] init];
    CGRect pickerFrame = dateTimePicker.frame;
    pickerFrame.size.height = heightOfPicker;
    dateTimePicker.frame = pickerFrame;
    [dateTimePicker.pickerDoneButton setAction:@selector(pickerDoneBtnPress:)];
    [dateTimePicker.datePicker addTarget:self action:@selector(pickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [dateTimePicker.datePicker setDatePickerMode:UIDatePickerModeDate];
    
    [self.birthdayField setTintColor:[UIColor clearColor]];
    self.birthdayField.inputView = dateTimePicker;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)pickerValueChanged:(UIDatePicker *)timePicker
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *date = [formatter stringFromDate:timePicker.date];
    NSLog(@"Date : %@", date);
    self.birthdayField.text = date;
}

#pragma mark - Open PickerView
-(void)showDatePicker:(id)sender
{
    for(UIView *vw in self.personalDataScroll.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    [self.birthdayField becomeFirstResponder];
}

#pragma mark - PickerView Done Button Press
-(IBAction)pickerDoneBtnPress:(id)sender
{
    [self.birthdayField resignFirstResponder];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if([textField.text length] > 0){
        if([textField isEqual:self.pswrdField] || [textField isEqual:self.cnfrmPswrdField]){
            textField.background = nil;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField.text length] > 0){
        if([textField isEqual:self.pswrdField] || [textField isEqual:self.cnfrmPswrdField]){
            textField.background = nil;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
        }
    }
    [textField resignFirstResponder];
    return TRUE;
}

-(void)checkBoxBtnPress:(id)sender
{
    self.termsAndConditions = !self.termsAndConditions;
    if(self.termsAndConditions) {
        [self.checkBoxBtn setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
        self.terms = @"true";
    }
    else {
        [self.checkBoxBtn setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
        self.terms = @"false";
    }
}

-(void)termsAndCndtnsBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = UnfallheldenTermsAndConditions;
    customPopup.titleLabel.text = @"AGB";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}

-(void)registerBtnPress:(id)sender
{
    //    CarDataVC *carDataVC = [[CarDataVC alloc] init];
    //    [self.navigationController pushViewController:carDataVC animated:YES];
    for(UIView *vw in self.personalDataScroll.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    if([self.pswrdField.text length]>0 && [self.cnfrmPswrdField.text length]>0){
        if([self.terms isEqualToString:@"true"]){
            if([self.pswrdField.text isEqualToString:self.cnfrmPswrdField.text]){
                //                    NSString *name = [NSString stringWithFormat:@"%@ %@",self.firstNameField.text,self.lastNameField.text];
                ReportAccidentData *accidentData = [ReportAccidentData sharedSingletonInstance];
                [[UnfallheldenHttpClient sharedClient] registerUserWithAccidentHashWithEmail:accidentData.email andPassword:self.pswrdField.text andName:accidentData.firstName andFamily:accidentData.lastName andGender:@"" andPhone:accidentData.phoneNumber andTerms:@"" andFBUserId:@"" WithCompletionBlock:^(NSData *data){
                    
                    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    NSDictionary *dataDictionary = [responseString JSONValue];
                    //                        NSDictionary *userData = [dataDictionary valueForKey:@"data"];
                    int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                    NSLog(@"repsonce from server : %@",dataDictionary);
                    if(status == 1){
                        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Erfolgreich registriert" andMessage:@"Sie haben sich erfolgreich für die Unfallhelden registriert"];
                        //                        NSDictionary *ownerProfileData = [dataDictionary objectForKey:@"data"];
                        [[PDKeychainBindings sharedKeychainBindings] setObject:@"" forKey:kKeychainUserAccessToken];
                        [[PDKeychainBindings sharedKeychainBindings] setObject:[[[dataDictionary valueForKey:@"data"] valueForKey:@"token"] description] forKey:kKeychainUserAccessToken];
                        NSLog(@"token : %@ andsaved token : %@", [[[dataDictionary valueForKey:@"data"] valueForKey:@"token"] description], kKeychainUserAccessToken);
                        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:kUserDefaultsUserAlreadyLoggedIn];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNameUserLogin object:nil userInfo:nil];
                        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                        NSArray *controllers = [NSArray arrayWithObject:[[HomeScreenVC alloc] init]];
                        navigationController.viewControllers = controllers;
                        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
                    }
                    else{
                        NSString* message = [[[dataDictionary objectForKey:@"error"] valueForKey:@"message"] description];
                        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Registrierung fehlgeschlagen" andMessage:message];
                    }
                }];
            }
            else{
                if(self.pswrdField.background == nil){
                    self.pswrdField.layer.borderColor=[[UIColor redColor]CGColor];
                    self.pswrdField.layer.borderWidth= 1.3f;
                    [self.pswrdField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                    [self.pswrdField setTextColor:[UIColor whiteColor]];
                }
                if(self.cnfrmPswrdField.background == nil){
                    self.cnfrmPswrdField.layer.borderColor=[[UIColor redColor]CGColor];
                    self.cnfrmPswrdField.layer.borderWidth= 1.3f;
                    [self.cnfrmPswrdField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                    [self.cnfrmPswrdField setTextColor:[UIColor whiteColor]];
                }
                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Die Passwörter stimmen nicht überein"];
            }
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte stimmen Sie den AGB und den Datenschutzbestimmungen zu."];
        }
    }
    else{
        for(UIView *vw in self.personalDataScroll.subviews) {
            if([vw isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField*)vw;
                if([textField.text length] == 0 && textField.background == nil){
                    if([textField isEqual:self.pswrdField] || [textField isEqual:self.cnfrmPswrdField]){
                        textField.layer.borderColor=[[UIColor redColor]CGColor];
                        textField.layer.borderWidth= 1.3f;
                        [textField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                        [textField setTextColor:[UIColor whiteColor]];
                        UIColor *color = [UIColor whiteColor];
                        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ wird benötigt",textField.placeholder] attributes:@{NSForegroundColorAttributeName: color}];
                    }
                }
            }
        }
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte füllen Sie zuerst alle Felder aus"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
