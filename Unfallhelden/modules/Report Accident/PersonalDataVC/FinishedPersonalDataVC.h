//
//  FinishedPersonalDataVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/3/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinishedPersonalDataVC : UIViewController

-(IBAction)closePopup:(id)sender;

@end
