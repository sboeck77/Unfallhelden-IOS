//
//  FinishedPersonalDataVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 2/3/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "FinishedPersonalDataVC.h"

@interface FinishedPersonalDataVC ()

@end

@implementation FinishedPersonalDataVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)closePopup:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
