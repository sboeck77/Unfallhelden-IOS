//
//  PersonalDataVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/3/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@interface PersonalDataVC : UnfallheldenParentVC<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *personalDataScroll;
@property (nonatomic, weak) IBOutlet UITextField *pswrdField;
@property (nonatomic, weak) IBOutlet UITextField *cnfrmPswrdField;
@property (nonatomic, weak) IBOutlet UITextField *streetNameField;
@property (nonatomic, weak) IBOutlet UITextField *streetNumField;
@property (nonatomic, weak) IBOutlet UITextField *cityNameField;
@property (nonatomic, weak) IBOutlet UITextField *zipCodeField;
@property (nonatomic, weak) IBOutlet UITextField *birthdayField;
@property (nonatomic, weak) IBOutlet UIButton *checkBoxBtn;
@property (nonatomic, strong) NSString *terms;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpacingContinueToNxtBtn;

-(IBAction)registerBtnPress:(id)sender;
-(IBAction)checkBoxBtnPress:(id)sender;
-(IBAction)showDatePicker:(id)sender;
-(IBAction)termsAndCndtnsBtnPress:(id)sender;

@end
