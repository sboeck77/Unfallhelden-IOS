//
//  AddressView.m
//  Unfallhelden
//
//  Created by Adil Anwer on 3/5/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "AddressView.h"

@implementation AddressView
@synthesize addressScrollView,streetNameField,streetNumField,phoneNumField,cityNameField,zipCodeField,mobileNumField;
@synthesize profile,addMobileNumBtn,phoneNumField2,mobileNumField2,isSecondPhoneAdded,countryTextField,countryArray;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AddressView" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

-(void)setupView
{
    for(UIView *vw in self.addressScrollView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
        }
    }
    CGFloat screenWidth = 0.0f;
    if(IS_IPHONE_4){
        screenWidth = 320;
    }
    else if (IS_IPHONE_5){
        screenWidth = 320;
    }
    else if(IS_IPHONE_6){
        screenWidth = 375;
    }
    else if (IS_IPHONE_6_Plus){
        screenWidth = 414;
    }
    CGRect frame = self.frame;
    frame.size.width = screenWidth;
    self.frame = frame;
    self.addressScrollView.contentSize = CGSizeMake(screenWidth, self.addressScrollView.contentSize.height);
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];
    UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[self class] toolbarHeight])];
    [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
    self.phoneNumField.inputAccessoryView = toolbar;
    self.streetNumField.inputAccessoryView = toolbar;
    self.zipCodeField.inputAccessoryView = toolbar;
    self.mobileNumField.inputAccessoryView = toolbar;
    self.mobileNumField2.inputAccessoryView = toolbar;
    self.phoneNumField2.inputAccessoryView = toolbar;
    isSecondPhoneAdded = FALSE;
    CGFloat heightOfPicker = 0.0f;
    
    if(IS_IPHONE_5){
        heightOfPicker = 206;
    }
    else if (IS_IPHONE_4){
        heightOfPicker = 206;
    }
    if (IS_IPHONE_6 || IS_IPHONE_6_Plus){
        heightOfPicker = 260;
    }
    
    UnfallheldenPickerView *unfallheldenPickerView = [[UnfallheldenPickerView alloc] init];
    CGRect pickerFrame = unfallheldenPickerView.frame;
    pickerFrame.size.height = heightOfPicker;
    unfallheldenPickerView.frame = pickerFrame;
    [unfallheldenPickerView.pickerDoneButton setAction:@selector(pickerDoneBtnPress:)];
    
    unfallheldenPickerView.pickerView.dataSource = self;
    unfallheldenPickerView.pickerView.delegate = self;
    
    [self.countryTextField setTintColor:[UIColor clearColor]];
    self.countryTextField.inputView = unfallheldenPickerView;
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CountryList" ofType:@"txt"];
    NSData *jsonData = [NSData dataWithContentsOfFile:filePath];
    NSString *responseString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *dataDictionary = [responseString JSONValue];
    self.countryArray = [[dataDictionary objectForKey:@"string-array"] objectForKey:@"item"];
}

- (void)doneButtonDidPressed:(id)sender {
    [self.phoneNumField resignFirstResponder];
    [self.streetNumField resignFirstResponder];
    [self.zipCodeField resignFirstResponder];
    [self.countryTextField resignFirstResponder];
    [self.mobileNumField resignFirstResponder];
    [self.phoneNumField2 resignFirstResponder];
    [self.mobileNumField2 resignFirstResponder];
}

+ (CGFloat)toolbarHeight {
    // This method will handle the case that the height of toolbar may change in future iOS.
    return 44.f;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

-(void)showCountryPicker:(id)sender
{
    for(UIView *vw in self.addressScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    
    [self.countryTextField becomeFirstResponder];
}

#pragma mark - PickerView Done Button Press
-(IBAction)pickerDoneBtnPress:(id)sender
{
    [self.countryTextField resignFirstResponder];
}
#pragma mark - UIPickerView DataSource

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.countryArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.countryArray objectAtIndex:row];
}

#pragma mark - UIPickerView Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    
    if(row == 0){
        self.countryTextField.text = @"";
    }
    else{
        self.countryTextField.text = [self.countryArray objectAtIndex:row];
    }
}

-(void)saveChangesBtnPress:(id)sender
{
    NSString *secondMobile;
    NSString *secondPhone;
    if(self.isSecondPhoneAdded ==  TRUE){
        secondMobile = self.mobileNumField2.text;
        secondPhone = self.phoneNumField2.text;
    }
    else{
        secondMobile = self.profile.mobile2;
        secondPhone = self.profile.phone2;
    }
    [[UnfallheldenHttpClient sharedClient] uploadUserProfileWithUserName:self.profile.name andAnrede:self.profile.gender andFamily:self.profile.family andBirthdate:self.profile.birthdate andStreet:self.streetNameField.text andStreetNum:self.streetNumField.text andZip:self.zipCodeField.text andResidence:self.cityNameField.text andMobile:self.mobileNumField.text andCompany:self.profile.company andPhone:self.phoneNumField.text andPhoneSecond:secondPhone andMobileSecond:secondMobile country:self.countryTextField.text city:self.cityNameField.text WithCompletionBlock:^(NSData *data) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *dataDictionary = [responseString JSONValue];
        NSLog(@"data dictionary : %@",dataDictionary);
        int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
        if(status == 1){
            NSDictionary *userData = [dataDictionary objectForKey:@"data"];
//            residence in get for city
            self.profile = [UserProfile updateUserProfileWithProfile:self.profile userId:[[userData valueForKey:@"id"] description] fbId:[[userData valueForKey:@"facebook_id"] description] gender:[[userData valueForKey:@"gender"] description] email:[[userData valueForKey:@"email"] description] name:[[userData valueForKey:@"name"] description] family:[[userData valueForKey:@"family"] description] company:[[userData valueForKey:@"company"] description] birthdate:[[userData valueForKey:@"birthdate"] description] streetName:[[userData valueForKey:@"street"] description] streetNum:[[userData valueForKey:@"street_number"] description] postCode:[[userData valueForKey:@"postcode"] description] city:[[userData valueForKey:@"city"] description] phone:[[userData valueForKey:@"phone"] description] mobile:[[userData valueForKey:@"mobile"] description] dateTime:[[userData valueForKey:@"datetime"] description] lastLoginDateTime:[[userData valueForKey:@"last_login_datetime"] description] lastLoginIp:[[userData valueForKey:@"last_login_ip"] description] phone2:[[userData valueForKey:@"phone2"] description] mobile2:[[userData valueForKey:@"mobile2"] description] country:[[userData valueForKey:@"country"] description]];
            
            self.streetNameField.text = self.profile.streetName;
            self.streetNumField.text = self.profile.streetNum;
            self.cityNameField.text = self.profile.city;
            self.zipCodeField.text = self.profile.postCode;
            self.phoneNumField.text = self.profile.phone;
            self.mobileNumField.text = self.profile.mobile;
            self.countryTextField.text = self.profile.country;
            
            if([self.profile.phone2 length] > 0){
                self.addMobileNumBtn.hidden = YES;
                self.phoneNumField2.hidden = NO;
                self.mobileNumField2.hidden = NO;
                self.isSecondPhoneAdded = YES;
                self.phoneNumField2.text = self.profile.phone2;
                self.mobileNumField2.text = self.profile.mobile2;
            }
            if([self.profile.phone2 length] == 0 && [self.profile.mobile2 length] == 0){
                self.addMobileNumBtn.hidden = NO;
                self.phoneNumField2.hidden = YES;
                self.mobileNumField2.hidden = YES;
                self.isSecondPhoneAdded = NO;
            }
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Aktualisierung des Profils fehlgeschlagen" andMessage:@"Es gibt ein Problem bei der Aktualisierung ihre Profils. Bitte versuchen Sie es erneut"];
        }
    }];
    
}

-(void)addMobileNumBtnPress:(id)sender
{
    self.addMobileNumBtn.hidden = YES;
    self.phoneNumField2.hidden = NO;
    self.mobileNumField2.hidden = NO;
    self.isSecondPhoneAdded = YES;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
