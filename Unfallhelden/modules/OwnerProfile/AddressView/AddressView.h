//
//  AddressView.h
//  Unfallhelden
//
//  Created by Adil Anwer on 3/5/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "UserProfile+DAO.h"

@interface AddressView : UIView<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *addressScrollView;
@property (nonatomic, weak) IBOutlet UITextField *streetNameField;
@property (nonatomic, weak) IBOutlet UITextField *streetNumField;
@property (nonatomic, weak) IBOutlet UITextField *cityNameField;
@property (nonatomic, weak) IBOutlet UITextField *zipCodeField;
@property (nonatomic, weak) IBOutlet UITextField *phoneNumField;
@property (nonatomic, weak) IBOutlet UITextField *mobileNumField;
@property (nonatomic, weak) IBOutlet UITextField *phoneNumField2;
@property (nonatomic, weak) IBOutlet UITextField *mobileNumField2;
@property (nonatomic, weak) IBOutlet UITextField *countryTextField;
@property (nonatomic, weak) IBOutlet UIButton *addMobileNumBtn;
@property (nonatomic, strong) NSMutableArray *countryArray;
@property (nonatomic) BOOL isSecondPhoneAdded;

@property (nonatomic, strong) UserProfile *profile;

-(void)setupView;
-(IBAction)saveChangesBtnPress:(id)sender;
-(IBAction)addMobileNumBtnPress:(id)sender;
-(IBAction)showCountryPicker:(id)sender;

@end
