//
//  NotificationsView.h
//  Unfallhelden
//
//  Created by Adil Anwer on 3/5/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationsView : UIView<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *notificationsTbl;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableArray *noDataFoundArray;
@property (nonatomic, weak) IBOutlet UILabel *noDataFoundLbl;

-(void)setupView;

@end
