//
//  NotificationsView.m
//  Unfallhelden
//
//  Created by Adil Anwer on 3/5/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "NotificationsView.h"
#import "NotificationsCell.h"
#import "Notifications.h"
static NSString * const NotificationsCellIdentifier = @"NotificationsCell";

@implementation NotificationsView
@synthesize notificationsTbl,dataArray, noDataFoundLbl, noDataFoundArray;

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NotificationsView" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

-(void)setupView
{
    self.dataArray = [[NSMutableArray alloc] init];
    self.noDataFoundArray = [[NSMutableArray alloc] init];
    [self.notificationsTbl reloadData];
}

#pragma mark - UITableViewDataSourceMethods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([self.dataArray count] > 0){
        return [self.dataArray count];
    }
    else{
        return [self.noDataFoundArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self.dataArray count] > 0){
        NotificationsCell *notificationsCell = (NotificationsCell*)[tableView dequeueReusableCellWithIdentifier:NotificationsCellIdentifier];
        if (notificationsCell == nil) {
            notificationsCell = [[NotificationsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NotificationsCellIdentifier];
        }
        [self configureBasicCell:notificationsCell atIndexPath:indexPath];
        return notificationsCell;
    }
    else{
        UITableViewCell *notificationsCell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:NotificationsCellIdentifier];
        if (notificationsCell == nil) {
            notificationsCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NotificationsCellIdentifier];
        }
        notificationsCell.textLabel.text = @"No Data Found";
        notificationsCell.textLabel.textColor = [UIColor whiteColor];
        notificationsCell.textLabel.textAlignment = NSTextAlignmentCenter;
        notificationsCell.textLabel.font = [UIFont regularUnfallheldenFontWithSize:17];
        notificationsCell.backgroundColor = [UIColor clearColor];
        return notificationsCell;
    }
}

#pragma mark - UITableViewDelegateMethods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([self.dataArray count] > 0){
        return [self heightForBasicCellAtIndexPath:indexPath];
    }
    else{
        return 45;
    }
    //    return 65;
}

- (CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static NotificationsCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [[NotificationsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NotificationsCellIdentifier];
    });
    
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1.0f; // Add 1.0f for the cell separator height
}

- (void)configureBasicCell:(NotificationsCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    //    RSSItem *item = self.feedItems[indexPath.row];
    Notifications *notification = [self.dataArray objectAtIndex:indexPath.row];
    [self setTitleForCell:cell item:notification.notificationDateTime];
    [self setSubtitleForCell:cell item:notification.notificationText];
}

- (void)setTitleForCell:(NotificationsCell *)cell item:(NSString *)item {
    //    NSString *title = @"13. Jan. 2015";
    [cell.dateLbl setText:item];
}

- (void)setSubtitleForCell:(NotificationsCell *)cell item:(NSString *)item {
    NSString *subtitle = item;
    
    // Some subtitles can be really long, so only display the
    // first 200 characters
    if (subtitle.length > 200) {
        subtitle = [NSString stringWithFormat:@"%@...", [subtitle substringToIndex:200]];
    }
    
    [cell.statusLbl setText:subtitle];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
