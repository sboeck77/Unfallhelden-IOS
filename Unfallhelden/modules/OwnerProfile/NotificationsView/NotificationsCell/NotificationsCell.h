//
//  NotificationsCell.h
//  Unfallhelden
//
//  Created by Adil Anwer on 3/5/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *dateLbl;
@property (nonatomic, weak) IBOutlet UILabel *statusLbl;
@end
