//
//  NotificationsCell.m
//  Unfallhelden
//
//  Created by Adil Anwer on 3/5/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "NotificationsCell.h"

@implementation NotificationsCell
@synthesize dateLbl,statusLbl;

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NotificationsCell" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
