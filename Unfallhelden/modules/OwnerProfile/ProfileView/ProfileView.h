//
//  ProfileView.h
//  Unfallhelden
//
//  Created by Adil Anwer on 3/4/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "UserProfile+DAO.h"

@interface ProfileView : UIView<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *profileScrollView;
@property (nonatomic, weak) IBOutlet UITextField *birthdayField;
@property (nonatomic, weak) IBOutlet UITextField *firstNameField;
@property (nonatomic, weak) IBOutlet UITextField *lastNameField;
@property (nonatomic, weak) IBOutlet UITextField *emailField;
@property (nonatomic, weak) IBOutlet UITextField *changePswrdField;
@property (nonatomic, weak) IBOutlet UIButton *changePswrdBtn;
@property (nonatomic, strong) UserProfile *profile;
@property (nonatomic, strong) NSString *dateOfBirth;

-(void)setupView;
-(IBAction)saveChangesBtnPress:(id)sender;

@end
