//
//  ProfileView.m
//  Unfallhelden
//
//  Created by Adil Anwer on 3/4/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "ProfileView.h"
#import "UnfallheldenDatePickerView.h"

@implementation ProfileView
@synthesize profileScrollView,birthdayField,firstNameField,lastNameField,emailField,changePswrdField;
@synthesize changePswrdBtn,profile, dateOfBirth;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ProfileView" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

-(void)setupView
{
    for(UIView *vw in self.profileScrollView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
        }
    }
    CGFloat heightOfPicker = 0.0f;
    CGFloat screenWidth = 0.0f;
    if(IS_IPHONE_4){
        screenWidth = 320;
        heightOfPicker = 206;
    }
    else if (IS_IPHONE_5){
        screenWidth = 320;
        heightOfPicker = 206;
    }
    else if(IS_IPHONE_6){
        screenWidth = 375;
    }
    else if (IS_IPHONE_6_Plus){
        screenWidth = 414;
    }
    CGRect frame = self.frame;
    frame.size.width = screenWidth;
    self.frame = frame;
    self.profileScrollView.contentSize = CGSizeMake(screenWidth, self.profileScrollView.contentSize.height);
    
    if (IS_IPHONE_6 || IS_IPHONE_6_Plus){
        heightOfPicker = 260;
    }
    UnfallheldenDatePickerView *dateTimePicker = [[UnfallheldenDatePickerView alloc] init];
    CGRect pickerFrame = dateTimePicker.frame;
    pickerFrame.size.height = heightOfPicker;
    dateTimePicker.frame = pickerFrame;
    [dateTimePicker.pickerDoneButton setAction:@selector(pickerDoneBtnPress:)];
    [dateTimePicker.datePicker addTarget:self action:@selector(pickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [dateTimePicker.datePicker setDatePickerMode:UIDatePickerModeDate];
    
    [self.birthdayField setTintColor:[UIColor clearColor]];
    self.birthdayField.inputView = dateTimePicker;
    
    self.changePswrdBtn.titleLabel.font = [UIFont boldUnfallheldenFontWithSize:17];
}

-(void)pickerValueChanged:(UIDatePicker *)timePicker
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd.MM.yyy"];
    NSString *date = [formatter stringFromDate:timePicker.date];
    NSLog(@"Date : %@", date);
    self.birthdayField.text = date;
    NSDateFormatter *formatterSecond = [[NSDateFormatter alloc] init];
    [formatterSecond setDateFormat:@"yyyy-MM-dd"];
    self.dateOfBirth = [formatterSecond stringFromDate:timePicker.date];
}

#pragma mark - Open PickerView
-(void)showDatePicker:(id)sender
{
    for(UIView *vw in self.profileScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    [self.birthdayField becomeFirstResponder];
}

#pragma mark - PickerView Done Button Press
-(IBAction)pickerDoneBtnPress:(id)sender
{
    [self.birthdayField resignFirstResponder];
}

-(void)saveChangesBtnPress:(id)sender
{
    if([self.firstNameField.text length]>0 && [self.lastNameField.text length] >0){
        NSString *birthdate;
        if([self.birthdayField.text length] >0){
            birthdate =  self.dateOfBirth;
        }
        else{
            birthdate = self.profile.birthdate;
        }
        [[UnfallheldenHttpClient sharedClient] uploadUserProfileWithUserName:self.firstNameField.text andAnrede:self.profile.gender andFamily:self.lastNameField.text andBirthdate:birthdate andStreet:self.profile.streetName andStreetNum:self.profile.streetNum andZip:self.profile.postCode andResidence:self.profile.city andMobile:self.profile.mobile andCompany:self.profile.company andPhone:self.profile.phone andPhoneSecond:profile.phone2 andMobileSecond:profile.mobile2 country:self.profile.country city:self.profile.city WithCompletionBlock:^(NSData *data) {
            NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSDictionary *dataDictionary = [responseString JSONValue];
            NSLog(@"data dictionary : %@",dataDictionary);
            int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
            if(status == 1){
                NSDictionary *userData = [dataDictionary objectForKey:@"data"];
                self.profile = [UserProfile updateUserProfileWithProfile:self.profile userId:[[userData valueForKey:@"id"] description] fbId:[[userData valueForKey:@"facebook_id"] description] gender:[[userData valueForKey:@"gender"] description] email:[[userData valueForKey:@"email"] description] name:[[userData valueForKey:@"name"] description] family:[[userData valueForKey:@"family"] description] company:[[userData valueForKey:@"company"] description] birthdate:[[userData valueForKey:@"birthdate"] description] streetName:[[userData valueForKey:@"street"] description] streetNum:[[userData valueForKey:@"street_number"] description] postCode:[[userData valueForKey:@"postcode"] description] city:[[userData valueForKey:@"city"] description] phone:[[userData valueForKey:@"phone"] description] mobile:[[userData valueForKey:@"mobile"] description] dateTime:[[userData valueForKey:@"datetime"] description] lastLoginDateTime:[[userData valueForKey:@"last_login_datetime"] description] lastLoginIp:[[userData valueForKey:@"last_login_ip"] description] phone2:[[userData valueForKey:@"phone2"] description] mobile2:[[userData valueForKey:@"mobile2"] description] country:[[userData valueForKey:@"country"] description]];
                
                self.firstNameField.text = self.profile.name;
                self.lastNameField.text = self.profile.family;
                if([self.profile.birthdate isEqualToString:@"0000-00-00"]){
                    self.birthdayField.text = @"";
                }
                else{
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"yyyy-MM-dd"];
                    NSDate *dateReg = [formatter dateFromString:self.profile.birthdate];
                    [formatter setDateFormat:@"dd.MM.yyy"];
                    self.birthdayField.text =  [formatter stringFromDate:dateReg];
                }
            }
            else{
                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Aktualisierung des Profils fehlgeschlagen" andMessage:@"Es gibt ein Problem bei der Aktualisierung ihre Profils. Bitte versuchen Sie es erneut"];
            }
        }];
    }
    else{
        for(UIView *vw in self.profileScrollView.subviews) {
            if([vw isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField*)vw;
                if([textField isEqual:self.firstNameField] || [textField isEqual:self.lastNameField]){
                    if([textField.text length] == 0 && textField.background == nil){
                        textField.layer.borderColor=[[UIColor redColor]CGColor];
                        textField.layer.borderWidth= 1.3f;
                        [textField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                        [textField setTextColor:[UIColor whiteColor]];
                        UIColor *color = [UIColor whiteColor];
                        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ wird benötigt",textField.placeholder] attributes:@{NSForegroundColorAttributeName: color}];
                    }
                }
            }
        }
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Vorname und Nachname werden benötigt."];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if([textField.text length] > 0){
        if([textField isEqual:self.firstNameField] || [textField isEqual:self.lastNameField]){
            textField.background = nil;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

@end
