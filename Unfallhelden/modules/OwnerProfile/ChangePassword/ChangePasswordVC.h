//
//  ChangePasswordVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 4/15/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordVC : UnfallheldenParentVC<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *oldPasswordField;
@property (nonatomic, weak) IBOutlet UITextField *confirmPasswordField;
@property (nonatomic, weak) IBOutlet UITextField *passwordNewField;

-(IBAction)saveChangedBtnPress:(id)sender;

@end
