//
//  ChangePasswordVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 4/15/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "ChangePasswordVC.h"

@interface ChangePasswordVC ()

@end

@implementation ChangePasswordVC
@synthesize oldPasswordField,passwordNewField,confirmPasswordField;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"Passwort ändern"];
    // Do any additional setup after loading the view from its nib.
    [super setupMenuBarButtonItems];
    for(UIView *vw in self.view.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

-(void)saveChangedBtnPress:(id)sender
{
    if([self.oldPasswordField.text length]>0 && [self.passwordNewField.text length]>0 && [self.confirmPasswordField.text length]>0){
        if([self.passwordNewField.text isEqualToString:self.confirmPasswordField.text]){
            [[UnfallheldenHttpClient sharedClient] changePasswordWithOldPassword:self.oldPasswordField.text newPswrd:self.passwordNewField.text confirmPassword:self.confirmPasswordField.text WithCompletionBlock:^(NSData *data) {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dataDictionary = [responseString JSONValue];
                NSLog(@"data dictionary : %@",dataDictionary);
                int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                if(status == 1){
                    [self.navigationController popViewControllerAnimated:YES];
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Ihr Passwort wurde erfolgreich geändert"];
                }
                else{
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Änderung des Passworts fehlgeschlagen" andMessage:@"Es gibt ein Problem bei der Änderung Ihres Passworts. Bitte versuchen Sie es erneut"];
                }
            }];
        }
        else{
             [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Die Passwörter stimmen nicht überein"];
        }
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte füllen Sie alle Felder aus."];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
