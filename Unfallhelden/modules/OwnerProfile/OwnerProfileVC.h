//
//  OwnerProfile.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/10/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Profile.h"
#import "UserProfile+DAO.h"
#import "ProfileView.h"
#import "AddressView.h"
#import "GAIDictionaryBuilder.h"

@interface OwnerProfileVC : UnfallheldenParentVC<UITextFieldDelegate>
{
    BOOL pageControlBeingUsed;
}

@property (nonatomic, weak) IBOutlet UIView *headingsView;
@property (nonatomic, weak) IBOutlet UILabel *profileLbl;
@property (nonatomic, weak) IBOutlet UILabel *addressLbl;
@property (nonatomic, weak) IBOutlet UILabel *notificationsLbl;
@property (nonatomic,weak) IBOutlet UIPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UIScrollView *profileScroll;
@property (nonatomic, strong) UserProfile *profile;
@property (nonatomic, strong) ProfileView *profileView;
@property (nonatomic, strong) AddressView *addressView;


-(IBAction)logoutBtnPress:(id)sender;

@end
