//
//  OwnerProfile.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/10/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "OwnerProfileVC.h"
#import "NotificationsView.h"
#import "ChangePasswordVC.h"
#import "HomeScreenVC.h"
#import "Notifications.h"

@interface OwnerProfileVC ()

@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;
@property (nonatomic, strong) NotificationsView *notificationsView;

@end

@implementation OwnerProfileVC
@synthesize headingsView,profileLbl,addressLbl,notificationsLbl,profileScroll;
@synthesize pageControl,profile,profileView,addressView, notificationsView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [super setupMenuBarButtonItems];
    
    //GoogleAnalytics
//    self.screenName = @"ProfileScreen";
    
    for(UIView *vw in self.headingsView.subviews) {
        if([vw isKindOfClass:[UIImageView class]]) {
            vw.hidden = YES;
        }
        if ([vw isKindOfClass:[UILabel class]]) {
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:((UILabel*)vw).text];
            [attributeString addAttribute:NSUnderlineStyleAttributeName
                                    value:[NSNumber numberWithInt:1]
                                    range:(NSRange){0,[attributeString length]}];
            ((UILabel*)vw).attributedText = attributeString;
            ((UILabel*)vw).textColor = [UIColor lightGrayColor];
        }
    }
    self.profileLbl.textColor = [UIColor whiteColor];
    pageControlBeingUsed = NO;
    
    self.navigationItem.rightBarButtonItems = nil;
    
    UIBarButtonItem *barButtonItem = [UIBarButtonItem addAnotherRightMenuBarButtonItemWithtarget:self andSelector:@selector(logoutBtnPress:) andImage:[UIImage imageNamed:@"logoutBtn"]];
    
    //    UIBarButtonItem *barButtonItemHelp = [UIBarButtonItem rightMenuBarButtonItemWithtarget:self andSelector:@selector(helpBtnPress:) andImage:[UIImage imageNamed:@"helpBtn"]];
    //
    //    UIBarButtonItem *barButtonItemEmpty = [UIBarButtonItem createEmptyBarButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    //    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,barButtonItemHelp,barButtonItemEmpty,barButtonItem, nil] animated:NO];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,barButtonItem, nil] animated:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ProfileScreen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    if(self.profileView == nil){
        
        [self.view layoutSubviews];
        NSArray *colors = [NSArray arrayWithObjects:[UIColor redColor], [UIColor greenColor], nil];
        CGFloat screenWidth = 0.0f;
        if(IS_IPHONE_4){
            screenWidth = 320;
        }
        else if (IS_IPHONE_5){
            screenWidth = 320;
        }
        else if(IS_IPHONE_6){
            screenWidth = 375;
        }
        else if (IS_IPHONE_6_Plus){
            screenWidth = 414;
        }
        for (int i = 0; i < colors.count; i++) {
            CGRect frame;
            frame.origin.x = self.view.frame.size.width * i;
            frame.origin.y = 0;
            frame.size  = CGSizeMake(screenWidth, self.profileScroll.frame.size.height);
            
            if(i == 0){
                self.profileView = [[ProfileView alloc] init];
                self.profileView.frame = frame;
                [self.profileView setupView];
                [self.profileScroll addSubview:profileView];
                self.profileView.profileScrollView.frame = frame;
                [self.profileView.changePswrdBtn addTarget:self action:@selector(changePswrdBtnPress:) forControlEvents:UIControlEventTouchUpInside];
                self.profileView.profileScrollView.contentSize = frame.size;
                self.profileView.profile = self.profile;
                self.profileView.firstNameField.text = self.profile.name;
                self.profileView.lastNameField.text = self.profile.family;
                if([self.profile.birthdate isEqualToString:@"0000-00-00"]){
                    self.profileView.birthdayField.text = @"";
                }
                else{
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"yyyy-MM-dd"];
                    NSDate *dateReg = [formatter dateFromString:self.profile.birthdate];
                    [formatter setDateFormat:@"dd.MM.yyy"];
                    self.profileView.birthdayField.text =  [formatter stringFromDate:dateReg];
                }
                self.profileView.emailField.text = self.profile.email;
            }
            else if(i == 1){
                self.addressView = [[AddressView alloc] init];
                self.addressView.frame = frame;
                [self.addressView setupView];
                [self.profileScroll addSubview:addressView];
                self.addressView.addressScrollView.frame = frame;
                self.addressView.profile = self.profile;
                self.addressView.addressScrollView.contentSize = frame.size;
                self.addressView.streetNameField.text = self.profile.streetName;
                self.addressView.streetNumField.text = self.profile.streetNum;
                self.addressView.cityNameField.text = self.profile.city;
                self.addressView.zipCodeField.text = self.profile.postCode;
                self.addressView.phoneNumField.text = self.profile.phone;
                self.addressView.mobileNumField.text = self.profile.mobile;
            }
            else{
                //                self.notificationsView = [[NotificationsView alloc] init];
                //                self.notificationsView.frame = frame;
                //                [self.notificationsView setupView];
                //                [self.profileScroll addSubview:self.notificationsView];
            }
        }
        
        self.profileScroll.contentSize = CGSizeMake(self.profileScroll.frame.size.width * colors.count, self.profileScroll.frame.size.height);
        self.pageControl.currentPage = 0;
        self.pageControl.numberOfPages = colors.count;
        
        if ([[UnfallheldenHttpClient sharedClient] connectedToWiFi]){
            [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                            message:nil];
            
            [[UnfallheldenHttpClient sharedClient] getUserProfileWithCompletionBlock:^(NSData *data) {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dataDictionary = [responseString JSONValue];
                NSLog(@"data dictionary : %@",dataDictionary);
                int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
                if(status == 1){
                    
                    //                    BOOL profileAlreadyExistInDB = [UserProfile checkIfProfileExistWithUserId:[[[dataDictionary objectForKey:@"data"] valueForKey:@"id"] description]];
                    if([[UserProfile getUserProfile] count] > 0){
                        //                        self.profile = (UserProfile*)[[UserProfile getUserProfile] objectAtIndex:0];
                        long profileCount = [[UserProfile getUserProfile] count];
                        for(int count = 0; count < profileCount; count++){
                            [UserProfile deleteUserProfile:(UserProfile*)[[UserProfile getUserProfile] objectAtIndex:count]];
                        }
                    }
                    
                    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                    NSString* foofile = [documentsPath stringByAppendingPathComponent:UserProfileFileName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
                    if(fileExists == TRUE){
                        [self removeSavedUserProfileFile];
                    }
                    
                    // Save user profile to lcoal file
                    NSArray *pathComponents = [NSArray arrayWithObjects:
                                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                                               UserProfileFileName,
                                               nil];
                    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
                    [data writeToURL:outputFileURL atomically:YES];
                    //            residence in get for city
                    NSDictionary *userData = [dataDictionary objectForKey:@"data"];
                    self.profile = [UserProfile saveUserProfileWithUserId:[[userData valueForKey:@"id"] description] fbId:[[userData valueForKey:@"facebook_id"] description] gender:[[userData valueForKey:@"gender"] description] email:[[userData valueForKey:@"email"] description] name:[[userData valueForKey:@"name"] description] family:[[userData valueForKey:@"family"] description] company:[[userData valueForKey:@"company"] description] birthdate:[[userData valueForKey:@"birthdate"] description] streetName:[[userData valueForKey:@"street"] description] streetNum:[[userData valueForKey:@"street_number"] description] postCode:[[userData valueForKey:@"postcode"] description] city:[[userData valueForKey:@"city"] description] phone:[[userData valueForKey:@"phone"] description] mobile:[[userData valueForKey:@"mobile"] description] dateTime:[[userData valueForKey:@"datetime"] description] lastLoginDateTime:[[userData valueForKey:@"last_login_datetime"] description] lastLoginIp:[[userData valueForKey:@"last_login_ip"] description] phone2:[[userData valueForKey:@"phone2"] description] mobile2:[[userData valueForKey:@"mobile2"] description] country:[[userData valueForKey:@"country"] description]];
                    
                    self.profileView.profile = self.profile;
                    self.profileView.firstNameField.text = self.profile.name;
                    self.profileView.lastNameField.text = self.profile.family;
                    if([self.profile.birthdate isEqualToString:@"0000-00-00"]){
                        self.profileView.birthdayField.text = @"";
                    }
                    else{
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"yyyy-MM-dd"];
                        NSDate *dateReg = [formatter dateFromString:self.profile.birthdate];
                        [formatter setDateFormat:@"dd.MM.yyy"];
                        self.profileView.birthdayField.text =  [formatter stringFromDate:dateReg];
                    }
                    self.profileView.emailField.text = self.profile.email;
                    self.addressView.profile = self.profile;
                    self.addressView.streetNameField.text = self.profile.streetName;
                    self.addressView.streetNumField.text = self.profile.streetNum;
                    self.addressView.cityNameField.text = self.profile.city;
                    self.addressView.zipCodeField.text = self.profile.postCode;
                    self.addressView.phoneNumField.text = self.profile.phone;
                    self.addressView.mobileNumField.text = self.profile.mobile;
                    self.addressView.countryTextField.text = self.profile.country;
                    
                    if([self.profile.phone2 length] > 0){
                        self.addressView.addMobileNumBtn.hidden = YES;
                        self.addressView.phoneNumField2.hidden = NO;
                        self.addressView.mobileNumField2.hidden = NO;
                        self.addressView.isSecondPhoneAdded = YES;
                        self.addressView.phoneNumField2.text = self.profile.phone2;
                        self.addressView.mobileNumField2.text = self.profile.mobile2;
                    }
                
                }
                else{
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Abruf der Profildaten fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Abruf ihres Profils. Bitte versuchen Sie es erneut"];
                }
            }];
            
            /*
             [[UnfallheldenHttpClient sharedClient] getNotificationsWithCompletionBlock:^(NSData *data) {
             NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             NSDictionary *dataDictionary = [responseString JSONValue];
             NSLog(@"data dictionary : %@",dataDictionary);
             int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
             [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
             
             if(status == 1){
             NSMutableArray *dataArray = [dataDictionary objectForKey:@"data"];
             if([dataArray count] > 0){
             
             NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
             NSString* foofile = [documentsPath stringByAppendingPathComponent:NotificationsFileName];
             BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
             if(fileExists == TRUE){
             [self removeNotificationFile];
             }
             
             // Save user profile to lcoal file
             NSArray *pathComponents = [NSArray arrayWithObjects:
             [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
             NotificationsFileName,
             nil];
             NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
             [data writeToURL:outputFileURL atomically:YES];
             
             for (int count = 0; count < [dataArray count]; count++) {
             
             Notifications *notification = [[Notifications alloc] init];
             notification.notificationId = [[dataArray objectAtIndex:count] valueForKey:@"id"];
             notification.notificationType = [[dataArray objectAtIndex:count] valueForKey:@"type"];
             notification.notificationTypeId = [[dataArray objectAtIndex:count] valueForKey:@"type_id"];
             notification.notificationText = [[dataArray objectAtIndex:count] valueForKey:@"text"];
             notification.notificationDateTime = [[dataArray objectAtIndex:count] valueForKey:@"datetime"];
             notification.notificationRead = [[dataArray objectAtIndex:count] valueForKey:@"read"];
             notification.notificationPnIdentifier = [[dataArray objectAtIndex:count] valueForKey:@"pn_identifier"];
             notification.notificationPnSend = [[dataArray objectAtIndex:count] valueForKey:@"pn_send"];
             notification.notificationPnFail = [[dataArray objectAtIndex:count] valueForKey:@"pn_fail"];
             [self.notificationsView.dataArray addObject:notification];
             [self.notificationsView.notificationsTbl reloadData];
             }
             }
             else{
             
             [self.notificationsView.noDataFoundArray addObject:@"No Data Found"];
             [self.notificationsView.notificationsTbl reloadData];
             }
             }
             }];
             */
            
        }
        else{
            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString* savedAccidentsFile = [documentsPath stringByAppendingPathComponent:UserProfileFileName];
            
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:savedAccidentsFile];
            if(fileExists == TRUE){
                
                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut. Zuvor gespeicherte Daten werden aktuell in der App angezeigt."];
                
                //            NSData *accidentsData = [NSData dataWithContentsOfFile:savedAccidentsFile];
                NSArray *pathComponents = [NSArray arrayWithObjects:
                                           [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                                           UserProfileFileName,
                                           nil];
                NSURL *userProfileFileURL = [NSURL fileURLWithPathComponents:pathComponents];
                
                NSData *userProfileData = [NSData dataWithContentsOfURL:userProfileFileURL];
                
                NSString *responseString = [[NSString alloc] initWithData:userProfileData encoding:NSUTF8StringEncoding];
                NSDictionary *dataDictionary = [responseString JSONValue];
                if([[UserProfile getUserProfile] count] > 0){
                    //                        self.profile = (UserProfile*)[[UserProfile getUserProfile] objectAtIndex:0];
                    long profileCount = [[UserProfile getUserProfile] count];
                    for(int count = 0; count < profileCount; count++){
                        [UserProfile deleteUserProfile:(UserProfile*)[[UserProfile getUserProfile] objectAtIndex:count]];
                    }
                }
                NSDictionary *userData = [dataDictionary objectForKey:@"data"];
                self.profile = [UserProfile saveUserProfileWithUserId:[[userData valueForKey:@"id"] description] fbId:[[userData valueForKey:@"facebook_id"] description] gender:[[userData valueForKey:@"gender"] description] email:[[userData valueForKey:@"email"] description] name:[[userData valueForKey:@"name"] description] family:[[userData valueForKey:@"family"] description] company:[[userData valueForKey:@"company"] description] birthdate:[[userData valueForKey:@"birthdate"] description] streetName:[[userData valueForKey:@"street"] description] streetNum:[[userData valueForKey:@"street_number"] description] postCode:[[userData valueForKey:@"postcode"] description] city:[[userData valueForKey:@"city"] description] phone:[[userData valueForKey:@"phone"] description] mobile:[[userData valueForKey:@"mobile"] description] dateTime:[[userData valueForKey:@"datetime"] description] lastLoginDateTime:[[userData valueForKey:@"last_login_datetime"] description] lastLoginIp:[[userData valueForKey:@"last_login_ip"] description] phone2:[[userData valueForKey:@"phone2"] description] mobile2:[[userData valueForKey:@"mobile2"] description] country:[[userData valueForKey:@"country"] description]];
                
                self.profileView.profile = self.profile;
                self.profileView.firstNameField.text = self.profile.name;
                self.profileView.lastNameField.text = self.profile.family;
                if([self.profile.birthdate isEqualToString:@"0000-00-00"]){
                    self.profileView.birthdayField.text = @"";
                }
                else{
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"yyyy-MM-dd"];
                    NSDate *dateReg = [formatter dateFromString:self.profile.birthdate];
                    [formatter setDateFormat:@"dd.MM.yyy"];
                    self.profileView.birthdayField.text =  [formatter stringFromDate:dateReg];
                }
                self.profileView.emailField.text = self.profile.email;
                
                self.addressView.profile = self.profile;
                self.addressView.streetNameField.text = self.profile.streetName;
                self.addressView.streetNumField.text = self.profile.streetNum;
                self.addressView.cityNameField.text = self.profile.city;
                self.addressView.zipCodeField.text = self.profile.postCode;
                self.addressView.phoneNumField.text = self.profile.phone;
                self.addressView.mobileNumField.text = self.profile.mobile;
                self.addressView.countryTextField.text = self.profile.country;
                
                if([self.profile.phone2 length] > 0){
                    self.addressView.addMobileNumBtn.hidden = YES;
                    self.addressView.phoneNumField2.hidden = NO;
                    self.addressView.mobileNumField2.hidden = NO;
                    self.addressView.isSecondPhoneAdded = YES;
                    self.addressView.phoneNumField2.text = self.profile.phone2;
                    self.addressView.mobileNumField2.text = self.profile.mobile2;
                }
            }
            /*
             NSString* savedNotificaitonFile = [documentsPath stringByAppendingPathComponent:NotificationsFileName];
             BOOL fileExists1 = [[NSFileManager defaultManager] fileExistsAtPath:savedNotificaitonFile];
             
             if(fileExists1 == TRUE){
             
             NSArray *pathComponents = [NSArray arrayWithObjects:
             [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
             NotificationsFileName,
             nil];
             NSURL *notificationFileURL = [NSURL fileURLWithPathComponents:pathComponents];
             
             NSData *notificationData = [NSData dataWithContentsOfURL:notificationFileURL];
             NSString *responseString = [[NSString alloc] initWithData:notificationData encoding:NSUTF8StringEncoding];
             NSDictionary *dataDictionary = [responseString JSONValue];
             NSMutableArray *dataArray = [dataDictionary objectForKey:@"data"];
             if([dataArray count] > 0){
             for (int count = 0; count < [dataArray count]; count++) {
             
             Notifications *notification = [[Notifications alloc] init];
             notification.notificationId = [[dataArray objectAtIndex:count] valueForKey:@"id"];
             notification.notificationType = [[dataArray objectAtIndex:count] valueForKey:@"type"];
             notification.notificationTypeId = [[dataArray objectAtIndex:count] valueForKey:@"type_id"];
             notification.notificationText = [[dataArray objectAtIndex:count] valueForKey:@"text"];
             notification.notificationDateTime = [[dataArray objectAtIndex:count] valueForKey:@"datetime"];
             notification.notificationRead = [[dataArray objectAtIndex:count] valueForKey:@"read"];
             notification.notificationPnIdentifier = [[dataArray objectAtIndex:count] valueForKey:@"pn_identifier"];
             notification.notificationPnSend = [[dataArray objectAtIndex:count] valueForKey:@"pn_send"];
             notification.notificationPnFail = [[dataArray objectAtIndex:count] valueForKey:@"pn_fail"];
             [self.notificationsView.dataArray addObject:notification];
             [self.notificationsView.notificationsTbl reloadData];
             }
             }
             else{
             
             [self.notificationsView.noDataFoundArray addObject:@"No Data Found"];
             [self.notificationsView.notificationsTbl reloadData];
             }
             }
             */
        }
    }
}

- (void)removeSavedUserProfileFile
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:UserProfileFileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

- (void)removeNotificationFile
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:NotificationsFileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (!pageControlBeingUsed) {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = self.profileScroll.frame.size.width;
        int page = floor((self.profileScroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.pageControl.currentPage = page;
        for(UIView *vw in self.headingsView.subviews) {
            if ([vw isKindOfClass:[UILabel class]]) {
                ((UILabel*)vw).textColor = [UIColor lightGrayColor];
            }
        }
        
        if(self.pageControl.currentPage == 0){
            self.profileLbl.textColor = [UIColor whiteColor];
        }
        else if (self.pageControl.currentPage == 1){
            self.addressLbl.textColor = [UIColor whiteColor];
        }
        else if (self.pageControl.currentPage == 2){
            self.notificationsLbl.textColor = [UIColor whiteColor];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (IBAction)changePage {
    // Update the scroll view to the appropriate page
    CGRect frame;
    frame.origin.x = self.profileScroll.frame.size.width * self.pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.profileScroll.frame.size;
    [self.profileScroll scrollRectToVisible:frame animated:YES];
    
    // Keep track of when scrolls happen in response to the page control
    // value changing. If we don't do this, a noticeable "flashing" occurs
    // as the the scroll delegate will temporarily switch back the page
    // number.
    pageControlBeingUsed = YES;
}

-(IBAction)sliderTitleBtnpress:(id)sender{
    
    CGRect frame;
    frame.origin.x = self.profileScroll.frame.size.width * ((UIButton*)sender).tag;
    frame.origin.y = 0;
    frame.size = self.profileScroll.frame.size;
    [self.profileScroll scrollRectToVisible:frame animated:YES];
    
    for(UIView *vw in self.headingsView.subviews) {
        if ([vw isKindOfClass:[UILabel class]]) {
            ((UILabel*)vw).textColor = [UIColor lightGrayColor];
        }
    }
    
    self.pageControl.currentPage  = ((UIButton*)sender).tag;
    if(self.pageControl.currentPage == 0){
        self.profileLbl.textColor = [UIColor whiteColor];
    }
    else if (self.pageControl.currentPage == 1){
        self.addressLbl.textColor = [UIColor whiteColor];
    }
    else if (self.pageControl.currentPage == 2){
        self.notificationsLbl.textColor = [UIColor whiteColor];
    }
    // Keep track of when scrolls happen in response to the page control
    // value changing. If we don't do this, a noticeable "flashing" occurs
    // as the the scroll delegate will temporarily switch back the page
    // number.
    pageControlBeingUsed = YES;
}

-(IBAction)changePswrdBtnPress:(id)sender
{
    ChangePasswordVC *changePswrdVC = [[ChangePasswordVC alloc] init];
    [self.navigationController pushViewController:changePswrdVC animated:YES];
}

-(void)logoutBtnPress:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ausloggen" message:@"Bitte bestätigen Sie Ihren Logout." delegate:self cancelButtonTitle:@"Abbrechen" otherButtonTitles:@"Logout", nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Logout"]) {
        
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Ausgeloggt" andMessage:@"Sie wurden erfolgreich abgemeldet."];
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:kUserDefaultsUserAlreadyLoggedIn];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNameUserLogout object:nil userInfo:nil];
        [UserProfile deleteUserProfile:self.profile];
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:[[HomeScreenVC alloc] init]];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
}

-(IBAction)helpBtnPress:(id)sender
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
