//
//  AddCarVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 4/29/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "AddCarVC.h"
#import "EnterDataManually.h"
#import "CarInfoVC.h"
#import "ELCImagePickerVC.h"
#import "CarDataVC.h"
#import "UnfallheldenDatePickerView.h"

@interface AddCarVC ()

@end

@implementation AddCarVC
@synthesize carScrollView,isAlreadyViewedSetup, enterDMHeightContraint, enterDataManuallyView;
@synthesize firstRegDateToUpload, issueDateToUplaod;

- (void)viewDidLoad {
    [self setTitle:@"Fahrzeug hinzufügen"];
    [super viewDidLoad];
    
    //GoogleAnalytics
//    self.screenName = @"AddCarScreen";
    
    [super setupMenuBarButtonItems];
    for(UIView *vw in self.carScrollView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            if(IS_IPHONE_5 || IS_IPHONE_4){
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:13]];
            }
            else{
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
            }
            if([vw isKindOfClass:[UIButton class]]){
                UIButton *btn = (UIButton*)vw;
                [btn.titleLabel setFont:[UIFont regularUnfallheldenFontWithSize:19]];
            }
        }
    }
    // Do any additional setup after loading the view from its nib.
    self.isAlreadyViewedSetup = NO;
    self.selectedCarBrandId = @"";
    self.selectedInsurerId = @"";
    self.issueDateToUplaod = @"";
    self.firstRegDateToUpload = @"";
    
    [self.companyCar setFont:[UIFont regularUnfallheldenFontWithSize:9]];
    [self.leasingCar setFont:[UIFont regularUnfallheldenFontWithSize:9]];
    
    for(UIView *vw in self.enterDataManuallyView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            if(IS_IPHONE_5 || IS_IPHONE_4){
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:13]];
            }
            else{
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
            }
        }
        
        if([vw isKindOfClass:[UILabel class]]){
            UILabel *lbl = (UILabel*)vw;
            [lbl setFont:[UIFont boldUnfallheldenFontWithSize:17]];
        }
        
        if([vw isKindOfClass:[UIButton class]]){
            UIButton *btn = (UIButton*)vw;
            [btn.titleLabel setFont:[UIFont regularUnfallheldenFontWithSize:19]];
        }
    }
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(documentImageSelected)
     name:kNotificationNameDocumentImageSelected
     object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"AddCarScreen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    if(self.isAlreadyViewedSetup == NO){
        self.isAlreadyViewedSetup = YES;
        [self.view layoutIfNeeded];
        [self.view layoutSubviews];
        CGFloat screenWidth = 0.0f;
        if(IS_IPHONE_4){
            screenWidth = 320;
        }
        else if (IS_IPHONE_5){
            screenWidth = 320;
        }
        else if(IS_IPHONE_6){
            screenWidth = 375;
        }
        else if (IS_IPHONE_6_Plus){
            screenWidth = 414;
        }
        
        self.insuranceCompanyArray = [GlobalScopeObjects sharedSingletonInstance].insurerArray;
        self.carBrandArray = [GlobalScopeObjects sharedSingletonInstance].carBrandArray;
        
        self.isCompanyCar = NO;
        self.isLeaseCar = NO;
        
        self.registrationDocumentPhoto  = @"";
        CGFloat heightOfPicker = 0.0f;
        
        if(IS_IPHONE_5){
            heightOfPicker = 206;
        }
        else if (IS_IPHONE_4){
            heightOfPicker = 206;
        }
        if (IS_IPHONE_6 || IS_IPHONE_6_Plus){
            heightOfPicker = 260;
        }
        
        self.unfallheldenPickerView = [[UnfallheldenPickerView alloc] init];
        CGRect pickerFrame = self.unfallheldenPickerView.frame;
        pickerFrame.size.height = heightOfPicker;
        self.unfallheldenPickerView.frame = pickerFrame;
        [self.unfallheldenPickerView.pickerDoneButton setAction:@selector(pickerDoneBtnPress:)];
        
        self.unfallheldenPickerView.pickerView.dataSource = self;
        self.unfallheldenPickerView.pickerView.delegate = self;
        
        //    [self.countryTxtField setTintColor:[UIColor clearColor]];
        //    self.countryTxtField.inputView = self.unfallheldenPickerView;
        
        [self.carBrandField setTintColor:[UIColor clearColor]];
        self.carBrandField.inputView = self.unfallheldenPickerView;
        
        [self.insuranceCompanyField setTintColor:[UIColor clearColor]];
        self.insuranceCompanyField.inputView = self.unfallheldenPickerView;
        
        CGRect frame = self.carScrollView.frame;
        
        if(IS_IPHONE_4){
            self.carScrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height+600);
        }
        else if (IS_IPHONE_5){
            self.carScrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height+600);
        }
        else if(IS_IPHONE_6_Plus){
            self.carScrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height+850);
            self.enterDMHeightContraint.constant = 856;
        }
        else{
            self.carScrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height+800);
        }
        CGFloat heightOfPicker2 = 0.0f;
        if(IS_IPHONE_5){
            heightOfPicker2 = 206;
        }
        else if (IS_IPHONE_4){
            heightOfPicker2 = 206;
        }
        if (IS_IPHONE_6 || IS_IPHONE_6_Plus){
            heightOfPicker2 = 260;
        }
        UnfallheldenDatePickerView *dateTimePicker = [[UnfallheldenDatePickerView alloc] init];
        CGRect pickerFrame2 = dateTimePicker.frame;
        pickerFrame2.size.height = heightOfPicker2;
        dateTimePicker.frame = pickerFrame2;
        [dateTimePicker.pickerDoneButton setAction:@selector(pickerDoneBtnPress:)];
        [dateTimePicker.datePicker addTarget:self action:@selector(pickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        [self.firstRegistrationDateField setTintColor:[UIColor clearColor]];
        [self.issueDateField setTintColor:[UIColor clearColor]];
        self.firstRegistrationDateField.inputView = dateTimePicker;
        self.issueDateField.inputView = dateTimePicker;
        [dateTimePicker.datePicker setDatePickerMode:UIDatePickerModeDate];
    }
    
}

-(void)pickerValueChanged:(UIDatePicker *)timePicker
{
    if(self.typeOfPicker == FirstRegDatePciker){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd.MM.yyy"];
        NSString *date = [formatter stringFromDate:timePicker.date];
        NSLog(@"Date : %@", date);
        self.firstRegistrationDateField.text = date;
        NSDateFormatter *formatterSecond = [[NSDateFormatter alloc] init];
        [formatterSecond setDateFormat:@"yyyy-MM-dd"];
        self.firstRegDateToUpload = [formatterSecond stringFromDate:timePicker.date];
    }
    else if(self.typeOfPicker == IssueDatePciker){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd.MM.yyy"];
        NSString *date = [formatter stringFromDate:timePicker.date];
        NSLog(@"Date : %@", date);
        self.issueDateField.text = date;
        NSDateFormatter *formatterSecond = [[NSDateFormatter alloc] init];
        [formatterSecond setDateFormat:@"yyyy-MM-dd"];
        self.issueDateToUplaod = [formatterSecond stringFromDate:timePicker.date];
    }
}

#pragma mark - UITextFieldDelegateMethods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if ([textField isEqual:self.carBrandField]){
        self.typeOfPicker = CarBrandPicker2;
    }
    else if ([textField isEqual:self.insuranceCompanyField]){
        self.typeOfPicker = InsuranceCompanyPicker2;
    }
    else if ([textField isEqual:self.firstRegistrationDateField]){
        self.typeOfPicker = FirstRegDatePciker;
    }
    else if ([textField isEqual:self.issueDateField]){
        self.typeOfPicker = IssueDatePciker;
    }
    
    [self.unfallheldenPickerView.pickerView reloadAllComponents];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

-(void)saveChangesBtnPress:(id)sender
{
    if([GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData != nil){
        self.carData = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData;
    }
    else{
        self.carData = [[UserCarData alloc] init];
        self.carData.firstReg = @"";
        self.carData.issueDate = @"";
        self.carData.nxtGeneralInspctn = @"";
        self.carData.vin = @"";
        self.carData.type = @"";
        self.carData.tradeName = @"";
        self.carData.brand = @"";
        self.carData.fuelType = @"";
        self.carData.displacement = @"";
        self.carData.powerKw = @"";
        self.carData.tireDimFront = @"";
        self.carData.tireDimBack = @"";
        self.carData.color = @"";
        self.carData.extras = @"";
        self.carData.regDate = @"";
    }
    
    NSString *insurerName;
    NSMutableArray *insurerArray = [GlobalScopeObjects sharedSingletonInstance].insurerArray;
    for (int count = 0; count < [insurerArray count]; count++) {
        
        Insurer *insurer = [insurerArray objectAtIndex:count];
        if([insurer.insurerId isEqualToString:self.carData.insurerId]){
            insurerName = insurer.insurerName;
            break;
        }
    }
    
    NSString *compantCar = @"0", *leaseCar = @"0";
    NSString *companyName = @"", *leasingCompanyName = @"";
    
    
    if(self.isCompanyCar == TRUE){
        companyName = self.companyNameField.text;
        compantCar = @"1";
    }
    
    if(self.isLeaseCar == TRUE){
        leasingCompanyName = self.leasingCompanyField.text;
        leaseCar = @"1";
    }
    
    if([self.numPlateField.text length]>0 && [self.insuranceCompanyField.text length]>0){
        if((self.isCompanyCar == TRUE && [self.companyNameField.text length] > 0) || self.isCompanyCar == FALSE){
            if((self.isLeaseCar == TRUE && [self.leasingCompanyField.text length] > 0) || self.isLeaseCar == FALSE){
                
                [[UnfallheldenHttpClient sharedClient] addCarWithinsurerId:self.selectedInsurerId regNum:self.numPlateField.text regDate:@"" issueDate:self.issueDateToUplaod firstReg:self.firstRegDateToUpload vin:self.vinField.text type:self.carTypeField.text tradeName:self.tradeNameField.text brand:self.selectedCarBrandId fuel:self.fuelField.text displacement:self.dsplcmntField.text ratedPower:self.rtdPwrField.text fTireDimen:self.fTireDimnsnField.text bTireDimen:self.rTireDimnsnField.text color:self.colorField.text leasingCar:leaseCar leasingCompany:leasingCompanyName companyCar:compantCar companyName:companyName extras:self.specialStrctrField.text regTicketPhoto:self.registrationDocumentPhoto  WithCompletionBlock:^(NSData *data) {
                    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    NSDictionary *dataDictionary = [responseString JSONValue];
                    NSLog(@"data dictionary : %@",dataDictionary);
                    int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                    if(status == 1){
                        NSDictionary *car = [dataDictionary objectForKey:@"data"];
                        NSMutableArray *arrayOfCars = [[NSMutableArray alloc] initWithObjects:car, nil];
                        for (int count = 0; count < [arrayOfCars count]; count++) {
                            UserCarData *car = [UserCarData saveUserCarWithCarId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"id"] description] userId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"user_id"] description] insurerId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"insurer"] description] leasingCar:[[[arrayOfCars objectAtIndex:count] valueForKey:@"leasing_car"] description] leasingCompany:[[[arrayOfCars objectAtIndex:count] valueForKey:@"leasing_company"] description] companyCar:[[[arrayOfCars objectAtIndex:count] valueForKey:@"company_car"] description] companyName:[[[arrayOfCars objectAtIndex:count] valueForKey:@"company_name"] description] regTicketPhoto:[[[arrayOfCars objectAtIndex:count] valueForKey:@"reg_ticket_photo"] description] issueDate:[[[arrayOfCars objectAtIndex:count] valueForKey:@"issue_date"] description] vin:[[[arrayOfCars objectAtIndex:count] valueForKey:@"vin"] description]  type:[[[arrayOfCars objectAtIndex:count] valueForKey:@"type"] description] tradeName:[[[arrayOfCars objectAtIndex:count] valueForKey:@"trade_name"] description]  body:[[[arrayOfCars objectAtIndex:count] valueForKey:@"body"] description] displacement:[[[arrayOfCars objectAtIndex:count] valueForKey:@"displacement"] description] fuelType:[[[arrayOfCars objectAtIndex:count] valueForKey:@"fuel_type"] description] color:[[arrayOfCars objectAtIndex:count] valueForKey:@"color"] extras:[[arrayOfCars objectAtIndex:count] valueForKey:@"extras"] powerKw:[[[arrayOfCars objectAtIndex:count] valueForKey:@"power_kw"] description]  carDeleted:[[[arrayOfCars objectAtIndex:count] valueForKey:@"deleted"] description] dateTime:[[[arrayOfCars objectAtIndex:count] valueForKey:@"datetime"] description] registrationNum:[[[arrayOfCars objectAtIndex:count] valueForKey:@"registration_number"] description] brand:[[[arrayOfCars objectAtIndex:count] valueForKey:@"brand"] description] firstReg:[[[arrayOfCars objectAtIndex:count] valueForKey:@"first_registration"] description]  regDate:[[[arrayOfCars objectAtIndex:count] valueForKey:@"registration_date"] description] tireDimFront:[[[arrayOfCars objectAtIndex:count] valueForKey:@"tire_dimension_front"] description] tireDimBack:[[[arrayOfCars objectAtIndex:count] valueForKey:@"tire_dimension_back"] description]];
                            [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData = nil;
                            //                CarDataVC *carData = [[CarDataVC alloc] init];
                            //                carData.carData = car;
                            //                [self.navigationController pushViewController:carData animated:YES];
                            
                            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                            NSArray *controllers = [NSArray arrayWithObject:[[CarInfoVC alloc] init]];
                            navigationController.viewControllers = controllers;
                            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
                            
                        }
                    }
                    else{
                        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Abruf der Fahrzeugdaten fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Abruf ihrer Fahrzeugdaten. Bitte versuchen Sie es erneut"];
                    }
                }];
            }
            else{
                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte geben Sie die Leasingfirma an."];
            }
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte geben Sie das Unternehmen an."];
        }
    }
    else{
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fehlende Felder" andMessage:@"Bitte tragen Sie das KFZ-Kennzeichen und Ihre Versicherung ein"];
    }
}

-(void)addCarDocumentPhotoBtnPress:(id)sender
{
    ELCImagePickerVC *imagePicker = [[ELCImagePickerVC alloc] init];
    imagePicker.imagePickerType = ELCImagePickerTypeSingleImage;
    imagePicker.imagePickerCallFor = @"DocumentImage";
    //    if([GlobalScopeObjects sharedSingletonInstance].carImage != nil){
    //        imagePicker.preselectedImage = [GlobalScopeObjects sharedSingletonInstance].carImage;
    //    }
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:imagePicker];
    navController.navigationBar.translucent = YES;
    [self presentViewController:navController animated:YES completion:nil];
}

-(void)documentImageSelected
{
    
    [[UnfallheldenHttpClient sharedClient] uploadCarDocumentPhotoWithImage:[GlobalScopeObjects sharedSingletonInstance].documentImage WithCompletionBlock:^(NSData *data) {
        
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *dataDictionary = [responseString JSONValue];
        //            NSDictionary *userData = [[dataDictionary valueForKey:@"data"] description];
        NSLog(@"data dictionary : %@",dataDictionary);
        int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
        if(status == 1){
            
            self.registrationDocumentPhoto = [[dataDictionary valueForKey:@"data"] description];
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Photo wurde erfolgreich hochgeladen" andMessage:@"Ihre Photos wurden erfolgreich in unserer Datenbank erfasst."];
            //            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            //            NSArray *controllers = [NSArray arrayWithObject:[[CarInfoVC alloc] init]];
            //            navigationController.viewControllers = controllers;
            //            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Bildupload fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Hochladen des Photos. Bitte versuchen Sie es erneut"];
        }
    }];
}


-(void)showBrandsPicker:(id)sender
{
    for(UIView *vw in self.carScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    for(UIView *vw in self.enterDataManuallyView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    self.typeOfPicker = CarBrandPicker2;
    [self.unfallheldenPickerView.pickerView reloadAllComponents];
    [self.carBrandField becomeFirstResponder];
}

-(void)showInsuranceComapniesPicker:(id)sender
{
    for(UIView *vw in self.carScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    for(UIView *vw in self.enterDataManuallyView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    self.typeOfPicker = InsuranceCompanyPicker2;
    [self.unfallheldenPickerView.pickerView reloadAllComponents];
    [self.insuranceCompanyField becomeFirstResponder];
}

-(void)showFirstRegDatePicker:(id)sender
{
    for(UIView *vw in self.carScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    for(UIView *vw in self.enterDataManuallyView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    self.typeOfPicker = FirstRegDatePciker;
    [self.firstRegistrationDateField becomeFirstResponder];
}

-(void)showIssueDatePicker:(id)sender
{
    for(UIView *vw in self.carScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    for(UIView *vw in self.enterDataManuallyView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    self.typeOfPicker = IssueDatePciker;
    [self.issueDateField becomeFirstResponder];
}

#pragma mark - PickerView Done Button Press
-(IBAction)pickerDoneBtnPress:(id)sender
{
    [self.insuranceCompanyField resignFirstResponder];
    [self.carBrandField resignFirstResponder];
    [self.firstRegistrationDateField resignFirstResponder];
    [self.issueDateField resignFirstResponder];
    //    [self.carTypeField resignFirstResponder];
}

#pragma mark - UIPickerView DataSource

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    //    if(self.typeOfPicker == CountryPicker){
    //        return [self.countryArray count];
    //    }
    //    else
    if (self.typeOfPicker == CarBrandPicker2){
        return [self.carBrandArray count];
    }
    //    else if (self.typeOfPicker == CarTypePicker){
    //        return [self.carTypeArray count];
    //    }
    else {
        return [self.insuranceCompanyArray count];
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    //    if(self.typeOfPicker == CountryPicker){
    //        return [self.countryArray objectAtIndex:row];
    //    }
    //    else
    if (self.typeOfPicker == CarBrandPicker2){
        CarBrand *carBrand =  [self.carBrandArray objectAtIndex:row];
        return carBrand.carBrandName;
    }
    //    else if (self.typeOfPicker == CarTypePicker){
    //        return [self.carTypeArray objectAtIndex:row];
    //    }
    else {
        Insurer *insurerCompany = [self.insuranceCompanyArray objectAtIndex:row];
        return insurerCompany.insurerName;
    }
}

#pragma mark - UIPickerView Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    //    if(self.typeOfPicker == CountryPicker){
    //        self.countryTxtField.text = [self.countryArray objectAtIndex:row];
    //    }
    //    else
    if (self.typeOfPicker == CarBrandPicker2){
        CarBrand *carBrand =  [self.carBrandArray objectAtIndex:row];
        self.carBrandField.text = carBrand.carBrandName;
        self.selectedCarBrandId = carBrand.carBrandId;
    }
    //    else if (self.typeOfPicker == CarTypePicker){
    //        self.carTypeField.text = [self.carTypeArray objectAtIndex:row];
    //    }
    else {
        Insurer *insurerCompany = [self.insuranceCompanyArray objectAtIndex:row];
        self.insuranceCompanyField.text = insurerCompany.insurerName;
        self.selectedInsurerId = insurerCompany.insurerId;
    }
}

-(void)companyCheckBtnPress:(id)sender
{
    self.isCompanyCar = !self.isCompanyCar;
    if(self.isCompanyCar) {
        [self.companyCarCheckBox setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
    else {
        [self.companyCarCheckBox setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
    }
}

-(void)leaseCheckBtnPress:(id)sender
{
    self.isLeaseCar = !self.isLeaseCar;
    if(self.isLeaseCar) {
        [self.leaseCarCheckBox setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
    else {
        [self.leaseCarCheckBox setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
