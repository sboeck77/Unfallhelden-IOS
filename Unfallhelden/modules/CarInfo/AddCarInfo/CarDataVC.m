//
//  CarDataVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 2/3/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "CarDataVC.h"
#import "FinishedPersonalDataVC.h"
#import "EnterDataManually.h"
#import "ELCImagePickerVC.h"
#import "CarInfoVC.h"

@interface CarDataVC ()

@end

@implementation CarDataVC
@synthesize carData;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"Step 2 of 2"];
    [self setupMenuBarButtonItems];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(documentImageSelected)
     name:kNotificationNameDocumentImageSelected
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(userEnteredData)
     name:kNotificationNameuserEnteredData
     object:nil];
}

-(void)doneBtnPress:(id)sender
{
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:[[CarInfoVC alloc] init]];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addCarDocumentPhotoBtnPress:(id)sender
{
    ELCImagePickerVC *imagePicker = [[ELCImagePickerVC alloc] init];
    imagePicker.imagePickerType = ELCImagePickerTypeSingleImage;
    imagePicker.imagePickerCallFor = @"DocumentImage";
    //    if([GlobalScopeObjects sharedSingletonInstance].carImage != nil){
    //        imagePicker.preselectedImage = [GlobalScopeObjects sharedSingletonInstance].carImage;
    //    }
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:imagePicker];
    navController.navigationBar.translucent = YES;
    [self presentViewController:navController animated:YES completion:nil];
}

#pragma mark - Other Functions

-(void)enterDataManually:(id)sender
{
    /*
     if(self.enterDMView == nil){
     self.enterDMView = [[EnterDataManually alloc] init];
     self.enterDMView.frame = self.frame;
     [self addSubview:self.enterDMView];
     }*/
    EnterDataManually *manuallyEnterData = [[EnterDataManually alloc] init];
    manuallyEnterData.calledFrom = @"AddCarVC";
    manuallyEnterData.carData = self.carData;
    [self.navigationController presentViewController:[[UINavigationController alloc] initWithRootViewController:manuallyEnterData] animated:YES completion:nil];
}

-(void)documentImageSelected
{
    
    [[UnfallheldenHttpClient sharedClient] uploadCarDocumentPhotoWithImage:[GlobalScopeObjects sharedSingletonInstance].documentImage andCarId:self.carData.carId WithCompletionBlock:^(NSData *data) {
        
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *dataDictionary = [responseString JSONValue];
        //            NSDictionary *userData = [[dataDictionary valueForKey:@"data"] description];
        NSLog(@"data dictionary : %@",dataDictionary);
        int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
        if(status == 1){
            //     [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Photo wurde erfolgreich hochgeladen" andMessage:@"Ihre Photos wurden erfolgreich in unserer Datenbank erfasst."];
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:[[CarInfoVC alloc] init]];
            navigationController.viewControllers = controllers;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Bildupload fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Hochladen des Photos. Bitte versuchen Sie es erneut"];
        }
    }];
}

-(void)userEnteredData{
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:[[CarInfoVC alloc] init]];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
