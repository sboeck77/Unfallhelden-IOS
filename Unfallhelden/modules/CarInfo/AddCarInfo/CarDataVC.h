//
//  CarDataVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/3/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserCarData.h"



@interface CarDataVC : UnfallheldenParentVC

@property (nonatomic, strong) UserCarData *carData;

-(IBAction)doneBtnPress:(id)sender;

-(IBAction)enterDataManually:(id)sender;
-(IBAction)addCarDocumentPhotoBtnPress:(id)sender;

@end
