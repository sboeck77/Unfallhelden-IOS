//
//  EnterDataManually.m
//  Unfallhelden
//
//  Created by Adil Anwer on 3/8/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "EnterDataManually.h"
#import "CarInfoVC.h"
#import "Insurer.h"

@implementation EnterDataManually
@synthesize dataScroll,contentView, firstRegistrationDateField, issueDateField, nxtGeneralInspctnField, vinField, typeField, tradeNameField, brandField;
@synthesize fuelField, dsplcmntField, rtdPwrField, fTireDimnsnField, rTireDimnsnField, colorField, specialStrctrField, carData, calledFrom;

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setTranslatesAutoresizingMaskIntoConstraints:YES];
    [[CustomizeNavbar sharedSingletonInstance] setBackgroundImageForNavBar:self.navigationController.navigationBar withImage:[UIImage imageNamed:@"navBarBg"]];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIFont boldUnfallheldenFontWithSize:15], NSFontAttributeName,
                                                                     [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                     nil]];
    [self setTitle:@"Car1"];
    [self.dataScroll contentSizeToFit];
    for(UIView *vw in self.contentView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            //            UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
            //            leftView.backgroundColor = [UIColor clearColor];
            //            textField.leftView = leftView;
            //            textField.leftViewMode = UITextFieldViewModeAlways;
            //            textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
        }
        
        if([vw isKindOfClass:[UILabel class]]){
            UILabel *lbl = (UILabel*)vw;
            [lbl setFont:[UIFont boldUnfallheldenFontWithSize:17]];
        }
    }
    if(![self.calledFrom isEqualToString:@"AddCarVC"]){
        if(self.carData != nil){
            self.firstRegistrationDateField.text = self.carData.firstReg;
            self.issueDateField.text = self.carData.issueDate;
            //        self.nxtGeneralInspctnField.text = self.carData.
            self.vinField.text = self.carData.vin;
            self.typeField.text = self.carData.type;
            self.tradeNameField.text = self.carData.tradeName;
            self.brandField.text = self.carData.brand;
            self.fuelField.text = self.carData.fuelType;
            self.dsplcmntField.text = self.carData.displacement;
            self.rtdPwrField.text = self.carData.powerKw;
            self.fTireDimnsnField.text = self.carData.tireDimFront;
            self.rTireDimnsnField.text = self.carData.tireDimBack;
            self.colorField.text = self.carData.color;
            self.specialStrctrField.text = self.carData.extras;
        }
    }
    else{
        if([GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData != nil){
            self.firstRegistrationDateField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.firstReg;
            self.issueDateField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.issueDate;
            self.nxtGeneralInspctnField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.nxtGeneralInspctn;
            self.vinField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.vin;
            self.typeField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.type;
            self.tradeNameField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.tradeName;
            self.brandField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.brand;
            self.fuelField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.fuelType;
            self.dsplcmntField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.displacement;
            self.rtdPwrField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.powerKw;
            self.fTireDimnsnField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.tireDimFront;
            self.rTireDimnsnField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.tireDimBack;
            self.colorField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.color;
            self.specialStrctrField.text = [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.extras;
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.contentView layoutIfNeeded];
    self.dataScroll.frame = CGRectMake(self.view.frame.origin.x, self.dataScroll.frame.origin.y, self.view.frame.size.width, self.dataScroll.frame.size.height);
    self.dataScroll.contentSize = CGSizeMake(self.view.frame.size.width, self.contentView.frame.size.height);
    
    [self.dataScroll contentSizeToFit];
    [self setupView];
}

-(void)setupView
{
    CGFloat screenWidth = 0.0f;
    if(IS_IPHONE_4){
        screenWidth = 320;
    }
    else if (IS_IPHONE_5){
        screenWidth = 320;
    }
    else if(IS_IPHONE_6){
        screenWidth = 375;
    }
    else if (IS_IPHONE_6_Plus){
        screenWidth = 414;
    }
    
    CGRect frame = self.view.frame;
    frame.size.width = screenWidth;
    self.view.frame = frame;
    self.dataScroll.contentSize = CGSizeMake(screenWidth, self.dataScroll.contentSize.height);
    if(IS_IPHONE_4){
        self.dataScroll.contentSize = CGSizeMake(self.view.frame.size.width, self.contentView.frame.size.height-240);
    }
    else if (IS_IPHONE_5){
        self.dataScroll.contentSize = CGSizeMake(self.view.frame.size.width, self.contentView.frame.size.height-160);
    }
}

-(void)saveChanges:(id)sender
{
    if(![self.calledFrom isEqualToString:@"AddCarVC"]){
        ((CarInfoVC*)[GlobalScopeObjects sharedSingletonInstance].carInfoVC).dismissedFrom = @"enterCarDataManually";
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        if(self.carData != nil){
            NSString *insurerName;
            NSMutableArray *insurerArray = [GlobalScopeObjects sharedSingletonInstance].insurerArray;
            for (int count = 0; count < [insurerArray count]; count++) {
                
                Insurer *insurer = [insurerArray objectAtIndex:count];
                if([insurer.insurerId isEqualToString:self.carData.insurerId]){
                    insurerName = insurer.insurerName;
                    break;
                }
            }
            [[UnfallheldenHttpClient sharedClient] updateUserCarWithInsurer:insurerName carId:self.carData.carId insurerId:self.carData.insurerId regNum:self.carData.registrationNum regDate:self.carData.regDate issueDate:self.issueDateField.text firstReg:self.firstRegistrationDateField.text vin:self.vinField.text type:self.typeField.text tradeName:self.tradeNameField.text brand:self.brandField.text fuel:self.fuelField.text displacement:self.dsplcmntField.text ratedPower:self.rtdPwrField.text fTireDimen:self.fTireDimnsnField.text bTireDimen:self.rTireDimnsnField.text color:self.colorField.text leasingCar:self.carData.leasingCar leasingCompany:self.carData.leasingCompany companyCar:self.carData.companyCar companyName:self.carData.companyName extras:self.carData.extras regTicketPhoto:self.carData.regTicketPhoto WithCompletionBlock:^(NSData *data) {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dataDictionary = [responseString JSONValue];
                NSLog(@"data dictionary : %@",dataDictionary);
                int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                if(status == 1){
                    NSDictionary *car = [dataDictionary objectForKey:@"data"];
                    NSMutableArray *arrayOfCars = [[NSMutableArray alloc] initWithObjects:car, nil];
                    for (int count = 0; count < [arrayOfCars count]; count++) {
                        UserCarData *car = [UserCarData saveUserCarWithCarId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"id"] description] userId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"user_id"] description] insurerId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"insurer"] description] leasingCar:[[[arrayOfCars objectAtIndex:count] valueForKey:@"leasing_car"] description] leasingCompany:[[[arrayOfCars objectAtIndex:count] valueForKey:@"leasing_company"] description] companyCar:[[[arrayOfCars objectAtIndex:count] valueForKey:@"company_car"] description] companyName:[[[arrayOfCars objectAtIndex:count] valueForKey:@"company_name"] description] regTicketPhoto:[[[arrayOfCars objectAtIndex:count] valueForKey:@"reg_ticket_photo"] description] issueDate:[[[arrayOfCars objectAtIndex:count] valueForKey:@"issue_date"] description] vin:[[[arrayOfCars objectAtIndex:count] valueForKey:@"vin"] description]  type:[[[arrayOfCars objectAtIndex:count] valueForKey:@"type"] description] tradeName:[[[arrayOfCars objectAtIndex:count] valueForKey:@"trade_name"] description]  body:[[[arrayOfCars objectAtIndex:count] valueForKey:@"body"] description] displacement:[[[arrayOfCars objectAtIndex:count] valueForKey:@"displacement"] description] fuelType:[[[arrayOfCars objectAtIndex:count] valueForKey:@"fuel_type"] description] color:[[arrayOfCars objectAtIndex:count] valueForKey:@"color"] extras:[[arrayOfCars objectAtIndex:count] valueForKey:@"extras"] powerKw:[[[arrayOfCars objectAtIndex:count] valueForKey:@"power_kw"] description]  carDeleted:[[[arrayOfCars objectAtIndex:count] valueForKey:@"deleted"] description] dateTime:[[[arrayOfCars objectAtIndex:count] valueForKey:@"datetime"] description] registrationNum:[[[arrayOfCars objectAtIndex:count] valueForKey:@"registration_number"] description] brand:[[[arrayOfCars objectAtIndex:count] valueForKey:@"brand"] description] firstReg:[[[arrayOfCars objectAtIndex:count] valueForKey:@"first_registration"] description]  regDate:[[[arrayOfCars objectAtIndex:count] valueForKey:@"registration_date"] description] tireDimFront:[[[arrayOfCars objectAtIndex:count] valueForKey:@"tire_dimension_front"] description] tireDimBack:[[[arrayOfCars objectAtIndex:count] valueForKey:@"tire_dimension_back"] description]];
                        int index;
                        for (int carsCount = 0; carsCount < [GlobalScopeObjects sharedSingletonInstance].userCarsArray.count; carsCount++) {
                            ReportAccidentData *data = [[GlobalScopeObjects sharedSingletonInstance].userCarsArray objectAtIndex:carsCount];
                            if(data.carId == self.carData.carId){
                                index = carsCount;
                                break;
                            }
                        }
                        [[GlobalScopeObjects sharedSingletonInstance].userCarsArray replaceObjectAtIndex:index withObject:car];
                    }
                }
                else{
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Abruf der Fahrzeugdaten fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Abruf ihrer Fahrzeugdaten. Bitte versuchen Sie es erneut"];
                }
            }];
        }
    }
    else{
        if([GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData == nil){
            [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData = [[UserCarData alloc] init];
        }
        
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.firstReg = self.firstRegistrationDateField.text;
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.issueDate = self.issueDateField.text;
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.nxtGeneralInspctn = self.nxtGeneralInspctnField.text;
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.vin = self.vinField.text;
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.type = self.typeField.text;
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.tradeName = self.tradeNameField.text;
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.brand = self.brandField.text;
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.fuelType = self.fuelField.text;
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.displacement = self.dsplcmntField.text;
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.powerKw = self.rtdPwrField.text;
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.tireDimFront = self.fTireDimnsnField.text;
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.tireDimBack = self.rTireDimnsnField.text;
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.color = self.colorField.text;
        [GlobalScopeObjects sharedSingletonInstance].manuallyEnteredCarData.extras = self.specialStrctrField.text;
        
        if(self.carData != nil){
            NSString *insurerName;
            NSMutableArray *insurerArray = [GlobalScopeObjects sharedSingletonInstance].insurerArray;
            for (int count = 0; count < [insurerArray count]; count++) {
                
                Insurer *insurer = [insurerArray objectAtIndex:count];
                if([insurer.insurerId isEqualToString:self.carData.insurerId]){
                    insurerName = insurer.insurerName;
                    break;
                }
            }
            [[UnfallheldenHttpClient sharedClient] updateUserCarWithInsurer:insurerName carId:self.carData.carId insurerId:self.carData.insurerId regNum:self.carData.registrationNum regDate:self.carData.regDate issueDate:self.issueDateField.text firstReg:self.firstRegistrationDateField.text vin:self.vinField.text type:self.typeField.text tradeName:self.tradeNameField.text brand:self.brandField.text fuel:self.fuelField.text displacement:self.dsplcmntField.text ratedPower:self.rtdPwrField.text fTireDimen:self.fTireDimnsnField.text bTireDimen:self.rTireDimnsnField.text color:self.colorField.text leasingCar:self.carData.leasingCar leasingCompany:self.carData.leasingCompany companyCar:self.carData.companyCar companyName:self.carData.companyName extras:self.carData.extras regTicketPhoto:self.carData.regTicketPhoto WithCompletionBlock:^(NSData *data) {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dataDictionary = [responseString JSONValue];
                NSLog(@"data dictionary : %@",dataDictionary);
                int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                if(status == 1){
                    NSDictionary *car = [dataDictionary objectForKey:@"data"];
                    NSMutableArray *arrayOfCars = [[NSMutableArray alloc] initWithObjects:car, nil];
                    for (int count = 0; count < [arrayOfCars count]; count++) {
                        UserCarData *car = [UserCarData saveUserCarWithCarId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"id"] description] userId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"user_id"] description] insurerId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"insurer"] description] leasingCar:[[[arrayOfCars objectAtIndex:count] valueForKey:@"leasing_car"] description] leasingCompany:[[[arrayOfCars objectAtIndex:count] valueForKey:@"leasing_company"] description] companyCar:[[[arrayOfCars objectAtIndex:count] valueForKey:@"company_car"] description] companyName:[[[arrayOfCars objectAtIndex:count] valueForKey:@"company_name"] description] regTicketPhoto:[[[arrayOfCars objectAtIndex:count] valueForKey:@"reg_ticket_photo"] description] issueDate:[[[arrayOfCars objectAtIndex:count] valueForKey:@"issue_date"] description] vin:[[[arrayOfCars objectAtIndex:count] valueForKey:@"vin"] description]  type:[[[arrayOfCars objectAtIndex:count] valueForKey:@"type"] description] tradeName:[[[arrayOfCars objectAtIndex:count] valueForKey:@"trade_name"] description]  body:[[[arrayOfCars objectAtIndex:count] valueForKey:@"body"] description] displacement:[[[arrayOfCars objectAtIndex:count] valueForKey:@"displacement"] description] fuelType:[[[arrayOfCars objectAtIndex:count] valueForKey:@"fuel_type"] description] color:[[arrayOfCars objectAtIndex:count] valueForKey:@"color"] extras:[[arrayOfCars objectAtIndex:count] valueForKey:@"extras"] powerKw:[[[arrayOfCars objectAtIndex:count] valueForKey:@"power_kw"] description]  carDeleted:[[[arrayOfCars objectAtIndex:count] valueForKey:@"deleted"] description] dateTime:[[[arrayOfCars objectAtIndex:count] valueForKey:@"datetime"] description] registrationNum:[[[arrayOfCars objectAtIndex:count] valueForKey:@"registration_number"] description] brand:[[[arrayOfCars objectAtIndex:count] valueForKey:@"brand"] description] firstReg:[[[arrayOfCars objectAtIndex:count] valueForKey:@"first_registration"] description]  regDate:[[[arrayOfCars objectAtIndex:count] valueForKey:@"registration_date"] description] tireDimFront:[[[arrayOfCars objectAtIndex:count] valueForKey:@"tire_dimension_front"] description] tireDimBack:[[[arrayOfCars objectAtIndex:count] valueForKey:@"tire_dimension_back"] description]];
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNameuserEnteredData object:nil userInfo:nil];
                        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                    }
                }
                else{
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Abruf der Fahrzeugdaten fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Abruf ihrer Fahrzeugdaten. Bitte versuchen Sie es erneut"];
                }
            }];
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
