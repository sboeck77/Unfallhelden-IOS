//
//  EnterDataManually.h
//  Unfallhelden
//
//  Created by Adil Anwer on 3/8/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "UserCarData.h"

@interface EnterDataManually : UIViewController<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *dataScroll;
@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet UITextField *firstRegistrationDateField;
@property (nonatomic, weak) IBOutlet UITextField *issueDateField;
@property (nonatomic, weak) IBOutlet UITextField *nxtGeneralInspctnField;
@property (nonatomic, weak) IBOutlet UITextField *vinField;
@property (nonatomic, weak) IBOutlet UITextField *typeField;
@property (nonatomic, weak) IBOutlet UITextField *tradeNameField;
@property (nonatomic, weak) IBOutlet UITextField *brandField;
@property (nonatomic, weak) IBOutlet UITextField *fuelField;
@property (nonatomic, weak) IBOutlet UITextField *dsplcmntField;
@property (nonatomic, weak) IBOutlet UITextField *rtdPwrField;
@property (nonatomic, weak) IBOutlet UITextField *fTireDimnsnField;
@property (nonatomic, weak) IBOutlet UITextField *rTireDimnsnField;
@property (nonatomic, weak) IBOutlet UITextField *colorField;
@property (nonatomic, weak) IBOutlet UITextField *specialStrctrField;
@property (nonatomic, strong) UserCarData *carData;
@property (nonatomic, strong) NSString *calledFrom;

-(IBAction)saveChanges:(id)sender;

@end
