//
//  CarInfoVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/7/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarView.h"

@interface CarInfoVC : UnfallheldenParentVC<UITextFieldDelegate>
{
    BOOL pageControlBeingUsed;
}

@property (nonatomic,weak) IBOutlet UITableView *carsTbl;
@property (nonatomic,weak) IBOutlet UIPageControl *pageControl;
@property (nonatomic, retain) IBOutlet UIScrollView* scrollView;
@property (nonatomic, retain) IBOutlet UIScrollView *carNameScrollview;
@property (nonatomic, retain) IBOutlet UIView *titlesContainer;
@property (nonatomic, weak) IBOutlet UIView *contentSizeview;
@property (nonatomic, weak) IBOutlet UIImageView *backgroundImg;
@property (nonatomic) CGRect originalTitleScrollFrame;
@property (nonatomic, strong) NSMutableArray *carsArray;
@property (nonatomic, strong) NSString *dismissedFrom;
@property (nonatomic) BOOL isAlreadyViewedSetup;
@property (nonatomic, weak) IBOutlet UILabel *noCarsFoundLbl;

//@property (nonatomic, strong) CarView *carView;

-(IBAction)addCar:(id)sender;

@end
