//
//  CarInfoVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/7/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "CarInfoVC.h"
#import "AddCarVC.h"
#import "CarInfoCell.h"
#import "CarView.h"
#import "PersonalDataVC.h"
#import "UserCarData.h"
#import "Insurer.h"

@interface CarInfoVC ()

@end

@implementation CarInfoVC
@synthesize carsTbl,carsArray,pageControl,scrollView,carNameScrollview,titlesContainer;
@synthesize dismissedFrom, isAlreadyViewedSetup, noCarsFoundLbl;

- (void)viewDidLoad {
    //GoogleAnalytics
//    self.screenName = @"MyCarsScreen";
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [super setupMenuBarButtonItems];
    
    [self.titlesContainer addSubview:self.carNameScrollview];
    pageControlBeingUsed = NO;
    [GlobalScopeObjects sharedSingletonInstance].carInfoVC = self;
    
    UIBarButtonItem *barButtonItem = [UIBarButtonItem addAnotherRightMenuBarButtonItemWithtarget:self andSelector:@selector(addCar:) andImage:[UIImage imageNamed:@"addBtn"]];
    
    //    UIBarButtonItem *barButtonItemHelp = [UIBarButtonItem rightMenuBarButtonItemWithtarget:self andSelector:@selector(helpBtnPress:) andImage:[UIImage imageNamed:@"helpBtn"]];
    
    //    UIBarButtonItem *barButtonItemEmpty = [UIBarButtonItem createEmptyBarButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer, barButtonItem, nil] animated:NO];
    //    self.carsArray = [[NSMutableArray alloc] init];
    self.originalTitleScrollFrame = self.carNameScrollview.frame;
    self.isAlreadyViewedSetup = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"MyCarsScreen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [self.view layoutIfNeeded];
    [self.view layoutSubviews];
    if(self.isAlreadyViewedSetup == NO){
        if(![self.dismissedFrom isEqualToString:@"enterCarDataManually"]){
            self.isAlreadyViewedSetup = YES;
            //        NSArray *colors = [NSArray arrayWithObjects:[UIColor redColor], [UIColor greenColor], [UIColor blueColor], nil];
            CGFloat screenWidth = 0.0f;
            if(IS_IPHONE_4){
                screenWidth = 320;
            }
            else if (IS_IPHONE_5){
                screenWidth = 320;
            }
            else if(IS_IPHONE_6){
                screenWidth = 375;
            }
            else if (IS_IPHONE_6_Plus){
                screenWidth = 414;
            }
            
            self.carsArray = [[NSMutableArray alloc] init];
            if ([[UnfallheldenHttpClient sharedClient] connectedToWiFi]){
                
                [[UnfallheldenHttpClient sharedClient] getUserCarsWithCompletionBlock:^(NSData *data) {
                    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    NSDictionary *dataDictionary = [responseString JSONValue];
                    NSLog(@"data dictionary : %@",dataDictionary);
                    int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                    if(status == 1){
                        NSMutableArray *arrayOfCars = [dataDictionary objectForKey:@"data"];
                        for (int count = 0; count < [arrayOfCars count]; count++) {
                            UserCarData *car = [UserCarData saveUserCarWithCarId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"id"] description] userId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"user_id"] description] insurerId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"insurer"] description] leasingCar:[[[arrayOfCars objectAtIndex:count] valueForKey:@"leasing_car"] description] leasingCompany:[[[arrayOfCars objectAtIndex:count] valueForKey:@"leasing_company"] description] companyCar:[[[arrayOfCars objectAtIndex:count] valueForKey:@"company_car"] description] companyName:[[[arrayOfCars objectAtIndex:count] valueForKey:@"company_name"] description] regTicketPhoto:[[[arrayOfCars objectAtIndex:count] valueForKey:@"reg_ticket_photo"] description] issueDate:[[[arrayOfCars objectAtIndex:count] valueForKey:@"issue_date"] description] vin:[[[arrayOfCars objectAtIndex:count] valueForKey:@"vin"] description]  type:[[[arrayOfCars objectAtIndex:count] valueForKey:@"type"] description] tradeName:[[[arrayOfCars objectAtIndex:count] valueForKey:@"trade_name"] description]  body:[[[arrayOfCars objectAtIndex:count] valueForKey:@"body"] description] displacement:[[[arrayOfCars objectAtIndex:count] valueForKey:@"displacement"] description] fuelType:[[[arrayOfCars objectAtIndex:count] valueForKey:@"fuel_type"] description] color:[[arrayOfCars objectAtIndex:count] valueForKey:@"color"] extras:[[arrayOfCars objectAtIndex:count] valueForKey:@"extras"] powerKw:[[[arrayOfCars objectAtIndex:count] valueForKey:@"power_kw"] description]  carDeleted:[[[arrayOfCars objectAtIndex:count] valueForKey:@"deleted"] description] dateTime:[[[arrayOfCars objectAtIndex:count] valueForKey:@"datetime"] description] registrationNum:[[[arrayOfCars objectAtIndex:count] valueForKey:@"registration_number"] description] brand:[[[arrayOfCars objectAtIndex:count] valueForKey:@"brand"] description] firstReg:[[[arrayOfCars objectAtIndex:count] valueForKey:@"first_registration"] description]  regDate:[[[arrayOfCars objectAtIndex:count] valueForKey:@"registration_date"] description] tireDimFront:[[[arrayOfCars objectAtIndex:count] valueForKey:@"tire_dimension_front"] description] tireDimBack:[[[arrayOfCars objectAtIndex:count] valueForKey:@"tire_dimension_back"] description]];
                            [self.carsArray addObject:car];
                        }
                        if([self.carsArray count] > 0){
                            self.noCarsFoundLbl.hidden = YES;
                            NSString *carCount = [NSString stringWithFormat:@"%lu",(unsigned long)[self.carsArray count]];
                            [[NSUserDefaults standardUserDefaults] setValue:carCount forKey:kNumberOfCarsAdded];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            [GlobalScopeObjects sharedSingletonInstance].userCarsArray = self.carsArray;
                            //
                            [self setupTitleScroll];
                            
                            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString* foofile = [documentsPath stringByAppendingPathComponent:UserCarsFileName];
                            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
                            if(fileExists == TRUE){
                                [self removeSavedUserCarsFile];
                            }
                            
                            // Save user profile to lcoal file
                            NSArray *pathComponents = [NSArray arrayWithObjects:
                                                       [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                                                       UserCarsFileName,
                                                       nil];
                            NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
                            [data writeToURL:outputFileURL atomically:YES];
                            
                            for (int i = 0; i < [self.carsArray count]; i++) {
                                CGRect frame;
                                frame.origin.x = self.view.frame.size.width * i;
                                frame.origin.y = 0;
                                frame.size  = CGSizeMake(screenWidth, self.scrollView.frame.size.height);
                                
                                //            if(i == 0){
                                //                if(self.carView == nil){
                                CarView *carView = [[CarView alloc] init];
                                carView.frame = frame;
                                [carView setupView];
                                carView.currentCarTag = i;
                                [self.scrollView addSubview:carView];
                                carView.carScrollView.frame = frame;
                                UserCarData *data = [self.carsArray objectAtIndex:i];
                                [self setupCarDataForCarView:carView andUserCarData:data];
                                
                                if(IS_IPHONE_4){
                                    carView.carScrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height+700);
                                }
                                else if (IS_IPHONE_5){
                                    carView.carScrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height+700);
                                }
                                else if(IS_IPHONE_6_Plus){
                                    carView.carScrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height+850);
                                    carView.enterDMHeightContraint.constant = 856;
                                }
                                else{
                                    carView.carScrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height+800);
                                }
                            }
                            
                            self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * self.carsArray.count, self.scrollView.frame.size.height);
                            self.carNameScrollview.contentSize = CGSizeMake(self.carNameScrollview.frame.size.width * self.carsArray.count, self.carNameScrollview.frame.size.height);
                            self.pageControl.currentPage = 0;
                            self.pageControl.numberOfPages = self.carsArray.count;
                        }
                        else{
                            self.noCarsFoundLbl.hidden = NO;
                        }
                    }
                    else{
                        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Abruf der Fahrzeugdaten fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Abruf ihrer Fahrzeugdaten. Bitte versuchen Sie es erneut"];
                    }
                }];
                
            }
            else{
                NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString* savedAccidentsFile = [documentsPath stringByAppendingPathComponent:UserCarsFileName];
                
                BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:savedAccidentsFile];
                if(fileExists == TRUE){
                    
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut. Zuvor gespeicherte Daten werden aktuell in der App angezeigt."];
                    
                    
                    NSArray *pathComponents = [NSArray arrayWithObjects:
                                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                                               UserCarsFileName,
                                               nil];
                    NSURL *userCarsFileURL = [NSURL fileURLWithPathComponents:pathComponents];
                    
                    NSData *userCarsData = [NSData dataWithContentsOfURL:userCarsFileURL];
                    
                    //78
                    
                    NSString *responseString = [[NSString alloc] initWithData:userCarsData encoding:NSUTF8StringEncoding];
                    NSDictionary *dataDictionary = [responseString JSONValue];
                    NSMutableArray *arrayOfCars = [dataDictionary objectForKey:@"data"];
                    for (int count = 0; count < [arrayOfCars count]; count++) {
                        UserCarData *car = [UserCarData saveUserCarWithCarId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"id"] description] userId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"user_id"] description] insurerId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"insurer"] description] leasingCar:[[[arrayOfCars objectAtIndex:count] valueForKey:@"leasing_car"] description] leasingCompany:[[[arrayOfCars objectAtIndex:count] valueForKey:@"leasing_company"] description] companyCar:[[[arrayOfCars objectAtIndex:count] valueForKey:@"company_car"] description] companyName:[[[arrayOfCars objectAtIndex:count] valueForKey:@"company_name"] description] regTicketPhoto:[[[arrayOfCars objectAtIndex:count] valueForKey:@"reg_ticket_photo"] description] issueDate:[[[arrayOfCars objectAtIndex:count] valueForKey:@"issue_date"] description] vin:[[[arrayOfCars objectAtIndex:count] valueForKey:@"vin"] description]  type:[[[arrayOfCars objectAtIndex:count] valueForKey:@"type"] description] tradeName:[[[arrayOfCars objectAtIndex:count] valueForKey:@"trade_name"] description]  body:[[[arrayOfCars objectAtIndex:count] valueForKey:@"body"] description] displacement:[[[arrayOfCars objectAtIndex:count] valueForKey:@"displacement"] description] fuelType:[[[arrayOfCars objectAtIndex:count] valueForKey:@"fuel_type"] description] color:[[arrayOfCars objectAtIndex:count] valueForKey:@"color"] extras:[[arrayOfCars objectAtIndex:count] valueForKey:@"extras"] powerKw:[[[arrayOfCars objectAtIndex:count] valueForKey:@"power_kw"] description]  carDeleted:[[[arrayOfCars objectAtIndex:count] valueForKey:@"deleted"] description] dateTime:[[[arrayOfCars objectAtIndex:count] valueForKey:@"datetime"] description] registrationNum:[[[arrayOfCars objectAtIndex:count] valueForKey:@"registration_number"] description] brand:[[[arrayOfCars objectAtIndex:count] valueForKey:@"brand"] description] firstReg:[[[arrayOfCars objectAtIndex:count] valueForKey:@"first_registration"] description]  regDate:[[[arrayOfCars objectAtIndex:count] valueForKey:@"registration_date"] description] tireDimFront:[[[arrayOfCars objectAtIndex:count] valueForKey:@"tire_dimension_front"] description] tireDimBack:[[[arrayOfCars objectAtIndex:count] valueForKey:@"tire_dimension_back"] description]];
                        [self.carsArray addObject:car];
                    }
                    if([self.carsArray count] > 0){
                        self.noCarsFoundLbl.hidden = YES;
                        NSString *carCount = [NSString stringWithFormat:@"%lu",(unsigned long)[self.carsArray count]];
                        [[NSUserDefaults standardUserDefaults] setValue:carCount forKey:kNumberOfCarsAdded];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [GlobalScopeObjects sharedSingletonInstance].userCarsArray = self.carsArray;
                        
                        [self setupTitleScroll];
                        
                        for (int i = 0; i < [self.carsArray count]; i++) {
                            CGRect frame;
                            frame.origin.x = self.view.frame.size.width * i;
                            frame.origin.y = 0;
                            frame.size  = CGSizeMake(screenWidth, self.scrollView.frame.size.height);
                            
                            //            if(i == 0){
                            //                if(self.carView == nil){
                            CarView *carView = [[CarView alloc] init];
                            carView.frame = frame;
                            [carView setupView];
                            carView.currentCarTag = i;
                            [self.scrollView addSubview:carView];
                            carView.carScrollView.frame = frame;
                            UserCarData *data = [self.carsArray objectAtIndex:i];
                            [self setupCarDataForCarView:carView andUserCarData:data];
                            
                            if(IS_IPHONE_4){
                                carView.carScrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height+700);
                            }
                            else if (IS_IPHONE_5){
                                carView.carScrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height+700);
                            }
                            else if(IS_IPHONE_6_Plus){
                                carView.carScrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height+850);
                                carView.enterDMHeightContraint.constant = 856;
                            }
                            else{
                                carView.carScrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height+800);
                            }
                        }
                        
                        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * self.carsArray.count, self.scrollView.frame.size.height);
                        self.carNameScrollview.contentSize = CGSizeMake(self.carNameScrollview.frame.size.width * self.carsArray.count, self.carNameScrollview.frame.size.height);
                        self.pageControl.currentPage = 0;
                        self.pageControl.numberOfPages = self.carsArray.count;
                    }
                    else{
                        self.noCarsFoundLbl.hidden = NO;
                    }
                }
            }
        }
    }
}

- (void)removeSavedUserCarsFile
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:UserCarsFileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

-(void)setupCarDataForCarView:(CarView*)carView andUserCarData:(UserCarData*)data
{
    carView.carId = data.carId;
    carView.numPlateField.text = data.registrationNum;
    carView.carBrandField.text = data.brand;
    carView.carTypeField.text = data.type;
    carView.registrationDocumentPhoto = data.regTicketPhoto;
    if([carView.registrationDocumentPhoto length] > 0){
        [carView.carPhotoUploadBtn setBackgroundImage:[UIImage imageNamed:@"takePhotoBtnGreen"] forState:UIControlStateNormal];
        [carView.carPhotoUploadBtn setTitle:@"" forState:UIControlStateNormal];
    }
    else{
        carView.registrationDocumentPhoto  = @"";
    }
    
    carView.selectedCarBrandId = data.brand;
    carView.selectedInsurerId = data.insurerId;
    
    NSLog(@"photo : %@",data.regTicketPhoto);
    
    NSString *insurerName = @"";
    NSMutableArray *insurerArray = [GlobalScopeObjects sharedSingletonInstance].insurerArray;
    for (int count = 0; count < [insurerArray count]; count++) {
        
        Insurer *insurer = [insurerArray objectAtIndex:count];
        if([insurer.insurerId isEqualToString:data.insurerId]){
            insurerName = insurer.insurerName;
            break;
        }
    }
    carView.insuranceCompanyField.text = insurerName;
    
    NSString *carBrandName = @"";
    NSMutableArray *carBrandsArray = [GlobalScopeObjects sharedSingletonInstance].carBrandArray;
    for (int count = 0; count < [carBrandsArray count]; count++) {
        
        CarBrand *carBrand = [carBrandsArray objectAtIndex:count];
        if([carBrand.carBrandId isEqualToString:data.brand]){
            carBrandName = carBrand.carBrandName;
            break;
        }
    }
    carView.carBrandField.text = carBrandName;
    
    if([data.companyCar intValue] == 1){
        [carView.companyCarCheckBox setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
        carView.companyNameField.text = data.companyName;
        carView.isCompanyCar = YES;
    }
    
    if([data.leasingCar intValue] == 1){
        [carView.leaseCarCheckBox setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
        carView.leasingCompanyField.text = data.leasingCompany;
        carView.isLeaseCar = YES;
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateReg = [formatter dateFromString:data.firstReg];
    NSDate *dateIssue = [formatter dateFromString:data.issueDate];
    [formatter setDateFormat:@"dd.MM.yyy"];
    
    carView.firstRegistrationDateField.text = [formatter stringFromDate:dateReg];
    carView.issueDateField.text = [formatter stringFromDate:dateIssue];
    carView.nxtGeneralInspctnField.text = data.nxtGeneralInspctn;
    carView.vinField.text = data.vin;
    carView.typeField.text = data.type;
    carView.tradeNameField.text = data.tradeName;
    carView.brandField.text = data.brand;
    carView.fuelField.text = data.fuelType;
    carView.dsplcmntField.text = data.displacement;
    carView.rtdPwrField.text = data.powerKw;
    carView.fTireDimnsnField.text = data.tireDimFront;
    carView.rTireDimnsnField.text = data.tireDimBack;
    carView.colorField.text = data.color;
    carView.specialStrctrField.text = data.extras;
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (!pageControlBeingUsed) {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = self.scrollView.frame.size.width;
        int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.pageControl.currentPage = page;
        
        if([self.carNameScrollview.subviews count] > 0){
            for(UIView *vw in self.carNameScrollview.subviews) {
                if ([vw isKindOfClass:[UILabel class]]) {
                    ((UILabel*)vw).textColor = [UIColor lightGrayColor];
                }
            }
            
            UILabel *currentActiveTitle;
            if(self.pageControl.currentPage == 0){
                currentActiveTitle = ((UILabel*)[self.carNameScrollview.subviews objectAtIndex:self.pageControl.currentPage]);
            }
            else{
                currentActiveTitle = ((UILabel*)[self.carNameScrollview.subviews objectAtIndex:self.pageControl.currentPage+self.pageControl.currentPage]);
            }
            currentActiveTitle.textColor = [UIColor whiteColor];
            CGRect frame;
            frame.origin.x = currentActiveTitle.frame.origin.x;
            frame.origin.y = 0;
            frame.size = self.originalTitleScrollFrame.size;
            [self moveTitleScroll:frame];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (IBAction)changePage {
    // Update the scroll view to the appropriate page
    CGRect frame;
    frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.scrollView.frame.size;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    
    // Keep track of when scrolls happen in response to the page control
    // value changing. If we don't do this, a noticeable "flashing" occurs
    // as the the scroll delegate will temporarily switch back the page
    // number.
    pageControlBeingUsed = YES;
}

-(void)moveTitleScroll:(CGRect)frame
{
    //    double xConstant = CGRectGetMinX(frame);
    //    if(xConstant >= self.view.frame.size.width - 100){
    [UIView animateWithDuration:0.1 animations:^{
        CGRect newFrame = frame;
        newFrame.origin.x = self.originalTitleScrollFrame.origin.x - frame.origin.x + 12;
        self.carNameScrollview.frame = newFrame;
    }];
    //    }
}

-(void)setupTitleScroll
{
    if([[NSUserDefaults standardUserDefaults] valueForKey:kNumberOfCarsAdded]){
        int count = [[[NSUserDefaults standardUserDefaults] valueForKey:kNumberOfCarsAdded] intValue];
        int lblWidth = 100;
        for (int i=0; i < count; i++) {
            UILabel *carNameLbl;
            UIButton *touchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            touchBtn.tag = i;
            if(i==0){
                carNameLbl = [[UILabel alloc] initWithFrame:CGRectMake(8, 9, lblWidth, 21)];
                carNameLbl.text = [NSString stringWithFormat:@"Fahrzeug %d",count];
            }
            else if (i > 0){
                int extraSpace = 10 * i + 8;
                int xCoordinate = (lblWidth * i) + extraSpace;
                carNameLbl = [[UILabel alloc] initWithFrame:CGRectMake(xCoordinate, 9, lblWidth, 21)];
            }
            carNameLbl.text = [NSString stringWithFormat:@"Fahrzeug %d",i+1];
            [carNameLbl setFont:[UIFont boldUnfallheldenFontWithSize:15]];
            
            touchBtn.frame = carNameLbl.frame;
            [touchBtn addTarget:self action:@selector(sliderTitleBtnpress:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.carNameScrollview addSubview:carNameLbl];
            [self.carNameScrollview addSubview:touchBtn];
        }
        CGRect frame = ((UILabel*)[self.carNameScrollview.subviews lastObject]).frame;
        double contentWidth = CGRectGetMaxX(frame)+100;
        NSLog(@"content width : %f",contentWidth);
        self.carNameScrollview.frame = CGRectMake(self.carNameScrollview.frame.origin.x, self.carNameScrollview.frame.origin.y, contentWidth, self.titlesContainer.frame.size.height);
    }
    for(UIView *vw in self.carNameScrollview.subviews) {
        if ([vw isKindOfClass:[UILabel class]]) {
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:((UILabel*)vw).text];
            [attributeString addAttribute:NSUnderlineStyleAttributeName
                                    value:[NSNumber numberWithInt:1]
                                    range:(NSRange){0,[attributeString length]}];
            ((UILabel*)vw).attributedText = attributeString;
            ((UILabel*)vw).textColor = [UIColor lightGrayColor];
        }
    }
    ((UILabel*)[self.carNameScrollview.subviews objectAtIndex:self.pageControl.currentPage+2]).textColor = [UIColor whiteColor];
}

-(IBAction)sliderTitleBtnpress:(id)sender{
    
    CGRect frame;
    frame.origin.x = self.scrollView.frame.size.width * ((UIButton*)sender).tag;
    frame.origin.y = 0;
    frame.size = self.scrollView.frame.size;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    
    if([self.carNameScrollview.subviews count] > 0){
        for(UIView *vw in self.carNameScrollview.subviews) {
            if ([vw isKindOfClass:[UILabel class]]) {
                ((UILabel*)vw).textColor = [UIColor lightGrayColor];
            }
        }
        self.pageControl.currentPage  = ((UIButton*)sender).tag;
        UILabel *currentActiveTitle;
        if(self.pageControl.currentPage == 0){
            currentActiveTitle = ((UILabel*)[self.carNameScrollview.subviews objectAtIndex:self.pageControl.currentPage]);
        }
        else{
            currentActiveTitle = ((UILabel*)[self.carNameScrollview.subviews objectAtIndex:self.pageControl.currentPage+self.pageControl.currentPage]);
        }
        currentActiveTitle.textColor = [UIColor whiteColor];
        CGRect frame;
        frame.origin.x = currentActiveTitle.frame.origin.x;
        frame.origin.y = 0;
        frame.size = self.originalTitleScrollFrame.size;
        [self moveTitleScroll:frame];
    }
    // Keep track of when scrolls happen in response to the page control
    // value changing. If we don't do this, a noticeable "flashing" occurs
    // as the the scroll delegate will temporarily switch back the page
    // number.
    pageControlBeingUsed = YES;
}

-(void)addCar:(id)sender
{
    AddCarVC *addCar = [[AddCarVC alloc] init];
    [self.navigationController pushViewController:addCar animated:YES];
    /*
     AddCarInfo *addCar = [[AddCarInfo alloc] init];
     [self.navigationController pushViewController:addCar animated:YES];
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)helpBtnPress:(id)sender
{
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
