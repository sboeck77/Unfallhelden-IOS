//
//  CarView.m
//  Unfallhelden
//
//  Created by Adil Anwer on 3/8/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "CarView.h"
#import "EnterDataManually.h"
#import "AppDelegate.h"
#import "CarBrand.h"
#import "Insurer.h"
#import "ELCImagePickerVC.h"
#import "UnfallheldenDatePickerView.h"
#import "CarInfoVC.h"

#define iPhone4PickerMoveConstant 60

@implementation CarView
@synthesize carScrollView,companyCar,leasingCar,enterDMView,typeOfPicker;
@synthesize insuranceCompanyField,heightOfViewForPicker, carBrandArray, insuranceCompanyArray;
@synthesize currentCarTag, currentCarId, leasingCompanyField, companyNameField, carBrandField, carTypeField, numPlateField, carPhotoUploadBtn;
@synthesize unfallheldenPickerView, selectedCarBrandId, selectedInsurerId, isLeaseCar, isCompanyCar, carData, registrationDocumentPhoto;
@synthesize enterDataManuallyView,enterDMHeightContraint, firstRegistrationDateField, issueDateField, nxtGeneralInspctnField, vinField, typeField;
@synthesize tradeNameField, brandField, fuelField, dsplcmntField, rtdPwrField, fTireDimnsnField, rTireDimnsnField, colorField, specialStrctrField;
@synthesize firstRegDateToUpload, issueDateToUplaod, carId;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CarView" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

-(void)setupView
{
    [self layoutIfNeeded];
    [self layoutSubviews];
    for(UIView *vw in self.carScrollView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            //            UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
            //            leftView.backgroundColor = [UIColor clearColor];
            //            textField.leftView = leftView;
            //            textField.leftViewMode = UITextFieldViewModeAlways;
            //            textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
        }
        
        if([vw isKindOfClass:[UILabel class]]){
            UILabel *lbl = (UILabel*)vw;
            [lbl setFont:[UIFont boldUnfallheldenFontWithSize:17]];
        }
        
        if([vw isKindOfClass:[UIButton class]]){
            UIButton *btn = (UIButton*)vw;
            [btn.titleLabel setFont:[UIFont regularUnfallheldenFontWithSize:19]];
        }
    }
    
    for(UIView *vw in self.enterDataManuallyView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            //            UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
            //            leftView.backgroundColor = [UIColor clearColor];
            //            textField.leftView = leftView;
            //            textField.leftViewMode = UITextFieldViewModeAlways;
            //            textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
        }
        
        if([vw isKindOfClass:[UILabel class]]){
            UILabel *lbl = (UILabel*)vw;
            [lbl setFont:[UIFont boldUnfallheldenFontWithSize:17]];
        }
        
        if([vw isKindOfClass:[UIButton class]]){
            UIButton *btn = (UIButton*)vw;
            [btn.titleLabel setFont:[UIFont regularUnfallheldenFontWithSize:19]];
        }
    }
    
    CGFloat screenWidth = 0.0f;
    CGFloat heightOfPicker = 0.0f;
    if(IS_IPHONE_4){
        screenWidth = 320;
        heightOfPicker = 206;
        [self.companyCar setFont:[UIFont regularUnfallheldenFontWithSize:9]];
        [self.leasingCar setFont:[UIFont regularUnfallheldenFontWithSize:9]];
    }
    else if (IS_IPHONE_5){
        screenWidth = 320;
        heightOfPicker = 206;
        [self.companyCar setFont:[UIFont regularUnfallheldenFontWithSize:9]];
        [self.leasingCar setFont:[UIFont regularUnfallheldenFontWithSize:9]];
    }
    else if(IS_IPHONE_6){
        screenWidth = 375;
        heightOfPicker = 260;
        [self.companyCar setFont:[UIFont regularUnfallheldenFontWithSize:9]];
        [self.leasingCar setFont:[UIFont regularUnfallheldenFontWithSize:9]];
    }
    else if (IS_IPHONE_6_Plus){
        screenWidth = 414;
        heightOfPicker = 260;
        [self.companyCar setFont:[UIFont regularUnfallheldenFontWithSize:9]];
        [self.leasingCar setFont:[UIFont regularUnfallheldenFontWithSize:9]];
    }
    
    self.insuranceCompanyArray = [GlobalScopeObjects sharedSingletonInstance].insurerArray;
    self.carBrandArray = [GlobalScopeObjects sharedSingletonInstance].carBrandArray;
    
    self.isCompanyCar = NO;
    self.isLeaseCar = NO;
    
    //    self.registrationDocumentPhoto  = @"";
    
    CGRect frame = self.frame;
    frame.size.width = screenWidth;
    self.frame = frame;
    self.carScrollView.contentSize = CGSizeMake(screenWidth, self.carScrollView.contentSize.height);
    self.heightOfViewForPicker.constant = heightOfPicker;
    [self setNeedsUpdateConstraints];
    
    self.unfallheldenPickerView = [[UnfallheldenPickerView alloc] init];
    CGRect pickerFrame = self.unfallheldenPickerView.frame;
    pickerFrame.size.height = heightOfPicker;
    self.unfallheldenPickerView.frame = pickerFrame;
    [self.unfallheldenPickerView.pickerDoneButton setAction:@selector(pickerDoneBtnPress:)];
    
    self.unfallheldenPickerView.pickerView.dataSource = self;
    self.unfallheldenPickerView.pickerView.delegate = self;
    
    //    [self.countryTxtField setTintColor:[UIColor clearColor]];
    //    self.countryTxtField.inputView = self.unfallheldenPickerView;
    
    [self.carBrandField setTintColor:[UIColor clearColor]];
    self.carBrandField.inputView = self.unfallheldenPickerView;
    
    [self.insuranceCompanyField setTintColor:[UIColor clearColor]];
    self.insuranceCompanyField.inputView = self.unfallheldenPickerView;
    
    /*
     NSString *currentNotificationName = [NSString stringWithFormat:@"%@-%d",kNotificationNameUpdatedDocumentImageSelected,self.currentCarTag];
     
     [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(updatedDocumentImageSelected)
     name:currentNotificationName
     object:nil];
     */
    
    if([self.registrationDocumentPhoto length] > 0){
        [self.carPhotoUploadBtn setBackgroundImage:[UIImage imageNamed:@"takePhotoBtnGreen"] forState:UIControlStateNormal];
    }
    else{
        self.registrationDocumentPhoto  = @"";
    }
    
    self.selectedInsurerId = @"";
    self.selectedCarBrandId = @"";
    self.issueDateToUplaod = @"";
    self.firstRegDateToUpload = @"";
    
    CGFloat heightOfPicker2 = 0.0f;
    if(IS_IPHONE_5){
        heightOfPicker2 = 206;
    }
    else if (IS_IPHONE_4){
        heightOfPicker2 = 206;
    }
    if (IS_IPHONE_6 || IS_IPHONE_6_Plus){
        heightOfPicker2 = 260;
    }
    
    UnfallheldenDatePickerView *dateTimePicker = [[UnfallheldenDatePickerView alloc] init];
    CGRect pickerFrame2 = dateTimePicker.frame;
    pickerFrame2.size.height = heightOfPicker2;
    dateTimePicker.frame = pickerFrame2;
    [dateTimePicker.pickerDoneButton setAction:@selector(pickerDoneBtnPress:)];
    [dateTimePicker.datePicker addTarget:self action:@selector(pickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.firstRegistrationDateField setTintColor:[UIColor clearColor]];
    [self.issueDateField setTintColor:[UIColor clearColor]];
    self.firstRegistrationDateField.inputView = dateTimePicker;
    self.issueDateField.inputView = dateTimePicker;
    [dateTimePicker.datePicker setDatePickerMode:UIDatePickerModeDate];
}

-(void)pickerValueChanged:(UIDatePicker *)timePicker
{
    if(self.typeOfPicker == FirstRegDatePciker3){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd.MM.yyy"];
        NSString *date = [formatter stringFromDate:timePicker.date];
        NSLog(@"Date : %@", date);
        self.firstRegistrationDateField.text = date;
        NSDateFormatter *formatterSecond = [[NSDateFormatter alloc] init];
        [formatterSecond setDateFormat:@"yyyy-MM-dd"];
        self.firstRegDateToUpload = [formatterSecond stringFromDate:timePicker.date];
        
    }
    else if(self.typeOfPicker == IssueDatePciker3){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd.MM.yyy"];
        NSString *date = [formatter stringFromDate:timePicker.date];
        NSLog(@"Date : %@", date);
        self.issueDateField.text = date;
        NSDateFormatter *formatterSecond = [[NSDateFormatter alloc] init];
        [formatterSecond setDateFormat:@"yyyy-MM-dd"];
        self.issueDateToUplaod = [formatterSecond stringFromDate:timePicker.date];
    }
}

-(void)showFirstRegDatePicker:(id)sender
{
    for(UIView *vw in self.carScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    for(UIView *vw in self.enterDataManuallyView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    self.typeOfPicker = FirstRegDatePciker3;
    [self.firstRegistrationDateField becomeFirstResponder];
}

-(void)showIssueDatePicker:(id)sender
{
    for(UIView *vw in self.carScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    for(UIView *vw in self.enterDataManuallyView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    self.typeOfPicker = IssueDatePciker3;
    [self.issueDateField becomeFirstResponder];
}

#pragma mark - PickerView Done Button Press
-(IBAction)pickerDoneBtnPress:(id)sender
{
    [self.insuranceCompanyField resignFirstResponder];
    [self.carBrandField resignFirstResponder];
    [self.firstRegistrationDateField resignFirstResponder];
    [self.issueDateField resignFirstResponder];
    //    [self.carTypeField resignFirstResponder];
}

#pragma mark - UITextField Delegate Methods

#pragma mark - UITextFieldDelegateMethods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if ([textField isEqual:self.carBrandField]){
        self.typeOfPicker = CarBrandPicker3;
    }
    else if ([textField isEqual:self.insuranceCompanyField]){
        self.typeOfPicker = InsuranceCompanyPicker3;
    }
    else if ([textField isEqual:self.firstRegistrationDateField]){
        self.typeOfPicker = FirstRegDatePciker3;
    }
    else if ([textField isEqual:self.issueDateField]){
        self.typeOfPicker = IssueDatePciker3;
    }
    
    [self.unfallheldenPickerView.pickerView reloadAllComponents];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"current car tag : %d",self.currentCarTag);
    [textField resignFirstResponder];
    return TRUE;
}

-(void)saveChangesBtnPress:(id)sender
{
    self.carData = [[GlobalScopeObjects sharedSingletonInstance].userCarsArray objectAtIndex:self.currentCarTag];
    
    NSString *insurerName;
    NSMutableArray *insurerArray = [GlobalScopeObjects sharedSingletonInstance].insurerArray;
    for (int count = 0; count < [insurerArray count]; count++) {
        
        Insurer *insurer = [insurerArray objectAtIndex:count];
        if([insurer.insurerId isEqualToString:self.carData.insurerId]){
            insurerName = insurer.insurerName;
            break;
        }
    }
    
    NSString *compantCar = @"0", *leaseCar = @"0";
    NSString *companyName = @"", *leasingCompanyName = @"";
    
    
    if(self.isCompanyCar == TRUE){
        companyName = self.companyNameField.text;
        compantCar = @"1";
    }
    
    if(self.isLeaseCar == TRUE){
        leasingCompanyName = self.leasingCompanyField.text;
        leaseCar = @"1";
    }
    
    NSLog(@"carType : %@ and tradeName : %@",self.carTypeField.text, self.carData.tradeName);
    if(self.carData.fuelType == nil || [self.carData.fuelType length] == 0){
        self.carData.fuelType = @"";
    }
    
    [[UnfallheldenHttpClient sharedClient] updateUserCarWithInsurer:self.insuranceCompanyField.text carId:self.carData.carId insurerId:self.selectedInsurerId regNum:self.numPlateField.text regDate:self.carData.regDate issueDate:self.issueDateToUplaod firstReg:self.firstRegDateToUpload vin:self.vinField.text type:self.carTypeField.text tradeName:self.tradeNameField.text brand:self.selectedCarBrandId fuel:self.fuelField.text displacement:self.dsplcmntField.text ratedPower:self.rtdPwrField.text fTireDimen:self.fTireDimnsnField.text bTireDimen:self.rTireDimnsnField.text color:self.colorField.text leasingCar:leaseCar leasingCompany:leasingCompanyName companyCar:compantCar companyName:companyName extras:self.specialStrctrField.text regTicketPhoto:self.registrationDocumentPhoto  WithCompletionBlock:^(NSData *data) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *dataDictionary = [responseString JSONValue];
        NSLog(@"data dictionary : %@",dataDictionary);
        int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
        if(status == 1){
            NSDictionary *car = [dataDictionary objectForKey:@"data"];
            NSMutableArray *arrayOfCars = [[NSMutableArray alloc] initWithObjects:car, nil];
            for (int count = 0; count < [arrayOfCars count]; count++) {
                UserCarData *car = [UserCarData saveUserCarWithCarId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"id"] description] userId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"user_id"] description] insurerId:[[[arrayOfCars objectAtIndex:count] valueForKey:@"insurer"] description] leasingCar:[[[arrayOfCars objectAtIndex:count] valueForKey:@"leasing_car"] description] leasingCompany:[[[arrayOfCars objectAtIndex:count] valueForKey:@"leasing_company"] description] companyCar:[[[arrayOfCars objectAtIndex:count] valueForKey:@"company_car"] description] companyName:[[[arrayOfCars objectAtIndex:count] valueForKey:@"company_name"] description] regTicketPhoto:[[[arrayOfCars objectAtIndex:count] valueForKey:@"reg_ticket_photo"] description] issueDate:[[[arrayOfCars objectAtIndex:count] valueForKey:@"issue_date"] description] vin:[[[arrayOfCars objectAtIndex:count] valueForKey:@"vin"] description]  type:[[[arrayOfCars objectAtIndex:count] valueForKey:@"type"] description] tradeName:[[[arrayOfCars objectAtIndex:count] valueForKey:@"trade_name"] description]  body:[[[arrayOfCars objectAtIndex:count] valueForKey:@"body"] description] displacement:[[[arrayOfCars objectAtIndex:count] valueForKey:@"displacement"] description] fuelType:[[[arrayOfCars objectAtIndex:count] valueForKey:@"fuel_type"] description] color:[[arrayOfCars objectAtIndex:count] valueForKey:@"color"] extras:[[arrayOfCars objectAtIndex:count] valueForKey:@"extras"] powerKw:[[[arrayOfCars objectAtIndex:count] valueForKey:@"power_kw"] description]  carDeleted:[[[arrayOfCars objectAtIndex:count] valueForKey:@"deleted"] description] dateTime:[[[arrayOfCars objectAtIndex:count] valueForKey:@"datetime"] description] registrationNum:[[[arrayOfCars objectAtIndex:count] valueForKey:@"registration_number"] description] brand:[[[arrayOfCars objectAtIndex:count] valueForKey:@"brand"] description] firstReg:[[[arrayOfCars objectAtIndex:count] valueForKey:@"first_registration"] description]  regDate:[[[arrayOfCars objectAtIndex:count] valueForKey:@"registration_date"] description] tireDimFront:[[[arrayOfCars objectAtIndex:count] valueForKey:@"tire_dimension_front"] description] tireDimBack:[[[arrayOfCars objectAtIndex:count] valueForKey:@"tire_dimension_back"] description]];
                int index;
                for (int carsCount = 0; carsCount < [GlobalScopeObjects sharedSingletonInstance].userCarsArray.count; carsCount++) {
                    ReportAccidentData *data = [[GlobalScopeObjects sharedSingletonInstance].userCarsArray objectAtIndex:carsCount];
                    if(data.carId == self.carData.carId){
                        index = carsCount;
                        break;
                    }
                }
                [[GlobalScopeObjects sharedSingletonInstance].userCarsArray replaceObjectAtIndex:index withObject:car];
            }
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Abruf der Fahrzeugdaten fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Abruf ihrer Fahrzeugdaten. Bitte versuchen Sie es erneut"];
        }
    }];
}

-(void)deleteCar:(id)sender
{
    self.carData = [[GlobalScopeObjects sharedSingletonInstance].userCarsArray objectAtIndex:self.currentCarTag];
    [[UnfallheldenHttpClient sharedClient] deleteCarWithCarId:self.carData.carId andCompletionBlock:^(NSData *data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSDictionary *dataDictionary = [responseString JSONValue];
            int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
            if(status == 1){
                UINavigationController *navigationController = [GlobalScopeObjects sharedSingletonInstance].carInfoVC.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:[[CarInfoVC alloc] init]];
                navigationController.viewControllers = controllers;
                [[GlobalScopeObjects sharedSingletonInstance].carInfoVC.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            }
            [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
        });
    }];
}

#pragma mark - Other Functions

-(void)enterDataManually:(id)sender
{
    /*
     if(self.enterDMView == nil){
     self.enterDMView = [[EnterDataManually alloc] init];
     self.enterDMView.frame = self.frame;
     [self addSubview:self.enterDMView];
     }*/
    EnterDataManually *manuallyEnterData = [[EnterDataManually alloc] init];
    if([[GlobalScopeObjects sharedSingletonInstance].userCarsArray count] > 0){
        manuallyEnterData.carData = [[GlobalScopeObjects sharedSingletonInstance].userCarsArray objectAtIndex:self.currentCarTag];
    }
    [[GlobalScopeObjects sharedSingletonInstance].carInfoVC.navigationController presentViewController:[[UINavigationController alloc] initWithRootViewController:manuallyEnterData] animated:YES completion:nil];
}

-(void)openPhotoOfTheCar:(id)sender
{
    ELCImagePickerVC *imagePicker = [[ELCImagePickerVC alloc] init];
    imagePicker.imagePickerType = ELCImagePickerTypeSingleImage;
    imagePicker.imgPickerCallFrom = @"CarView";
    imagePicker.currentCarTag = self.currentCarTag;
    imagePicker.carImageSelectedDelegate = self;
    if([[GlobalScopeObjects sharedSingletonInstance].userCarsArray count] > 0){
        //        UserCarData *data = [[GlobalScopeObjects sharedSingletonInstance].userCarsArray objectAtIndex:self.currentCarTag];
        imagePicker.documentPhotoName = self.registrationDocumentPhoto;
    }
    [[GlobalScopeObjects sharedSingletonInstance].carInfoVC.navigationController presentViewController:[[UINavigationController alloc] initWithRootViewController:imagePicker] animated:YES completion:nil];
}

-(void)showBrandsPicker:(id)sender
{
    for(UIView *vw in self.carScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    for(UIView *vw in self.enterDataManuallyView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    self.typeOfPicker = CarBrandPicker3;
    [self.unfallheldenPickerView.pickerView reloadAllComponents];
    [self.carBrandField becomeFirstResponder];
}

-(void)showInsuranceComapniesPicker:(id)sender
{
    for(UIView *vw in self.carScrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    for(UIView *vw in self.enterDataManuallyView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    self.typeOfPicker = InsuranceCompanyPicker3;
    [self.unfallheldenPickerView.pickerView reloadAllComponents];
    [self.insuranceCompanyField becomeFirstResponder];
}

#pragma mark - UIPickerView DataSource

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    //    if(self.typeOfPicker == CountryPicker){
    //        return [self.countryArray count];
    //    }
    //    else
    if (self.typeOfPicker == CarBrandPicker3){
        return [self.carBrandArray count];
    }
    //    else if (self.typeOfPicker == CarTypePicker){
    //        return [self.carTypeArray count];
    //    }
    else {
        return [self.insuranceCompanyArray count];
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    //    if(self.typeOfPicker == CountryPicker){
    //        return [self.countryArray objectAtIndex:row];
    //    }
    //    else
    if (self.typeOfPicker == CarBrandPicker3){
        CarBrand *carBrand =  [self.carBrandArray objectAtIndex:row];
        return carBrand.carBrandName;
    }
    //    else if (self.typeOfPicker == CarTypePicker){
    //        return [self.carTypeArray objectAtIndex:row];
    //    }
    else {
        Insurer *insurerCompany = [self.insuranceCompanyArray objectAtIndex:row];
        return insurerCompany.insurerName;
    }
}

#pragma mark - UIPickerView Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    //    if(self.typeOfPicker == CountryPicker){
    //        self.countryTxtField.text = [self.countryArray objectAtIndex:row];
    //    }
    //    else
    if (self.typeOfPicker == CarBrandPicker3){
        CarBrand *carBrand =  [self.carBrandArray objectAtIndex:row];
        self.carBrandField.text = carBrand.carBrandName;
        self.selectedCarBrandId = carBrand.carBrandId;
    }
    //    else if (self.typeOfPicker == CarTypePicker){
    //        self.carTypeField.text = [self.carTypeArray objectAtIndex:row];
    //    }
    else {
        Insurer *insurerCompany = [self.insuranceCompanyArray objectAtIndex:row];
        self.insuranceCompanyField.text = insurerCompany.insurerName;
        self.selectedInsurerId = insurerCompany.insurerId;
    }
}

-(void)companyCheckBtnPress:(id)sender
{
    self.isCompanyCar = !self.isCompanyCar;
    if(self.isCompanyCar) {
        [self.companyCarCheckBox setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
    else {
        [self.companyCarCheckBox setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
    }
}

-(void)leaseCheckBtnPress:(id)sender
{
    self.isLeaseCar = !self.isLeaseCar;
    if(self.isLeaseCar) {
        [self.leaseCarCheckBox setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
    else {
        [self.leaseCarCheckBox setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
    }
}

-(void)updatedDocumentImageSelected
{
    self.carData = [[GlobalScopeObjects sharedSingletonInstance].userCarsArray objectAtIndex:self.currentCarTag];
    
    [[UnfallheldenHttpClient sharedClient] uploadCarDocumentPhotoWithImage:[GlobalScopeObjects sharedSingletonInstance].documentImage andCarId:self.carData.carId WithCompletionBlock:^(NSData *data) {
        
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *dataDictionary = [responseString JSONValue];
        //            NSDictionary *userData = [[dataDictionary valueForKey:@"data"] description];
        NSLog(@"data dictionary : %@",dataDictionary);
        int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
        if(status == 1){
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Car Photo wurde erfolgreich hochgeladen" andMessage:@"Ihre Photos wurden erfolgreich in unserer Datenbank erfasst."];
            self.registrationDocumentPhoto = [[dataDictionary valueForKey:@"data"] description];
            [self.carPhotoUploadBtn setBackgroundImage:[UIImage imageNamed:@"takePhotoBtnGreen"] forState:UIControlStateNormal];
            [self.carPhotoUploadBtn setTitle:@"" forState:UIControlStateNormal];
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Bildupload fehlgeschlagen" andMessage:@"Es gibt ein Problem beim Hochladen des Photos. Bitte versuchen Sie es erneut"];
        }
    }];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
