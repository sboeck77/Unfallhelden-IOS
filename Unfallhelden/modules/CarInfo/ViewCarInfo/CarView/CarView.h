//
//  CarView.h
//  Unfallhelden
//
//  Created by Adil Anwer on 3/8/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "UserCarData.h"
#import "ELCImagePickerVC.h"

typedef enum {
    CarBrandPicker3 = 0,
    InsuranceCompanyPicker3 = 1,
    CountryPicker3 = 2,
    CarTypePicker3 = 3,
    FirstRegDatePciker3 = 4,
    IssueDatePciker3 = 5
} MoreDataPickerType3;

@interface CarView : UIView<UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, CarImageSelectedDelegate>

@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *carScrollView;
@property (nonatomic, weak) IBOutlet UILabel *companyCar;
@property (nonatomic, weak) IBOutlet UILabel *leasingCar;
@property (nonatomic, weak) IBOutlet UITextField *insuranceCompanyField;
@property (nonatomic, weak) IBOutlet UITextField *numPlateField;
@property (nonatomic, weak) IBOutlet UITextField *carBrandField;
@property (nonatomic, weak) IBOutlet UITextField *carTypeField;
@property (nonatomic, weak) IBOutlet UITextField *leasingCompanyField;
@property (nonatomic, weak) IBOutlet UITextField *companyNameField;

@property (nonatomic, weak) IBOutlet UITextField *firstRegistrationDateField;
@property (nonatomic, weak) IBOutlet UITextField *issueDateField;
@property (nonatomic, weak) IBOutlet UITextField *nxtGeneralInspctnField;
@property (nonatomic, weak) IBOutlet UITextField *vinField;
@property (nonatomic, weak) IBOutlet UITextField *typeField;
@property (nonatomic, weak) IBOutlet UITextField *tradeNameField;
@property (nonatomic, weak) IBOutlet UITextField *brandField;
@property (nonatomic, weak) IBOutlet UITextField *fuelField;
@property (nonatomic, weak) IBOutlet UITextField *dsplcmntField;
@property (nonatomic, weak) IBOutlet UITextField *rtdPwrField;
@property (nonatomic, weak) IBOutlet UITextField *fTireDimnsnField;
@property (nonatomic, weak) IBOutlet UITextField *rTireDimnsnField;
@property (nonatomic, weak) IBOutlet UITextField *colorField;
@property (nonatomic, weak) IBOutlet UITextField *specialStrctrField;

@property (nonatomic, strong) UIView *enterDMView;
@property (nonatomic, weak) IBOutlet UIView *enterDataManuallyView;
@property (nonatomic, strong) UnfallheldenPickerView *unfallheldenPickerView;
@property (nonatomic) MoreDataPickerType3 typeOfPicker;
@property (nonatomic, strong) NSMutableArray *carBrandArray;
@property (nonatomic, strong) NSMutableArray *insuranceCompanyArray;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightOfViewForPicker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *enterDMHeightContraint;
@property (nonatomic, strong) NSString *selectedCarBrandId;
@property (nonatomic, strong) NSString *selectedInsurerId;
@property (nonatomic) int currentCarTag;
@property (nonatomic, strong) NSString *currentCarId;
@property (nonatomic, weak) IBOutlet UIButton *companyCarCheckBox;
@property (nonatomic, weak) IBOutlet UIButton *leaseCarCheckBox;
@property (nonatomic, weak) IBOutlet UIButton *carPhotoUploadBtn;
@property (nonatomic) BOOL isCompanyCar;
@property (nonatomic) BOOL isLeaseCar;
@property (nonatomic, strong) UserCarData *carData;
@property (nonatomic, strong) NSString *registrationDocumentPhoto;
@property (nonatomic, strong) NSString *carId;

@property (nonatomic, strong) NSString *firstRegDateToUpload;
@property (nonatomic, strong) NSString *issueDateToUplaod;


-(void)setupView;

-(IBAction)enterDataManually:(id)sender;
-(IBAction)showBrandsPicker:(id)sender;
-(IBAction)showInsuranceComapniesPicker:(id)sender;
-(IBAction)showFirstRegDatePicker:(id)sender;
-(IBAction)showIssueDatePicker:(id)sender;
-(IBAction)companyCheckBtnPress:(id)sender;
-(IBAction)leaseCheckBtnPress:(id)sender;
-(IBAction)saveChangesBtnPress:(id)sender;
-(IBAction)openPhotoOfTheCar:(id)sender;
-(IBAction)deleteCar:(id)sender;

@end
