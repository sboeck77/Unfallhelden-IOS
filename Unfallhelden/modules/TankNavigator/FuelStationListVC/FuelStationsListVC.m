//
//  FuelStationsListVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 1/27/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "FuelStationsListVC.h"
#import "FuelStationsListCell.h"

@interface FuelStationsListVC ()

@end

@implementation FuelStationsListVC
@synthesize fuelStationsTbl,calledFrom;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"Tank Navigator"];
    //    if([self.calledFrom isEqualToString:@"sideMenu"]){
    [super setupMenuBarButtonItems];
    //    }
    [self.fuelStationsTbl reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

#pragma mark - UITableViewDataSourceMethods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BOOL isSelected = [self.selectedIndexPaths containsObject:indexPath];
    NSString *cellIdentifier = @"FuelStationsListCell";
    FuelStationsListCell *stationsListCell = (FuelStationsListCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(isSelected == FALSE){
        if (stationsListCell == nil) {
            NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FuelStationsListCell" owner:self options:nil];
            stationsListCell = [topLevelObjects objectAtIndex:0];
        }
        
        [stationsListCell.expandCellBtn addTarget:self action:@selector(seeMoreButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [stationsListCell.expandCellBtn setTag:indexPath.row];
    }
    else if (isSelected == TRUE){
        if (stationsListCell == nil) {
            NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FuelStationsListCellLarge" owner:self options:nil];
            stationsListCell = [topLevelObjects objectAtIndex:0];
        }
        
        [stationsListCell.expandCellBtn addTarget:self action:@selector(seeMoreButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [stationsListCell.expandCellBtn setTag:indexPath.row];
    }
    return stationsListCell;
}

#pragma mark - UITableViewDelegateMethods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    BOOL isSelected = [self.selectedIndexPaths containsObject:indexPath];
    if(isSelected == TRUE){
        return 204;
    }
    else if (isSelected ==FALSE){
        return 95;
    }
    return 95;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self addOrRemoveSelectedIndexPath:indexPath];
    
}

- (void)seeMoreButtonPressed:(UIButton *)button
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    [self addOrRemoveSelectedIndexPath:indexPath];
}

- (void)addOrRemoveSelectedIndexPath:(NSIndexPath *)indexPath
{
    if (!self.selectedIndexPaths) {
        self.selectedIndexPaths = [NSMutableArray new];
    }
    
    BOOL containsIndexPath = [self.selectedIndexPaths containsObject:indexPath];
    
    if (containsIndexPath) {
        [self.selectedIndexPaths removeObject:indexPath];
    }else{
        [self.selectedIndexPaths addObject:indexPath];
    }
    
    [self.fuelStationsTbl reloadRowsAtIndexPaths:@[indexPath]
                                withRowAnimation:UITableViewRowAnimationFade];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
