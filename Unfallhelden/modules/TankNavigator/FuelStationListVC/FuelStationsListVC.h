//
//  FuelStationsListVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 1/27/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FuelStationsListVC : UnfallheldenParentVC<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *fuelStationsTbl;
@property (nonatomic, strong) NSString *calledFrom;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *selectedIndexPaths;

@end
