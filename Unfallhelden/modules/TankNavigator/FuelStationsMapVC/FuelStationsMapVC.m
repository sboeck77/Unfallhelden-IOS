//
//  GasStationsVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/7/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "FuelStationsMapVC.h"
#import "MapAnnotation.h"
#import "MFSideMenu.h"
#import "RAccidentCategoriesVC.h"

@interface FuelStationsMapVC ()

@end

@implementation FuelStationsMapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //25.132703, 55.380464
    //    UIButton *menuBtn = [[CustomizeNavbar sharedSingletonInstance] addLeftNavBarButtonForNavigationItem:self.navigationItem WithTitle:@"" andImage:[UIImage imageNamed:@"menuBtn"]];
    [self setTitle:@"Gas Stations"];
    [super setupMenuBarButtonItems];
    UIButton *addAccidentBtn = [[CustomizeNavbar sharedSingletonInstance] addRightNavBarButtonForNavigationItem:self.navigationItem WithTitle:@"" andImage:[UIImage imageNamed:@"reportAccidentBtn"]];
    [addAccidentBtn addTarget:self action:@selector(addAccidentBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self refreshMap];
}

-(void)refreshMap
{
    [self.map removeAnnotations:self.map.annotations];
    MapAnnotation *annotation = [[MapAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake(25.132703, 55.380464);
    annotation.title = @"First Gas Station";
    annotation.gsIdentifier = [NSNumber numberWithInt:123];
    [self.map addAnnotation:annotation];
    self.map.showsUserLocation = YES;
}

#pragma mark - MKMapView Delegate
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    static NSString *RestoAnnotationIdentifier = @"GasStationAnnotationIdentifier";
    MKAnnotationView *annView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:RestoAnnotationIdentifier];
    annView.canShowCallout = YES;
    if (annotation == self.map.userLocation) {
        annView.image = [UIImage imageNamed:@"markerUserLocation"];
        annView.calloutOffset = CGPointMake(0.0,0.0);
    } else {
        annView.image = [UIImage imageNamed:@"markerStation"];
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        MapAnnotation *restoAnnotation = (MapAnnotation*)annotation;
        rightButton.tag = [restoAnnotation.gsIdentifier integerValue];
        annView.rightCalloutAccessoryView = rightButton;
        annView.calloutOffset = CGPointMake(1.0,3.0);
    }
    return annView;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)annotationViews {
    
    for (MKAnnotationView *annView in annotationViews){
        
        CGRect endFrame = annView.frame;
        annView.frame = CGRectOffset(endFrame, 0, -500);
        [UIView animateWithDuration:0.5 animations:^{
            annView.frame = endFrame;
        }];
    }
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    //    Resto *resto = [Resto getRestoWithId:[NSNumber numberWithInteger:control.tag]];
    //    DetailViewController *vc = [[DetailViewController alloc] initWithResto:resto];
    //    [self.parentViewController.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Others

-(IBAction)addAccidentBtnPressed:(id)sender{
    
    RAccidentCategoriesVC *categoriesVC = [[RAccidentCategoriesVC alloc] init];
    [self.navigationController pushViewController:categoriesVC animated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
// now is there any thing i will need to get passport when it will be ready?
@end
