//
//  GasStationsVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/7/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface FuelStationsMapVC : UnfallheldenParentVC<MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *map;

-(void)refreshMap;

@end
