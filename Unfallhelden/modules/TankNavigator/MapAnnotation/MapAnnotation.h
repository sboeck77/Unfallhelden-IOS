//
//  MapAnnotation.h
//  Tablr
//
//  Created by Laurens Baes on 08/08/14.
//  Copyright (c) 2014 adilanwer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapAnnotation : NSObject <MKAnnotation>

@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSNumber *gsIdentifier;
@property (nonatomic, assign)CLLocationCoordinate2D coordinate;

@end
