//
//  ViewController.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/1/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "MenuVC.h"
#import "SettingsVC.h"
#import "FuelStationsMapVC.h"
#import "ReportAccidentVC.h"
#import "MyAccidentsVC.h"
#import "CarInfoVC.h"
#import "OwnerProfileVC.h"

@interface MenuVC ()

@end

@implementation MenuVC
@synthesize lbl,btnFuel,btnNotifications,calledFrom;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
//    self.btnFuel.titleLabel.font = [UIFont fontAwesomeFontOfSize:28];
//    [self.btnFuel setTitle:[NSString fontAwesomeIconStringForEnum:FACheck] forState:UIControlStateNormal];
//    self.navigationController.navigationBar.translucent = NO;
//    [super setupMenuBarButtonItems];
    if([self.calledFrom isEqualToString:@"login"]){
        self.navigationItem.backBarButtonItem = nil;
        UIBarButtonItem *sideMenuBtn = [UIBarButtonItem leftMenuBarButtonItemWithtarget:self andSelector:@selector(leftSideMenuBtnPress:)];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        negativeSpacer.width = -8;
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,sideMenuBtn, nil] animated:NO];
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:self];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    else{
        [super setupMenuBarButtonItems];
    }
}

-(IBAction)leftSideMenuBtnPress:(id)sender
{
    [super leftSideMenuButtonPressed:sender];
}

-(void)gotoSettings:(id)sender
{
    SettingsVC *optionsVC = [[SettingsVC alloc] init];
    [self.navigationController pushViewController:optionsVC animated:YES];
}

-(void)viewFuelStations:(id)sender
{
    FuelStationsMapVC *gasStations = [[FuelStationsMapVC alloc] init];
    [self.navigationController pushViewController:gasStations animated:YES];
}

-(void)reportAnAccident:(id)sender
{
    ReportAccidentVC *reportAccident = [[ReportAccidentVC alloc] init];
    [self.navigationController pushViewController:reportAccident animated:YES];
}

-(void)viewAccidents:(id)sender
{
    MyAccidentsVC *accidentsVC = [[MyAccidentsVC alloc] init];
    [self.navigationController pushViewController:accidentsVC animated:YES];
}

-(void)viewCarInfo:(id)sender
{
    CarInfoVC *carInfo = [[CarInfoVC alloc] init];
    [self.navigationController pushViewController:carInfo animated:YES];
}

-(void)viewOwnerProfile:(id)sender
{
    OwnerProfileVC *ownerProfile = [[OwnerProfileVC alloc] init];
    [self.navigationController pushViewController:ownerProfile animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
