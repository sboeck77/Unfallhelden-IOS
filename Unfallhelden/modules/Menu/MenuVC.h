//
//  ViewController.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/1/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

@class MenuVC;

@interface MenuVC : UnfallheldenParentVC

@property(nonatomic, strong) MenuVC *menuVC;
@property (nonatomic, weak) IBOutlet UIButton *btnFuel;
@property (nonatomic, weak) IBOutlet UILabel *lbl;
@property (nonatomic, weak) IBOutlet UIButton *btnNotifications;
@property (nonatomic, strong) NSString *calledFrom;

-(IBAction)gotoSettings:(id)sender;
-(IBAction)viewFuelStations:(id)sender;
-(IBAction)viewNotifications:(id)sender;
-(IBAction)reportAnAccident:(id)sender;
-(IBAction)viewAccidents:(id)sender;
-(IBAction)viewCarInfo:(id)sender;
-(IBAction)viewOwnerProfile:(id)sender;

@end