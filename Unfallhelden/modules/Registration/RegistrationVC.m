//
//  RegistrationVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/15/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "RegistrationVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "LoginVC.h"
#import "HomeScreenVC.h"
#import "CarBrand.h"

@interface RegistrationVC ()

@property (nonatomic) BOOL termsAndConditions;
@property (nonatomic, weak) IBOutlet UILabel *termsAndCndtnsLbl;
@property (nonatomic, weak) IBOutlet UILabel *dataPrivacyLbl;

@end

@implementation RegistrationVC
@synthesize scrollView,firstNameField,lastNameField,emailField,passwordField,cnfrmPasswordField,familyField,terms,phoneField,termsAndCndtnsLbl;
@synthesize checkBoxBtn,calledFrom,alreadyRegistredBtn,carBrandField, carBrandArray, registerBtn, verticalSpacingContinueToNxtBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"Registrierung"];
    
    //GoogleAnalytics
//    self.screenName = @"RegistrationScreen";
    
    self.termsAndConditions = NO;
    //    if([self.calledFrom isEqualToString:@"login"]){
    //
    //    }
    //    else{
    [super setupMenuBarButtonItems];
    self.carBrandArray = [[NSMutableArray alloc] init];
    //    }
    for(UIView *vw in self.scrollView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            textField.delegate = self;
            [textField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
        }
    }
    self.alreadyRegistredBtn.titleLabel.font = [UIFont regularUnfallheldenFontWithSize:19];
    [self.scrollView contentSizeToFit];
    [self.view layoutIfNeeded];
    [self.view layoutSubviews];
    if(IS_IPHONE_5){
        self.verticalSpacingContinueToNxtBtn.constant = 50;
    }
    else if (IS_IPHONE_4){
        self.verticalSpacingContinueToNxtBtn.constant = 30;
    }
    [self.view setNeedsUpdateConstraints];
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:self.termsAndCndtnsLbl.text];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    self.termsAndCndtnsLbl.attributedText = attributeString;
    self.termsAndCndtnsLbl.textColor = [UIColor whiteColor];
    [self.termsAndCndtnsLbl setFont:[UIFont regularUnfallheldenFontWithSize:11]];
    NSMutableAttributedString *attributeString2 = [[NSMutableAttributedString alloc] initWithString:self.dataPrivacyLbl.text];
    [attributeString2 addAttribute:NSUnderlineStyleAttributeName
                             value:[NSNumber numberWithInt:1]
                             range:(NSRange){0,[attributeString2 length]}];
    self.dataPrivacyLbl.attributedText = attributeString2;
    self.dataPrivacyLbl.textColor = [UIColor whiteColor];
    [self.dataPrivacyLbl setFont:[UIFont regularUnfallheldenFontWithSize:11]];
    
    /*
     [[UnfallheldenHttpClient sharedClient] getCarBrandsWithCompletionBlock:^(NSData *data) {
     NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
     NSDictionary *dataDictionary = [responseString JSONValue];
     //            NSDictionary *userData = [[dataDictionary valueForKey:@"data"] description];
     NSLog(@"data dictionary : %@",dataDictionary);
     int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
     if(status == 1){
     NSMutableArray *brandsArray = [dataDictionary objectForKey:@"data"];
     for (int count = 0; count < [brandsArray count]; count++) {
     [self.carBrandArray addObject:[[[brandsArray objectAtIndex:count] valueForKey:@"name"] description]];
     CarBrand *brand = [[CarBrand alloc] init];
     brand.carBrandId = [[[brandsArray objectAtIndex:count] valueForKey:@"id"] description];
     brand.carBrandName = [[[brandsArray objectAtIndex:count] valueForKey:@"name"] description];
     [self.carBrandArray addObject:brand];
     }
     }
     else{
     [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Fetching car brands failed." andMessage:@"There is some problem fetching car brands with Unfallhelden app please try again"];
     }
     }];
     
     //    self.carBrandArray = [[NSMutableArray alloc] initWithObjects:@"BMW",@"Mercedes-Benz",@"Honda",@"Mazda",@"Nissan",@"Audi",@"Porsche",@"Toyota", nil];
     */
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"RegistrationScreen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    if(IS_IPHONE_5){
        self.scrollView.frame = CGRectMake(self.view.frame.origin.x, self.scrollView.frame.origin.y, self.view.frame.size.width, self.scrollView.frame.size.height);
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.scrollView.contentSize.height + 80);
    }
    else if (IS_IPHONE_4){
        self.scrollView.frame = CGRectMake(self.view.frame.origin.x, self.scrollView.frame.origin.y, self.view.frame.size.width, self.scrollView.frame.size.height);
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.scrollView.contentSize.height + 80);
    }
    else{
        self.scrollView.frame = CGRectMake(self.view.frame.origin.x, self.scrollView.frame.origin.y, self.view.frame.size.width, self.scrollView.frame.size.height);
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.scrollView.contentSize.height);
    }
    [self.scrollView contentSizeToFit];
    CGFloat heightOfPicker = 0.0f;
    if(IS_IPHONE_5){
        heightOfPicker = 206;
    }
    else if (IS_IPHONE_4){
        heightOfPicker = 206;
    }
    else{
    }
    [self.scrollView contentSizeToFit];
    if (IS_IPHONE_6 || IS_IPHONE_6_Plus){
        heightOfPicker = 260;
    }
    
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];
    UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[self class] toolbarHeight])];
    [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
    self.phoneField.inputAccessoryView = toolbar;
    
    /*
     
     UnfallheldenPickerView *pickerView = [[UnfallheldenPickerView alloc] init];
     CGRect pickerFrame = pickerView.frame;
     pickerFrame.size.height = heightOfPicker;
     pickerView.frame = pickerFrame;
     [pickerView.pickerDoneButton setAction:@selector(pickerDoneBtnPress:)];
     
     pickerView.pickerView.dataSource = self;
     pickerView.pickerView.delegate = self;
     
     [self.carBrandField setTintColor:[UIColor clearColor]];
     self.carBrandField.inputView = pickerView;
     
     UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];
     UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
     UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[self class] toolbarHeight])];
     [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
     self.phoneField.inputAccessoryView = toolbar;
     */
}

- (void)doneButtonDidPressed:(id)sender {
    [self.phoneField resignFirstResponder];
}

+ (CGFloat)toolbarHeight {
    // This method will handle the case that the height of toolbar may change in future iOS.
    return 44.f;
}

-(IBAction)leftSideMenuBtnPress:(id)sender
{
    [super leftSideMenuButtonPressed:sender];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //    [self.scrollView adjustOffsetToIdealIfNeeded];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if([textField isEqual:self.emailField]){
        if([self validateEmail:self.emailField.text]) {
            textField.background = nil;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
        }
        else{
            if(self.emailField.background == nil){
                self.emailField.layer.borderColor=[[UIColor redColor]CGColor];
                self.emailField.layer.borderWidth= 1.3f;
                [self.emailField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                [self.emailField setTextColor:[UIColor whiteColor]];
                UIColor *color = [UIColor whiteColor];
                self.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ wird benötigt",self.emailField.placeholder] attributes:@{NSForegroundColorAttributeName: color}];
            }
        }
    }
    else if([textField.text length] > 0){
        textField.background = nil;
        textField.layer.borderColor=[[UIColor whiteColor]CGColor];
        textField.layer.borderWidth= 1.3f;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.emailField]){
        if([self validateEmail:self.emailField.text]) {
            textField.background = nil;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
        }
        else{
            if(self.emailField.background == nil){
                self.emailField.layer.borderColor=[[UIColor redColor]CGColor];
                self.emailField.layer.borderWidth= 1.3f;
                [self.emailField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                [self.emailField setTextColor:[UIColor whiteColor]];
                UIColor *color = [UIColor whiteColor];
                self.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ wird benötigt",self.emailField.placeholder] attributes:@{NSForegroundColorAttributeName: color}];
            }
        }
    }
    else if([textField.text length] > 0){
        textField.background = nil;
        textField.layer.borderColor=[[UIColor whiteColor]CGColor];
        textField.layer.borderWidth= 1.3f;
    }
    [textField resignFirstResponder];
    return TRUE;
}

#pragma mark - Open PickerView

-(void)openPickerForCarBrand:(id)sender
{
    for(UIView *vw in self.scrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    [self.carBrandField becomeFirstResponder];
}

#pragma mark - PickerView Done Button Press
-(IBAction)pickerDoneBtnPress:(id)sender
{
    [self.carBrandField resignFirstResponder];
}

#pragma mark - UIPickerView DataSource

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [self.carBrandArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    CarBrand *brand = [self.carBrandArray objectAtIndex:row];
    return brand.carBrandName;
}

#pragma mark - UIPickerView Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    CarBrand *brand = [self.carBrandArray objectAtIndex:row];
    self.carBrandField.text = brand.carBrandName;
}

-(void)checkBoxBtnPress:(id)sender
{
    self.termsAndConditions = !self.termsAndConditions;
    if(self.termsAndConditions) {
        [self.checkBoxBtn setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
        self.terms = @"true";
    }
    else {
        [self.checkBoxBtn setBackgroundImage:[UIImage imageNamed:@"uncheckedBox"] forState:UIControlStateNormal];
        self.terms = @"false";
    }
}

-(void)termsAndCndtnsBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = UnfallheldenTermsAndConditions;
    customPopup.titleLabel.text = @"AGB";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}

-(void)dataPrvcyBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = UnfallheldenPrivacyPolicy;
    customPopup.titleLabel.text = @"Datenschutz";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}

-(void)registerBtnPress:(id)sender
{
    for(UIView *vw in self.scrollView.subviews) {
        [((UITextField*)vw) resignFirstResponder];
    }
    if([self.emailField.text length]>0 && [self.firstNameField.text length]>0 && [self.lastNameField.text length]>0 && [self.passwordField.text length]>0 && [self.cnfrmPasswordField.text length]>0 && [self.phoneField.text length]>0){
        if([self.terms isEqualToString:@"true"]){
            if([self validateEmail:self.emailField.text]) {
                if([self.passwordField.text isEqualToString:self.cnfrmPasswordField.text]){
//                    NSString *name = [NSString stringWithFormat:@"%@ %@",self.firstNameField.text,self.lastNameField.text];
                    [[UnfallheldenHttpClient sharedClient] registerUserWithEmail:self.emailField.text andPassword:self.passwordField.text andName:self.firstNameField.text andFamily:self.lastNameField.text andGender:@"" andPhone:self.phoneField.text andTerms:@"" andFBUserId:@"" WithCompletionBlock:^(NSData *data){
                        
                        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                        NSDictionary *dataDictionary = [responseString JSONValue];
                        //                        NSDictionary *userData = [dataDictionary valueForKey:@"data"];
                        int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                        NSLog(@"repsonce from server : %@",dataDictionary);
                        if(status == 1){
                            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Erfolgreich registriert" andMessage:@"Sie haben sich erfolgreich für die Unfallhelden registriert"];
                            //                            NSDictionary *ownerProfileData = [dataDictionary objectForKey:@"data"];
                            //                            [OwnerProfile saveOwnerProfileWithName:[[ownerProfileData valueForKey:@"name"] description] andAnrede:[[ownerProfileData valueForKey:@"anrede"] description] andMobile:@"" andCompany:@"" andResidence:@"" andBirthdate:@"" andStreet:@"" andZip:@"" andUserId:[[ownerProfileData valueForKey:@"id"] description] andfbId:@"" andEmail:[[ownerProfileData valueForKey:@"email"] description] andInsurer:@"" andFamily:[[ownerProfileData valueForKey:@"family"] description] andPhone:@""];
                            [[PDKeychainBindings sharedKeychainBindings] setObject:[[[dataDictionary valueForKey:@"data"] valueForKey:@"token"] description] forKey:kKeychainUserAccessToken];
                            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:kUserDefaultsUserAlreadyLoggedIn];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            /*data =     {
                             anrede = "<null>";
                             birthdate = "<null>";
                             email = "a@d.com";
                             family = Musterman;
                             id = 77;
                             name = "addg ffg";
                             phone = 123;
                             token = 7ebf5fbeab83cb786640f79db72fb5babf9a2ca1013b787282014b744e80c20607a7b7eeaa5f5d3ddc163288861b765f62ef6f9f4f8f31793c65266dce28e637;
                             };
                             status = 1;
                             }*/
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNameUserLogin object:nil userInfo:nil];
                            HomeScreenVC *homeScreenVC = [[HomeScreenVC alloc] init];
                            homeScreenVC.calledFrom = @"login";
                            [self.navigationController pushViewController:homeScreenVC animated:YES];
                        }
                        else{
                            NSString* message = [[[dataDictionary objectForKey:@"error"] valueForKey:@"message"] description];
                            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Registrierung fehlgeschlagen" andMessage:message];
                        }
                    }];
                }
                else{
                    if(self.passwordField.background == nil){
                        self.passwordField.layer.borderColor=[[UIColor redColor]CGColor];
                        self.passwordField.layer.borderWidth= 1.3f;
                        [self.passwordField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                        [self.passwordField setTextColor:[UIColor whiteColor]];
                    }
                    if(self.cnfrmPasswordField.background == nil){
                        self.cnfrmPasswordField.layer.borderColor=[[UIColor redColor]CGColor];
                        self.cnfrmPasswordField.layer.borderWidth= 1.3f;
                        [self.cnfrmPasswordField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                        [self.cnfrmPasswordField setTextColor:[UIColor whiteColor]];
                    }
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Die Passwörter stimmen nicht überein"];
                }
            }
            else{
                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Diese E-Mail Adresse liegt uns nicht vor."];
            }
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte stimmen Sie den AGB und den Datenschutzbestimmungen zu."];
        }
    }
    else{
        for(UIView *vw in self.scrollView.subviews) {
            if([vw isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField*)vw;
                if([textField.text length] == 0 && textField.background == nil){
                    textField.layer.borderColor=[[UIColor redColor]CGColor];
                    textField.layer.borderWidth= 1.3f;
                    [textField setBackground:[UIImage imageNamed:@"errorFieldBg"]];
                    [textField setTextColor:[UIColor whiteColor]];
                    UIColor *color = [UIColor whiteColor];
                    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ wird benötigt",textField.placeholder] attributes:@{NSForegroundColorAttributeName: color}];
                }
            }
        }
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"" andMessage:@"Bitte füllen Sie alle Felder aus"];
    }
}

-(void)alreadyRegisterBtnPress:(id)sender
{
    LoginVC *loginVC = [[LoginVC alloc] init];
    [self.navigationController pushViewController:loginVC animated:YES];
}

#pragma mark - Others

- (BOOL)validateEmail:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
