//
//  RegistrationVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/15/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPKeyboardAvoidingScrollView;

@interface RegistrationVC : UnfallheldenParentVC<UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UITextField *firstNameField;
@property (nonatomic, weak) IBOutlet UITextField *lastNameField;
@property (nonatomic, weak) IBOutlet UITextField *passwordField;
@property (nonatomic, weak) IBOutlet UITextField *cnfrmPasswordField;
@property (nonatomic, weak) IBOutlet UITextField *emailField;
@property (nonatomic, weak) IBOutlet UITextField *familyField;
@property (nonatomic, weak) IBOutlet UITextField *carBrandField;
@property (nonatomic, weak) IBOutlet UITextField *phoneField;
@property (nonatomic, weak) IBOutlet UIButton *checkBoxBtn;
@property (nonatomic, weak) IBOutlet UIButton *alreadyRegistredBtn;
@property (nonatomic, weak) IBOutlet UIButton *registerBtn;
@property (nonatomic, strong) NSString *anrede;
@property (nonatomic, strong) NSString *terms;
@property (nonatomic, strong) NSString *calledFrom;
@property (nonatomic, strong) NSMutableArray *carBrandArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpacingContinueToNxtBtn;

-(IBAction)registerBtnPress:(id)sender;
-(IBAction)alreadyRegisterBtnPress:(id)sender;
-(IBAction)checkBoxBtnPress:(id)sender;
-(IBAction)openPickerForCarBrand:(id)sender;
-(IBAction)termsAndCndtnsBtnPress:(id)sender;
-(IBAction)dataPrvcyBtnPress:(id)sender;


@end
