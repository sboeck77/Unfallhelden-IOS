//
//  HomeScreenVCViewController.h
//  Unfallhelden
//
//  Created by Adil Anwer on 1/27/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeScreenVC : UnfallheldenParentVC<UIAlertViewDelegate>

@property (nonatomic,weak) IBOutlet UIButton *reportAccidentBtn;
@property (nonatomic,weak) IBOutlet UIButton *loginBtn;
@property (nonatomic,weak) IBOutlet UIButton *tankNavigatorBtn;
@property (nonatomic,weak) IBOutlet UIButton *tankNavigatorBtnSecond;
@property (nonatomic, strong) NSString *calledFrom;

-(IBAction)reportAccidentBtnPress:(id)sender;
-(IBAction)tankNavigatorBtnPress:(id)sender;
-(IBAction)loginBtnPress:(id)sender;

@end
