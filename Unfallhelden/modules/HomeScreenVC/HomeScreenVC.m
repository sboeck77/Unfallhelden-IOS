//
//  HomeScreenVCViewController.m
//  Unfallhelden
//
//  Created by Adil Anwer on 1/27/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "HomeScreenVC.h"
#import "AtAccidentSiteVC.h"
#import "FuelStationsListVC.h"
#import "LoginVC.h"

@interface HomeScreenVC ()

@end

@implementation HomeScreenVC
@synthesize reportAccidentBtn,loginBtn,calledFrom,tankNavigatorBtn,tankNavigatorBtnSecond;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"Home"];
    
    //GoogleAnalytics
    self.screenName = @"HomeScreen";
    
    // Do any additional setup after loading the view from its nib.
    if([self.calledFrom isEqualToString:@"login"]){
        self.navigationItem.backBarButtonItem = nil;
        UIBarButtonItem *sideMenuBtn = [UIBarButtonItem leftMenuBarButtonItemWithtarget:self andSelector:@selector(leftSideMenuBtnPress:)];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        negativeSpacer.width = -8;
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,sideMenuBtn, nil] animated:NO];
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:self];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    else{
        [super setupMenuBarButtonItems];
    }
    
    /*
     self.reportAccidentBtn.layer.cornerRadius = 2.0f;
     self.reportAccidentBtn.layer.masksToBounds = YES;
     [self.reportAccidentBtn.titleLabel setFont:[UIFont regularUnfallheldenFontWithSize:19]];
     */
    
    self.loginBtn.layer.cornerRadius = 2.0f;
    self.loginBtn.layer.masksToBounds = YES;
    self.loginBtn.layer.borderColor=[[UIColor whiteColor]CGColor];
    self.loginBtn.layer.borderWidth= 1.3f;
    [self.loginBtn.titleLabel setFont:[UIFont regularUnfallheldenFontWithSize:19]];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultsUserAlreadyLoggedIn] isEqualToString:@"YES"]){
        self.loginBtn.hidden = YES;
        //        self.tankNavigatorBtn.hidden = YES;
        //        self.tankNavigatorBtnSecond.hidden = NO;
    }
    
    UIBarButtonItem *barButtonItem = [UIBarButtonItem addAnotherRightMenuBarButtonItemWithtarget:self andSelector:@selector(callBtnPress:) andImage:[UIImage imageNamed:@"phoneBtn"]];
    
    UIBarButtonItem *barButtonItemHelp = [UIBarButtonItem rightMenuBarButtonItemWithtarget:self andSelector:@selector(helpBtnPress:) andImage:[UIImage imageNamed:@"helpBtn"]];
    
    UIBarButtonItem *barButtonItemEmpty = [UIBarButtonItem createEmptyBarButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,barButtonItemHelp,barButtonItemEmpty,barButtonItem, nil] animated:NO];
}

-(void)reportAccidentBtnPress:(id)sender
{
    AtAccidentSiteVC *reportAccidentVC = [[AtAccidentSiteVC alloc] init];
    [self.navigationController pushViewController:reportAccidentVC animated:YES];
}

-(void)tankNavigatorBtnPress:(id)sender
{
    FuelStationsListVC *fuelStationsListVC = [[FuelStationsListVC alloc] init];
    [self.navigationController pushViewController:fuelStationsListVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loginBtnPress:(id)sender
{
    LoginVC *loginVC = [[LoginVC alloc] init];
    [self.navigationController pushViewController:loginVC animated:YES];
}

-(IBAction)leftSideMenuBtnPress:(id)sender
{
    [super leftSideMenuButtonPressed:sender];
}

-(IBAction)callBtnPress:(id)sender
{
    NSString *phoneNum = @"";
    if([[UnfallheldenUtilities sharedSingletonInstance].hotlinePhoneNum length] > 0){
        phoneNum = [UnfallheldenUtilities sharedSingletonInstance].hotlinePhoneNum;
    }
    else{
        phoneNum = @"0800 72 41 794";
    }
    NSString *message = [NSString stringWithFormat:@"Möchten sie die Unfallhelden Hotline anrufen? : %@",phoneNum];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"Abbrechen" otherButtonTitles:@"Anrufen", nil];
    alert.tag = 123;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    /*
     "ambulance_number":"112",
     "hotline_number":""0800 72 41 794"
     */
    NSString *phoneNum = @"";
    if([[UnfallheldenUtilities sharedSingletonInstance].hotlinePhoneNum length] > 0){
        phoneNum = [UnfallheldenUtilities sharedSingletonInstance].hotlinePhoneNum;
    }
    else{
        phoneNum = @"0800 72 41 794";
    }
    
    if([title isEqualToString:@"Anrufen"]) {
        NSString *phNumber = [NSString stringWithFormat:@"tel:%@",[phoneNum stringByReplacingOccurrencesOfString:@" " withString:@""]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phNumber]];
    }
}

-(IBAction)helpBtnPress:(id)sender
{
    UnfallheldenCustomPopup *customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
    
    [self.view addSubview:customPopup];
    customPopup.frame = self.view.frame;
    
    customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
    customPopup.unfallheldenPopup.layer.masksToBounds = YES;
    
    [customPopup displayPopup];
    
    customPopup.descriptionTextView.text = HomeScreenHelpText;
    customPopup.titleLabel.text = @"Hilfe";
    
    [customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
    [customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
