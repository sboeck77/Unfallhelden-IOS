//
//  SideMenuViewController.h
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import <UIKit/UIKit.h>

@interface SideMenuViewController : UIViewController<UITabBarDelegate, UITableViewDataSource, UIAlertViewDelegate>

@property (nonatomic, strong) NSMutableArray *viewsArray;
@property (nonatomic, strong) UIViewController *currentVC;

@property (nonatomic, weak) IBOutlet UITableView *sideMenuTbl;

-(id)initWithViews:(NSMutableArray*)views;

@end