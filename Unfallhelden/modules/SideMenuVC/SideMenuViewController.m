//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import "SideMenuViewController.h"
#import "MFSideMenu.h"
#import "FuelStationsListVC.h"
#import "LoginVC.h"
#import "MenuVC.h"
#import "MyAccidentsVC.h"
#import "ReportAccidentVC.h"
#import "OwnerProfileVC.h"
#import "RegistrationVC.h"
#import "HomeScreenVC.h"
#import "SettingsVC.h"
#import "AboutVC.h"
#import "CarInfoVC.h"
#import "AtAccidentSiteVC.h"
#import "SideMenuCell.h"

@implementation SideMenuViewController
@synthesize viewsArray,currentVC,sideMenuTbl;

-(id)initWithViews:(NSMutableArray *)views
{
    self = [super init];
    if(self) {
        self.viewsArray = [[NSMutableArray alloc] init];
        self.viewsArray = views;
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    
    /*
     UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 24)];
     headerView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleWidth;
     //        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(XXX, YYY, XXX, YYY)];
     //        [headerView addSubview:imageView];
     //        UILabel *labelView = [[UILabel alloc] initWithFrame:CGRectMake(XXX, YYY, XXX, YYY)];
     //        [headerView addSubview:labelView];
     self.sideMenuTbl.tableHeaderView = headerView;
     */
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(updateSideMenu)
     name:kNotificationNameUserLogin
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(updateSideMenu)
     name:kNotificationNameUserLogout
     object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    if([[[[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsUserAlreadyLoggedIn] description] isEqualToString:@"YES"]){
        self.viewsArray = nil;
        //        self.viewsArray = [[NSMutableArray alloc] initWithObjects:@"Home",@"My Profile",@"My Car(s)",@"My Accidents",@"Report Accident",@"Settings",@"About Unfalhelden", nil];
        //        self.viewsArray = [[NSMutableArray alloc] initWithObjects:@"Home",@"My Profile",@"My Car(s)",@"My Accidents",@"Unfall melden",@"Einstellungen",@"Über Unfallhelden", nil];
        self.viewsArray = [[NSMutableArray alloc] initWithObjects:@"Home",@"Mein Profil",@"Meine Fahrzeuge",@"Meine Unfälle",@"Unfall melden",@"Über Unfallhelden",@"Logout", nil];
    }
    else{
        self.viewsArray = nil;
        
        //        self.viewsArray = [[NSMutableArray alloc] initWithObjects:@"Home",@"Register",@"Report Accident",@"Settings",@"About Unfallhelden", nil];
        //        self.viewsArray = [[NSMutableArray alloc] initWithObjects:@"Home",@"Account anlegen",@"Unfall melden",@"Einstellungen",@"Über Unfallhelden", nil];
        self.viewsArray = [[NSMutableArray alloc] initWithObjects:@"Home",@"Account anlegen",@"Einloggen",@"Unfall melden",@"Über Unfallhelden", nil];
        //        [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationNameUserLogout object:nil];
    }
    [self.sideMenuTbl reloadData];
    self.sideMenuTbl.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - UpdateSideMenu

-(void)updateSideMenu
{
    if([[[[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsUserAlreadyLoggedIn] description] isEqualToString:@"YES"]){
        self.viewsArray = nil;
        //        self.viewsArray = [[NSMutableArray alloc] initWithObjects:@"Home",@"My Profile",@"My Car(s)",@"My Accidents",@"Report Accident",@"Tank Navigator",@"Settings",@"About Unfalhelden", nil];
        //        self.viewsArray = [[NSMutableArray alloc] initWithObjects:@"Home",@"My Profile",@"My Car(s)",@"My Accidents",@"Report Accident",@"Settings",@"About Unfalhelden", nil];
        self.viewsArray = [[NSMutableArray alloc] initWithObjects:@"Home",@"Mein Profil",@"Meine Fahrzeuge",@"Meine Unfälle",@"Unfall melden",@"Über Unfallhelden",@"Logout", nil];
        //        [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationNameUserLogin object:nil];
    }
    else{
        self.viewsArray = nil;
        //        self.viewsArray = [[NSMutableArray alloc] initWithObjects:@"Home",@"Register",@"Report Accident",@"Tank Navigator",@"Settings",@"About Unfallhelden", nil];
        //        self.viewsArray = [[NSMutableArray alloc] initWithObjects:@"Home",@"Register",@"Report Accident",@"Settings",@"About Unfallhelden", nil];
        self.viewsArray = [[NSMutableArray alloc] initWithObjects:@"Home",@"Account anlegen",@"Einloggen",@"Unfall melden",@"Über Unfallhelden", nil];
        //        [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationNameUserLogout object:nil];
    }
    [self.sideMenuTbl reloadData];
}

#pragma mark -
#pragma mark - UITableViewDataSource

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return [NSString stringWithFormat:@"Section %ld", (long)section];
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"SideMenuCell";
    
    SideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SideMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.menuItemTitle.text = [self.viewsArray objectAtIndex:indexPath.row];
    
    if([[[[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsUserAlreadyLoggedIn] description] isEqualToString:@"YES"]){
        if(indexPath.row == 0){
            [cell.menuItemIcon setImage:[UIImage imageNamed:@"homeIcon"] forState:UIControlStateNormal];
        }
        else if (indexPath.row == 1){
            [cell.menuItemIcon setImage:[UIImage imageNamed:@"profileIcon"] forState:UIControlStateNormal];
        }
        else if (indexPath.row == 2){
            [cell.menuItemIcon setImage:[UIImage imageNamed:@"carsIcon"] forState:UIControlStateNormal];
        }
        else if (indexPath.row == 3){
            [cell.menuItemIcon setImage:[UIImage imageNamed:@"accidentIcon"] forState:UIControlStateNormal];
        }
        else if (indexPath.row == 4){
            [cell.menuItemIcon setImage:[UIImage imageNamed:@"reportIcon"] forState:UIControlStateNormal];
        }
        //        else if (indexPath.row == 5){
        //            [cell.menuItemIcon setImage:[UIImage imageNamed:@"tankIcon"] forState:UIControlStateNormal];
        //        }
        //        else if (indexPath.row == 5){
        //            [cell.menuItemIcon setImage:[UIImage imageNamed:@"settingsIcon"] forState:UIControlStateNormal];
        //        }
        else if (indexPath.row == 5){
            [cell.menuItemIcon setImage:[UIImage imageNamed:@"helpBtn"] forState:UIControlStateNormal];
        }
        else if (indexPath.row == 6){
            [cell.menuItemIcon setImage:[UIImage imageNamed:@"profileIcon"] forState:UIControlStateNormal];
        }
    }
    else{
        if(indexPath.row == 0){
            [cell.menuItemIcon setImage:[UIImage imageNamed:@"homeIcon"] forState:UIControlStateNormal];
        }
        else if (indexPath.row == 1){
            [cell.menuItemIcon setImage:[UIImage imageNamed:@"profileIcon"] forState:UIControlStateNormal];
        }
        else if (indexPath.row == 2){
            [cell.menuItemIcon setImage:[UIImage imageNamed:@"profileIcon"] forState:UIControlStateNormal];
        }
        else if (indexPath.row == 3){
            [cell.menuItemIcon setImage:[UIImage imageNamed:@"reportIcon"] forState:UIControlStateNormal];
        }
        //        else if (indexPath.row == 3){
        //            [cell.menuItemIcon setImage:[UIImage imageNamed:@"tankIcon"] forState:UIControlStateNormal];
        //        }
        //        else if (indexPath.row == 3){
        //            [cell.menuItemIcon setImage:[UIImage imageNamed:@"settingsIcon"] forState:UIControlStateNormal];
        //        }
        else if (indexPath.row == 4){
            [cell.menuItemIcon setImage:[UIImage imageNamed:@"helpBtn"] forState:UIControlStateNormal];
        }
    }
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIViewController *vcForPush;
    if([[[[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsUserAlreadyLoggedIn] description] isEqualToString:@"YES"]){
        if(indexPath.row != 6){
            if(indexPath.row == 0){
                vcForPush = [[HomeScreenVC alloc] initWithNibName:@"HomeScreenVC" bundle:nil];
                vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
            }
            else if (indexPath.row == 1){
                vcForPush = [[OwnerProfileVC alloc] initWithNibName:@"OwnerProfileVC" bundle:nil];
                vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
            }
            else if (indexPath.row == 2){
                vcForPush = [[CarInfoVC alloc] initWithNibName:@"CarInfoVC" bundle:nil];
                vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
            }
            else if (indexPath.row == 3){
                vcForPush = [[MyAccidentsVC alloc] initWithNibName:@"MyAccidentsVC" bundle:nil];
                vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
            }
            else if (indexPath.row == 4){
                vcForPush = [[AtAccidentSiteVC alloc] initWithNibName:@"AtAccidentSiteVC" bundle:nil];
                //            ((ReportAccidentVC*)vcForPush).calledFrom = @"sideMenu";
                vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
            }
            //        else if (indexPath.row == 5){
            //            vcForPush = [[FuelStationsListVC alloc] initWithNibName:@"FuelStationsListVC" bundle:nil];
            //            ((FuelStationsListVC*)vcForPush).calledFrom = @"sideMenu";
            //            vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
            //        }
            //        else if (indexPath.row == 5){
            //            vcForPush = [[SettingsVC alloc] initWithNibName:@"SettingsVC" bundle:nil];
            //            vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
            //        }
            else if (indexPath.row == 5){
                vcForPush = [[AboutVC alloc] initWithNibName:@"AboutVC" bundle:nil];
                vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
            }
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:vcForPush];
            navigationController.viewControllers = controllers;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else if (indexPath.row == 6){
            //            [UserProfile deleteUserProfile:self.profile];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ausloggen" message:@"Bitte bestätigen Sie Ihren Logout." delegate:self cancelButtonTitle:@"Abbrechen" otherButtonTitles:@"Logout", nil];
            [alert show];
        }
    }
    else{
        if(indexPath.row == 0){
            vcForPush = [[HomeScreenVC alloc] initWithNibName:@"HomeScreenVC" bundle:nil];
            vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
        }
        else if (indexPath.row == 1){
            vcForPush = [[RegistrationVC alloc] initWithNibName:@"RegistrationVC" bundle:nil];
            vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
        }
        else if (indexPath.row == 2){
            vcForPush = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
            //            ((ReportAccidentVC*)vcForPush).calledFrom = @"sideMenu";
            vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
        }
        else if (indexPath.row == 3){
            vcForPush = [[AtAccidentSiteVC alloc] initWithNibName:@"AtAccidentSiteVC" bundle:nil];
            //            ((ReportAccidentVC*)vcForPush).calledFrom = @"sideMenu";
            vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
        }
        //        else if (indexPath.row == 3){
        //            vcForPush = [[FuelStationsListVC alloc] initWithNibName:@"FuelStationsListVC" bundle:nil];
        //            ((FuelStationsListVC*)vcForPush).calledFrom = @"sideMenu";
        //            vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
        //        }
        //        else if (indexPath.row == 3){
        //            vcForPush = [[SettingsVC alloc] initWithNibName:@"SettingsVC" bundle:nil];
        //            vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
        //        }
        else if (indexPath.row == 4){
            vcForPush = [[AboutVC alloc] initWithNibName:@"AboutVC" bundle:nil];
            vcForPush.title = [self.viewsArray objectAtIndex:indexPath.row];
        }
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:vcForPush];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Logout"]) {
        
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Ausgeloggt" andMessage:@"Sie wurden erfolgreich abgemeldet."];
        HomeScreenVC *vcForPush = [[HomeScreenVC alloc] initWithNibName:@"HomeScreenVC" bundle:nil];
        vcForPush.title = [self.viewsArray objectAtIndex:0];
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:vcForPush];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:kUserDefaultsUserAlreadyLoggedIn];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNameUserLogout object:nil userInfo:nil];
    }
}

@end
