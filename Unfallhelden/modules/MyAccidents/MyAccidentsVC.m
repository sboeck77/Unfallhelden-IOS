//
//  AccidentsVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/7/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "MyAccidentsVC.h"
#import "AccidentsCell.h"
#import "ReportedAccidentView.h"
#import "AccidentStatusView.h"
#import "ReportAccidentData.h"

@interface MyAccidentsVC (){
    //    UnfallheldenPickerView *pickerView;
}

@property(nonatomic) CGPoint oldContentOffset;
@property (nonatomic) CGRect originalScrollViewFrame;
@property (nonatomic, strong) ReportedAccidentView *reportedAccidentView;
@property (nonatomic, strong)  AccidentStatusView *statusView;
@property (nonatomic, strong) UITextField *accidentDateField;
@property (nonatomic) BOOL accidentsDateTblHidden;

@end

@implementation MyAccidentsVC
@synthesize scrollView = _scrollView, pageControl, unfallGemeldetTxtLineLbl, gutachtenTxtLineLbl, werstattterminTxtLineLbl,schadenTxtLineLbl;
@synthesize lblScrollview,titlesContainer,originalTitleScrollFrame,contentSizeview= _contentSizeview, reportedAccidentsArray,accidentsTbl;

- (void)viewDidLoad {
    //GoogleAnalytics
    //    self.screenName = @"MyAccidentsScreen";
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [super setupMenuBarButtonItems];
    pageControlBeingUsed = NO;
    
    self.originalTitleScrollFrame = self.lblScrollview.frame;
    [self.titlesContainer addSubview:self.lblScrollview];
    for(UIView *vw in self.lblScrollview.subviews) {
        if([vw isKindOfClass:[UIImageView class]]) {
            vw.hidden = YES;
        }
        if ([vw isKindOfClass:[UILabel class]]) {
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:((UILabel*)vw).text];
            [attributeString addAttribute:NSUnderlineStyleAttributeName
                                    value:[NSNumber numberWithInt:1]
                                    range:(NSRange){0,[attributeString length]}];
            ((UILabel*)vw).attributedText = attributeString;
            ((UILabel*)vw).textColor = [UIColor lightGrayColor];
            ((UILabel*)vw).font = [UIFont boldUnfallheldenFontWithSize:17];
        }
    }
    self.unfallGemeldetTxtLineLbl.textColor = [UIColor whiteColor];
    [GlobalScopeObjects sharedSingletonInstance].myAccidentsVC = self;
    self.navigationItem.rightBarButtonItems = nil;
    
    self.reportedAccidentsArray = [[NSMutableArray alloc] init];
    [self setupNavBar];
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"MyAccidentsScreen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    if(self.reportedAccidentView == nil){
        [self.view layoutSubviews];
        [self.view layoutIfNeeded];
        NSArray *colors = [NSArray arrayWithObjects:[UIColor redColor], [UIColor greenColor], nil];
        CGFloat screenWidth = 0.0f;
        if(IS_IPHONE_4){
            screenWidth = 320;
        }
        else if (IS_IPHONE_5){
            screenWidth = 320;
        }
        else if(IS_IPHONE_6){
            screenWidth = 375;
        }
        else if (IS_IPHONE_6_Plus){
            screenWidth = 414;
        }
        for (int i = 0; i < colors.count; i++) {
            CGRect frame;
            frame.origin.x = self.view.frame.size.width * i;
            frame.origin.y = 0;
            frame.size  = CGSizeMake(screenWidth, self.scrollView.frame.size.height);
            self.originalScrollViewFrame = frame;
            
            if(i == 0){
                self.reportedAccidentView = [[ReportedAccidentView alloc] init];
                self.reportedAccidentView.frame = frame;
                [self.reportedAccidentView setupView];
                NSLog(@"height of scroll view %f",frame.size.height);
                [self.scrollView addSubview:self.reportedAccidentView];
                //            [reportedAccidentView.rAccidentScrollView contentSizeToFit];
            }
            else{
                self.statusView = [[AccidentStatusView alloc] init];
                [self.statusView setupView];
                self.statusView.frame = self.originalScrollViewFrame;
                [self.scrollView addSubview:self.statusView];
            }
        }
        
        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * colors.count, self.scrollView.frame.size.height);
        self.lblScrollview.contentSize = CGSizeMake(self.lblScrollview.frame.size.width * colors.count, self.lblScrollview.frame.size.height);
        self.pageControl.currentPage = 0;
        self.pageControl.numberOfPages = colors.count;
        
        CGFloat heightOfPicker = 0.0f;
        if(IS_IPHONE_5){
            heightOfPicker = 206;
        }
        else if (IS_IPHONE_4){
            heightOfPicker = 206;
        }
        else{
        }
        if (IS_IPHONE_6 || IS_IPHONE_6_Plus){
            heightOfPicker = 260;
        }
        
        if ([[UnfallheldenHttpClient sharedClient] connectedToWiFi]){
            
            [[UnfallheldenHttpClient sharedClient] getUserAccidentsWithCompletionBlock:^(NSData *data) {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                NSDictionary *dataDictionary = [responseString JSONValue];
                NSLog(@"data dictionary : %@",dataDictionary);
                int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
                if(status == 1){
                    NSMutableArray *accidentsArray = [dataDictionary objectForKey:@"data"];
                    if([accidentsArray count]>0){
                        
                        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                        NSString* foofile = [documentsPath stringByAppendingPathComponent:MyAccidentsFileName];
                        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
                        if(fileExists == TRUE){
                            [self removeSavedAccidentsFile];
                        }
                        
                        // Save accidents to local file
                        NSArray *pathComponents = [NSArray arrayWithObjects:
                                                   [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                                                   MyAccidentsFileName,
                                                   nil];
                        NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
                        [data writeToURL:outputFileURL atomically:YES];
                        
                        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"added_datetime"
                                                                                       ascending:NO];
                        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                        accidentsArray = [[accidentsArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
                        
                        for (int accidentsCount = 0; accidentsCount < [accidentsArray count]; accidentsCount++) {
                            ReportAccidentData *accidentData = [[ReportAccidentData alloc] init];
                            NSString *accidentDate = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"added_datetime"] description];
                            NSRange start = [accidentDate rangeOfString:@" "];
                            if (start.location != NSNotFound)
                            {
                                accidentDate = [accidentDate substringToIndex:start.location];
                            }
                            
                            accidentData.date = accidentDate;
                            accidentData.accidentId = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"id"] description];
                            accidentData.accidentHash = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"accident_hash"] description];
                            accidentData.userLatitude = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"accident_lat"] doubleValue];
                            accidentData.userLongitude = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"accident_lng"] doubleValue];
                            accidentData.placeName = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"address"] description];
                            accidentData.audioFileLink = [[[[accidentsArray objectAtIndex:accidentsCount] objectForKey:@"audio"] objectAtIndex:0] valueForKey:@"file"];
                            accidentData.audioFileId = [[[[accidentsArray objectAtIndex:accidentsCount] objectForKey:@"audio"] objectAtIndex:0] valueForKey:@"id"];
                            accidentData.carId = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"user_car_id"] description];
                            accidentData.acPlateNumber = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_reg_number"] description];
                            accidentData.acFirstName = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_name"] description];
                            accidentData.acLastName = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_family"] description];
                            accidentData.acPhone = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_phone"] description];
                            accidentData.acStreetName = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_street"] description];
                            accidentData.acStreetNumber = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_street_nr"] description];
                            accidentData.acCity = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_city"] description];
                            accidentData.acZipCode = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_postcode"] description];
                            accidentData.acAdditionalAddress = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_address"] description];
                            accidentData.acCarBrand = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_car_brand"] description];
                            accidentData.acCarType = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_car_model"] description];
                            accidentData.acInsuranceCompany = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_insurer"] description];
                            accidentData.acCompanyName = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_company"] description];
                            accidentData.acLeasingCompany = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_leasing"] description];
                            NSString *accidentTime = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"time"] description];
                            //                    NSRange startOfTimeStr = [accidentDate rangeOfString:@" "];
                            //                    if (startOfTimeStr.location != NSNotFound)
                            //                    {
                            //                        accidentTime = [accidentTime substringFromIndex:startOfTimeStr.location];
                            //                    }
                            //                    accidentData.time = accidentTime;
                            accidentData.accidentOccuredTime = accidentTime;
                            accidentData.accidentOccuredDate = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"date"] description];
                            accidentData.weather = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"weather"] description];
                            accidentData.accidentDescription = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"description"] description];
                            accidentData.witnessesArray = [[accidentsArray objectAtIndex:accidentsCount] objectForKey:@"witnesses"];
                            accidentData.photoArray = [[accidentsArray objectAtIndex:accidentsCount] objectForKey:@"photos"];
                            accidentData.accidentStatus = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"status"] description];
                            [self.reportedAccidentsArray addObject:accidentData];
                            [self.accidentsTbl reloadData];
                            ReportAccidentData *reportedAccident = [self.reportedAccidentsArray objectAtIndex:0];
                            self.accidentDateField.text = reportedAccident.date;
                            [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData = [self.reportedAccidentsArray objectAtIndex:0];
                            if(self.statusView != nil){
                                [self.statusView loadAccidentStatus];
                            }
                        }
                        self.reportedAccidentView.reportedAccidentTbl.hidden = NO;
                    }
                    else{
                        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"No Accidents Found" andMessage:@"You have not reported any accident with Unfallhelden"];
                        self.reportedAccidentView.reportedAccidentTbl.hidden = YES;
                        self.reportedAccidentView.noAccidentsReportedLbl.hidden = NO;
                    }
                }
                else{
                    [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Problem fetching accidents" andMessage:@"There is some problem fetching your accidents please try again"];
                }
            }];
        }
        else{
            
            NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString* savedAccidentsFile = [documentsPath stringByAppendingPathComponent:MyAccidentsFileName];
            
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:savedAccidentsFile];
            if(fileExists == TRUE){
                //            NSData *accidentsData = [NSData dataWithContentsOfFile:savedAccidentsFile];
                NSArray *pathComponents = [NSArray arrayWithObjects:
                                           [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                                           MyAccidentsFileName,
                                           nil];
                NSURL *accidentsFileURL = [NSURL fileURLWithPathComponents:pathComponents];
                
                NSData *accidentsData = [NSData dataWithContentsOfURL:accidentsFileURL];
                NSString *responseString = [[NSString alloc] initWithData:accidentsData encoding:NSUTF8StringEncoding];
                
                NSDictionary *dataDictionary = [responseString JSONValue];
                NSLog(@"data dictionary : %@",dataDictionary);
                
                NSMutableArray *accidentsArray = [dataDictionary objectForKey:@"data"];
                
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"added_datetime"
                                                                               ascending:NO];
                NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                accidentsArray = [[accidentsArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
                
                for (int accidentsCount = 0; accidentsCount < [accidentsArray count]; accidentsCount++) {
                    ReportAccidentData *accidentData = [[ReportAccidentData alloc] init];
                    NSString *accidentDate = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"added_datetime"] description];
                    NSRange start = [accidentDate rangeOfString:@" "];
                    if (start.location != NSNotFound)
                    {
                        accidentDate = [accidentDate substringToIndex:start.location];
                    }
                    
                    accidentData.date = accidentDate;
                    accidentData.accidentId = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"id"] description];
                    accidentData.accidentHash = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"accident_hash"] description];
                    accidentData.userLatitude = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"accident_lat"] doubleValue];
                    accidentData.userLongitude = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"accident_lng"] doubleValue];
                    accidentData.placeName = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"address"] description];
                    accidentData.audioFileLink = [[[[accidentsArray objectAtIndex:accidentsCount] objectForKey:@"audio"] objectAtIndex:0] valueForKey:@"file"];
                    accidentData.audioFileId = [[[[accidentsArray objectAtIndex:accidentsCount] objectForKey:@"audio"] objectAtIndex:0] valueForKey:@"id"];
                    accidentData.carId = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"user_car_id"] description];
                    accidentData.acPlateNumber = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_reg_number"] description];
                    accidentData.acFirstName = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_name"] description];
                    accidentData.acLastName = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_family"] description];
                    accidentData.acPhone = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_phone"] description];
                    accidentData.acStreetName = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_street"] description];
                    accidentData.acStreetNumber = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_street_nr"] description];
                    accidentData.acCity = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_city"] description];
                    accidentData.acZipCode = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_postcode"] description];
                    accidentData.acAdditionalAddress = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_address"] description];
                    accidentData.acCarBrand = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_car_brand"] description];
                    accidentData.acCarType = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_car_model"] description];
                    accidentData.acInsuranceCompany = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_insurer"] description];
                    accidentData.acCompanyName = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_company"] description];
                    accidentData.acLeasingCompany = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"opponent_leasing"] description];
                    NSString *accidentTime = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"time"] description];
                    //                    NSRange startOfTimeStr = [accidentDate rangeOfString:@" "];
                    //                    if (startOfTimeStr.location != NSNotFound)
                    //                    {
                    //                        accidentTime = [accidentTime substringFromIndex:startOfTimeStr.location];
                    //                    }
                    //                    accidentData.time = accidentTime;
                    accidentData.accidentOccuredTime = accidentTime;
                    accidentData.accidentOccuredDate = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"date"] description];
                    accidentData.weather = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"weather"] description];
                    accidentData.accidentDescription = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"description"] description];
                    accidentData.witnessesArray = [[accidentsArray objectAtIndex:accidentsCount] objectForKey:@"witnesses"];
                    accidentData.photoArray = [[accidentsArray objectAtIndex:accidentsCount] objectForKey:@"photos"];
                    accidentData.accidentStatus = [[[accidentsArray objectAtIndex:accidentsCount] valueForKey:@"status"] description];
                    [self.reportedAccidentsArray addObject:accidentData];
                    [self.accidentsTbl reloadData];
                    ReportAccidentData *reportedAccident = [self.reportedAccidentsArray objectAtIndex:0];
                    self.accidentDateField.text = reportedAccident.date;
                    [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData = [self.reportedAccidentsArray objectAtIndex:0];
                    if(self.statusView != nil){
                        [self.statusView loadAccidentStatus];
                    }
                }
                self.reportedAccidentView.reportedAccidentTbl.hidden = NO;
                
                [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Keine Datenverbindung" andMessage:@"Die App benötigt eine Internetverbindung. Bitte aktivieren Sie Ihre Datenverbindung und versuchen Sie es erneut. Zuvor gespeicherte Daten werden aktuell in der App angezeigt."];
                //        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
            }
        }
    }
}

- (void)removeSavedAccidentsFile
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:MyAccidentsFileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

-(void)setupNavBar{
    
    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *ddImage = [UIImage imageNamed:@"ddArrow"];
    [aButton setImage:ddImage forState:UIControlStateNormal];
    aButton.frame = CGRectMake(101, 5, 14,   20);
    
    aButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    UIButton *aButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    aButton2.frame = CGRectMake(0, 0, 125,   30);
    
    UIView *topDropDownView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 125, 30)];
    [topDropDownView addSubview:aButton];
    topDropDownView.backgroundColor = [UIColor clearColor];
    self.accidentDateField = [[UITextField alloc] initWithFrame:CGRectMake(5, 2.5, 101, 25)];
    [topDropDownView addSubview:self.accidentDateField];
    topDropDownView.layer.cornerRadius = 2.0f;
    topDropDownView.layer.masksToBounds = YES;
    topDropDownView.layer.borderColor=[[UIColor whiteColor]CGColor];
    topDropDownView.layer.borderWidth= 0.5f;
    [topDropDownView addSubview:aButton2];
    
    self.accidentDateField.placeholder = @"Date :";
    self.accidentDateField.delegate = self;
    
    [self.accidentDateField setTextColor:[UIColor whiteColor]];
    UIColor *color = [UIColor whiteColor];
    self.accidentDateField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.accidentDateField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    [self.accidentDateField setFont:[UIFont regularUnfallheldenFontWithSize:15]];
    [self.accidentDateField setTintColor:[UIColor clearColor]];
    [self.accidentDateField setEnabled:NO];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:topDropDownView];
    [aButton addTarget:self action:@selector(showAccidentDates:) forControlEvents:UIControlEventTouchUpInside];
    [aButton2 addTarget:self action:@selector(showAccidentDates:) forControlEvents:UIControlEventTouchUpInside];
    
    //    UIBarButtonItem *barButtonItemHelp = [UIBarButtonItem rightMenuBarButtonItemWithtarget:self andSelector:@selector(helpBtnPress:) andImage:[UIImage imageNamed:@"helpBtn"]];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -8;
    //    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,barButtonItemHelp,barButtonItemEmpty,barButtonItem, nil] animated:NO];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects: negativeSpacer,barButtonItem, nil] animated:NO];
    
    UIView *accidentDates = (UIView*)[[self.navigationItem.rightBarButtonItems objectAtIndex:1] customView];
    self.accidentsTbl = [[UITableView alloc] initWithFrame:CGRectMake(accidentDates.frame.origin.x, accidentDates.frame.origin.y+58, 125, 150)];
    [self.view addSubview:self.accidentsTbl];
    self.accidentsTbl.delegate = self;
    self.accidentsTbl.dataSource = self;
    self.accidentsTbl.hidden = TRUE;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
    [self.navigationController.navigationBar addGestureRecognizer:tapRecognizer];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(didTapAnywhere:)
     name:kMyAccidentsDidTappedAnyWhere
     object:nil];
    self.accidentsDateTblHidden = YES;
    self.accidentsTbl.backgroundColor = [UIColor clearColor];
    self.accidentsTbl.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

-(void)didTapAnywhere:(id)sender {
    self.accidentsTbl.hidden = YES;
    self.accidentsDateTblHidden = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (!pageControlBeingUsed) {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = self.scrollView.frame.size.width;
        int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.pageControl.currentPage = page;
        for(UIView *vw in self.lblScrollview.subviews) {
            if([vw isKindOfClass:[UIImageView class]]) {
                vw.hidden = YES;
            }
            if ([vw isKindOfClass:[UILabel class]]) {
                ((UILabel*)vw).textColor = [UIColor lightGrayColor];
            }
        }
        if(self.pageControl.currentPage == 0){
            self.unfallGemeldetTxtLineLbl.textColor = [UIColor whiteColor];
            CGRect frame;
            frame.origin.x = self.unfallGemeldetTxtLineLbl.frame.origin.x;
            frame.origin.y = 0;
            frame.size = self.originalTitleScrollFrame.size;
            //            [self moveTitleScroll:frame];
        }
        else if (self.pageControl.currentPage == 1){
            self.gutachtenTxtLineLbl.textColor = [UIColor whiteColor];
            CGRect frame;
            frame.origin.x = self.gutachtenTxtLineLbl.frame.origin.x;
            frame.origin.y = 0;
            frame.size = self.originalTitleScrollFrame.size;
            //            [self moveTitleScroll:frame];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

-(IBAction)sliderTitleBtnpress:(id)sender{
    
    CGRect frame;
    frame.origin.x = self.scrollView.frame.size.width * ((UIButton*)sender).tag;
    frame.origin.y = 0;
    frame.size = self.scrollView.frame.size;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    
    for(UIView *vw in self.lblScrollview.subviews) {
        if([vw isKindOfClass:[UIImageView class]]) {
            vw.hidden = YES;
        }
        if ([vw isKindOfClass:[UILabel class]]) {
            ((UILabel*)vw).textColor = [UIColor lightGrayColor];
        }
    }
    
    self.pageControl.currentPage  = ((UIButton*)sender).tag;
    if(self.pageControl.currentPage == 0){
        self.unfallGemeldetTxtLineLbl.textColor = [UIColor whiteColor];
        CGRect frame;
        frame.origin.x = self.unfallGemeldetTxtLineLbl.frame.origin.x;
        frame.origin.y = 0;
        frame.size = self.originalTitleScrollFrame.size;
        //        [self moveTitleScroll:frame];
    }
    else if (self.pageControl.currentPage == 1){
        self.gutachtenTxtLineLbl.textColor = [UIColor whiteColor];
        CGRect frame;
        frame.origin.x = self.gutachtenTxtLineLbl.frame.origin.x;
        frame.origin.y = 0;
        frame.size = self.originalTitleScrollFrame.size;
        //        [self moveTitleScroll:frame];
    }
    // Keep track of when scrolls happen in response to the page control
    // value changing. If we don't do this, a noticeable "flashing" occurs
    // as the the scroll delegate will temporarily switch back the page
    // number.
    pageControlBeingUsed = YES;
}

-(IBAction)hidePickerView:(id)sender
{
    [self.accidentDateField resignFirstResponder];
}

#pragma mark - Open PickerView

-(IBAction)showAccidentDates:(id)sender
{
    if([self.reportedAccidentsArray count] == 0){
        [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"No accident reported" andMessage:@"you dont have reported any accident with Unfallhelden."];
    }
    else{
        self.accidentsDateTblHidden = !self.accidentsDateTblHidden;
        if(self.accidentsDateTblHidden == YES){
            self.accidentsTbl.hidden = YES;
        }
        else{
            self.accidentsTbl.hidden = NO;
        }
    }
}

#pragma mark - UITableViewDataSourceMethods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.reportedAccidentsArray count];
    //    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *accidentDateCell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"AccidentDatesCell"];
    if (accidentDateCell == nil) {
        accidentDateCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AccidentDatesCell"];
    }
    ReportAccidentData *reportedAccident = [self.reportedAccidentsArray objectAtIndex:indexPath.row];
    accidentDateCell.textLabel.text = reportedAccident.date;
    accidentDateCell.textLabel.font = [UIFont regularUnfallheldenFontWithSize:17];
    accidentDateCell.textLabel.textColor = [UIColor whiteColor];
    accidentDateCell.backgroundColor = [UIColor darkGrayColor];
    accidentDateCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return accidentDateCell;
}

#pragma mark - UITableViewDelegateMethods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.accidentsTbl.hidden = YES;
    self.accidentsDateTblHidden = YES;
    ReportAccidentData *reportedAccident = [self.reportedAccidentsArray objectAtIndex:indexPath.row];
    self.accidentDateField.text = reportedAccident.date;
    [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData = [self.reportedAccidentsArray objectAtIndex:indexPath.row];
    if(self.statusView != nil){
        [self.statusView loadAccidentStatus];
    }
}

- (IBAction)changePage {
    // Update the scroll view to the appropriate page
    CGRect frame;
    frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.scrollView.frame.size;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    
    // Keep track of when scrolls happen in response to the page control
    // value changing. If we don't do this, a noticeable "flashing" occurs
    // as the the scroll delegate will temporarily switch back the page
    // number.
    pageControlBeingUsed = YES;
}

-(IBAction)helpBtnPress:(id)sender
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)moveTitleScroll:(CGRect)frame
{
    [UIView animateWithDuration:0.1 animations:^{
        CGRect newFrame = frame;
        newFrame.origin.x = self.originalTitleScrollFrame.origin.x - frame.origin.x + 12;
        self.lblScrollview.frame = newFrame;
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
