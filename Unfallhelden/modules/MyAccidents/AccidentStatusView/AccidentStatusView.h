//
//  AccidentStatusView.h
//  Unfallhelden
//
//  Created by Adil Anwer on 3/16/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@interface AccidentStatusView : UIView

@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *accidentStatusScroll;
@property (nonatomic, weak) IBOutlet UILabel *accidentStatusLbl;
@property (nonatomic, weak) IBOutlet UIButton *accidentReportedBtn;
@property (nonatomic, weak) IBOutlet UIButton *expertReportReceivedBtn;
@property (nonatomic, weak) IBOutlet UIButton *insuranceCompanyInfrmdBtn;
@property (nonatomic, weak) IBOutlet UIButton *appointmentGarageCmpnyBtn;
@property (nonatomic, weak) IBOutlet UIButton *accidentManaged;

-(void)setupView;
-(void)loadAccidentStatus;

@end
