//
//  AccidentStatusView.m
//  Unfallhelden
//
//  Created by Adil Anwer on 3/16/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "AccidentStatusView.h"

@implementation AccidentStatusView
@synthesize accidentStatusScroll, accidentStatusLbl, accidentManaged, accidentReportedBtn,insuranceCompanyInfrmdBtn, expertReportReceivedBtn,appointmentGarageCmpnyBtn;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AccidentStatusView" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

-(void)setupView
{
    CGFloat screenWidth = 0.0f;
    if(IS_IPHONE_4){
        screenWidth = 320;
    }
    else if (IS_IPHONE_5){
        screenWidth = 320;
    }
    else if(IS_IPHONE_6){
        screenWidth = 375;
    }
    else if (IS_IPHONE_6_Plus){
        screenWidth = 414;
    }
    CGRect frame = self.frame;
    frame.size.width = screenWidth;
    self.frame = frame;
    self.accidentStatusScroll.contentSize = CGSizeMake(screenWidth, self.accidentStatusScroll.contentSize.height);
    self.accidentStatusLbl.font = [UIFont boldUnfallheldenFontWithSize:17];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
    [self addGestureRecognizer:tapRecognizer];
    self.accidentReportedBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    UIEdgeInsets insets = self.accidentReportedBtn.contentEdgeInsets;
    insets.left = 10;
    [self.accidentReportedBtn setContentEdgeInsets:insets];
    self.accidentReportedBtn.adjustsImageWhenHighlighted = NO;
    [self.expertReportReceivedBtn setContentEdgeInsets:insets];
    self.expertReportReceivedBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.expertReportReceivedBtn.adjustsImageWhenHighlighted = NO;
    [self.insuranceCompanyInfrmdBtn setContentEdgeInsets:insets];
    self.insuranceCompanyInfrmdBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.insuranceCompanyInfrmdBtn.adjustsImageWhenHighlighted = NO;
    [self.appointmentGarageCmpnyBtn setContentEdgeInsets:insets];
    self.appointmentGarageCmpnyBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.appointmentGarageCmpnyBtn.adjustsImageWhenHighlighted = NO;
    [self.accidentManaged setContentEdgeInsets:insets];
    self.accidentManaged.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.accidentManaged.adjustsImageWhenHighlighted = NO;
}

-(void)loadAccidentStatus
{
    if([[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.accidentStatus intValue] == 1){
        [self.accidentReportedBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
    }
    else if ([[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.accidentStatus intValue] == 2){
        [self.accidentReportedBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
        [self.expertReportReceivedBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
    }
    else if ([[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.accidentStatus intValue] == 3){
        [self.accidentReportedBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
        [self.expertReportReceivedBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
        [self.insuranceCompanyInfrmdBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
        
    }
    else if ([[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.accidentStatus intValue] == 4){
        [self.accidentReportedBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
        [self.expertReportReceivedBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
        [self.insuranceCompanyInfrmdBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
        [self.appointmentGarageCmpnyBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
        
    }
    else if ([[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.accidentStatus intValue] == 5){
        [self.accidentReportedBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
        [self.expertReportReceivedBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
        [self.insuranceCompanyInfrmdBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
        [self.appointmentGarageCmpnyBtn setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
        [self.accidentManaged setBackgroundImage:[UIImage imageNamed:@"greenBtnBg"] forState:UIControlStateNormal];
        
    }
}

-(void)didTapAnywhere:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kMyAccidentsDidTappedAnyWhere object:sender];
}

@end
