//
//  MoreInfoAboutAccident.m
//  Unfallhelden
//
//  Created by Adil Anwer on 4/20/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "MoreInfoAboutAccident.h"
#import "WitnessesFields.h"
#import "AudioRecorder.h"

@interface MoreInfoAboutAccident ()<AudioPlayerDelegate>

@property (nonatomic) int witnsNameFieldTag;
@property (nonatomic) int witnsPhoneFieldTag;
@property (nonatomic) int witnsesCount;

@property (nonatomic, strong) UITextField *wNameField2;
@property (nonatomic, strong) UITextField *wPhoneField2;
@property (nonatomic, strong) UITextField *wNameField3;
@property (nonatomic, strong) UITextField *wPhoneField3;
@property (nonatomic, strong) UITextField *wNameField4;
@property (nonatomic, strong) UITextField *wPhoneField4;
@property (nonatomic, strong) AudioRecorder *recorder;
@property (nonatomic, strong) NSString *audioFileName;

@end

@implementation MoreInfoAboutAccident
@synthesize moreInfoScroll, descriptionTxtView, witnessesContainer,witnessesFieldsView,witnessFieldsContainerHeight;
@synthesize kaScrollView,playAudioBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"Mehr Informationen über den Unfall"];
    [super setupMenuBarButtonItems];
    for(UIView *vw in self.witnessesContainer.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            //            textField.delegate = self;
            if(IS_IPHONE_5 || IS_IPHONE_4){
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:11.5]];
            }
            else{
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:14]];
            }
            textField.enabled = NO;
        }
    }
    self.descriptionTxtView.layer.cornerRadius = 2.0f;
    self.descriptionTxtView.layer.masksToBounds = YES;
    self.descriptionTxtView.layer.borderColor=[[UIColor whiteColor]CGColor];
    self.descriptionTxtView.layer.borderWidth= 1.3f;
    [self.descriptionTxtView setTextColor:[UIColor whiteColor]];
    //    self.descriptionTxtView.delegate = self;
    [self.descriptionTxtView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
    
    self.descriptionTxtView.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.accidentDescription;
    
    [self.nameField addTarget:self
                       action:@selector(textFieldValueChanged:)
             forControlEvents:UIControlEventEditingChanged];
    [self.phoneField addTarget:self
                        action:@selector(textFieldValueChanged:)
              forControlEvents:UIControlEventEditingChanged];
    self.witnsesCount = 1;
    self.witnsNameFieldTag = 1001;
    self.witnsPhoneFieldTag = 2001;
    if([[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.witnessesArray count] > 0){
        self.nameField.text = [[[[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.witnessesArray objectAtIndex:0] valueForKey:@"name"] description];
        self.phoneField.text = [[[[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.witnessesArray objectAtIndex:0] valueForKey:@"phone"] description];
    }
    
    [self downloadAudioFile];
}

-(void)viewDidAppear:(BOOL)animated
{
    if([[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.witnessesArray count] > 1){
        [self addWitnessesFields];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    if([[self.playAudioBtn currentTitle] isEqualToString:@"Pause/Stopp"]){
        [self.recorder stopPlaying];
    }
}

-(void)downloadAudioFile{
    
    NSString *audioFileDownloadUrl = [NSString stringWithFormat:@"http://unfallhelden.de/data/uploads/audio/%@",[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.audioFileLink];
    [[UnfallheldenHttpClient sharedClient] downloadAudioWithURL:audioFileDownloadUrl WithCompletionBlock:^(NSData *data) {
        
        self.audioFileName = @"downloadedRecording.mp3";
        NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString* foofile = [documentsPath stringByAppendingPathComponent:self.audioFileName];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
        if(fileExists == TRUE){
            
            [self removeAudio];
        }
        
        // Set the audio file
        NSArray *pathComponents = [NSArray arrayWithObjects:
                                   [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                                   self.audioFileName,
                                   nil];
        NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
        [data writeToURL:outputFileURL atomically:YES];
        
        self.recorder = [AudioRecorder sharedSingletonInstance];
        [self.recorder setupAudioRecorderWithURL:outputFileURL];
        [self.recorder setAudioPlayerDelegate:self];
        [self.playAudioBtn addTarget:self action:@selector(playAudio) forControlEvents:UIControlEventTouchUpInside];
    }];
}

- (void)removeAudio
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:self.audioFileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

- (void)addWitnessesFields {
    long witnessesCount = [[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.witnessesArray count]-1;
    for (int count = 0; count < witnessesCount; count++) {
        
        WitnessesFields *witnessesFields = [[WitnessesFields alloc] init];
        NSMutableArray *viewsArray = [[NSMutableArray alloc] initWithArray:self.witnessesFieldsView.subviews];
        UIView *lastSubview = (UIView*)[viewsArray objectAtIndex:[viewsArray count]-1];
        witnessesFields.frame = CGRectMake(lastSubview.frame.origin.x, lastSubview.frame.origin.y + self.nameField.frame.size.height + 10, self.witnessesFieldsView.frame.size.width, self.witnessesFieldsView.frame.size.height);
        
        //        self.containerView.autoresizesSubviews = NO;
        witnessesFields.autoresizesSubviews = NO;
        self.witnessesFieldsView.autoresizesSubviews = NO;
        
        for(UIView *vw in witnessesFields.subviews) {
            if([vw isKindOfClass:[UITextField class]]) {
                UITextField *textField = (UITextField*)vw;
                textField.layer.cornerRadius = 2.0f;
                textField.layer.masksToBounds = YES;
                textField.layer.borderColor=[[UIColor whiteColor]CGColor];
                textField.layer.borderWidth= 1.3f;
                UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
                textField.leftView = paddingView;
                textField.leftViewMode = UITextFieldViewModeAlways;
                [textField setTextColor:[UIColor whiteColor]];
                UIColor *color = [UIColor whiteColor];
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
                textField.delegate = self;
                textField.enabled = NO;
                if(IS_IPHONE_5 || IS_IPHONE_4){
                    [textField setFont:[UIFont regularUnfallheldenFontWithSize:11.5]];
                }
                else{
                    [textField setFont:[UIFont regularUnfallheldenFontWithSize:14]];
                }
                
            }
        }
        
        witnessesFields.witnessNameField.frame = CGRectMake(self.nameField.frame.origin.x, witnessesFields.witnessNameField.frame.origin.y, self.nameField.frame.size.width, self.nameField.frame.size.height);
        witnessesFields.witnessPhoneField.frame = CGRectMake(self.phoneField.frame.origin.x, witnessesFields.witnessPhoneField.frame.origin.y, self.phoneField.frame.size.width, self.phoneField.frame.size.height);
        
        [UIView animateWithDuration:0.3 animations:^{
            //            CGRect frame = self.witnessesFieldsView.frame;
            //            frame.size.height += witnessesFields.frame.size.height;
            //            self.witnessesFieldsView.frame = frame;
            //            CGRect bottomBtnsContainerFrame = self.bottomBtnsContainer.frame;
            //            bottomBtnsContainerFrame.origin.y = CGRectGetMaxY(self.witnessesFieldsView.frame);
            //            self.bottomBtnsContainer.frame = bottomBtnsContainerFrame;
            [self.witnessesFieldsView addSubview:witnessesFields];
            //            CGRect containerViewFrame = self.containerView.frame;
            //            containerViewFrame.size.height = CGRectGetMaxY(self.bottomBtnsContainer.frame);
            //            self.containerView.frame = containerViewFrame;
            //            self.kaScrollView.contentSize = CGSizeMake(self.containerView.frame.size.width, self.containerView.frame.size.height+100);
            //            self.contentViewHeightContraint.constant += self.nameField.frame.size.height + 10;
            //            self.bottomBtnsContainerViewTopConstraint.constant += self.nameField.frame.size.height + 12;
            self.witnessFieldsContainerHeight.constant += self.nameField.frame.size.height + 10;
            ;
        }];
        [self.kaScrollView contentSizeToFit];
        if([self.witnessesFieldsView.subviews count] == 4){
            //            self.continueBtn.hidden = YES;
            //            self.addFieldBtn.hidden = YES;
            //            self.secondContinueBtn.hidden = NO;
            //            self.contentViewHeightContraint.constant -= witnessesFields.frame.size.height;
        }
        
        witnessesFields.witnessNameField.tag = self.witnsNameFieldTag;
        witnessesFields.witnessPhoneField.tag = self.witnsPhoneFieldTag;
        
        [witnessesFields.witnessNameField addTarget:self
                                             action:@selector(textFieldValueChanged:)
                                   forControlEvents:UIControlEventEditingChanged];
        [witnessesFields.witnessPhoneField addTarget:self
                                              action:@selector(textFieldValueChanged:)
                                    forControlEvents:UIControlEventEditingChanged];
        
        if(self.witnsNameFieldTag == 1001){
            self.wNameField2 = witnessesFields.witnessNameField;
            self.wPhoneField2 = witnessesFields.witnessPhoneField;
            self.wNameField2.text = [[[[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.witnessesArray objectAtIndex:1] valueForKey:@"name"] description];
            self.wPhoneField2.text = [[[[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.witnessesArray objectAtIndex:1] valueForKey:@"phone"] description];
        }
        else if(self.witnsNameFieldTag == 1002){
            self.wNameField3 = witnessesFields.witnessNameField;
            self.wPhoneField3 = witnessesFields.witnessPhoneField;
            self.wNameField3.text = [[[[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.witnessesArray objectAtIndex:2] valueForKey:@"name"] description];
            self.wPhoneField3.text = [[[[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.witnessesArray objectAtIndex:2] valueForKey:@"phone"] description];
        }
        else if(self.witnsNameFieldTag == 1003){
            self.wNameField4 = witnessesFields.witnessNameField;
            self.wPhoneField4 = witnessesFields.witnessPhoneField;
            self.wNameField4.text = [[[[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.witnessesArray objectAtIndex:3] valueForKey:@"name"] description];
            self.wPhoneField4.text = [[[[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.witnessesArray objectAtIndex:3] valueForKey:@"phone"] description];
        }
        
        self.witnsNameFieldTag++;
        self.witnsPhoneFieldTag++;
    }
}

-(void)playAudio{
    if([[self.playAudioBtn currentTitle] isEqualToString:@"Sprachnotiz abspielen"]){
        [self.playAudioBtn setTitle:@"Pause/Stopp" forState:UIControlStateNormal];
        // Set the audio file
        NSArray *pathComponents = [NSArray arrayWithObjects:
                                   [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                                   self.audioFileName,
                                   nil];
        NSURL *audioFileUrl = [NSURL fileURLWithPathComponents:pathComponents];
        [self.recorder playAudioWithFileName:audioFileUrl];
    }
    else if([[self.playAudioBtn currentTitle] isEqualToString:@"Pause/Stopp"]){
        [self.playAudioBtn setTitle:@"Sprachnotiz abspielen" forState:UIControlStateNormal];
        [self.recorder stopPlaying];
    }
}

#pragma mark - AudioPlayerDelegate
-(void)audioPlayerFinishPlayingAudio
{
    [self.playAudioBtn setTitle:@"Sprachnotiz abspielen" forState:UIControlStateNormal];
}

-(IBAction)textFieldValueChanged:(UITextField *)textField
{
    if(([textField isEqual:self.nameField] || [textField isEqual:self.phoneField]) && [textField.text length] > 0){
        NSDictionary *witnesses = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:self.nameField.text,self.phoneField.text, nil] forKeys:[NSArray arrayWithObjects:@"name",@"phone", nil]];
        [self.witnessesArray replaceObjectAtIndex:0 withObject:witnesses];
    }
    
    if(([textField isEqual:self.wNameField2] || [textField isEqual:self.wPhoneField2]) && [textField.text length] > 0){
        NSDictionary *witnesses = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:self.wNameField2.text,self.wPhoneField2.text, nil] forKeys:[NSArray arrayWithObjects:@"name",@"phone", nil]];
        [self.witnessesArray replaceObjectAtIndex:1 withObject:witnesses];
    }
    
    if(([textField isEqual:self.wNameField3] || [textField isEqual:self.wPhoneField3]) && [textField.text length] > 0){
        NSDictionary *witnesses = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:self.wNameField3.text,self.wPhoneField3.text, nil] forKeys:[NSArray arrayWithObjects:@"name",@"phone", nil]];
        [self.witnessesArray replaceObjectAtIndex:2 withObject:witnesses];
    }
    
    if(([textField isEqual:self.wNameField4] || [textField isEqual:self.wPhoneField4]) && [textField.text length] > 0){
        NSDictionary *witnesses = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:self.wNameField4.text,self.wPhoneField4.text, nil] forKeys:[NSArray arrayWithObjects:@"name",@"phone", nil]];
        [self.witnessesArray replaceObjectAtIndex:3 withObject:witnesses];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
