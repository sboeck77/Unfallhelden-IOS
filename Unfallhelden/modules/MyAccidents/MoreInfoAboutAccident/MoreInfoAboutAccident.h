//
//  MoreInfoAboutAccident.h
//  Unfallhelden
//
//  Created by Adil Anwer on 4/20/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@interface MoreInfoAboutAccident : UnfallheldenParentVC<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *moreInfoScroll;
@property (nonatomic, strong) IBOutlet UITextView *descriptionTxtView;
@property (nonatomic, weak) IBOutlet UIView *witnessesContainer;
@property (weak, nonatomic) IBOutlet UIView *witnessesFieldsView;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UIButton *playAudioBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *witnessFieldsContainerHeight;
@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *kaScrollView;
@property (nonatomic, strong) NSMutableArray *witnessesArray;

-(IBAction)playAudio;

@end
