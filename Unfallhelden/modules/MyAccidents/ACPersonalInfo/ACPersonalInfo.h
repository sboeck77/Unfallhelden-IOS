//
//  ACPersonalInfo.h
//  Unfallhelden
//
//  Created by Adil Anwer on 4/20/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACPersonalInfo : UnfallheldenParentVC

@property (nonatomic, weak) IBOutlet UITextField *nameField;
@property (nonatomic, weak) IBOutlet UITextField *plateNumField;
@property (nonatomic, weak) IBOutlet UITextField *familyField;

@end
