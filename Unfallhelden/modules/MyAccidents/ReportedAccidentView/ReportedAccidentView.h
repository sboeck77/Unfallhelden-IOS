//
//  ReportedAccidentView.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/10/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@interface ReportedAccidentView : UIView<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UIImage *carImage;
@property (nonatomic, weak) IBOutlet UITableView *reportedAccidentTbl;
@property (nonatomic, strong) NSMutableArray *viewAccidentArray;
@property (nonatomic, strong) IBOutlet UILabel *noAccidentsReportedLbl;

-(void)setupView;

@end
