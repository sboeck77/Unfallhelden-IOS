//
//  RAccidentContentCell.m
//  Unfallhelden
//
//  Created by Adil Anwer on 2/12/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "RAccidentContentCell.h"
#import "PicturesCollectionViewCell.h"

@implementation RAccidentContentCell
@synthesize rAccidentScrollView,contentSizeView,carImage,picturesCollectionView,firstNameField,lastNameField;
@synthesize numPlateField,numPlateACField,carBrandlbl,companyNameField,cityField,houseNumField,zipCodeField,phoneNumField,nameOfLCField;
@synthesize dDTypeOfCarBtn,dropDownCarBrandBtn,dDIncsuranceCompanyBtn,locationField,timeField,dateField,weatherField,infoTextView,firstWitnessNameField;
@synthesize firstWPhoneField,secondWNameField,secondWPhoneField;

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RAccidentContentCell" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)setupView
{
     [self layoutSubviews];
    for(UIView *vw in self.contentSizeView.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            //            UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(10,0,7,26)];
            //            leftView.backgroundColor = [UIColor clearColor];
            //            textField.leftView = leftView;
            //            textField.leftViewMode = UITextFieldViewModeAlways;
            //            textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            textField.delegate = self;
        }
    }
    CGFloat screenWidth = 0.0f;
    if(IS_IPHONE_4){
        screenWidth = 320;
    }
    else if (IS_IPHONE_5){
        screenWidth = 320;
    }
    else if(IS_IPHONE_6){
        screenWidth = 375;
    }
    else if (IS_IPHONE_6_Plus){
        screenWidth = 414;
    }
    CGRect frame = self.frame;
    frame.size.width = screenWidth;
    self.frame = frame;
    self.rAccidentScrollView.frame = self.frame;
    self.rAccidentScrollView.contentSize = CGSizeMake(screenWidth, self.contentSizeView.frame.size.height+100);
    NSString *identifier = @"PicturesCollectionViewCell";
    UINib *nib = [UINib nibWithNibName:@"PicturesCollectionViewCell" bundle: nil];
    [self.picturesCollectionView registerNib:nib forCellWithReuseIdentifier:identifier];
    self.carImage = [UIImage imageNamed:@"accidentImg"];
}

#pragma mark - UICollectionView delegate and datasource methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 15;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PicturesCollectionViewCell *cell=[self.picturesCollectionView dequeueReusableCellWithReuseIdentifier:@"PicturesCollectionViewCell" forIndexPath:indexPath];
    cell.carPictureImageView.image = self.carImage;
    //    cell.backgroundColor=[UIColor greenColor];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(140, 110);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    long ind = indexPath.item;
    long section=indexPath.section;
    NSLog(@"%ld,%ld",section,ind);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
