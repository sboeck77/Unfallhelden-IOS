//
//  RAccidentContentCell.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/12/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@interface RAccidentContentCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate>

@property (nonatomic, strong) UIImage *carImage;
@property (nonatomic, weak) IBOutlet UICollectionView *picturesCollectionView;
@property (nonatomic, weak) IBOutlet UITextField *numPlateField;
@property (nonatomic, weak) IBOutlet UITextField *firstNameField;
@property (nonatomic, weak) IBOutlet UITextField *lastNameField;
@property (nonatomic, weak) IBOutlet UITextField *streetNameField;
@property (nonatomic, weak) IBOutlet UITextField *houseNumField;
@property (nonatomic, weak) IBOutlet UITextField *cityField;
@property (nonatomic, weak) IBOutlet UITextField *zipCodeField;
@property (nonatomic, weak) IBOutlet UITextField *phoneNumField;
@property (nonatomic, weak) IBOutlet UITextField *numPlateACField;
@property (nonatomic, weak) IBOutlet UITextField *companyNameField;
@property (nonatomic, weak) IBOutlet UITextField *nameOfLCField;
@property (nonatomic, weak) IBOutlet UITextField *locationField;
@property (nonatomic, weak) IBOutlet UITextField *timeField;
@property (nonatomic, weak) IBOutlet UITextField *dateField;
@property (nonatomic, weak) IBOutlet UITextField *weatherField;
@property (nonatomic, weak) IBOutlet UITextView *infoTextView;
@property (nonatomic, weak) IBOutlet UILabel *carBrandlbl;
@property (nonatomic, weak) IBOutlet UILabel *typeOfCarLbl;
@property (nonatomic, weak) IBOutlet UILabel *insuranceCompanyLbl;
@property (nonatomic, weak) IBOutlet UITextField *firstWitnessNameField;
@property (nonatomic, weak) IBOutlet UITextField *secondWNameField;
@property (nonatomic, weak) IBOutlet UITextField *firstWPhoneField;
@property (nonatomic, weak) IBOutlet UITextField *secondWPhoneField;
@property (nonatomic, weak) IBOutlet TPKeyboardAvoidingScrollView *rAccidentScrollView;
@property (nonatomic, weak) IBOutlet UIView *contentSizeView;
@property (weak, nonatomic) IBOutlet UIButton *dropDownCarBrandBtn;
@property (weak, nonatomic) IBOutlet UIButton *dDTypeOfCarBtn;
@property (weak, nonatomic) IBOutlet UIButton *dDIncsuranceCompanyBtn;

-(void)setupView;

@end
