//
//  ReportedAccidentView.m
//  Unfallhelden
//
//  Created by Adil Anwer on 2/10/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "ReportedAccidentView.h"
#import "RAccidentContentCell.h"
#import "PicturesCollectionViewCell.h"
#import "ACPersonalInfo.h"
#import "ACAddress.h"
#import "ACCarDetails.h"
#import "LocationTimeWeather.h"
#import "MoreInfoAboutAccident.h"
#import "Pictures.h"
static NSString * const ReportedAccidentCellIdentifier = @"ReportedAccidentCell";

@implementation ReportedAccidentView
@synthesize reportedAccidentTbl, viewAccidentArray, noAccidentsReportedLbl;

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ReportedAccidentView" owner:self options:nil];
        self = [topLevelObjects objectAtIndex:0];
    }
    return self;
}

-(void)setupView
{
    [self setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self setNeedsLayout];
    [self layoutIfNeeded];
    [self layoutSubviews];
    
    CGFloat screenWidth = 0.0f;
    if(IS_IPHONE_4){
        screenWidth = 320;
    }
    else if (IS_IPHONE_5){
        screenWidth = 320;
    }
    else if(IS_IPHONE_6){
        screenWidth = 375;
    }
    else if (IS_IPHONE_6_Plus){
        screenWidth = 414;
    }
    CGRect frame = self.frame;
    frame.size.width = screenWidth;
    self.frame = frame;
    self.viewAccidentArray = [[NSMutableArray alloc] initWithObjects:@"Persönliche Daten zum Unfallursacher",@"Adresse des Unfallverursachers",@"Angaben zum Fahrzeug des Unfallverursachers",@"Unfallstelle/Uhrzeit/Wetter",@"Mehr Informationen über den Unfall",@"Fotos", nil];
    self.reportedAccidentTbl.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.reportedAccidentTbl reloadData];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self addGestureRecognizer:tapRecognizer];
}

-(void)didTapAnywhere:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kMyAccidentsDidTappedAnyWhere object:sender];
}


#pragma mark - UITableViewDataSourceMethods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.viewAccidentArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *viewAccodentCell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:ReportedAccidentCellIdentifier];
    if (viewAccodentCell == nil) {
        viewAccodentCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ReportedAccidentCellIdentifier];
    }
    [self configureBasicCell:viewAccodentCell atIndexPath:indexPath];
    return viewAccodentCell;
}

#pragma mark - UITableViewDelegateMethods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self heightForBasicCellAtIndexPath:indexPath];
    //    return 65;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        ACPersonalInfo *personalInfo = [[ACPersonalInfo alloc] init];
        [[GlobalScopeObjects sharedSingletonInstance].myAccidentsVC.navigationController pushViewController:personalInfo animated:YES];
    }
    else if (indexPath.row == 1){
        ACAddress *address = [[ACAddress alloc] init];
        [[GlobalScopeObjects sharedSingletonInstance].myAccidentsVC.navigationController pushViewController:address animated:YES];
    }
    else if (indexPath.row == 2){
        ACCarDetails *carDetails = [[ACCarDetails alloc] init];
        [[GlobalScopeObjects sharedSingletonInstance].myAccidentsVC.navigationController pushViewController:carDetails animated:YES];
    }
    else if (indexPath.row == 3){
        LocationTimeWeather *ltw = [[LocationTimeWeather alloc] init];
        [[GlobalScopeObjects sharedSingletonInstance].myAccidentsVC.navigationController pushViewController:ltw animated:YES];
    }
    else if (indexPath.row == 4){
        MoreInfoAboutAccident *moreInfo = [[MoreInfoAboutAccident alloc] init];
        [[GlobalScopeObjects sharedSingletonInstance].myAccidentsVC.navigationController pushViewController:moreInfo animated:YES];
    }
    else if (indexPath.row == 5){
        Pictures *pictures = [[Pictures alloc] init];
        [[GlobalScopeObjects sharedSingletonInstance].myAccidentsVC.navigationController pushViewController:pictures animated:YES];
    }
}

- (CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static UITableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ReportedAccidentCellIdentifier];
    });
    
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 15.0f; // Add 1.0f for the cell separator height
}

- (void)configureBasicCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    //    RSSItem *item = self.feedItems[indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = [self.viewAccidentArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(IS_IPHONE_4 || IS_IPHONE_5){
        cell.textLabel.font = [UIFont regularUnfallheldenFontWithSize:13];
    }
    if(IS_IPHONE_6 || IS_IPHONE_6_Plus){
        cell.textLabel.font = [UIFont regularUnfallheldenFontWithSize:15];
    }
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

@end
