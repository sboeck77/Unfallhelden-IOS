//
//  Pictures.m
//  Unfallhelden
//
//  Created by Adil Anwer on 4/20/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "Pictures.h"
#import "PicturesCollectionViewCell.h"

//#define pictureDownloadBaseUrl @"http://api.unfallhelden.de/data/uploads/images/"
#define pictureDownloadBaseUrl @"http://unfallhelden.de/data/uploads/images/"

@interface Pictures ()

@end

@implementation Pictures
@synthesize imagesArray,picturesCollectionView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [super setupMenuBarButtonItems];
    [self setTitle:@"Fotos"];
    self.imagesArray = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.photoArray;
    [self.picturesCollectionView reloadData];
    NSString *identifier = @"PicturesCollectionViewCell";
    UINib *nib = [UINib nibWithNibName:@"PicturesCollectionViewCell" bundle: nil];
    [self.picturesCollectionView registerNib:nib forCellWithReuseIdentifier:identifier];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.imagesArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PicturesCollectionViewCell *cell=[self.picturesCollectionView dequeueReusableCellWithReuseIdentifier:@"PicturesCollectionViewCell" forIndexPath:indexPath];
    NSString *urlStr= [NSString stringWithFormat:@"%@%@",pictureDownloadBaseUrl,[[self.imagesArray objectAtIndex:indexPath.item] valueForKey:@"photo"]];
    NSURL *imageUrl = [NSURL URLWithString:urlStr];
    [cell.carPictureImageView sd_setImageWithURL:imageUrl];
    //    if(self.imgDeleteBtnHidden == YES){
    cell.deleteImgBtn.hidden = YES;
    //    }
    //    else{
    //        cell.deleteImgBtn.hidden = NO;
    //    }
    //    cell.deleteImgBtn.tag = indexPath.item;
    //    [cell.deleteImgBtn addTarget:self action:@selector(deleteCellAndImage:) forControlEvents:UIControlEventTouchUpInside];
    //    cell.backgroundColor=[UIColor greenColor];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPHONE_4 || IS_IPHONE_5){
        return CGSizeMake(145, 110);
    }
    else if(IS_IPHONE_6) {
        return CGSizeMake(170, 140);
    }
    else{
        return CGSizeMake(189, 165);
    }
    /*
     [self _configureCell:sizingCell forIndexPath:indexPath];
     return [sizingCell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
     */
}

/*
 - (void)_configureCell:(PicturesCollectionViewCell *)cell forIndexPath:(NSIndexPath *)indexPath
 {
 sizingCell.carPictureImageView.image = [self.carImagesArray objectAtIndex:indexPath.row];
 }
 */

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    long ind = indexPath.item;
    long section=indexPath.section;
    NSLog(@"%ld,%ld",section,ind);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
