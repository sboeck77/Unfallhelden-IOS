//
//  ACCarDetails.h
//  Unfallhelden
//
//  Created by Adil Anwer on 4/20/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACCarDetails : UnfallheldenParentVC

@property (nonatomic, weak) IBOutlet UITextField *carBrandField;
@property (nonatomic, weak) IBOutlet UITextField *carTypeField;
@property (nonatomic, weak) IBOutlet UITextField *insuranceComapnyField;
@property (nonatomic, weak) IBOutlet UITextField *companyNameField;
@property (nonatomic, weak) IBOutlet UITextField *leasingCompanyField;
@property (nonatomic, weak) IBOutlet UITextField *addtionalAddressField;
@property (nonatomic, weak) IBOutlet UIButton *leasingCompanyCheck;
@property (nonatomic, weak) IBOutlet UIButton *companyCheck;

@end
