//
//  ACCarDetails.m
//  Unfallhelden
//
//  Created by Adil Anwer on 4/20/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "ACCarDetails.h"

@interface ACCarDetails ()

@end

@implementation ACCarDetails
@synthesize carBrandField,carTypeField,insuranceComapnyField,leasingCompanyCheck,companyCheck,companyNameField,leasingCompanyField;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"Angaben zum Fahrzeig des Unfallverursachers"];
    // Do any additional setup after loading the view from its nib.
    [super setupMenuBarButtonItems];
    for(UIView *vw in self.view.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            //            textField.delegate = self;
            if(IS_IPHONE_5 || IS_IPHONE_4){
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:11.5]];
            }
            else{
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:14]];
            }
        }
    }
    if(![[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acCarBrand isEqualToString:@"<null>"]){
        self.carBrandField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acCarBrand;
    }
    self.carTypeField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acCarType;
    if(![[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acInsuranceCompany isEqualToString:@"<null>"]){
        self.insuranceComapnyField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acInsuranceCompany;
    }
    if([[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acCompanyName length]>0){
        self.companyNameField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acCompanyName;
        [self.companyCheck setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
    if ([[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acLeasingCompany length]>0){
        self.leasingCompanyField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acLeasingCompany;
        [self.leasingCompanyCheck setBackgroundImage:[UIImage imageNamed:@"checkedBox"] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
