//
//  AccidentsVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/7/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAccidentsVC : UnfallheldenParentVC<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    BOOL pageControlBeingUsed;
}

@property (nonatomic,weak) IBOutlet UIPageControl *pageControl;
@property (nonatomic,weak) IBOutlet UILabel *unfallGemeldetTxtLineLbl;
@property (nonatomic,weak) IBOutlet UILabel *gutachtenTxtLineLbl;
@property (nonatomic,weak) IBOutlet UILabel *schadenTxtLineLbl;
@property (nonatomic,weak) IBOutlet UILabel *werstattterminTxtLineLbl;
@property (nonatomic, retain) IBOutlet UIScrollView* scrollView;
@property (nonatomic, retain) IBOutlet UIScrollView *lblScrollview;
@property (nonatomic, retain) IBOutlet UIView *titlesContainer;
@property (nonatomic, weak) IBOutlet UIView *contentSizeview;
@property (nonatomic, weak) IBOutlet UIImageView *backgroundImg;
@property (nonatomic) CGRect originalTitleScrollFrame;
@property (nonatomic, strong) NSMutableArray *reportedAccidentsArray;
@property (nonatomic, strong) UITableView *accidentsTbl;

- (IBAction)changePage;

@end
