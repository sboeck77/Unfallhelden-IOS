//
//  ACAddress.m
//  Unfallhelden
//
//  Created by Adil Anwer on 4/20/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "ACAddress.h"

@interface ACAddress ()

@end

@implementation ACAddress
@synthesize phoneField,streetNameField,streetNumField,cityField,zipCodeField,addtionalAddressField;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"Adresse des Unfallverursachers"];
    [super setupMenuBarButtonItems];
    for(UIView *vw in self.view.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            //            textField.delegate = self;
            if(IS_IPHONE_5 || IS_IPHONE_4){
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:11.5]];
            }
            else{
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:14]];
            }
        }
    }
    self.phoneField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acPhone;
    self.streetNameField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acStreetName;
    self.streetNumField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acStreetNumber;
    self.cityField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acCity;
    self.zipCodeField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acZipCode;
    self.addtionalAddressField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.acAdditionalAddress;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
