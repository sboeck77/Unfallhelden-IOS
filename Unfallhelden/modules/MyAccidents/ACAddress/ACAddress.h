//
//  ACAddress.h
//  Unfallhelden
//
//  Created by Adil Anwer on 4/20/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACAddress : UnfallheldenParentVC

@property (nonatomic, weak) IBOutlet UITextField *phoneField;
@property (nonatomic, weak) IBOutlet UITextField *streetNameField;
@property (nonatomic, weak) IBOutlet UITextField *streetNumField;
@property (nonatomic, weak) IBOutlet UITextField *cityField;
@property (nonatomic, weak) IBOutlet UITextField *zipCodeField;
@property (nonatomic, weak) IBOutlet UITextField *addtionalAddressField;

@end
