//
//  LocationTimeWeather.m
//  Unfallhelden
//
//  Created by Adil Anwer on 4/20/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "LocationTimeWeather.h"

@interface LocationTimeWeather ()

@end

@implementation LocationTimeWeather
@synthesize mapLocationField,timeField,dateField,weatherField;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"Unfallstelle/Uhrzeit/Wetter"];
    [super setupMenuBarButtonItems];
    for(UIView *vw in self.view.subviews) {
        if([vw isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField*)vw;
            textField.layer.cornerRadius = 2.0f;
            textField.layer.masksToBounds = YES;
            textField.layer.borderColor=[[UIColor whiteColor]CGColor];
            textField.layer.borderWidth= 1.3f;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
            [textField setTextColor:[UIColor whiteColor]];
            UIColor *color = [UIColor whiteColor];
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: color}];
            //            textField.delegate = self;
            
            if(IS_IPHONE_5 || IS_IPHONE_4){
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:11.5]];
            }
            else{
                [textField setFont:[UIFont regularUnfallheldenFontWithSize:14]];
            }
        }
    }
    
    self.mapLocationField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.placeName;
    if(![[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.accidentOccuredDate isEqualToString:@"0000-00-00"]){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *dateReg = [formatter dateFromString:[GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.accidentOccuredDate];
        [formatter setDateFormat:@"dd.MM.yyy"];
        self.dateField.text = [formatter stringFromDate:dateReg];
    }
    self.timeField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.accidentOccuredTime;
    self.weatherField.text = [GlobalScopeObjects sharedSingletonInstance].reportedAccidentData.weather;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
