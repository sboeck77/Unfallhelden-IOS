//
//  AboutVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 1/27/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutVC : UnfallheldenParentVC<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *aboutTblView;
@property (nonatomic, strong) NSMutableArray *aboutTblArray;

@end
