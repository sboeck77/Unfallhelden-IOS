//
//  AboutVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 1/27/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "AboutVC.h"

static NSString * const AboutCellIdentifier = @"AboutCellIdentifier";

@interface AboutVC ()

@property (nonatomic, strong) UnfallheldenCustomPopup *customPopup;

@end

@implementation AboutVC
@synthesize aboutTblView,aboutTblArray,customPopup;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super setupMenuBarButtonItems];
    //    self.aboutTblArray = [[NSMutableArray alloc] initWithObjects:@"Version 1.0",@"About us",@"Terms and Conditions",@"Privacy Policy",@"Delete Cache and Saved Data", nil];
    self.aboutTblArray = [[NSMutableArray alloc] initWithObjects:@"Version 1.0",@"Über uns",@"AGB",@"Datenschutz",@"Cache und gespeicherte Daten löschen", nil];
    self.aboutTblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.aboutTblView reloadData];
    //GoogleAnalytics
//    self.screenName = @"AboutScreen";
}

-(void)viewDidAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"AboutScreen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - UITableViewDataSourceMethods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.aboutTblArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *aboutTblCell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:AboutCellIdentifier];
    if (aboutTblCell == nil) {
        aboutTblCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AboutCellIdentifier];
    }
    [self configureBasicCell:aboutTblCell atIndexPath:indexPath];
    return aboutTblCell;
}

#pragma mark - UITableViewDelegateMethods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self heightForBasicCellAtIndexPath:indexPath];
    //    return 65;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
     if(indexPath.row == [self.aboutTblArray indexOfObject:[self.aboutTblArray lastObject]]){
     [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:kUserDefaultsUserAlreadyLoggedIn];
     [[NSUserDefaults standardUserDefaults] synchronize];
     [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNameUserLogout object:nil userInfo:nil];
     [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Logged Out" andMessage:@"You are successfully logged out."];
     }
     */
    if(indexPath.row == 1){
        self.customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
        
        [self.view addSubview:self.customPopup];
        self.customPopup.frame = self.view.frame;
        
        self.customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
        self.customPopup.unfallheldenPopup.layer.masksToBounds = YES;
        
        [self.customPopup displayPopup];
        
        self.customPopup.descriptionTextView.text = @"Die Unfallhelden sind ein Team von Experten in der Unfallabwicklung mit der Erfahrung aus vielen tausend Autounfällen.\n\nNach einem Autounfall übernehmen wir für den Geschädigten die komplette Unfallabwicklung aus einer Hand.\n\nWir verfügen über ein deutschlandweites Netzwerk aus versicherungsunabhängigen Gutachtern, Werkstätten, Rechtsanwälten und Mietwagenunternehmen, so dass wir für jede einzelne Leistung den richtigen Ansprechpartner zur Verfügung stellen können.\n\nDadurch gewährleisten wir, dass unser Kunde zu seinem Recht kommt und ihm der entstandene Schaden vollständig ersetzt wird.\n\nWarum das Schadenmanagement nicht dem gegnerischen Versicherer überlassen?\n\nGanz einfach: Wer zahlen muss, ist nicht Ihr Freund!\n\nDer gegnerische Versicherer verhilft Ihnen nicht zu Ihrem Recht, er spart sich durch das Schadenmanagement nur Geld. Deshalb arbeiten wir nicht für Versicherer, wir sind Ihre Unfallhelden!\n\nUnabhängig und ausschließlich im Interesse unseres Kunden!\n\nhttps://www.youtube.com/watch?v=b4uDYGLuLeY";
        self.customPopup.titleLabel.text = @"Uber uns";
        
        [self.customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
        [self.customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
    }
    else if (indexPath.row == 2){
        self.customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
        
        [self.view addSubview:self.customPopup];
        self.customPopup.frame = self.view.frame;
        
        self.customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
        self.customPopup.unfallheldenPopup.layer.masksToBounds = YES;
        
        [self.customPopup displayPopup];
        
        self.customPopup.descriptionTextView.text = UnfallheldenTermsAndConditions;
        self.customPopup.titleLabel.text = @"AGB";
        
        [self.customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
        [self.customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
    }
    else if (indexPath.row == 3){
        
        self.customPopup = [[UnfallheldenCustomPopup alloc] initWithDialogPopupNib];
        
        [self.view addSubview:self.customPopup];
        self.customPopup.frame = self.view.frame;
        
        self.customPopup.unfallheldenPopup.layer.cornerRadius = 5.0f;
        self.customPopup.unfallheldenPopup.layer.masksToBounds = YES;
        
        [self.customPopup displayPopup];
        
        self.customPopup.descriptionTextView.text = UnfallheldenPrivacyPolicy;
        self.customPopup.titleLabel.text = @"Datenschutz";
        
        [self.customPopup.titleLabel setFont:[UIFont boldUnfallheldenFontWithSize:17]];
        [self.customPopup.descriptionTextView setFont:[UIFont regularUnfallheldenFontWithSize:15]];
        self.customPopup.descriptionTextView.textAlignment = NSTextAlignmentLeft;
    }
    else if (indexPath.row == 4){
        NSString *message = [NSString stringWithFormat:@"Möchten Sie den Chache und alle gespeicherten Daten wirklich löschen?"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"Abbrechen" otherButtonTitles:@"Löschen", nil];
        alert.tag = 123;
        [alert show];
    }
}

- (CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static UITableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AboutCellIdentifier];
    });
    
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 15.0f; // Add 1.0f for the cell separator height
}

- (void)configureBasicCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    //    RSSItem *item = self.feedItems[indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = [self.aboutTblArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(IS_IPHONE_4 || IS_IPHONE_5){
        cell.textLabel.font = [UIFont regularUnfallheldenFontWithSize:14];
    }
    if(IS_IPHONE_6 || IS_IPHONE_6_Plus){
        cell.textLabel.font = [UIFont regularUnfallheldenFontWithSize:16];
    }
    cell.textLabel.textColor = [UIColor whiteColor];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Löschen"]) {
        [self deleteLocalSavedData];
    }
}

-(void)deleteLocalSavedData{
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* foofile = [documentsPath stringByAppendingPathComponent:MyAccidentsFileName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
    if(fileExists == TRUE){
        [self removeSavedFile:MyAccidentsFileName];
    }
    
    NSString* foofile1 = [documentsPath stringByAppendingPathComponent:UserProfileFileName];
    BOOL fileExists1 = [[NSFileManager defaultManager] fileExistsAtPath:foofile1];
    if(fileExists1 == TRUE){
        [self removeSavedFile:UserProfileFileName];
    }
    
    NSString* foofile2 = [documentsPath stringByAppendingPathComponent:UserCarsFileName];
    BOOL fileExists2 = [[NSFileManager defaultManager] fileExistsAtPath:foofile2];
    if(fileExists2 == TRUE){
        [self removeSavedFile:UserCarsFileName];
    }
    
    NSString* savedNotificaitonFile = [documentsPath stringByAppendingPathComponent:NotificationsFileName];
    BOOL notificationFileExist = [[NSFileManager defaultManager] fileExistsAtPath:savedNotificaitonFile];
    
    if(notificationFileExist == TRUE){
        [self removeSavedFile:NotificationsFileName];
    }
}

- (void)removeSavedFile:(NSString*)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
