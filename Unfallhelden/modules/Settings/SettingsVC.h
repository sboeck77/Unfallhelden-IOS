//
//  OptionsVC.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/8/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UnfallheldenParentVC<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *settingsTitlesArray;
@property (nonatomic, weak) IBOutlet UITableView *settingsTblView;

@end
