//
//  SettingsCell.h
//  Unfallhelden
//
//  Created by Adil Anwer on 3/17/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *settingsTitleLabel;
@property (nonatomic, weak) IBOutlet UIButton *onOffButton;

@end
