//
//  OptionsVC.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/8/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "SettingsVC.h"
#import "SettingsCell.h"

static NSString * const SettingsCellIdentifier = @"SettingsCellIdentifier";

@interface SettingsVC ()

@end

@implementation SettingsVC
@synthesize settingsTitlesArray,settingsTblView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [super setupMenuBarButtonItems];
    self.settingsTitlesArray = [[NSMutableArray alloc] initWithObjects:@"System notifications",@"Push notifications",@"Connect with Facebook", nil];
    [self.settingsTblView reloadData];
    self.settingsTblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.navigationController.navigationBar.translucent = YES;
    
    //GoogleAnalytics
    self.screenName = @"SettingsScreen";
}

#pragma mark - UITableViewDataSourceMethods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.settingsTitlesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SettingsCell *notificationsCell = (SettingsCell*)[tableView dequeueReusableCellWithIdentifier:SettingsCellIdentifier];
    if (notificationsCell == nil) {
        notificationsCell = [[SettingsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SettingsCellIdentifier];
    }
    [self configureBasicCell:notificationsCell atIndexPath:indexPath];
    return notificationsCell;
}

#pragma mark - UITableViewDelegateMethods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self heightForBasicCellAtIndexPath:indexPath];
    //    return 65;
}

- (CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static SettingsCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [[SettingsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SettingsCellIdentifier];
    });
    
    [self configureBasicCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1.0f; // Add 1.0f for the cell separator height
}

- (void)configureBasicCell:(SettingsCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    //    RSSItem *item = self.feedItems[indexPath.row];
    cell.settingsTitleLabel.text = [self.settingsTitlesArray objectAtIndex:indexPath.row];
    cell.settingsTitleLabel.font = [UIFont regularUnfallheldenFontWithSize:21];
    cell.settingsTitleLabel.textColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
