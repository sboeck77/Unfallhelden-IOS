//
//  AudioRecorder.m
//  Unfallhelden
//
//  Created by Adil Anwer on 2/22/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "AudioRecorder.h"
#import <QuartzCore/QuartzCore.h>

@implementation AudioRecorder
@synthesize audioPlayerDelegate, audioFileName, recorder;

+(AudioRecorder *)sharedSingletonInstance
{
    static AudioRecorder *sharedSingleton;
    @synchronized(self){
        if(!sharedSingleton){
            sharedSingleton = [[AudioRecorder alloc] init];
        }
    }
    
    return sharedSingleton;
}

-(void)setupAudioRecorder
{
    // Disable Stop/Play button when application launches
    //    [stopButton setEnabled:NO];
    //    [playButton setEnabled:NO];
    
    self.audioFileName = @"UnfallheldenAudioMemo.m4a";
    
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               self.audioFileName,
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    //    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    NSError *setCategoryError = nil;
    if (![session setCategory:AVAudioSessionCategoryPlayAndRecord
                  withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker
                        error:&setCategoryError]) {
        // handle error
    }
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    self.audioFilePath = outputFileURL;
    
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
#if TARGET_IPHONE_SIMULATOR
    // where are you?
    NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
#endif
    
}


- (void)convert {
    
    int *type = nil;
    type = PRESET_VOICE;
    
    NSString *outputName = @"";
    outputName = [outputName stringByAppendingString:[NSString stringWithFormat:@"UnfallheldenAudioMemo"]];
    outputName = [outputName stringByAppendingString:@".mp3"];
    
    MP3Converter *mp3Converter = [[MP3Converter alloc] initWithPreset:type];
    mp3Converter.delegate = self;
    [mp3Converter initializeLame];
    mp3Converter.conversionStartPoint = 2.0;
    mp3Converter.conversionLength = 35.0;
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* savedFile = [documentsPath stringByAppendingPathComponent:self.audioFileName];
    [[UnfallheldenUtilities sharedSingletonInstance] showProgressAlertTitle:@"Lädt..."
                                                                    message:nil];
    
    [mp3Converter convertMP3WithFilePath:savedFile outputName:outputName];
    
//    NSString* savedFileMp3 = [documentsPath stringByAppendingPathComponent:outputName];
//    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:savedFileMp3];
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               outputName,
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    self.audioFilePath = outputFileURL;
}

#pragma mark - MP3 Converter Engine Callbacks

- (void)convertFailed:(MP3Converter *)converter {
    
}

- (void)convertDone:(MP3Converter *)converter {
    [[UnfallheldenHttpClient sharedClient] uploadRecordingWithAudioFilePath:self.audioFilePath WithCompletionBlock:^(NSData *data) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *dataDictionary = [responseString JSONValue];
        //            NSDictionary *userData = [[dataDictionary valueForKey:@"data"] description];
        int status = [[[dataDictionary valueForKey:@"status"] description] intValue];
        if(status == 1){
            [GlobalScopeObjects sharedSingletonInstance].audioFileName = [[dataDictionary valueForKey:@"data"] description];
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Aufnahme wurde erfolgreich hochgeladen" andMessage:@"Ihre Aufnahme wurde erfolgreich an unsere Datenbank übermittelt."];
        }
        else{
            [[UnfallheldenUtilities sharedSingletonInstance] showAlertWithTitle:@"Audio upload Failed" andMessage:@"There is some problem uploading an audio with Unfallhelden app please try again"];
        }
        [[UnfallheldenUtilities sharedSingletonInstance] hideProgressAlert];
    }];
}

- (void)onConvertionProgress:(NSNumber *)percentage {
    
}

-(void)setupAudioRecorderWithURL:(NSURL *)url
{
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    //    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    NSError *setCategoryError = nil;
    if (![session setCategory:AVAudioSessionCategoryPlayAndRecord
                  withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker
                        error:&setCategoryError]) {
        // handle error
    }
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:NULL];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
#if TARGET_IPHONE_SIMULATOR
    // where are you?
    NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
#endif
}

- (void)recordAudio{
    // Stop the audio player before recording
    if (player.playing) {
        [player stop];
    }
    
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [recorder record];
        //        [recordPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
        
    } else {
        
        // Pause recording
        //        [recorder pause];
        //        [recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    }
    
    //    [stopButton setEnabled:YES];
    //    [playButton setEnabled:NO];
}

- (void)stopRecording{
    [recorder stop];
    [self convert];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}

- (void)playAudio{
    if (!recorder.recording){
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
        [player setDelegate:self];
        [player setVolume: 1.0];
        [player play];
    }
}

-(void)playAudioWithFileName:(NSURL*)fileUrl
{
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:fileUrl error:nil];
    [player setDelegate:self];
    [player setVolume: 1.0];
    [player play];
}

-(void)stopPlaying{
    [player stop];
}

- (void)removeAudio
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:self.audioFileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        UIAlertView *removeSuccessFulAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"Die Aufnahme wurde gelöscht" delegate:self cancelButtonTitle:@"Schließen" otherButtonTitles:nil];
        [removeSuccessFulAlert show];
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

- (void)removeAudioWithNoAlert
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:self.audioFileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    //    [recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    
    //    [stopButton setEnabled:NO];
    //    [playButton setEnabled:YES];
}

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [self.audioPlayerDelegate audioPlayerFinishPlayingAudio];
}

@end
