//
//  AudioRecorder.h
//  Unfallhelden
//
//  Created by Adil Anwer on 2/22/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "MP3Converter.h"

@protocol AudioPlayerDelegate <NSObject>

@required
-(void)audioPlayerFinishPlayingAudio;

@end

@interface AudioRecorder : NSObject<AVAudioRecorderDelegate, AVAudioPlayerDelegate,MP3ConverterDelegate>
{
    AVAudioPlayer *player;
}

@property (nonatomic, strong) id <AudioPlayerDelegate> audioPlayerDelegate;
@property (nonatomic, strong) NSString *audioFileName;
@property (nonatomic, strong) NSURL *audioFilePath;
@property (nonatomic, strong) AVAudioRecorder *recorder;

+(AudioRecorder *)sharedSingletonInstance;

-(void)setupAudioRecorder;
-(void)setupAudioRecorderWithURL:(NSURL*)url;
- (void)recordAudio;
- (void)stopRecording;
- (void)playAudio;
- (void)removeAudio;
- (void)removeAudioWithNoAlert;
- (void)stopPlaying;
-(void)playAudioWithFileName:(NSURL*)fileUrl;

@end
