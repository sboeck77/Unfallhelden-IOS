//
//  CarBrands.h
//  Unfallhelden
//
//  Created by Adil Anwer on 4/13/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarBrand : NSObject

@property (nonatomic, strong) NSString *carBrandName;
@property (nonatomic, strong) NSString *carBrandId;

@end
