//
//  ReportAccidentData.m
//  Unfallhelden
//
//  Created by Adil Anwer on 3/30/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "ReportAccidentData.h"

@implementation ReportAccidentData
@synthesize firstName,lastName,phoneNumber,email,carBrand,password,confirmPassword,isTermsAccepted,acFirstName,acLastName;
@synthesize acPlateNumber,acPlateNumNotSure,acPhone,acStreetName,acStreetNumber,acCity,acZipCode,acCountry,acAdditionalAddress;
@synthesize acCarBrand,acCarType,acInsuranceCompany,acCompanyCar,acLeaseCar,acCompanyName,acLeasingCompany,placeName,time;
@synthesize date,weather,accidentDescription,witnessesArray, userLatitude, userLongitude, audioFileLink,plateNumber,accidentId, accidentHash,accidentCausesId,audioFileId,photoArray,accidentStatus;

+(ReportAccidentData *)sharedSingletonInstance
{
    static ReportAccidentData *sharedSingleton;
    @synchronized(self){
        if(!sharedSingleton){
            sharedSingleton = [[ReportAccidentData alloc] init];
        }
    }
    
    return sharedSingleton;
}

-(void)prepareStepOneDataWithFirstName :(NSString*)userFirstName lastName:(NSString*)userLastName phoneNumber:(NSString*)userPhoneNumber email:(NSString*)userEmail plateNumber:(NSString*)userPlateNumber termsAccepted:(BOOL)termsAccepted acCompanyCar:(BOOL)companyCar acLeaseCar:(BOOL)leaseCar acCompanyName:(NSString *)userACCompanyName acLeasingCompany:(NSString *)userACLeasingCompany
{
    self.firstName = userFirstName;
    self.lastName = userLastName;
    self.phoneNumber = userPhoneNumber;
    self.email = userEmail;
    self.plateNumber = userPlateNumber;
    self.isTermsAccepted = termsAccepted;
    self.acCompanyCar = companyCar;
    self.acLeaseCar = leaseCar;
    self.acCompanyName = userACCompanyName;
    self.acLeasingCompany = userACLeasingCompany;
}

-(void)prepareStepOneDataLoggedInUserWithCarId:(NSString *)userCarId andRegistrationNumber:(NSString *)userRegistrationNumber acCompanyCar:(BOOL)companyCar acLeaseCar:(BOOL)leaseCar acCompanyName:(NSString *)userACCompanyName acLeasingCompany:(NSString *)userACLeasingCompany
{
    self.carId = userCarId;
    self.registrationNumber = userRegistrationNumber;
    self.acCompanyCar = companyCar;
    self.acLeaseCar = leaseCar;
    self.acCompanyName = userACCompanyName;
    self.acLeasingCompany = userACLeasingCompany;
}

-(void)prepareMoreDataWithACFirstName:(NSString *)userACFirstName acLastName:(NSString *)userACLastName acPlateNumber:(NSString *)userACPlateNumber acPlateNumSure:(BOOL)plateNumSure acPhone:(NSString *)userACPhone acStreetName:(NSString *)userACStreetName acStreetNum:(NSString *)userACStreetNum acCity:(NSString *)userACCity acZipCode:(NSString *)userACZipCode acCountry:(NSString *)userACCountry acAdditionalAddress:(NSString *)userACAdditonalAddress acCarBrand:(NSString *)userACCarBrand acCarType:(NSString *)userACCarType acInsuranceCompany:(NSString *)userACInsuranceCompany
{
    self.acFirstName = userACFirstName;
    self.acLastName = userACLastName;
    self.acPlateNumber = userACPlateNumber;
    self.acPlateNumNotSure = plateNumSure;
    self.acPhone = userACPhone;
    self.acStreetName = userACStreetName;
    self.acStreetNumber = userACStreetNum;
    self.acCity = userACCity;
    self.acZipCode = userACZipCode;
    self.acCountry = userACCountry;
    self.acAdditionalAddress = userACAdditonalAddress;
    self.acCarBrand = userACCarBrand;
    self.acCarType = userACCarType;
    self.acInsuranceCompany = userACInsuranceCompany;
}

-(void)prepareLTWDataWithLocationName:(NSString *)location latitude:(double)latitude longitude:(double)longitude time:(NSString *)enteredTime date:(NSString *)enteredDate weather:(NSString *)enteredWeather
{
    self.userLatitude = latitude;
    self.userLongitude = longitude;
    self.placeName = location;
    self.time = enteredTime;
    self.date = enteredDate;
    self.weather = enteredWeather;
}

-(void)prepareWitnessesDataWithDescription:(NSString *)enetredDescription audioFileLink:(NSString *)link witnessesArray:(NSMutableArray *)givenWitnessesArray
{
    self.accidentDescription = enetredDescription;
    self.audioFileLink = link;
    self.witnessesArray = givenWitnessesArray;
}

@end
