//
//  ReportAccidentData.h
//  Unfallhelden
//
//  Created by Adil Anwer on 3/30/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportAccidentData : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *carBrand;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *confirmPassword;
@property (nonatomic, strong) NSString *plateNumber;
@property (nonatomic) BOOL isTermsAccepted;
@property (nonatomic, strong) NSString *acFirstName;
@property (nonatomic, strong) NSString *acLastName;
@property (nonatomic, strong) NSString *acPlateNumber;
@property (nonatomic) BOOL acPlateNumNotSure;
@property (nonatomic, strong) NSString *acPhone;
@property (nonatomic, strong) NSString *acStreetName;
@property (nonatomic, strong) NSString *acStreetNumber;
@property (nonatomic, strong) NSString *acCity;
@property (nonatomic, strong) NSString *acZipCode;
@property (nonatomic, strong) NSString *acCountry;
@property (nonatomic, strong) NSString *acAdditionalAddress;
@property (nonatomic, strong) NSString *acCarBrand;
@property (nonatomic, strong) NSString *acCarType;
@property (nonatomic, strong) NSString *acInsuranceCompany;
@property (nonatomic) BOOL acCompanyCar;
@property (nonatomic) BOOL acLeaseCar;
@property (nonatomic, strong) NSString *acCompanyName;
@property (nonatomic, strong) NSString *acLeasingCompany;
@property (nonatomic, strong) NSString *placeName;
@property (nonatomic) double userLatitude;
@property (nonatomic) double userLongitude;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *weather;
@property (nonatomic, strong) NSString *accidentDescription;
@property (nonatomic, strong) NSString *audioFileLink;
@property (nonatomic, strong) NSMutableArray *witnessesArray;

//additional data
@property (nonatomic, strong) NSString *accidentId;
@property (nonatomic, strong) NSString *accidentHash;
@property (nonatomic, strong) NSString *accidentCausesId;
@property (nonatomic, strong) NSString *audioFileId;
@property (nonatomic, strong) NSMutableArray *photoArray;
@property (nonatomic, strong) NSString *accidentStatus;
@property (nonatomic, strong) NSString *accidentOccuredDate;
@property (nonatomic, strong) NSString *accidentOccuredTime;

//logged in user
@property (nonatomic, strong) NSString *carId;
@property (nonatomic, strong) NSString *registrationNumber;

+(ReportAccidentData *)sharedSingletonInstance;

-(void)prepareStepOneDataWithFirstName :(NSString*)userFirstName lastName:(NSString*)userLastName phoneNumber:(NSString*)userPhoneNumber email:(NSString*)userEmail plateNumber:(NSString*)userPlateNumber termsAccepted:(BOOL)termsAccepted acCompanyCar:(BOOL)companyCar acLeaseCar:(BOOL)leaseCar acCompanyName:(NSString *)userACCompanyName acLeasingCompany:(NSString *)userACLeasingCompany;

-(void)prepareMoreDataWithACFirstName:(NSString *)userACFirstName acLastName:(NSString *)userACLastName acPlateNumber:(NSString *)userACPlateNumber acPlateNumSure:(BOOL)plateNumSure acPhone:(NSString *)userACPhone acStreetName:(NSString *)userACStreetName acStreetNum:(NSString *)userACStreetNum acCity:(NSString *)userACCity acZipCode:(NSString *)userACZipCode acCountry:(NSString *)userACCountry acAdditionalAddress:(NSString *)userACAdditonalAddress acCarBrand:(NSString *)userACCarBrand acCarType:(NSString *)userACCarType acInsuranceCompany:(NSString *)userACInsuranceCompany;

-(void)prepareStepOneDataLoggedInUserWithCarId:(NSString *)userCarId andRegistrationNumber:(NSString *)userRegistrationNumber acCompanyCar:(BOOL)companyCar acLeaseCar:(BOOL)leaseCar acCompanyName:(NSString *)userACCompanyName acLeasingCompany:(NSString *)userACLeasingCompany;

-(void)prepareLTWDataWithLocationName:(NSString *)location latitude:(double)latitude longitude:(double)longitude time:(NSString *)enteredTime date:(NSString *)enteredDate weather:(NSString *)enteredWeather;

-(void)prepareWitnessesDataWithDescription:(NSString*)enetredDescription audioFileLink:(NSString*)link witnessesArray:(NSMutableArray*)witnessesArray;

@end
