//
//  Profile.m
//  Unfallhelden
//
//  Created by Adil Anwer on 12/28/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import "Profile.h"

@implementation Profile
@synthesize mobile,family,zip,residence,street,anrede,birthdate,company,phone,city;
@synthesize firstName,lastName,name,country;

+(Profile *)sharedSingletonInstance
{
    static Profile *sharedSingleton;
    @synchronized(self){
        if(!sharedSingleton){
            sharedSingleton = [[Profile alloc] init];
        }
    }
    
    return sharedSingleton;
}

@end
