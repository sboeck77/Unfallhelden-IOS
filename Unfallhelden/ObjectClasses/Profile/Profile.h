//
//  Profile.h
//  Unfallhelden
//
//  Created by Adil Anwer on 12/28/14.
//  Copyright (c) 2014 Adil Anwer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Profile : NSObject

+(Profile *)sharedSingletonInstance;

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *anrede;
@property (nonatomic, strong) NSString *family;
@property (nonatomic, strong) NSString *birthdate;
@property (nonatomic, strong) NSString *street;
@property (nonatomic, strong) NSString *zip;
@property (nonatomic, strong) NSString *residence;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *company;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *country;

@end
