//
//  Notifications.m
//  Unfallhelden
//
//  Created by Adil Anwer on 5/5/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "Notifications.h"

@implementation Notifications

@synthesize notificationId, notificationDateTime, notificationPnFail,notificationPnIdentifier, notificationPnSend, notificationRead;
@synthesize notificationText, notificationType, notificationTypeId;

@end
