//
//  Notifications.h
//  Unfallhelden
//
//  Created by Adil Anwer on 5/5/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notifications : NSObject

/*{
 id: "1"
 type: "1"
 type_id: "1"
 text: "NEW ACCIDENT :)"
 datetime: "2014-12-14 23:51:42"
 read: "0"
 pn_identifier: "0"
 pn_send: "0"
 pn_fail: "0"
 },
 */

@property (nonatomic, strong) NSString *notificationId;
@property (nonatomic, strong) NSString *notificationType;
@property (nonatomic, strong) NSString *notificationTypeId;
@property (nonatomic, strong) NSString *notificationText;
@property (nonatomic, strong) NSString *notificationDateTime;
@property (nonatomic, strong) NSString *notificationRead;
@property (nonatomic, strong) NSString *notificationPnIdentifier;
@property (nonatomic, strong) NSString *notificationPnSend;
@property (nonatomic, strong) NSString *notificationPnFail;

@end
