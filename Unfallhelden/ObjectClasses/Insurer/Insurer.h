//
//  Insurer.h
//  Unfallhelden
//
//  Created by Adil Anwer on 4/14/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Insurer : NSObject

@property (nonatomic, strong) NSString *insurerName;
@property (nonatomic, strong) NSString *insurerId;

@end
