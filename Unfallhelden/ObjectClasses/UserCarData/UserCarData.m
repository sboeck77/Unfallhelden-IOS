//
//  UserCarData.m
//  Unfallhelden
//
//  Created by Adil Anwer on 4/27/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import "UserCarData.h"

@implementation UserCarData

@synthesize carId;
@synthesize userId;
@synthesize insurerId;
@synthesize leasingCar;
@synthesize leasingCompany;
@synthesize companyCar;
@synthesize companyName;
@synthesize regTicketPhoto;
@synthesize issueDate;
@synthesize vin;
@synthesize type;
@synthesize tradeName;
@synthesize body;
@synthesize displacement;
@synthesize fuelType;
@synthesize color;
@synthesize extras;
@synthesize powerKw;
@synthesize carDeleted;
@synthesize dateTime;
@synthesize registrationNum;
@synthesize brand;
@synthesize firstReg;
@synthesize regDate;
@synthesize tireDimFront;
@synthesize tireDimBack;
@synthesize nxtGeneralInspctn;


+(UserCarData *)saveUserCarWithCarId:(NSString *)carId userId:(NSString *)userId insurerId:(NSString *)insurerId leasingCar:(NSString *)leasingCar leasingCompany:(NSString *)leasingCompany companyCar:(NSString *)companyCar companyName:(NSString *)companyName regTicketPhoto:(NSString *)regTicketPhoto issueDate:(NSString *)issueDate vin:(NSString *)vin type:(NSString *)type tradeName:(NSString *)tradeName body:(NSString *)body displacement:(NSString *)displacement fuelType:(NSString *)fuelType color:(NSString *)color extras:(NSString *)extras powerKw:(NSString *)powerKw carDeleted:(NSString *)carDeleted dateTime:(NSString *)dateTime registrationNum:(NSString *)registrationNum brand:(NSString *)brand firstReg:(NSString *)firstReg regDate:(NSString *)regDate tireDimFront:(NSString *)tireDimFront tireDimBack:(NSString *)tireDimBack
{
    UserCarData *car = [[UserCarData alloc] init];
    
    car.carId = carId;
    car.userId = userId;
    car.insurerId = insurerId;
    car.leasingCar = leasingCar;
    car.companyCar = companyCar;
    car.leasingCompany = leasingCompany;
    car.companyName = companyName;
    car.regTicketPhoto = regTicketPhoto;
    car.issueDate = issueDate;
    car.vin = vin;
    car.type = type;
    car.tradeName = tradeName;
    car.body = body;
    car.displacement = displacement;
    car.fuelType = fuelType;
    car.color = color;
    car.extras = extras;
    car.powerKw = powerKw;
    car.carDeleted = carDeleted;
    car.dateTime = dateTime;
    car.registrationNum = registrationNum;
    car.brand = brand;
    car.firstReg = firstReg;
    car.regDate = regDate;
    car.tireDimBack = tireDimBack;
    car.tireDimFront = tireDimFront;
    
    return car;
}

@end
