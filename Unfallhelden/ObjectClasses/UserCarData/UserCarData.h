//
//  UserCarData.h
//  Unfallhelden
//
//  Created by Adil Anwer on 4/27/15.
//  Copyright (c) 2015 Adil Anwer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserCarData : NSObject

@property (nonatomic, strong) NSString * carId;
@property (nonatomic, strong) NSString * userId;
@property (nonatomic, strong) NSString * insurerId;
@property (nonatomic, strong) NSString * leasingCar;
@property (nonatomic, strong) NSString * leasingCompany;
@property (nonatomic, strong) NSString * companyCar;
@property (nonatomic, strong) NSString * companyName;
@property (nonatomic, strong) NSString * regTicketPhoto;
@property (nonatomic, strong) NSString * issueDate;
@property (nonatomic, strong) NSString * vin;
@property (nonatomic, strong) NSString * type;
@property (nonatomic, strong) NSString * tradeName;
@property (nonatomic, strong) NSString * body;
@property (nonatomic, strong) NSString * displacement;
@property (nonatomic, strong) NSString * fuelType;
@property (nonatomic, strong) NSString * color;
@property (nonatomic, strong) NSString * extras;
@property (nonatomic, strong) NSString * powerKw;
@property (nonatomic, strong) NSString * carDeleted;
@property (nonatomic, strong) NSString * dateTime;
@property (nonatomic, strong) NSString * registrationNum;
@property (nonatomic, strong) NSString * brand;
@property (nonatomic, strong) NSString * firstReg;
@property (nonatomic, strong) NSString * regDate;
@property (nonatomic, strong) NSString * tireDimFront;
@property (nonatomic, strong) NSString * tireDimBack;
@property (nonatomic, strong) NSString * nxtGeneralInspctn;

+(UserCarData *)saveUserCarWithCarId:(NSString *)carId userId:(NSString *)userId insurerId:(NSString *)insurerId leasingCar:(NSString *)leasingCar leasingCompany:(NSString *)leasingCompany companyCar:(NSString *)companyCar companyName:(NSString *)companyName regTicketPhoto:(NSString *)regTicketPhoto issueDate:(NSString *)issueDate vin:(NSString *)vin type:(NSString *)type tradeName:(NSString *)tradeName body:(NSString *)body displacement:(NSString *)displacement fuelType:(NSString *)fuelType color:(NSString *)color extras:(NSString *)extras powerKw:(NSString *)powerKw carDeleted:(NSString *)carDeleted dateTime:(NSString *)dateTime registrationNum:(NSString *)registrationNum brand:(NSString *)brand firstReg:(NSString *)firstReg regDate:(NSString *)regDate tireDimFront:(NSString *)tireDimFront tireDimBack:(NSString *)tireDimBack;

@end
